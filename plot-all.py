#!/usr/bin/env python3
import argparse
import warnings
from pathlib import Path
from subprocess import run

HOME = Path.home()

parser = argparse.ArgumentParser(description='Plot all snapshots of the given '
	'fuzzy dark matter simulations')
parser.add_argument('path', nargs='+', type=Path,
	help='the paths to the simulations’ output directories')
parser.add_argument('-n', default='8',
	help='number of MPI ranks to use (default: %(default)d)')
parser.add_argument('plot_opts', nargs='...',
	help='options to pass to the plotting script')
args = parser.parse_args()

for output_dir in args.path:
	print(output_dir)
	output_dir_name = (
		output_dir.parent.name if output_dir.name == 'output' else
		output_dir.name
	)
	plot_dir = HOME / 'plots' / output_dir_name
	plot_dir.mkdir(parents=True, exist_ok=True)
	snaps = sorted(snap.resolve() for snap in output_dir.glob('snap*'))
	if snaps:
		command = (
			['mpiexec', '-n', args.n,
			HOME / 'arepo-tools' / 'plot_snapshot.py'] + args.plot_opts + snaps
		)
		run(command, cwd=plot_dir)
	else:
		warnings.warn(f'No snapshots found in directory {output_dir}')

