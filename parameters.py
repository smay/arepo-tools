#!/usr/bin/env python3
import argparse
import re
import warnings
from pathlib import Path

#TODO: keep track of comments when reading parameter file

# parse parameter file in the same way as AREPO’s
# src/parameters.c:read_parameter_file() function

COMMENT_CHAR = '%'
INCLUDE_DIRECTIVE = '!include'
EXTRA_PARAMS_CHOICES = ('warn', 'ignore')
INPUT_FILE_SUFFIX = '_incl'
OUTPUT_FILE_SUFFIX = '_merged'

def read_parameter_file(
	path, extra_params='warn', allow_missing_values=False, sep=r'\s+',
	comment_char=COMMENT_CHAR
):
	assert extra_params in EXTRA_PARAMS_CHOICES
	res = {}
	with open(path) as f:
		for line_num, line in enumerate(f, start=1):
			# Note that this method of parsing implies that there must always
			# be whitespace between any value and a potential comment following
			# it!
			# Further, comments after a parameter (on the same line) do not
			# technically need to be introduced by a special character;
			# everything after the second whitespace is simply thrown away. By
			# corollary, string parameters cannot contain whitespace.
			parts = re.split(sep, line.rstrip())
			# ignore whitespace-only lines and comment lines
			if not parts[0] or parts[0].startswith(comment_char):
				continue
			if len(parts) == 1:
				parts.append('')
			key = parts[0]
			value = parts[1].split(comment_char)[0].rstrip()
			# check for missing values
			if not value and not allow_missing_values:
				warnings.warn(
					f'Found parameter tag without value: “{parts[0]}” '
					f'in “{path}”, line {line_num}'
				)
				continue
			# set key/value pairs in dict
			if key == INCLUDE_DIRECTIVE:
				incl_path = value
				incl_params = read_parameter_file(
					incl_path, extra_params=extra_params,
					allow_missing_values=allow_missing_values, sep=sep,
					comment_char=comment_char
				)
			else:
				incl_path = path
				incl_params = {key: value}
			if extra_params == 'warn':
				for incl_key in incl_params:
					if incl_key in res:
						warnings.warn(
							f'Parameter tag “{incl_key}” is multiply defined\n'
							f'(old value: “{res[incl_key]}”, new value: '
								f'“{incl_params[incl_key]}”)\n'
							f'in “{path}”, line {line_num}'
						)
			res.update(incl_params)
	return res

def write_parameter_file(path, params, sep=' '):
	max_key_len = max(len(key) for key in params)
	with open(path, 'w') as f:
		for key, value in params.items():
			f.write(f'{key:<{max_key_len}}{sep}{value}\n')

def read_camb_parameter_file(
	path, extra_params='warn', allow_missing_values=True, sep=r'\s*=\s*',
	comment_char='#'
):
	return read_parameter_file(
		path, extra_params=extra_params,
		allow_missing_values=allow_missing_values, sep=sep,
		comment_char=comment_char
	)

def write_camb_parameter_file(path, params, sep=r' = '):
	write_parameter_file(path, params, sep=sep)


# if called as a program (not loaded as a module)
if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='read GADGET/AREPO parameter '
		'files and process included files')
	parser.add_argument('--extra-params', choices=EXTRA_PARAMS_CHOICES,
		default='warn',
		help='how to treat multiply defined parameters (note: currently, '
			'the last-specified value “wins” regardless of this option’s '
			'value)')
	parser.add_argument('--print', action='store_true',
		help='print each processed file')
	parser.add_argument('--output-suffix',
		help=f'suffix to append to the output file name (default: strip '
			f'“{INPUT_FILE_SUFFIX}” if input file name ends with it, else '
			f'append “{OUTPUT_FILE_SUFFIX}”))')
	parser.add_argument('path', nargs='+', type=Path,
		help='the paths to the parameter files')
	args = parser.parse_args()

	for path in args.path:
		print(path)
		params = read_parameter_file(path, args.extra_params)
		if args.print:
			pad_len = max(len(key) for key in params) + 1
			for key, value in params.items():
				print(f'{key: <{pad_len}}{value}')
			print()
		# determine output path and write to output file
		stem = path.stem
		ext = path.suffix
		if args.output_suffix is None:
			if stem.endswith(INPUT_FILE_SUFFIX):
				stem = stem[:-len(INPUT_FILE_SUFFIX)]
			else:
				stem += OUTPUT_FILE_SUFFIX
		else:
			stem += args.output_suffix
		write_parameter_file(path.with_name(stem + ext), params)
