#!/usr/bin/env python3
import argparse
import collections
import time
from pathlib import Path
import h5py
import numba
import numpy
import bin_density
import mpi_util
import read_snapshot
import read_snapshot.hdf5 as read_hdf5
import snapshot
from mpi_util import MPI, mpi_comm, mpi_rank, mpi_size, print0
from snapshot import Snapshot

parser = argparse.ArgumentParser(description='Calculate 2-point correlation '
	'function for fuzzy dark matter halos')
parser.add_argument('--r-bins', type=int, default=500,
	help='the number of bins to use for the correlation distance (default: '
		'%(default)d)')
parser.add_argument('--r-max-factor', type=float, default=1.0,
	help='maximum radius to use for each halo, as a multiple of R_200 (default: '
		'%(default)g)')
parser.add_argument('--halo-id-offset', type=int, default=0,
	help='do not process halos with IDs smaller than the given ID '
		'(default: %(default)d)')
parser.add_argument('--fof-group-min-len', type=int, default=8,
	help='minimum FoF length to be processed (default: %(default)d)')
parser.add_argument('--fof-group-min-m200', type=float, default=0.,
	help='minimum FoF mass M_200 to be processed (default: %(default)d)')
parser.add_argument('--snap-chunk-mem-size', type=float,
	default=snapshot.CHUNK_MEM_SIZE_DEFAULT / 1024**2,
	help='maximum amount of memory to use for snapshot data per core, in MiB '
		'(default: %(default)g)')
parser.add_argument('--load', type=Path,
	help='path to a previously-computed set of correlation functions (default: '
		'<path>_correlation.hdf5')
#TODO
parser.add_argument('--save-frequency-large', type=int, default=1,
	help='save frequency for iterations with large halos (default: %(default)d)')
parser.add_argument('--save-frequency-small', type=int, default=1,
	help='save frequency for iterations with small halos (default: %(default)d)')
parser.add_argument('--output-file-prefix', default='',
	help='prefix for the output file names')
parser.add_argument('path', type=Path, nargs='+',
	help='the path to the fof_tab catalog; any normal files are interpreted '
		'as single files, while all files in any given directory are '
		'interpreted as a single multi-file output')
args = parser.parse_args()


GROUP_LEN_FACTOR = 3.0
Param = collections.namedtuple(
	'Param', 'ir_min, ir_max, ix_min, ix_max, binlog'
)

def main():
	mpi_util.default_print_args['flush'] = True
	if args.load:
		assert len(args.path) == 1, (
			'--load doesn’t really make sense with more than 1 path'
		)
	# process paths
	for path in args.path:
		process_path(path)
		print0()


def process_path(path):
	print0(path)
	fof_tab_files = read_snapshot.snapshot_files(path)
	# read fof_tab data
	fof_tab_data = None
	if mpi_rank == 0:
		fof_tab_data = read_hdf5.read_fof_tab(fof_tab_files, read_ids=False)
	# broadcast fof_tab data to other ranks
	if mpi_size > 1:
		fof_tab_data = mpi_comm.bcast(fof_tab_data, root=0)
	[fof_config, fof_param, fof_header], groups, _ = fof_tab_data
	print0(f'Processing fof_tab with {len(groups)} groups')
	if len(groups) == 0:
		print0('fof_tab contains no groups')
	# read snapshot metadata
	print0('Reading snapshot metadata…')
	snap = Snapshot(read_snapshot.output_path(path, fof_config, fof_param))
	snaptime = snap.header['Time']
	z = 1 / snaptime - 1
	print0(f'\tRedshift: z = {z}')
	cell_len = snap.box_size / snap.pmgrid
	# check existing correlation function data
	halo_ids_todo = None
	num_chunks = None
	output_path = args.load if args.load else path.with_name(
		f'{args.output_file_prefix}{path.name}_correlation.hdf5'
	)
	if mpi_rank == 0:
		if output_path.exists():
			print0(f'Not recomputing existing data in file “{output_path}”')
			with h5py.File(output_path, 'r') as of:
				assert of['xi'].shape == of['count'].shape
				assert of['xi'].shape == (len(groups), args.r_bins), (
					f'“{output_path}” contains data with shape '
					f'{of["xi"].shape} (expected '
					f'{(len(groups), args.r_bins)})'
				)
				assert of.attrs['r_max_factor'] == args.r_max_factor
		else:
			print0(
				f'“{output_path}” not found as existing file, starting from '
				'scratch'
			)
			assert not args.load
			# initialize HDF5 datasets
			with h5py.File(output_path, 'w') as of:
				of.create_dataset(
					'xi', shape=(len(groups), args.r_bins),
					dtype='f8', fillvalue=numpy.nan
				)
				of.create_dataset_like(
					'count', of['xi'], dtype='u8', fillvalue=0
				)
				of.attrs['r_max_factor'] = args.r_max_factor
		# determine which halos are both requested and missing
		with h5py.File(output_path, 'r') as of:
			halos_not_done = (
				numpy.all(numpy.isnan(of['xi'][...]), axis=1) &
				~(groups.r200 < 2 * cell_len)
			)
			halo_ids_todo = []
			for i in range(len(groups)):
				if (
					i >= args.halo_id_offset and
					groups[i].tot_len >= args.fof_group_min_len and
					groups[i].m200 >= args.fof_group_min_m200 and
					halos_not_done[i]
				):
					halo_ids_todo.append(i)
			del halos_not_done
		print0(f'{len(halo_ids_todo)} halos left to process for file “{path}”')
		if len(halo_ids_todo) > 0:
			print0(f'(starting with halo {halo_ids_todo[0]})')
			#TODO chunk calculation (although not really necessary on clusters)
			max_num_halos = len(halo_ids_todo)
			chunk_num_halos = min(max_num_halos, len(halo_ids_todo))
			num_chunks = int(numpy.ceil(len(halo_ids_todo) / chunk_num_halos))
			print0(
				f'Can process at most {max_num_halos} halos at once; iteration '
				f'will be split into {num_chunks} chunks'
			)
			print0()
	num_chunks = mpi_comm.bcast(num_chunks, root=0)
	halo_ids_todo = mpi_comm.bcast(halo_ids_todo, root=0)
	if len(halo_ids_todo) == 0:
		return
	for num_iteration, halo_ids_chunk in enumerate(
		numpy.array_split(halo_ids_todo, num_chunks), start=1
	):
		print0(
			f'Starting grid iteration {num_iteration}/{num_chunks}'
		)
		process_chunk(snap, groups, halo_ids_chunk, output_path)
		print0()


def save_results(output_path, halo_id, xi, count):
	if mpi_rank == 0:
		with h5py.File(output_path, 'a') as of:
			of['xi'][halo_id] = xi
			of['count'][halo_id] = count


@numba.njit
def halo_twopoint(delta, param, data):
	prod_sum, count = data
	assert len(prod_sum) == len(count)
	num_bins = len(count)
	ir0 = param.ir_min
	ir1 = param.ir_max
	binfac = (
		num_bins / numpy.log(ir1 / ir0) if param.binlog else
			num_bins / (ir1 - ir0)
	)
	Nx, Ny, Nz = delta.shape
	for ix1 in range(param.ix_min, param.ix_max):
		for iy1 in range(Ny):
			for iz1 in range(Nz):
				delta1 = delta[ix1, iy1, iz1]
				for ix2 in range(Nx):
					dix = ix2 - ix1
					for iy2 in range(Ny):
						diy = iy2 - iy1
						for iz2 in range(Nz):
							diz = iz2 - iz1
							delta2 = delta[ix2, iy2, iz2]
							ir = numpy.sqrt(dix**2 + diy**2 + diz**2)
							if ir0 <= ir < ir1:
								if param.binlog:
									r_bin = int(numpy.log(ir / ir0) * binfac)
								else:
									r_bin = int((ir - ir0) * binfac)
								assert 0 <= r_bin < num_bins
								prod_sum[r_bin] += delta1 * delta2
								count[r_bin] += 1


def is_group_large(r, box_size, pmgrid):
	cell_len = box_size / pmgrid
	dr = r / cell_len
	assert dr < pmgrid // 2
	ir = int(dr)
	#TODO
#	cube_num_cells = (2 * ir + 1)**3
#	return cube_num_cells >= GROUP_LEN_FACTOR * mpi_size
	return GROUP_LEN_FACTOR * ir > mpi_size


def dist_pos_to_grid(box_size, pmgrid, r):
	cell_len = box_size / pmgrid
	dr = r / cell_len
	assert dr < pmgrid // 2
	return dr


def load_delta(snap, halo):
	cell_len = snap.box_size / snap.pmgrid
	rho_avg = (
		snap.header['Omega0'] *
		snap.constant_in_internal_units('critical_density0')
	)
	assert not numpy.isnan(halo.r200)
	halo_center = halo.pos
	halo_center_g3 = numpy.floor(halo_center / cell_len + 0.1).astype('i8')
	dr200 = dist_pos_to_grid(
		snap.box_size, snap.pmgrid, halo.r200 * args.r_max_factor
	)
	di = min(int(dr200), snap.pmgrid // 2)
	sl = tuple(
		numpy.s_[
			#TODO
			i - di - snap.pmgrid if i + di + 1 > snap.pmgrid else i - di:
			i + di + 1 - snap.pmgrid if i + di + 1 > snap.pmgrid else i + di + 1
		] for i in halo_center_g3
	)
	delta = snap.read_snapshot('/FuzzyDM/PsiRe', data_slice=sl)**2
	delta += snap.read_snapshot('/FuzzyDM/PsiIm', data_slice=sl)**2
	delta /= rho_avg
	delta -= 1
	# filter out spherical region around halo center
	ix, iy, iz = numpy.ogrid[numpy.index_exp[-di:di + 1] * 3]
	ir2 = ix**2 + iy**2 + iz**2
	delta[ir2 > di**2] = 0
	return delta


def process_chunk(snap, groups, halo_ids, output_path):
	t0_tot = time.perf_counter()
	prod_sum = numpy.zeros(args.r_bins, dtype='f8')
	count = numpy.zeros(args.r_bins, dtype='u8')
	# iterate over halos
	# process large halos in parallel across all ranks
	print0('Correlation function computation for large halos…')
	for halo_id in halo_ids:
		halo = groups[halo_id]
		r = halo.r200 * args.r_max_factor
		# check if group is “large” (i.e. should be processed here)
		if not is_group_large(r, snap.box_size, snap.pmgrid):
			continue
		# process group
		t0 = time.perf_counter()
		print_info = halo_id < 50 or halo_id % max(len(groups) // 200, 1) == 0
		if print_info:
			print0(f'Starting correlation function for GrNr = {halo_id}')
		# calculate correlation function using all cells within given radius
		delta = load_delta(snap, halo)
		dr = dist_pos_to_grid(snap.box_size, snap.pmgrid, r)
		# distribute cells to correlation function potential for across all tasks
		#TODO: This will result in idle tasks even for relatively large groups!
		#      (and unbalanced loads)
		chunks = snap.determine_chunks(
			mpi_rank, mpi_size, chunk_mem_size=numpy.inf, num_slabs=len(delta)
		)
		assert len(chunks) <= 1
		if chunks:
			[ix_min, ix_max], = chunks
			halo_twopoint(
				delta, Param(
					ir_min=1., ir_max=dr, ix_min=ix_min, ix_max=ix_max,
					binlog=True
				), (prod_sum, count)
			)
		# reduce on rank 0
		mpi_comm.Reduce(
			MPI.IN_PLACE if mpi_rank == 0 else prod_sum, prod_sum, op=MPI.SUM,
			root=0
		)
		mpi_comm.Reduce(
			MPI.IN_PLACE if mpi_rank == 0 else count, count, op=MPI.SUM,
			root=0
		)
		xi, nonzero = bin_density.density_mean_division(prod_sum, count)
		xi[~nonzero] = numpy.nan
		t1 = time.perf_counter()
		if print_info:
			mpi_comm.Barrier()
			print0(f'\ttook {t1 - t0} sec.')
		# save results to file
		print0(f'\tSaving results to {output_path}…')
		save_results(output_path, halo_id, xi, count)
		prod_sum[...] = 0
		count[...] = 0
	# for small groups: process using one rank per group
	xi_g = None
	count_g = None
	if mpi_rank == 0:
		xi_g = numpy.full_like(
			prod_sum, numpy.nan, shape=(mpi_size,) + prod_sum.shape
		)
		count_g = numpy.zeros_like(count, shape=(mpi_size,) + count.shape)
	print0('Correlation function computation for small halos…')
	halo_ids_idx = 0
	while halo_ids_idx < len(halo_ids):
		# assign groups to ranks
		thisrank_hid = None
		rank = 0
		while halo_ids_idx < len(halo_ids) and rank < mpi_size:
			# check if group is “small” (i.e. should be processed here)
			if is_group_large(
				groups[halo_id].r200 * args.r_max_factor, snap.box_size,
				snap.pmgrid
			):
				continue
			if rank == mpi_rank:
				thisrank_hid = halo_ids[halo_ids_idx]
			rank += 1
			halo_ids_idx += 1
		halo_id_max = (
			halo_ids[halo_ids_idx] if halo_ids_idx < len(halo_ids)
			else halo_ids[halo_ids_idx - 1]
		)
		# process groups
		t0 = time.perf_counter()
		thisrank_halo = None
		if thisrank_hid is None:
			xi = prod_sum
		else:
			thisrank_halo = groups[thisrank_hid]
			print0(
				f'Starting correlation function for GrNr = {thisrank_hid} to '
				f'{halo_id_max}'
			)
			# calculate correlation function for all cells within given radius for
			# (up to) mpi_size halos in parallel
			delta = load_delta(snap, thisrank_halo)
			r = thisrank_halo.r200 * args.r_max_factor
			dr = dist_pos_to_grid(snap.box_size, snap.pmgrid, r)
			halo_twopoint(
				delta, Param(
					ir_min=1., ir_max=dr, ix_min=0, ix_max=len(delta),
					binlog=True
				), (prod_sum, count)
			)
			xi, nonzero = bin_density.density_mean_division(prod_sum, count)
			xi[~nonzero] = numpy.nan
		mpi_comm.Gather(xi, xi_g, root=0)
		mpi_comm.Gather(count, count_g, root=0)
		t1 = time.perf_counter()
		mpi_comm.Barrier()
		print0(f'\ttook {t1 - t0} sec.')
		# save results to file
		print0(f'\tSaving results to {output_path}…')
		rank_hids = mpi_comm.gather(thisrank_hid, root=0)
		if mpi_rank == 0:
			save_results(
				output_path, rank_hids[:rank], xi_g[:rank], count_g[:rank]
			)
		prod_sum[...] = 0
		count[...] = 0
	t1_tot = time.perf_counter()
	print0(
		f'Total correlation function computation took {t1_tot - t0_tot} sec.'
	)


if __name__ == '__main__':
	main()
