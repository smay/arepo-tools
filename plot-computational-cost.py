#!/usr/bin/env python3
import argparse
import importlib
import astropy.cosmology
import matplotlib
import matplotlib.pyplot as plt
import numpy
fdm_cost = importlib.import_module('fdm-cost')
import matplotlib_settings

parser = argparse.ArgumentParser(description='Plot the behavior of the '
	'computational of fuzzy dark matter simulations as a function of '
	'simulation parameters, as in fdm_simulation_cost.ipynb')
parser.add_argument('--x-var', choices=('L', 'z'), required=True,
	help='variable to use on the horizontal axis (L, z)')
parser.add_argument('--labels', nargs='+',
	help='legend labels (used instead of automatic legend)')
parser.add_argument('--plot-format', default='png',
	help='the file format of the generated plot (default: %(default)s)')
args = parser.parse_args()


UnitLength_in_cm         = 3.085678e21 # 1.0 kpc
UnitMass_in_g            = 1.989e43    # 1.0e10 solar masses
UnitVelocity_in_cm_per_s = 1e5         # 1.0 km/s
UnitTime_in_s = UnitLength_in_cm / UnitVelocity_in_cm_per_s
EV_TO_G = 1.78266191e-33
HBAR = 1.0545718e-27 / UnitLength_in_cm**2 / UnitMass_in_g * UnitTime_in_s

Z_START = 127
C = astropy.cosmology.FlatLambdaCDM(H0=70, Om0=0.3)

PMGRID_VALUES = [2**13]#, 2**12]#, 2**14]
BOX_SIZES = numpy.array(fdm_cost.IC_BOX_SIZES)
MASS_VALUES = [1e-23, 2.5e-23, 5e-23, 1e-22, 2.5000000000000001e-22]
# NOTE: this corrects the spurious factor of 0.7
MASS_VALUES = [0.7 * m for m in MASS_VALUES]
Z_VALUES = [2, 4]
SAMPLE_LEN = 2000

Z_INTERVAL = (1, 10)
SAMPLE_LEN_Z = 50
FIT_MASS = 1e-232

def float_to_scientific_notation(x, integer=True):
	sign = '-' if x < 0 else ''
	exponent = int(numpy.floor(numpy.log10(numpy.abs(x))))
	significand = x / 10**exponent
	if numpy.isclose(1, significand):
		return fr'{sign} 10^{{{exponent}}}'
	else:
		if integer and numpy.isclose(significand / round(significand), 1):
			significand = round(significand)
		return fr'{sign}{significand} \times 10^{{{exponent}}}'

def box_size_max(pmgrid, mass, size_sample, z_end=0, factor=1):
	v_box_sample = vel_box_linear(size_sample, z_end)
	v_max_sample = factor * vel_max(size_sample / pmgrid, mass)
	v_ok = v_box_sample < v_max_sample
	size_ok = size_sample[v_ok]
	if len(size_ok) == 0 or len(size_ok) == len(v_box_sample):
		return None
	size_max = size_ok[-1]
	return size_max

def vel_box_linear(box_size, z_end):
	return fdm_cost.VEL_BOX_IC(box_size) * numpy.sqrt(C.scale_factor(z_end) / C.scale_factor(Z_START))

def vel_max(cell_len, mass):
	m = mass * EV_TO_G / UnitMass_in_g
	return (HBAR / m) * numpy.pi / cell_len


assert not args.labels or len(MASS_VALUES) == len(args.labels)


styles = ['-', '--']

if args.x_var == 'L':
	plt.figure()
	plt.xlabel(r'$L$ / $h^{-1}\,$Mpc')
	plt.ylabel(r'CPU time / h')
	plt.xscale('log')
	plt.yscale('log')

	styles_z = ['--', '-.']
	X = BOX_SIZES * 1e-3
	box_max = []
	for style_idx, pmgrid in enumerate(PMGRID_VALUES):
		style = styles[style_idx]
		size_max_prev = numpy.max(BOX_SIZES)
		for i, mass in enumerate(MASS_VALUES):
			color = i
			# plot CPU time vs. box size
			Y = [fdm_cost.cpu_time(pmgrid, box_size, mass) for box_size in BOX_SIZES]
			plot_opt = {}
			if style_idx == 0:
				if args.labels:
					plot_opt['label'] = args.labels[i]
				else:
					plot_opt['label'] = f'$mc^2 = {float_to_scientific_notation(mass)}\,$eV'
			else:
				plot_opt['label'] = ''
			plt.plot(X, Y, f'C{color}{style}', **plot_opt)
			# show v_max criterion
			# determine maximum box size
			size_sample = numpy.linspace(numpy.min(BOX_SIZES), numpy.max(BOX_SIZES), SAMPLE_LEN)
			size_max = box_size_max(pmgrid, mass, size_sample)
			assert size_max is not None
			size_max_z = [box_size_max(pmgrid, mass, size_sample, z_end=z) for z in Z_VALUES]
			size_max_fac = box_size_max(pmgrid, mass, size_sample, factor=4.5)
			# determine CPU time at size_max
			cost_at_max = fdm_cost.cpu_time(pmgrid, size_max, mass)
			box_max.append([pmgrid, mass, size_max, cost_at_max])
			# plot line and shaded area
			ax = plt.gca()
			trans = matplotlib.transforms.blended_transform_factory(ax.transData, ax.transAxes)
			X_max_prev = size_max_prev * 1e-3
			X_max = size_max * 1e-3
	#		plt.axvline(X_max, color=f'C{color}', linestyle=style)
			if size_max_fac is not None:
				X_max_fac = size_max_fac * 1e-3
	# 				plt.axvline(X_max_fac, color=f'C{color}', linestyle='-.')
			if pmgrid == 2**13:
				plt.fill_betweenx(
					[0, 1], X_max_prev, X_max, facecolor=f'C{color}',
					alpha=0.15, transform=trans
				)
			size_max_prev = size_max
			# plot max. box size for different values of z_end
			for style_z_idx, size_z in enumerate(size_max_z):
				if size_z is None: continue
				style_z = styles_z[style_z_idx]
	# 			plt.axvline(size_z * 1e-3, color=f'C{color}', linestyle=style_z)

	plt.xlim(numpy.min(X), numpy.max(X))
	plt.ylim(bottom=5e5, top=1e8)
	plt.legend()
	plt.savefig(f'cpu_time_vs_l.{args.plot_format}', bbox_inches='tight')

elif args.x_var == 'z':
	plt.figure()
	plt.xlabel(r'$z_{\mathrm{end}}$')
	plt.ylabel(r'CPU time at $L_{\mathrm{max}}$ / h')
	plt.xscale('log')
	plt.yscale('log')

	def fit_func_redshift(z, a, b): return a * z**b

	size_sample = numpy.linspace(numpy.min(BOX_SIZES), numpy.max(BOX_SIZES), SAMPLE_LEN)
	X = numpy.geomspace(*Z_INTERVAL, SAMPLE_LEN_Z)
	for style_idx, pmgrid in enumerate(PMGRID_VALUES):
		style = styles[style_idx]
		for i, mass in enumerate(MASS_VALUES):
			color = i
			Y = []
			Y_fac = []
			for z in X:
				# determine maximum box size and CPU time for given PMGRID, mass and z_end
				size_max = box_size_max(pmgrid, mass, size_sample, z_end=z)
	# 			size_max_fac = box_size_max(pmgrid, mass, size_sample, z_end=z, factor=3)
				if size_max is None: continue
				cost = fdm_cost.cpu_time(pmgrid, size_max, mass, z_end=z)
				Y.append(cost)
	# 			Y_fac.append(fdm_cost.cpu_time(pmgrid, size_max_fac, mass, z))
			# plot CPU time at size_max vs. z_end
			plot_opt = {}
			if style_idx == 0:
				if args.labels:
					plot_opt['label'] = args.labels[i]
				else:
					plot_opt['label'] = f'$mc^2 = {float_to_scientific_notation(mass)}\,$eV'
			else:
				plot_opt['label'] = ''
			plt.plot(X[:len(Y)], Y, f'C{color}{style}', **plot_opt)
	# 		plt.plot(X, Y_fac, f'C{color}-.', label='')
			if mass == FIT_MASS:
				[a, b], _ = scipy.optimize.curve_fit(fit_func_redshift, X, Y)
				plt.plot(X, fit_func_redshift(X, a, b), 'k', label=f'[{a:.3g} $z^{{{b:.3g}}}$]')

	import matplotlib.ticker
	ax = plt.gca()
	xformat_maj = ax.xaxis.get_major_formatter()
	xformat_min = ax.xaxis.get_minor_formatter()
	def format_x(formatter):
		def format_func(x, pos):
			format_result = formatter(x, pos)
			if x == 1:
				return format_result.replace(r'10^{0}', '1')
			elif x == 10:
				return format_result.replace(r'10^{1}', '10')
			elif 1 < x < 10:
				return format_result.replace(r'\times10^{0}', '')
			return format_result
		return format_func
	ax.xaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(format_x(xformat_maj)))
	ax.xaxis.set_minor_formatter(matplotlib.ticker.FuncFormatter(format_x(xformat_min)))
	plt.xlim(tuple(reversed(Z_INTERVAL)))
	plt.ylim(bottom=4e6, top=4e7)
	plt.legend()

	plt.savefig(f'cpu_time_vs_z.{args.plot_format}', bbox_inches='tight')

else: assert False

