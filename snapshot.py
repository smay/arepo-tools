import argparse
import collections
import collections.abc
import itertools
import math
import re
from pathlib import Path
import astropy.constants
import astropy.cosmology
import astropy.units
import h5py
import numba
import numpy
import read_snapshot
import read_snapshot.hdf5


#TODO: option to skip looking at headers for every snapshot file

GRID_ELEMENT_MEM_SIZE = 2 * numpy.dtype('float64').itemsize
# 2 GiB chunk size (80 GiB for 40 cores)
CHUNK_MEM_SIZE_DEFAULT = 2 * 1024**3
VELOCITY_DATASETS = {
	'/FuzzyDM/Velocities0', '/FuzzyDM/Velocities1', '/FuzzyDM/Velocities2',
	'/FuzzyDM/(GradSqrtRho)^2',
	'/FuzzyDM/Velocities0_fd', '/FuzzyDM/Velocities1_fd', '/FuzzyDM/Velocities2_fd',
	'/FuzzyDM/(GradSqrtRho)^2_fd',
	'/FuzzyDM/MomentumDensity0', '/FuzzyDM/MomentumDensity1',
	'/FuzzyDM/MomentumDensity2',
	'/FuzzyDM/MomentumDensity0_fd', '/FuzzyDM/MomentumDensity1_fd',
	'/FuzzyDM/MomentumDensity2_fd',
}
POTENTIAL_DATASETS = {'/FuzzyDM/Potential'}
NTYPES = 6

class SnapshotError(Exception): pass

# TODO: only supports HDF5
# TODO: also fully handle auxiliary files here (group catalogs etc.)
class Snapshot(object):
	def __init__(
		self, paths, snapshot_format='hdf5', sort_paths=True, is_slice=False
	):
		if isinstance(paths, Path) or isinstance(paths, str):
			paths = read_snapshot.snapshot_files(
				Path(paths), sort_paths=sort_paths
			)
		if sort_paths:
			self.paths = sorted(paths, key=read_snapshot.arepo_filename_key)
		else:
			self.paths = list(paths)
		assert self.paths
		if len(self.paths) == 1:
			self.path = self.paths[0]
		else:
			self.path = self.paths[0].parent
		self.snapshot_format = snapshot_format
		config, param, header = read_snapshot.read_metadata(
			self.paths, self.snapshot_format
		)
		self.config = config
		self.param = param
		self.header = header
		self.pmgrid = (
		    int(config['FDM_PMGRID']) if 'FDM_PMGRID' in config else
		    int(config['PMGRID']) if 'PMGRID' in config else
		    None
		)
		self.box_size = param['BoxSize']
		self.dataset_shape = {}
		self.dataset_shape_reshaped = {}
		self.dataset_dtype = {}
		self.slabs_x_per_file = {}
		self.first_slab_x_of_file = {}
		self.velocity_paths = None
		self.potential_paths = None
		# determine snapshot type and do sanity checks
		self.slice = is_slice
		self.types = set()
		first_part = True
		im_present = False
		d_len_grid = 0
		d_len_part = [0] * NTYPES
		for path in self.paths:
			with h5py.File(path, 'r') as snap_part:
				# check for grid data
				if '/FuzzyDM' in snap_part:
					if '/FuzzyDM/PsiRe' in snap_part:
						if first_part:
							self.types.add('grid')
							im_present = '/FuzzyDM/PsiIm' in snap_part
						assert 'grid' in self.types
						assert ('/FuzzyDM/PsiIm' in snap_part) == im_present
						d_re = snap_part['/FuzzyDM/PsiRe']
						d_im = snap_part['/FuzzyDM/PsiIm']
					else:
						if first_part:
							self.slice = True
						assert self.slice
						d_re = snap_part['/FuzzyDM/PsiReSlice']
						d_im = snap_part['/FuzzyDM/PsiImSlice']
					assert not im_present or d_re.shape == d_im.shape
					if len(d_re.shape) == 1:
						assert len(d_re) % self.pmgrid**2 == 0
					else:
						assert d_re.shape[1:] == (self.pmgrid,) * 2
					if first_part:
						d_shape_grid = d_re.shape[1:]
					assert d_re.shape[1:] == d_shape_grid
					d_len_grid += len(d_re)
				if 'grid' in self.types:
					assert '/FuzzyDM' in snap_part
				# check for particle data
				for i in range(NTYPES):
					if f'/PartType{i}/Coordinates' in snap_part:
						particle_pos = snap_part[f'/PartType{i}/Coordinates']
						if (
							len(particle_pos) > 0 and
							self.numpart_total(i) > self.pmgrid
						):
							if first_part:
								self.types.add('particles')
							assert 'particles' in self.types
						header = snap_part['Header'].attrs
						assert len(particle_pos) == header['NumPart_ThisFile'][i]
						d_len_part[i] += len(particle_pos)
					if 'particles' in self.types:
						assert any(l > 0 for l in d_len_part)
						if d_len_part[i] > 0:
							assert f'/PartType{i}/Coordinates' in snap_part
			first_part = False
		if 'grid' in self.types and not self.slice:
			if not d_shape_grid:
				assert d_len_grid == self.pmgrid**3
			else:
				assert d_len_grid == self.pmgrid
		if 'particles' in self.types:
			for i in range(NTYPES):
				assert d_len_part[i] == self.numpart_total(i)

	def add_velocities(self, paths, sort_paths=True):
		assert 'grid' in self.types
		if isinstance(paths, Path) or isinstance(paths, str):
			paths = read_snapshot.snapshot_files(
				Path(paths), sort_paths=sort_paths
			)
		if sort_paths:
			self.velocity_paths = sorted(
				paths, key=read_snapshot.arepo_filename_key
			)
		else:
			self.velocity_paths = list(paths)
		assert self.velocity_paths
		d_len = 0
		for path in self.velocity_paths:
			with h5py.File(path, 'r') as snap_part:
				assert (
					'/FuzzyDM/Velocities0' in snap_part or
					'/FuzzyDM/MomentumDensity0' in snap_part
				)
				assert (
					'/FuzzyDM/Velocities1' in snap_part or
					'/FuzzyDM/MomentumDensity1' in snap_part
				)
				assert (
					'/FuzzyDM/Velocities2' in snap_part or
					'/FuzzyDM/MomentumDensity2' in snap_part
				)
				if '/FuzzyDM/Velocities0' in snap_part:
					d = snap_part['/FuzzyDM/Velocities0']
				else:
					d = snap_part['/FuzzyDM/MomentumDensity0']
				assert len(d.shape) == 1
				assert len(d) % self.pmgrid**2 == 0
				d_len += len(d)
		assert d_len == self.pmgrid**3

	def add_potential(self, paths, sort_paths=True):
		assert 'grid' in self.types
		if isinstance(paths, Path) or isinstance(paths, str):
			paths = read_snapshot.snapshot_files(
				Path(paths), sort_paths=sort_paths
			)
		if sort_paths:
			self.potential_paths = sorted(
				paths, key=read_snapshot.arepo_filename_key
			)
		else:
			self.potential_paths = list(paths)
		assert self.potential_paths
		d_len = 0
		for path in self.potential_paths:
			with h5py.File(path, 'r') as snap_part:
				assert '/FuzzyDM/Potential' in snap_part
				d = snap_part['/FuzzyDM/Potential']
				assert len(d.shape) == 1
				assert len(d) % self.pmgrid**2 == 0
				d_len += len(d)
		assert d_len == self.pmgrid**3

	def fof_path(self):
		return read_snapshot.output_path(
			self.path, self.config, self.param, kind='fof'
		)

	def fof_files(self, fof_path=None):
		if fof_path is None:
			fof_path = self.fof_path()
		return read_snapshot.snapshot_files(fof_path)

	def read_fof_tab(self, fof_path=None, **kwargs):
		if fof_path is None:
			fof_path = self.fof_path()
		return read_snapshot.hdf5.read_fof_tab(
			self.fof_files(fof_path=fof_path), **kwargs
		)

	def get_group_particles(self, grnr, datasets, ptype=None):
		assert 'particles' in self.types
		g = self.read_fof_tab(read_ids=False)[1]
		if ptype is None:
			ptype_range = range(len(self.header['NumPart_Total']))
		elif isinstance(ptype, collections.abc.Iterable):
			ptype_range = ptype
		else:
			ptype_range = [ptype]
		result = {ptype: dict() for ptype in ptype_range}
		for ptype in ptype_range:
			offset = g.offset[grnr][ptype]
			length = g.len[grnr][ptype]
			for dataset in datasets:
				if self.numpart_total(ptype) > 0:
					result[ptype][dataset] = self.read_snapshot(
						f'/PartType{ptype}/{dataset}',
						data_slice=numpy.s_[offset:offset + length]
					)
				else:
					result[ptype][dataset] = None
		return result

	def get_type(self, preference):
		if preference in self.types:
			return preference
		elif 'particles' in self.types:
			return 'particles'
		elif 'grid' in self.types:
			return 'grid'
		else: assert False

	def numpart_total(self, ptype=None):
		return read_snapshot.numpart_total(self.header, ptype=ptype)

	def dataset_present(self, dataset):
		result = False
		first_part = True
		for path in self.paths:
			with h5py.File(path, 'r') as snap_part:
				if first_part:
					result = dataset in snap_part
				assert (dataset in snap_part) == result
			first_part = False
		return result

	def ix_to_file(self, dataset, ix):
		self.determine_dataset_info(dataset)
		ix_len = (
			self.first_slab_x_of_file[dataset][-1] +
			self.slabs_x_per_file[dataset][-1]
		)
		ix_it = isinstance(ix, collections.abc.Iterable)
		if not ix_it:
			ix = [ix]
		assert all(i >= 0 for i in ix)
		assert all(i < ix_len for i in ix)
		result = []
		for i_slice in ix:
			for i_file in range(len(self.paths)):
				if i_slice in self.file_to_ix(dataset, i_file):
					result.append(i_file)
					break
			else: assert False
		assert len(result) == len(ix)
		return result if ix_it else result[0]

	def file_to_ix(self, dataset, file_num):
		self.determine_dataset_info(dataset)
		file_num_it = isinstance(file_num, collections.abc.Iterable)
		if not file_num_it:
			file_num = [file_num]
		result = [
			range(
				self.first_slab_x_of_file[dataset][i],
				self.first_slab_x_of_file[dataset][i] +
					self.slabs_x_per_file[dataset][i]
			) for i in file_num
		]
		return result if file_num_it else result[0]

	def determine_dataset_info(self, *datasets):
		datasets = [
			normalize_dataset(ds) for ds in datasets
			if ds not in self.dataset_shape
		]
		if not datasets:
			return
		paths = [
			(
				self.velocity_paths if self.velocity_paths else self.paths,
				[ds for ds in datasets if ds in VELOCITY_DATASETS],
			),
			(
				self.potential_paths if self.potential_paths else self.paths,
				[ds for ds in datasets if ds in POTENTIAL_DATASETS],
			),
			(
				self.paths, [
					ds for ds in datasets if ds not in
						VELOCITY_DATASETS | POTENTIAL_DATASETS
				],
			)
		]
		# determine total dataset shapes, slabs_x_per_file,
		# first_slab_x_of_file
		first_part = {dataset: True for dataset in datasets}
		dataset_len = {dataset: 0 for dataset in datasets}
		dataset_shape = {dataset: None for dataset in datasets}
		slabstart_x = {dataset: 0 for dataset in datasets}
		for file_paths, file_datasets in paths:
			assert file_paths or not file_datasets
			for file_path in file_paths:
				with h5py.File(file_path, 'r') as snap_part:
					for dataset in file_datasets:
						if dataset not in snap_part:
							raise SnapshotError(
								f'Dataset {dataset} not found in snapshot'
							)
						d = snap_part[dataset]
						dataset_len[dataset] += len(d)
						shape = d.shape
						dtype = d.dtype
						if first_part[dataset]:
							dataset_shape[dataset] = shape
							self.dataset_dtype[dataset] = dtype
							self.slabs_x_per_file[dataset] = []
							self.first_slab_x_of_file[dataset] = []
						assert shape[1:] == dataset_shape[dataset][1:]
						assert dtype == self.dataset_dtype[dataset]
						# keep track of nslab_x and slabstart_x
						divisor = (
							self.pmgrid**2
							if dataset.startswith('/FuzzyDM/')
								and len(shape) == 1
							else 1
						)
						nslab_x = len(d) // divisor
						self.slabs_x_per_file[dataset].append(nslab_x)
						self.first_slab_x_of_file[dataset].append(
							slabstart_x[dataset]
						)
						# update slabstart_x
						slabstart_x[dataset] += nslab_x
						first_part[dataset] = False
		# store determined shape for each dataset
		for dataset in datasets:
			assert (
				len(self.slabs_x_per_file[dataset]) ==
				len(self.first_slab_x_of_file[dataset]) ==
				len(self.paths)
			)
			self.dataset_shape[dataset] = (
				(dataset_len[dataset],) + dataset_shape[dataset][1:]
			)
			self.dataset_shape_reshaped[dataset] = self.dataset_shape[dataset]
			grid_data = (
				dataset.startswith('/FuzzyDM/') or
				dataset.startswith('/DensityGrid/')
			)
			if grid_data:
				num_elements = numpy.prod(
					self.dataset_shape[dataset], dtype=int
				)
				is_slice = dataset.endswith('Slice') or self.slice
				if is_slice:
					assert num_elements % self.pmgrid**2 == 0
				else:
					assert num_elements == self.pmgrid**3, (num_elements, self.pmgrid)
				if len(self.dataset_shape[dataset]) == 1:
					# reshape 1D dataset into 3D
					if is_slice:
						assert dataset_len[dataset] % self.pmgrid**2 == 0
						self.dataset_shape_reshaped[dataset] = (
							(num_elements // self.pmgrid**2,) +
							(self.pmgrid,) * 2
						)
					else:
						assert dataset_len[dataset] == self.pmgrid**3
						self.dataset_shape_reshaped[dataset] = (
							(self.pmgrid,) * 3
						)
				else:
					if is_slice:
						assert self.dataset_shape[dataset][1:] == (
							(self.pmgrid,) * 2
						)
					else:
						assert self.dataset_shape[dataset] == (self.pmgrid,) * 3
			ptype_match = re.match(r'/PartType(\d)/', dataset)
			if ptype_match:
				assert dataset_len[dataset] == self.numpart_total(
					int(ptype_match[1])
				)
		assert (
			self.dataset_shape.keys() == self.dataset_shape_reshaped.keys() ==
			self.dataset_dtype.keys()
		)


	def read_snapshot_indices(self, dataset, data_slice):
		self.determine_dataset_info(dataset)
		dataset_shape = self.dataset_shape_reshaped[dataset]
		# ensure that data_slice is a tuple and convert attributes to Python int
		data_slice = tuple(
			s if not isinstance(s, slice) else numpy.s_[
				None if s.start is None else int(s.start):
				None if s.stop is None else int(s.stop):
				None if s.step is None else int(s.step)
			]
			for s in numpy.index_exp[data_slice]
		)
		# make sure that data_slice always specifies all dimensions to get
		# correct indices below
		if len(data_slice) < len(dataset_shape) and ... not in data_slice:
			data_slice += numpy.index_exp[...]
		# “fill in” slices without stop values (since numpy.ogrid doesn’t
		# know about the dataset’s shape)
		if ... in data_slice:
			assert data_slice.count(...) == 1
			eidx = data_slice.index(...)
			data_slice = (
				data_slice[:eidx] +
				numpy.index_exp[:] * (len(dataset_shape) - len(data_slice) + 1) +
				data_slice[eidx + 1:]
			)
		data_slice = slice_defaults(data_slice)
		assert all(s.step > 0 for s in data_slice)
		assert len(data_slice) == len(dataset_shape)
		data_slice = tuple(
			numpy.s_[s.start:dim:s.step] if s.stop is None else
			numpy.s_[s.start:s.stop + dim:s.step] if s.start >= s.stop else
			s
			for s, dim in zip(data_slice, dataset_shape)
		)
		# get indices using numpy.ogrid
		indices = numpy.ogrid[data_slice]
		for idx, dim in zip(indices, dataset_shape):
			# map all indices to the range [0, dim)
			idx %= dim
		# also map slice start/stop values to range [0, dim] (couldn’t be done
		# before because for normal Python slice semantics, start ≥ stop gives
		# an empty slice, which also applies to numpy.ogrid)
		data_slice = tuple(
			numpy.s_[s.start % dim:s.stop:s.step]
			for s, dim in zip(data_slice, dataset_shape)
		)
		data_slice = tuple(
			numpy.s_[s.start:s.stop % dim:s.step] if s.stop != dim else s
			for s, dim in zip(data_slice, dataset_shape)
		)
		return data_slice, indices


	# doesn’t support negative steps (start:stop:step); not supported by h5py
	# anyway
	# also only supports slices (and Ellipsis) in data_slice, i.e. no single
	# indices
	#TODO: rename to (e.g.) read_dataset()
	#TODO: investigate memory mapping
	#      -> numpy.memmap
	#      -> https://gist.github.com/maartenbreddels/09e1da79577151e5f7fec660c209f06e
	def read_snapshot(self, dataset, data_slice=numpy.s_[...]):
		assert dataset is not None
		paths = self.paths
		ds = normalize_dataset(dataset)
		if ds in VELOCITY_DATASETS and self.velocity_paths:
			paths = self.velocity_paths
		if ds in POTENTIAL_DATASETS and self.potential_paths:
			paths = self.potential_paths
		# get dataset shape
		self.determine_dataset_info(dataset)
		dataset_shape_orig = self.dataset_shape[dataset]
		dataset_shape = self.dataset_shape_reshaped[dataset]
		dataset_dtype = self.dataset_dtype[dataset]
		dataset_len = dataset_shape[0]
		num_elements = numpy.prod(dataset_shape, dtype=int)
		assert num_elements % dataset_len == 0
		# special cases: particle data, (fuzzy DM) grid data
		ptype_match = re.match(r'/PartType(\d)/', ds)
		particle_data = bool(ptype_match)
		particle_velocities = False
		if particle_data:
			ptype = int(ptype_match[1])
			particle_velocities = re.match(r'/PartType(\d)/Velocities', ds)
		grid_data = ds.startswith('/FuzzyDM/') or ds.startswith('/DensityGrid/')
		# calculate number of elements per dimension
		num_elements_per_dim = numpy.concatenate([
			numpy.cumprod(dataset_shape[::-1])[-2::-1], [1]
		])
		assert num_elements_per_dim[0] == num_elements // dataset_len
		# determine indices and result array size
		# Could be generalized even further by using numpy.ix_ instead of
		# slices with numpy.(o|m)grid, although h5py doesn’t directly support
		# this!
		# see https://stackoverflow.com/a/38775157
		# TODO: remove indices and do everything with slices?
		data_slice, indices = self.read_snapshot_indices(dataset, data_slice)
		x_indices = indices[0]
		result_shape = numpy.broadcast(*indices).shape
		num_elements_per_dim_result = numpy.concatenate([
			numpy.cumprod(result_shape[::-1])[-2::-1], [1]
		])
		# make all slice attributes positive (consistent with indices)
		# reconstructing the slice from slice.indices() should work for step > 0
		data_slice = tuple(
			slice(*s.indices(dim)) for s, dim in zip(data_slice, dataset_shape)
		)
		# determine files to load
		#TODO: get rid of file_nums_all for speed and to reduce memory for data
		#      with large length (e.g. particle data)
		file_nums_all = indices_to_files(
			dataset, numpy.ravel(x_indices),
			numpy.array(self.first_slab_x_of_file[dataset]),
			numpy.array(self.slabs_x_per_file[dataset])
		)
		# remove file number duplicates
		file_nums = list(collections.OrderedDict.fromkeys(file_nums_all))
		# split index ranges to make sure that indices are always in increasing
		# order (h5py limitation!)
		it_data = []
		for i, s, idx, dim in zip(
			range(len(dataset_shape)), data_slice, indices, dataset_shape
		):
			entry = argparse.Namespace()
			if s.start > s.stop and idx.size > 1:
				assert idx.flat[0] > idx.flat[-1]
				# make sure that slices line up correctly for s.step > 1
				offset = (s.step - (dim - s.start)) % s.step
				# explicitly specify stop for sl_left to simplify code below
				entry.sl_left = numpy.s_[s.start:dim:s.step]
				entry.sl_right = numpy.s_[offset:s.stop:s.step]
				mask = numpy.ravel(idx > idx.flat[-1])
				mask_pre = numpy.index_exp[:] * i
				mask_post = numpy.index_exp[:] * (len(idx.shape) - i - 1)
				entry.idx_left = idx[mask_pre + (mask,) + mask_post]
				entry.idx_right = idx[mask_pre + (~mask,) + mask_post]
				assert numpy.all(
					range(*entry.sl_left.indices(dim)) ==
					entry.idx_left.reshape(-1)
				)
				assert numpy.all(
					range(*entry.sl_right.indices(dim)) ==
					entry.idx_right.reshape(-1)
				)
			else:
				entry.sl_left = s
				entry.sl_right = numpy.s_[:0]
				entry.idx_left = idx
				entry.idx_right = idx[:0]
			it_data.append(entry)
		it_x = it_data[0]
		file_nums_left = file_nums_all[:it_x.idx_left.size]
		file_nums_right = file_nums_all[it_x.idx_left.size:]
		num_in_slab = numpy.broadcast(*indices[1:]).size
		assert num_in_slab == numpy.prod(
			[sum(idx.size for idx in dim) for dim in indices[1:]], dtype=int
		)
		# read data from corresponding files
		result = numpy.full(result_shape, numpy.nan, dtype=dataset_dtype)
		reshape_1d = grid_data and len(dataset_shape_orig) == 1
		if reshape_1d:
			# flatten array while ensuring that no copy is made
			result1d = result.view()
			result1d.shape = -1
		x_read = 0
		x_read_extra = 0
		for file_idx in file_nums:
			slabstart_x = self.first_slab_x_of_file[dataset][file_idx]
			nslab_x = self.slabs_x_per_file[dataset][file_idx]
			x_thisfile_indices_l = it_x.idx_left[file_nums_left == file_idx]
			x_thisfile_indices_r = it_x.idx_right[file_nums_right == file_idx]
			if x_thisfile_indices_l.size == 0 or x_thisfile_indices_r.size == 0:
				if x_thisfile_indices_l.size > 0:
					x_thisfile_indices = [x_thisfile_indices_l]
					x_thisfile_slice = [it_x.sl_left]
				else:
					x_thisfile_indices = [x_thisfile_indices_r]
					x_thisfile_slice = [it_x.sl_right]
			else:
				# handle case if first and last file are the same and the
				# end of this file file has to be read before the beginning
				assert it_x.idx_right.size > 0
				assert x_indices[0] > x_indices[-1]
				assert data_slice[0].start > data_slice[0].stop
				assert file_nums_all[0] == file_nums_all[-1] == file_idx
				x_thisfile_indices = [
					x_thisfile_indices_l, x_thisfile_indices_r
				]
				x_thisfile_slice = [it_x.sl_left, it_x.sl_right]
			xnum_thisfile = [xidx.size for xidx in x_thisfile_indices]
			# adjust for local indexing within the current file
			for xidx in x_thisfile_indices:
				xidx -= slabstart_x
			xsl_tmp = []
			for xsl in x_thisfile_slice:
				start, stop, step = xsl.indices(dataset_len)
				start -= slabstart_x
				if start < 0:
					# use correct (non-negative) start index, making sure that
					# slices line up correctly for step > 1
					start %= step
				stop -= slabstart_x
				assert stop > 0
				xsl_tmp.append(numpy.s_[start:stop:step])
			x_thisfile_slice = xsl_tmp
			for xidx, xsl in zip(x_thisfile_indices, x_thisfile_slice):
				xsl_rng = range(*xsl.indices(nslab_x))
				assert len(xsl_rng) == xidx.size
				assert numpy.all(xsl_rng == xidx.reshape(-1))
			# read HDF5 data
			x_read_thisfile = [0, 0]
			file_path = paths[file_idx]
			with h5py.File(file_path, 'r') as snap_part:
				d = snap_part[dataset]
				assert d.shape[1:] == dataset_shape_orig[1:]
				assert d.size % num_elements_per_dim[0] == 0
				if particle_data:
					header = snap_part['Header'].attrs
					assert header['NumPart_ThisFile'][ptype] == len(d)
				slices_src = [x_thisfile_slice] + [
					[e.sl_left, e.sl_right][:1 + (len(e.idx_right) > 0)]
					for e in it_data[1:]
				]
				# in principle it’s possible to get x_slices_dest without
				# keeping track of x_read (using
				# len(range(it_x.sl_*)[:slabstart_x]), but since slices have to
				# be split into left and right part, this is unnecessarily
				# complicated
				x_slices_dest = [
					numpy.s_[x_read:x_read + xnum_thisfile[0]],
					numpy.s_[-xnum_thisfile[-1]:]
				][:len(x_thisfile_indices)]
				slices_dest = [x_slices_dest] + [
					[
						# explicitly specify start/stop to simplify code below
						numpy.s_[0:e.idx_left.size],
						numpy.s_[e.idx_left.size:dim]
					][:1 + (len(e.idx_right) > 0)]
					for e, dim in zip(it_data[1:], result_shape[1:])
				]
				# when reshaping: (generally) have to use indices due to
				# non-constant step size in HDF5 dataset
				if reshape_1d:
					assert len(dataset_shape) > 1
					assert len(d.shape) == 1
					assert len(d) == nslab_x * num_elements_per_dim[0]
					# when reading full, contiguous slabs (most common case):
					# optimize read by using slices
					if data_slice[0].step == 1 and all(
						range(*s.indices(dim)) == range(dim)
						for s, dim in zip(data_slice[1:], dataset_shape[1:])
					):
						assert num_in_slab == num_elements_per_dim[0]
						for i, [xsl_dest, xsl_src] in enumerate(zip(
							x_slices_dest, x_thisfile_slice
						)):
							assert xsl_dest.step in (None, 1)
							assert xsl_src.step in (None, 1)
							sl_dest_slab = slice(*(
								x * num_in_slab
								for x in xsl_dest.indices(len(result))[:2]
							))
							sl_src_slab = slice(*(
								x * num_in_slab
								for x in xsl_src.indices(dataset_len)[:2]
							))
							d.read_direct(
								result1d,
								dest_sel=sl_dest_slab, source_sel=sl_src_slab
							)
							x_read_thisfile[i] = len(range(
								*xsl_src.indices(nslab_x)
							))
					# otherwise: would use indices, but this is extremely slow
					# in h5py/HDF5, see
					# https://github.com/h5py/h5py/issues/293
					# https://github.com/h5py/h5py/issues/368
					# read slices as large as possible in a loop as workaround
					# (still much slower than the ideal case, though!)
					# TODO: check how many read calls would be done and read
					#       larger slabs (discarding surplus data) if number is
					#       too high (i.e. impose a maximum number of read
					#       calls)?
					else:
						indices_src = [x_thisfile_indices] + [
							[e.idx_left, e.idx_right][
								:1 + (e.idx_right.size > 0)
							] for e in it_data[1:]
						]
						for sl_dest, sl_src, idx_src in zip(
							itertools.product(*slices_dest),
							itertools.product(*slices_src),
							itertools.product(*indices_src)
						):
							partial_slices = len(sl_src)
							for s, dim in zip(
								reversed(sl_src), reversed(dataset_shape)
							):
								if range(*s.indices(dim)) == range(dim):
									partial_slices -= 1
								else:
									break
							assert partial_slices > 0
							last_dim_partial = False
							if partial_slices == len(sl_src):
								last_dim_partial = True
								partial_slices -= 1
							for idx_offset_dest, partial_idx_src in zip(
								itertools.product(
									*(range(i.size)
										for i in idx_src[:partial_slices]
									)
								),
								itertools.product(
									*(numpy.ravel(i)
										for i in idx_src[:partial_slices]
									)
								)
							):
								offset_src = sum(
									i * dim_num for i, dim_num in zip(
										partial_idx_src, num_elements_per_dim
									)
								)
								offset_dest = sum(
									(s.start + o) * dim_num
									for s, o, dim_num in zip(
										sl_dest, idx_offset_dest,
										num_elements_per_dim_result
									)
								)
								if last_dim_partial:
									sl_src_slab = numpy.s_[
										offset_src + sl_src[-1].start:
										offset_src + sl_src[-1].stop:
										sl_src[-1].step
									]
									start_dest = offset_dest + sl_dest[-1].start
									stop_dest = offset_dest + sl_dest[-1].stop
								else:
									assert sl_dest[-1].step in (None, 1)
									slab_size = num_elements_per_dim[
										partial_slices - 1
									]
									assert (
										slab_size ==
										num_elements_per_dim_result[
											partial_slices - 1
										]
									)
									sl_src_slab = numpy.s_[
										offset_src:offset_src + slab_size
									]
									start_dest = offset_dest
									stop_dest = offset_dest + slab_size
								# slices don’t wrap around, so can’t use 0 as
								# stop value (always gives empty sequence)
								if stop_dest == 0:
									stop_dest = None
								else:
									assert (start_dest < 0) == (stop_dest < 0)
								sl_dest_slab = numpy.s_[
									start_dest:stop_dest:sl_dest[-1].step
								]
								d.read_direct(
									result1d, dest_sel=sl_dest_slab,
									source_sel=sl_src_slab
								)
							x_read_thisfile[int(
								sl_src[0] is not x_thisfile_slice[0]
							)] = idx_src[0].size
				# when not reshaping: directly use slices for increased
				# efficiency
				else:
					assert len(d) == nslab_x
					for sl_dest, sl_src in zip(
						itertools.product(*slices_dest),
						itertools.product(*slices_src)
					):
						d.read_direct(
							result, dest_sel=sl_dest, source_sel=sl_src
						)
						x_read_thisfile[int(
							sl_src[0] is not x_thisfile_slice[0]
						)] = len(range(*sl_src[0].indices(nslab_x)))
			for xr, xn in zip(x_read_thisfile, xnum_thisfile):
				assert xr == xn
			assert x_read_thisfile[0] > 0
			x_read += x_read_thisfile[0]
			if len(x_thisfile_indices) == 2:
				assert x_read_thisfile[1] > 0
				assert x_read_thisfile[0] == x_thisfile_indices_l.size
				assert x_read_thisfile[1] == x_thisfile_indices_r.size
				assert x_read_extra == 0
				x_read_extra += x_read_thisfile[1]
		x_read += x_read_extra
		assert x_read == it_x.idx_left.size + it_x.idx_right.size
		assert x_read * num_in_slab == result.size
		assert numpy.all(numpy.isfinite(result))
		if particle_velocities and self.param['ComovingIntegrationOn']:
			# convert from GADGET velocity
			result /= numpy.sqrt(self.header['Time'])
		return result


	def determine_chunks(
		self, mpi_rank, mpi_size, num_slabs=None, slab_size=None,
		element_size=GRID_ELEMENT_MEM_SIZE,
		chunk_mem_size=CHUNK_MEM_SIZE_DEFAULT, return_max_num_chunks=False
	):
		if num_slabs is None:
			num_slabs = self.pmgrid
		if slab_size is None:
			slab_size = self.pmgrid**2
		# make sure we’re using Python int
		num_slabs = int(num_slabs)
		slab_size = int(slab_size)
		# determine which region to read
		nslab_x = num_slabs // mpi_size
		nslab_x_max = math.ceil(num_slabs / mpi_size)
		slabstart_x = mpi_rank * nslab_x
		nslab_x_rest = num_slabs % mpi_size
		if mpi_rank < nslab_x_rest:
			nslab_x += 1
			slabstart_x += mpi_rank
		else:
			slabstart_x += nslab_x_rest
		# determine x chunk size
		x_chunk_size_target = (
			numpy.inf if numpy.isinf(chunk_mem_size) else
			max(chunk_mem_size // (slab_size * element_size), 1)
		)
		x_chunk_size = min(x_chunk_size_target, nslab_x)
		# determine chunks
		ix_len = slabstart_x + nslab_x
		res = []
		ix_start = slabstart_x
		while ix_start < ix_len:
			ix_end = min(ix_start + x_chunk_size, ix_len)
			res.append((ix_start, ix_end))
			ix_start = ix_end
		if return_max_num_chunks:
			return res, math.ceil(nslab_x_max / x_chunk_size_target)
		else:
			return res

	@property
	def hubble_param(self):
		return (
			self.header['HubbleParam']
			if self.param['ComovingIntegrationOn'] else 1.0
		)

	@property
	def cosmology(self):
		return astropy.cosmology.FlatLambdaCDM(
			100 * self.hubble_param, self.header['Omega0']
		)


	def convert_qty_from_internal_units(
		self, value, target_unit, return_qty=False
	):
		target_unit_in_internal_units = self.qty_in_internal_units(
			1, target_unit
		)
		if numpy.isclose(target_unit_in_internal_units, 1):
			target_unit_in_internal_units = 1.
		qty = astropy.units.Quantity(
			value / target_unit_in_internal_units, target_unit
		)
		return qty if return_qty else qty.value


	def qty_in_internal_units(self, value, unit, return_qty=False):
		h = self.hubble_param
		UnitLength_in_cm = self.header['UnitLength_in_cm'] / h
		UnitVelocity_in_cm_per_s = self.header['UnitVelocity_in_cm_per_s']
		UnitMass_in_g = self.header['UnitMass_in_g'] / h
		UnitTime_in_s = UnitLength_in_cm / UnitVelocity_in_cm_per_s
		au = astropy.units
		qty = au.Quantity(value, unit).decompose([
			au.Unit(UnitLength_in_cm * au.cm),
			au.Unit(UnitTime_in_s * au.s),
			au.Unit(UnitMass_in_g * au.g)
		])
		return qty if return_qty else qty.value


	def constant_in_internal_units(self, name, return_qty=False):
		if name == 'H0':
			result = self.qty_in_internal_units(
				100 * self.hubble_param, 'km/(s * Mpc)', return_qty=True
			)
		elif name == 'critical_density0':
			G = self.constant_in_internal_units('G', return_qty=True)
			H0 = self.constant_in_internal_units('H0', return_qty=True)
			result = 3 * H0**2 / (8 * numpy.pi * G)
			astropy_rho0 = self.cosmology.critical_density0
			result_astropy = self.qty_in_internal_units(
				astropy_rho0.value, astropy_rho0.unit, return_qty=True
			)
			assert numpy.isclose(result, result_astropy, atol=0)
		else:
			qty = getattr(astropy.constants, name)
			result = self.qty_in_internal_units(
				qty.value, qty.unit.to_string(), return_qty=True
			)
		return result if return_qty else result.value

	def hubble_function(self):
		a = self.header['Time']
		Omega0 = self.header['Omega0']
		OmegaLambda = self.header['OmegaLambda']
		H0 = self.constant_in_internal_units('H0')
		E = Omega0 / a**3 + (1 - Omega0 - OmegaLambda) / a**2 + OmegaLambda
		return H0 * numpy.sqrt(E)


def slice_defaults(sl):
	result = []
	for s in numpy.index_exp[sl]:
		if isinstance(s, slice):
			start = 0 if s.start is None else s.start
			stop = s.stop
			step = 1 if s.step is None else s.step
			ss = numpy.s_[start:stop:step]
		else:
			ss = s
		result.append(ss)
	result = result[0] if isinstance(sl, slice) else tuple(result)
	return result


@numba.njit
def indices_to_files(dataset, idx, first_slab_x_of_file, slabs_x_per_file):
	assert len(first_slab_x_of_file) == len(slabs_x_per_file)
	num_files = len(first_slab_x_of_file)
	idx_len = first_slab_x_of_file[-1] + slabs_x_per_file[-1]
	assert numpy.all(idx >= 0)
	assert numpy.all(idx < idx_len)
	result = numpy.zeros_like(idx, dtype='u4')
	file_num = 0
	for i in range(len(result)):
		ip = idx[i]
		while (
			ip < first_slab_x_of_file[file_num] or
			ip >= first_slab_x_of_file[file_num] +
				slabs_x_per_file[file_num]
		):
			file_num = (file_num + 1) % num_files
		result[i] = file_num
	return result

def normalize_dataset(dataset):
	return f'/{dataset.lstrip("/")}'

