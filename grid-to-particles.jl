#!/usr/bin/env julia
using ArgParse
using ElasticArrays
using EllipsisNotation
using EllipsisNotation: (..) as …
using FilePaths
using FilePathsBase: / as ⊘
using HDF5
using MPI
using PhysicalConstants.CODATA2018: G
using Unitful
using UnitfulAstro
using Util

push!(LOAD_PATH, joinpath(@__DIR__, "julia-modules"))
using IterateGrid
using MPIUtil
using Snapshots


@define_mpi_globals

# Note: doesn’t check for conflicts with other particles present in the input
#       snapshot
const parse_settings = ArgParseSettings(description="Convert a 3D density grid \
	to equal-mass N-body particles by randomly sampling grid cells",
	autofix_names=true)
@add_arg_table! parse_settings begin
	"--numpart"
		arg_type = Int
		default = 1024
		help = "∛ of the number of output particles to generate"
	"--ptype"
		arg_type = Int
		default = 1
		help = "The particle type to use for the generated particles"
	#TODO
	"--num-files"
		arg_type = Int
		default = 1
		help = "Number of files to use for output snapshot"
	"--output-suffix"
		default = "_to_particles"
		help = "Suffix for the output file names"
	"path"
		nargs = '+'
		arg_type = AbstractPath
		required = true
		help = "the paths to the snapshot files; any normal files are \
			interpreted as single-file snapshots, while all files in any \
			given directory are interpreted as a single multi-file snapshot"
end
const args = (; parse_args(parse_settings; as_symbols=true)...)


function main()
	#TODO: define global const in function?
#	args = (; parse_args(["--numpart", "512", "snapdir_011/"], parse_settings; as_symbols=true)...)
	for path in args.path
		println0(path)
		snap = Snapshot(path)
#		numpart_total = args.numpart^3
		numpart_total_target = args.numpart^3
		# determine particle mass
		#TODO: do computations like this in Snapshots.jl
		h = snap.header["HubbleParam"]
		UnitLength_in_cm = snap.header["UnitLength_in_cm"] / h
		UnitVelocity_in_cm_per_s = snap.header["UnitVelocity_in_cm_per_s"]
		UnitMass_in_g = snap.header["UnitMass_in_g"] / h
		UnitTime_in_s = UnitLength_in_cm / UnitVelocity_in_cm_per_s
		#TODO: no way to locally define units using Unitful.jl?
#		UL = define_unit(:CodeUnitLength, UnitLength_in_cm * u"cm")
#		UT = define_unit(:CodeUnitTime, UnitTime_in_s * u"s")
#		UM = define_unit(:CodeUnitMass, UnitMass_in_g * u"g")
		H0 = 100h * u"km/(s * Mpc)"
		ρ_m = snap.header["Omega0"] * ustrip(u"g/cm^3", 3H0^2 / (8π * G)) /
			(UnitMass_in_g / UnitLength_in_cm^3)
		M_tot = ρ_m * snap.box_size^3
#		pmass = M_tot / numpart_total
		pmass_target = M_tot / numpart_total_target
		# make template file
		template = nothing
		output_tmp = nothing
		ds_type = nothing
		id_type = nothing
		if mpi_rank == 0
			tdir = mktmpdir()
			local ds_type, id_type
			ds_attrs = Dict()
			template_orig = cp_to_dir(snap.paths[begin], tdir)
			h5open(template_orig, "r+") do f
				g = f["/PartType$(args.ptype)"]
				ds_type = eltype(g["/PartType$(args.ptype)/Coordinates"])
				id_type = eltype(g["/PartType$(args.ptype)/ParticleIDs"])
				ds_attrs["Coordinates"] = Dict(attrs(g["Coordinates"]))
				ds_attrs["Masses"] = Dict(attrs(g["Masses"]))
				ds_attrs["ParticleIDs"] = Dict(attrs(g["ParticleIDs"]))
				delete_object(f, "/FuzzyDM")
				for ds in keys(g)
					delete_object(g, ds)
				end
			end
			template = parent(template_orig) ⊘ "template.hdf5"
			run(pipeline(`h5repack $template_orig $template`, devnull))
			rm(template_orig)
#			h5open(template, "r+") do f
#				# write attributes
#				@assert numpart_total ≤ typemax(Int32)
#				pidx = 1 + args.ptype
#				header = attrs(f["/Header"])
#				header["NumFilesPerSnapshot"] = args.num_files
#				setattr!(header, "NumPart_Total", pidx, numpart_total)
#				setattr!(header, "NumPart_Total_HighWord", pidx, 0)
#				setattr!(header, "NumPart_ThisFile", pidx, numpart_total)
#				setattr!(header, "MassTable", pidx, pmass)
#				# create datasets
#				g = f["/PartType$(args.ptype)"]
#				create_dataset(
#					g, "Coordinates", ds_type, (numpart_total, 3);
#					fill_value=ds_type(NaN)
#				)
#				create_dataset(
#					g, "Masses", ds_type, numpart_total; fill_value=ds_type(0)
#				)
#				#TODO: error when setting g["dataset"] = UInt(1):UInt(N)
#				create_dataset(g, "ParticleIDs", id_type, numpart_total)
#				g["ParticleIDs"][:] = id_type(1):id_type(numpart_total)
#				for (ds, attr_dict) in pairs(ds_attrs)
#					a = attributes(g[ds])
#					for (key, value) in pairs(attr_dict)
#						a[key] = value
#					end
#				end
#			end
			output_tmp = cp(
				template, tdir ⊘ "$(basename(snap.path))$(args.output_suffix).hdf5"
			)
		end
		template = MPI.bcast(template, 0, mpi_comm)
		output_tmp = MPI.bcast(output_tmp, 0, mpi_comm)
		ds_type, id_type = MPI.bcast((ds_type, id_type), 0, mpi_comm)
		# generate particles
		cell_len = snap.box_size / snap.pmgrid
		cell_vol = cell_len^3
		(part_first, part_stop), = determine_chunks(
			snap, mpi_rank, mpi_size; num_slabs=numpart_total_target,
			slab_size=1, chunk_mem_size=Inf
		)
#		numpart = length(part_first:part_stop)
#		coord = fill(ds_type(NaN), 3, numpart)
		numpart_expected = length(part_first:part_stop)
		coord = ElasticArray{ds_type}(undef, 3, 0)
		sizehint!(coord, 3, numpart_expected)
		#TODO
#		pidx = firstindex(coord, ndims(coord))
#		iterate_grid(snap; grid_type=:cumsum) do grid, ix_info, data_slice
##			numpart_per_cell = reshape(
##				counts(
##					wsample(eachindex(grid), reshape(grid, :), numpart),
##					eachindex(grid)
##				), size(grid)
##			)
#			rho, csum = grid
#			ix_start = ix_info.start
#			numpart_per_cell = zeros(Int32, size(rho))
#			for cell in wsample(eachindex(rho), reshape(rho, :), numpart)
#				numpart_per_cell[cell] += 1
#			end
#			for idx in CartesianIndices(numpart_per_cell)
#				tidx = Tuple(idx)
#				I = (tidx[begin:end - 1]..., ix_start + tidx[end])
#				for _ in 1:numpart_per_cell[idx]
#					coord[:, pidx] = (I .+ rand(length(I))) .* cell_len
#					pidx += 1
#				end
#			end
#		end
		iterate_grid(snap) do rho, ix_info, data_slice
			ix_start = ix_info.start
			for idx in CartesianIndices(rho)
				N_target = (rho[idx] * cell_vol) / pmass_target
				N = Int(floor(N_target))
				N += rand() < N_target - N
				tidx = Tuple(idx)
				I = reverse((tidx[begin:end - 1]..., ix_start + tidx[end]))
				for _ in 1:N
					append!(coord, (I .+ rand(length(I))) .* cell_len)
				end
			end
		end
#		@assert pidx == lastindex(coord, ndims(coord)) + 1
#		@assert all(isfinite(x) for x in coord)
		# write output
		mpi_idx = mpi_rank + 1
		numpart = size(coord)[end]
		numpart_per_rank = MPI.Allgather(numpart, mpi_comm)
		numpart_offset = sum(numpart_per_rank[begin:mpi_idx - 1])
		numpart_total = sum(numpart_per_rank)
		pmass = M_tot / numpart_total
		for rank in 0:mpi_size - 1
			if rank == mpi_rank
				h5open(output_tmp, "r+") do f
					g = f["/PartType$(args.ptype)"]
					if mpi_rank == 0
						# write attributes, including final values of particle
						# numbers and mass
						@assert numpart_total ≤ typemax(Int32)
						pidx = 1 + args.ptype
						header = attrs(f["/Header"])
						header["NumFilesPerSnapshot"] = args.num_files
						setattr!(header, "NumPart_Total", pidx, numpart_total)
						setattr!(header, "NumPart_Total_HighWord", pidx, 0)
						setattr!(header, "NumPart_ThisFile", pidx, numpart_total)
						setattr!(header, "MassTable", pidx, pmass)
						# create datasets
						create_dataset(
							g, "Coordinates", ds_type, (3, numpart_total);
							fill_value=ds_type(NaN)
						)
						create_dataset(
							g, "Masses", ds_type, numpart_total;
							fill_value=ds_type(0)
						)
						#TODO: error when setting g["dataset"] = UInt(1):UInt(N)
						create_dataset(g, "ParticleIDs", id_type, numpart_total)
						g["ParticleIDs"][:] = id_type(1):id_type(numpart_total)
						for (ds, attr_dict) in pairs(ds_attrs)
							a = attributes(g[ds])
							for (key, value) in pairs(attr_dict)
								a[key] = value
							end
						end
					end
					rng = numpart_offset .+ (1:numpart)
					g["Coordinates"][:, rng] = coord
					if mpi_rank == mpi_size - 1
						@assert all(isfinite.(g["Coordinates"]))
					end
				end
			end
			MPI.Barrier(mpi_comm)
		end
		if mpi_rank == 0
			# move output to target directory
			mv_to_dir(output_tmp, parent(snap.path); force=true)
		end
	end
end


if abspath(PROGRAM_FILE) == @__FILE__
	main()
end

