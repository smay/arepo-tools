#!/usr/bin/env python3
import argparse
import astropy.cosmology
import astropy.units
import numba
import numpy
import scipy.interpolate
from pathlib import Path


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Calculate the predicted cost '
		'of cosmological fuzzy dark matter simulations')
	parser.add_argument('--PMGRID', type=int, default=3200,
		help='mesh size per dimension (default: %(default)d)')
	parser.add_argument('-m', type=float, default=1e-22,
		help='FDM mass (default: %(default)g)')
	parser.add_argument('-z', type=float, default=0.,
		help='target redshift (default: %(default)f)')
	parser.add_argument('-L', type=float,
		help='target box size for comparison (in kpc/h)')
	args = parser.parse_args()


Z_START = 127
INPUTSPEC_PATH = Path.home() / 'arepo-tools' / 'ngenic_inputspec_z127.txt'
FFTW_COMPLEX_SIZE = 16 # bytes
FFTW_REAL_SIZE = FFTW_COMPLEX_SIZE // 2 # bytes
TIMEBASE = 1 << 29
PMGRID_FIT_A = 8.837807955070367e-12 # taken from fdm_simulation_cost.ipynb
PMGRID_FIT_MASS = 7e-23              # taken from fdm_simulation_cost.ipynb
                                     # NOTE: spurious factor of 0.7 has been
                                     #       corrected!
PMGRID_FIT_BOX_SIZE = 1000           # taken from fdm_simulation_cost.ipynb

UnitLength_in_cm         = 3.085678e21 # 1.0 kpc
UnitMass_in_g            = 1.989e43    # 1.0e10 solar masses
UnitVelocity_in_cm_per_s = 1e5         # 1.0 km/s
UnitTime_in_s = UnitLength_in_cm / UnitVelocity_in_cm_per_s
EV_TO_G = 1.78266191e-33
HBAR = 1.0545718e-27 / UnitLength_in_cm**2 / UnitMass_in_g * UnitTime_in_s

C = astropy.cosmology.FlatLambdaCDM(H0=70, Om0=0.3)
Om0 = C.Om0
Ode0 = C.Ode0
H0 = C.H0.to(1 / (UnitTime_in_s * astropy.units.s)).to_value()
h = C.h

# taken from fdm_simulation_cost.ipynb (v_data_ic.box_size)
IC_BOX_SIZES = [
	1000.0,
	1500.0,
	2000.0,
	2500.0,
	3000.0,
	3500.0,
	4000.0,
	4500.0,
	5000.0,
	5500.0,
	6000.0,
	6500.0,
	7000.0,
	7500.0,
	8000.0,
	8500.0,
	9000.0,
	9500.0,
	10000.0,
	12000.0,
	14000.0,
	16000.0,
	18000.0,
	20000.0,
	25000.0,
	30000.0,
	35000.0,
	40000.0,
	45000.0,
	50000.0,
]
# taken from fdm_simulation_cost.ipynb (v_data_ic.v_max)
IC_V_MAX = [
	8.589988,
	12.226343,
	15.602858,
	18.771742,
	21.767462,
	24.614735,
	27.332260,
	29.934731,
	32.434006,
	34.839882,
	37.160576,
	39.403091,
	41.573467,
	43.676971,
	45.718204,
	47.701263,
	49.629784,
	51.507042,
	53.335972,
	60.218224,
	66.503265,
	72.290443,
	77.654251,
	82.652359,
	93.827576,
	103.498199,
	111.997375,
	119.554359,
	126.334839,
	132.463181,
]
VEL_BOX_IC = scipy.interpolate.interp1d(IC_BOX_SIZES, IC_V_MAX)


def linear_velocity(cosmo, box_size, z):
	return VEL_BOX_IC(box_size) * numpy.sqrt(
		C.scale_factor(z) / C.scale_factor(Z_START)
	)

def fdm_v_max(cell_len, mass):
	m = mass * EV_TO_G / UnitMass_in_g
	return (HBAR / m) * numpy.pi / cell_len

@numba.njit
def time_step(cell_len, mass, a):
    m = mass * EV_TO_G / UnitMass_in_g
    return a**2 * m / (6 * HBAR) * cell_len**2

@numba.njit
def get_time_step_binned(dt, Timebase_interval):
    dt = int(dt / Timebase_interval)
    bin = -1
    while dt:
        bin += 1
        dt >>= 1
    dt = (1 << bin) * Timebase_interval
    return dt

@numba.njit
def get_Timebase_interval(a_start, a_end):
    return (numpy.log(a_end) - numpy.log(a_start)) / TIMEBASE

@numba.njit
def get_hubble(a):
    hubble_a = Om0 / a**3 + (1 - Om0 - Ode0) / a**2 + Ode0
    hubble_a = H0 / h * numpy.sqrt(hubble_a)
    return hubble_a

@numba.njit
def num_time_steps(cell_len, mass, a_start, a_end, progress=-1):
    Timebase_interval = get_Timebase_interval(a_start, a_end)
    a = a_start
    ti = 0
    steps = 0
    while a < a_end:
        hubble_a = get_hubble(a)
        dt = hubble_a * time_step(cell_len, mass, a)
        ti += get_time_step_binned(dt, Timebase_interval)
        a = a_start * numpy.exp(ti)
        steps += 1
        if progress > 0 and steps % progress == 0: print(steps)
    return steps

def fit_func_pmgrid(x, a): return a * x**5

def cpu_time(pmgrid, box_size, mass, z_end=0):
	# time step scales ~ m, so total time scales ~ 1/m
	cost_1mpc = (
		fit_func_pmgrid(pmgrid, PMGRID_FIT_A) / (mass / PMGRID_FIT_MASS)
	)
	# time step scales ~ Δx², so total time scales ~ 1/Δx²
	cost = cost_1mpc / (box_size / PMGRID_FIT_BOX_SIZE)**2
	# fit function gives cost for z = 0
	if z_end == 0:
		return cost
	timesteps_0 = num_time_steps(
		box_size / pmgrid, mass, C.scale_factor(Z_START), C.scale_factor(0)
	)
	timesteps_z = num_time_steps(
		box_size / pmgrid, mass, C.scale_factor(Z_START), C.scale_factor(z_end)
	)
	assert timesteps_z < 1.1 * timesteps_0
	# CPU time scales linearly with number of time steps
	# (all time steps take the same amount of time)
	return cost * timesteps_z / timesteps_0

def box_size_max(pmgrid, mass, box_sizes, z=0, factor=1):
	v_box_sample = linear_velocity(C, box_sizes, z)
	v_max_sample = factor * fdm_v_max(box_sizes / pmgrid, mass)
	v_ok = v_box_sample < v_max_sample
	size_ok = box_sizes[v_ok]
	if len(size_ok) == 0 or len(size_ok) == len(v_box_sample):
		return None
	size_max = size_ok[-1]
	return size_max


if __name__ == '__main__':
	# calculate results
	mem_gib = args.PMGRID**3 * 24 / 1024**3
	L_max = box_size_max(
		args.PMGRID, args.m, numpy.linspace(1000, 50000, 5000), z=args.z
	)
	if L_max is None:
		print('L_max could not be determined')
	else:
		cell_len = L_max / args.PMGRID
		v_max = fdm_v_max(cell_len, args.m)
		v_lin = linear_velocity(C, L_max, args.z)
		cost = cpu_time(args.PMGRID, L_max, args.m, args.z)
		# print results
		print(f'Mesh size:            {args.PMGRID}³')
		print(f'Memory used by mesh:  {mem_gib:,.1f} GiB '
			f'(Cobra max. memory: {636 * 192:,.1f} GiB)')
		print(f'FDM mass:             m = {args.m} eV')
		print(f'Target redshift:      z = {args.z}')
		print(f'Max. box size:        L_max = {L_max * 1e-3:.2f} Mpc/h')
		print(f'Resolution for L_max: Δx = {cell_len:.2f} kpc/h')
		print(f'FDM velocity criterion:          v_max = {v_max: 7.2f} km/s')
		print(f'Linear velocity for L_max and z: v_lin = {v_lin: 7.2f} km/s')
		# TODO: FDM Jeans length
		# arXiv:1610.08297 eq. (40) vs. arXiv:0806.0232 eq. (14)?
		print(f'CPU time for L_max: {cost * 1e-6:.3g} × 10⁶ CPU h')
	if args.L:
		cell_len = args.L / args.PMGRID
		v_max = fdm_v_max(cell_len, args.m)
		v_lin = linear_velocity(C, args.L, args.z)
		cost = cpu_time(args.PMGRID, args.L, args.m, args.z)
		print()
		print(f'Comparison for box size     L = {args.L * 1e-3:.2f} Mpc/h')
		print(f'Resolution for L:          Δx = {cell_len:.2f} kpc/h')
		print(f'FDM velocity criterion: v_max = {v_max: 7.2f} km/s')
		print(f'Linear velocity for L and z: v_lin = {v_lin: 7.2f} km/s')
		print(f'CPU time for L: {cost * 1e-6:.3g} × 10⁶ CPU h')
