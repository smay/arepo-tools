#!/usr/bin/env python3
import argparse
import collections
import contextlib
import warnings
from pathlib import Path
import h5py
import numpy
import matplotlib.lines
import matplotlib.offsetbox
import matplotlib.pyplot as plt
import scipy
import scipy.optimize
import bin_density
import m200_util
import matplotlib_settings
import read_snapshot
import read_snapshot.hdf5 as read_hdf5
import units_util
from snapshot import Snapshot


# constants
PARTICLE_TYPE = 1
FIT_XLIM_FACTORS = (0.9, 1.1)
FIT_R_BINS = 200

def mass_bin(s):
	try:
		low, high = map(float, s.split(','))
		return low, high
	except:
		raise argparse.ArgumentTypeError(
			'low and high bound of bin must be given as “low,high” '
			f'(problematic input: “{s}”)'
		)

parser = argparse.ArgumentParser(description='Plot halo density profiles for '
	'fuzzy dark matter or cold dark matter simulation snapshots')
parser.add_argument('--prefer-type', choices=('grid', 'particles'),
	default='particles',
	help='the type of snapshot data that should be preferred '
		'(default: %(default)s)')
parser.add_argument('--m200', type=int, nargs='+',
	help='use M_200 instead of FoF mass to bin the halos, for the '
		'corresponding file (default: True for all paths)')
parser.add_argument('--m200-prefer', choices=('hdf5, csv'), default='hdf5',
	help='which M_200 data source to prefer (default: %(default)s)')
parser.add_argument('--equal-time', action='store_true',
	help='whether all snapshots correspond to the same time (redshift)')
parser.add_argument('--mass-bins', type=mass_bin, nargs='+', default=[
		(2e11, 1e12),
		(2e10, 1e11),
		(2e09, 1e10),
	],
	help='the mass bins (units: solar masses) to sort the halos into for '
		'stacking (default: %(default)s')
parser.add_argument('--r-bins', type=int, default=100,
	help='the number of bins to use for the distance from the halo center '
		'in the plots, rebinning the data if necessary (default: %(default)d)')
parser.add_argument('--halo-id-offset', type=int, default=0,
	help='do not process halos with IDs smaller than the given ID '
		'(default: %(default)d)')
parser.add_argument('--fof-group-min-len', type=int, default=8,
	help='minimum FoF length to be included (default: %(default)d)')
parser.add_argument('--only-ids', type=int, nargs='+',
	help='only include halos with the given IDs for the corresponding file '
		'(assumes file name <path>_keep.npy with a boolean array of which '
		'halos to keep; e.g. to discard overlapping halos as found by '
		'find-duplicate-halos.py; default: false)')
parser.add_argument('--only-bound', type=int, nargs='+',
	help='only calculate for gravitationally bound halos for the corresponding '
		'file (assumes file name <path>_halo_energy.hdf5; default: false)')
parser.add_argument('--max-virial-ratio', type=float, nargs='+',
	help='only consider halos with a virial ratio less than the given value '
		'(assumes file name <path>_halo_energy.hdf5; default: no bound)')
parser.add_argument('--no-fits', action='store_true',
	help='disable NFW and soliton fits')
parser.add_argument('--no-fof', action='store_true',
	help='plot profiles without reading FOF files (in this case, the given '
		'paths are snapshot instead of FOF catalogs)')
parser.add_argument('--plot-individual', type=int, nargs='+',
	help='plot profiles of all halos individually (in addition to creating '
		'stacked with --mass-bins)')
parser.add_argument('--plot-individual-bins', type=mass_bin, nargs='+',
	default=[(0, numpy.inf)],
	help='mass bins to use for --plot-individual (analogous to --mass-bins for '
		'stacked profiles like with --mass-bins)')
parser.add_argument('--plot-individual-mark-percentile', type=float, nargs='+',
	help='highlight the given mass-percentile halos for --plot-individual')
parser.add_argument('--plot-individual-mark-line-styles', nargs='+',
	help='line styles for individual marked lines (see --line-styles)')
parser.add_argument('--hmf-units', choices=('rho_m', 'Ms/kpc3'),
	default='rho_m',
	help='units to use for the HMF (vertical axis; default: %(default)s)')
parser.add_argument('--title', help='a title for the plot')
parser.add_argument('--add-label',
	help='adds a label in the lower left corner of the plot')
parser.add_argument('--xlim', nargs=2, type=float, default=(1e0, 2e3),
	help='the radius range to plot (default: %(default)s)')
parser.add_argument('--ylim', nargs=2, type=float,
	help='the density range to plot')
parser.add_argument('--line-styles', nargs='+',
	help='line styles for the given plots')
parser.add_argument('--labels', nargs='+',
	help='legend labels for stacked lines (used instead of automatic legend)')
parser.add_argument('--legend-entries', nargs='+',
	help='legend line colors/styles (used instead of automatic legend); '
		'number of values determines number of legend entries (must be used '
		'with --labels)')
parser.add_argument('--no-legend', action='store_true',
	help='omit plot legend')
parser.add_argument('--plot-format', default='png',
	help='the file format of the generated plot (default: %(default)s)')
parser.add_argument('--dpi', type=int, default=200,
	help='the resolution of the generated plot (default: %(default)d)')
parser.add_argument('--output-file-prefix', default='',
	help='prefix for the output file names')
parser.add_argument('--plot-file-name', help='override output file name')
parser.add_argument('--halo-profiles-paths', type=Path, nargs='+',
	help='non-standard paths to the halo profile data corresponding to the '
		'given paths')
parser.add_argument('path', type=Path, nargs='+',
	help='the path to the fof_tab catalog; any normal files are interpreted '
		'as single files, while all files in any given directory are '
		'interpreted as a single multi-file output')
args = parser.parse_args()

profile_gidx = 0
profile_gidx_mark = 0


#TODO: de-duplicate these formatting functions
# Note: This is different from the function in plot_power_spectra.py
def float_to_scientific_notation(x, digits=2, integer=False):
	sign = '-' if x < 0 else ''
	if numpy.isinf(x):
		return fr'{sign}\infty'
	if x == 0:
		return '0'
	significand, exponent = units_util.split_num_exp(x)
	if integer and exponent == 0:
		return fr'{sign}{round(significand)}'
	elif numpy.isclose(significand, 1):
		return fr'{sign} 10^{{{exponent}}}'
	else:
		return fr'{sign}{significand:.{digits}f} \times 10^{{{exponent}}}'

def format_z(z):
	return f'{round(z, 2):.2f}'

def rebin_bin_avg(arr, weight):
	total = numpy.sum(weight)
	return numpy.sum(arr * weight) / total if total > 0 else 0

# bin_count argument is irrelevant for element_type == 'particles'
def rebin(r_bins, rho, bin_count, num_bins_target, element_type):
	bin_volumes = 4/3 * numpy.pi * (r_bins[1:]**3 - r_bins[:-1]**3)
	assert r_bins[0] == 0
	r = r_bins[1:]
	r_bin_centers = numpy.zeros_like(bin_volumes)
	r_bin_centers[0] = (r_bins[1] - r_bins[0]) / 2
	r_bin_centers[1:] = numpy.exp((numpy.log(r[:-1]) + numpy.log(r[1:])) / 2)
	r_min = numpy.min(r_bin_centers[1:])
	r_max = numpy.max(r_bin_centers[1:])
	min_dlog_r = (numpy.log10(r_max) - numpy.log10(r_min)) / num_bins_target
	r_list = []
	rho_list = []
	volumes_list = []
	count_list = []
	ind = [0]
	assert len(r_bin_centers) == len(rho) == len(bin_volumes)
	assert bin_count is None or len(bin_count) == len(rho)
	for istart in range(1, len(r_bin_centers) + 1):
		r_ind = r_bin_centers[ind]
		volume = bin_volumes[ind]
		if bin_count is not None:
			count = bin_count[ind]
		dlog_r = numpy.log10(numpy.max(r_ind)) - numpy.log10(numpy.min(r_ind))
		# continue combining bins until logarithmic bin width reaches
		# min_dlog_r
		# exception: keep sparsely-spaced bins in the inner region
		#            (< 3 * r_min) intact
		if dlog_r >= min_dlog_r or (
			element_type == 'grid' and numpy.max(r_ind) < 3 * r_min
		):
			if element_type == 'grid':
				r_avg = numpy.exp(rebin_bin_avg(numpy.log(r_ind), count))
				rho_avg = rebin_bin_avg(rho[ind], count)
			elif element_type == 'particles':
				r_avg = numpy.exp(numpy.mean(numpy.log(r_ind)))
				rho_avg = rebin_bin_avg(rho[ind], volume)
			else: assert False
			if rho_avg > 0:
				r_list.append(r_avg)
				rho_list.append(rho_avg)
				volumes_list.append(numpy.sum(volume))
				if bin_count is not None:
					count_list.append(numpy.sum(count))
			ind = [istart]
		else:
			ind.append(istart)
	volumes = numpy.array(volumes_list)
	counts = numpy.array(count_list)
	profile = numpy.array(rho_list)
	if element_type == 'grid':
		profile_sphere = numpy.cumsum(profile * counts) / numpy.cumsum(counts)
	elif element_type == 'particles':
		profile_sphere = (
			numpy.cumsum(profile * volumes) / numpy.cumsum(volumes)
		)
	else: assert False
	return numpy.array(r_list), profile, profile_sphere


def main():
	assert args.r_bins > 1
	if args.halo_profiles_paths:
		assert len(args.path) == len(args.halo_profiles_paths)
		# TODO: fix case of duplicates in args.path
		args.halo_profiles_paths = {
			fof_path: profiles_path for fof_path, profiles_path in
				zip(args.path, args.halo_profiles_paths)
		}
	assert not args.labels or (
		args.legend_entries and len(args.labels) == len(args.legend_entries)
	)
	# plot setup
	if args.title:
		plt.title(args.title)
	plt.xlabel(r'$r$ / $h^{-1}$ kpc (comoving)')
	if args.hmf_units == 'rho_m':
		plt.ylabel(r'$\rho / \langle \rho \rangle$')
	elif args.hmf_units == 'Ms/kpc3':
		plt.ylabel(r'$\rho$ / $(h^2 M_\odot/\mathrm{kpc}^3)$')
	else: assert False
	plt.xscale('log')
	plt.yscale('log')
	# process paths
	if not args.m200:
		args.m200 = [not args.no_fof] * len(args.path)
	if not args.plot_individual:
		args.plot_individual = [False] * len(args.path)
	if not args.only_ids:
		args.only_ids = [False] * len(args.path)
	if not args.only_bound:
		args.only_bound = [False] * len(args.path)
	if not args.max_virial_ratio:
		args.max_virial_ratio = [numpy.inf] * len(args.path)
	z_check = None
	individual_bins_list = [
		args.plot_individual_bins if x else None for x in args.plot_individual
	]
	for path_args in zip(
		args.path, args.m200, individual_bins_list, args.only_ids,
		args.only_bound, args.max_virial_ratio
	):
		z_check, plot_param = process_path(*path_args, z_check)
	# finalize plotting
	if plot_param:
		aux_legend_entries = []
		if plot_param['SOLITON_LABEL']:
			aux_legend_entries.append(matplotlib.lines.Line2D(
				[], [], color='k', linestyle='-.', linewidth=0.5,
				label='Soliton fit' + (
					r'\providecommand{\plothooksoliton}{\phantom{ (eq. (00))}}\plothooksoliton'
					if args.plot_format == 'pgf' else ''
				)
			))
		if plot_param['NFW_LABEL']:
			aux_legend_entries.append(matplotlib.lines.Line2D(
				[], [], color='k', linestyle='--', linewidth=0.5,
				label='NFW fit' + (
					r'\providecommand{\plothookNFW}{\phantom{ (eq. (00))}}\plothookNFW'
					if args.plot_format == 'pgf' else ''
				)
			))
		if args.xlim and plot_param['r0'] > min(args.xlim):
			vline_resolution = plt.axvline(
				plot_param['r0'], c='gray', ls=':', linewidth=0.4
			)
			vline_resolution_legend = matplotlib.lines.Line2D([], [])
			vline_resolution_legend.update_from(vline_resolution)
			vline_resolution_legend.set_label(plot_param['resolution_label'])
			aux_legend_entries.append(vline_resolution_legend)
		if aux_legend_entries and not args.no_legend:
			aux_legend = plt.legend(
				handles=aux_legend_entries,
				bbox_to_anchor=(0.55, 1.0), loc='upper right'
			)
			plt.gca().add_artist(aux_legend)
	if args.add_label:
		plt.gca().add_artist(matplotlib.offsetbox.AnchoredText(
			args.add_label, loc='lower left'
		))
	plt.xlim(*args.xlim)
	if args.ylim:
		plt.ylim(*args.ylim)
	if args.legend_entries:
		for style, label in zip(args.legend_entries, args.labels):
			plt.plot([], style, label=label)
	if not args.no_legend:
		plt.legend()
	# save plot
	savefig_opts = {}
	if args.plot_format != 'pdf':
		savefig_opts['dpi'] = args.dpi
	plot_file_name = 'halo_profiles'
	if args.plot_file_name:
		plot_file_name = args.plot_file_name
	plt.savefig(
		f'{args.output_file_prefix}{plot_file_name}.{args.plot_format}',
		transparent=True, **savefig_opts
	)


def process_path(
	path, use_m200, individual_bins, only_ids, only_bound, max_virial_ratio,
	z_check
):
	print(path)
	if hasattr(args, 'mask'):
		del args.mask
	groups = None
	if not args.no_fof:
		fof_tab_files = read_snapshot.snapshot_files(path)
		# read fof_tab data
		fof_tab_data = read_hdf5.read_fof_tab(fof_tab_files, read_ids=False)
		[fof_config, fof_param, fof_header], groups, _ = fof_tab_data
		print(f'Processing fof_tab with {len(groups)} groups')
		if len(groups) == 0:
			print('fof_tab contains no groups')
			return z_check, None
	mass_bins_stacked = bin_density.create_mass_bins(args.mass_bins)
	print(
		'Mass bins (stacked):',
		', '.join(f'({low:g}, {high:g})'
		for low, high, *_ in mass_bins_stacked.itertuples(index=False))
	)
	modes = [('stacked', mass_bins_stacked)]
	#TODO
	if args.mass_bins == [(0, 0)]:
		modes = []
	if individual_bins:
		mass_bins_individual = bin_density.create_mass_bins(individual_bins)
		print(
			'Mass bins (individual):',
			', '.join(f'({low:g}, {high:g})'
			for low, high, *_ in mass_bins_individual.itertuples(index=False))
		)
		modes = [('individual', mass_bins_individual)] + modes
	# read snapshot metadata
	print('Reading snapshot metadata…')
	snap = Snapshot(
		path if args.no_fof else
		read_snapshot.output_path(path, fof_config, fof_param)
	)
	units = units_util.get_units(snap.header)
	box_size_kpc = snap.header['BoxSize'] * units.length_in_kpc
	cell_len_kpc = box_size_kpc / snap.pmgrid
	snaptime = snap.header['Time']
	z = 1 / snaptime - 1
	if z_check is None:
		z_check = z
	elif args.equal_time:
		assert numpy.isclose(z, z_check, atol=1e-9, rtol=1e-2), (z_check, z)
	print(f'\tRedshift: z = {z}')
	h = snap.hubble_param
	rho_avg_ms_kpc = (
		snap.header['Omega0'] * snap.constant_in_internal_units(
			'critical_density0', return_qty=True
		).to_value(f'{h**2} * Msun / kpc**3')
	)
	if snap.get_type(args.prefer_type) == 'particles':
		numpart_total = read_snapshot.numpart_total(snap.header)
		particle_mass_ms = (
			snap.header['MassTable'][PARTICLE_TYPE] * units.mass_in_solar_mass
		)
		if particle_mass_ms == 0:
			particle_masses = snap.read_snapshot(
				f'/PartType{PARTICLE_TYPE}/Masses'
			)
			particle_masses = particle_masses[particle_masses > 0]
			numpart_total = len(particle_masses)
			assert numpy.all(particle_masses == particle_masses[0])
			particle_mass_ms = particle_masses[0] * units.mass_in_solar_mass
			del particle_masses
		# calculate average density (assumes fixed particle mass)
		rho_avg_particles_ms_kpc = (
			particle_mass_ms * numpart_total / box_size_kpc**3
		)
		assert not snap.param['ComovingIntegrationOn'] or numpy.isclose(
			rho_avg_particles_ms_kpc, rho_avg_ms_kpc, atol=0, rtol=1e-3
		), (rho_avg_particles_ms_kpc, rho_avg_ms_kpc, numpart_total, particle_mass_ms)
	print(f'\t〈ρ〉 = {rho_avg_ms_kpc} h² M☉/kpc³')
	# determine which halos are contained in which mass bin, enforcing
	# options such as minimum FoF lengths
	m200_data = (
		m200_util.read_m200_data(
			path, fof_data=fof_tab_data, prefer=args.m200_prefer
		)
		if use_m200 else None
	)
	load_energies = only_bound or max_virial_ratio < numpy.inf
	if only_ids or load_energies:
		args.mask = numpy.ones_like(groups, dtype=bool)
	if only_ids:
		keep = numpy.load(path.with_name(f'{path.name}_keep.npy'))
		assert len(keep) == len(args.mask)
		args.mask &= keep
	if load_energies:
		if numpy.any(numpy.isnan(groups.kinegy)):
			with h5py.File(
				path.with_name(f'{path.name}_halo_energy.hdf5'), 'r'
			) as f:
				energy_kin = f['KineticEnergy'][...] + f['QuantumEnergy'][...]
		else:
			energy_kin = groups.kinegy + groups.gradegy
		energy_pot = groups.potegy
		assert energy_kin.shape == energy_pot.shape
		energy_tot = energy_kin + energy_pot
	if only_bound:
		args.mask &= energy_tot < 0
	if max_virial_ratio < numpy.inf:
		virial_ratio = -2 * energy_kin / energy_pot
		args.mask &= virial_ratio < max_virial_ratio
	# plot resolution indicators
	if snap.get_type(args.prefer_type) == 'grid':
		r0 = cell_len_kpc
		resolution_label = 'FDM grid resolution'
	elif snap.get_type(args.prefer_type) == 'particles':
		r0 = fof_param[
			f'SofteningComovingType{fof_param["SofteningTypeOfPartType1"]}'
		]
		resolution_label = 'Softening length'
	else: assert False
	# process mass bins for both modes
	NFW_LABEL = False
	SOLITON_LABEL = False
	for mode, mass_bins in modes:
		print(f'Plotting {mode} halo profiles…')
		bin_halos = None
		if not args.no_fof:
			bin_halos = bin_density.find_bin_halos(
				mass_bins, groups, m200_data, units, use_m200, args
			)
			num_halos_tot = mass_bins['num_halos'].sum()
			assert num_halos_tot == sum(len(b.halos) for b in bin_halos)
			assert (
				not use_m200 or
				num_halos_tot == sum(len(b.m200_ms) for b in bin_halos)
			)
			# print info about mass bins
			for bin_idx, bin_low, bin_high, *_ in mass_bins.itertuples():
				print(
					f'Mass bin ({bin_low:g}, {bin_high:g}) M☉/h: containing '
					f'{mass_bins.at[bin_idx, "num_halos_unfiltered"]} halos '
					f'(processing {mass_bins.at[bin_idx, "num_halos"]}, '
					f'min. length = {args.fof_group_min_len})'
				)
				if len(bin_halos[bin_idx].ids) <= 5 and use_m200:
					for halo_id in bin_halos[bin_idx].ids:
						halo_pos = numpy.array(m200_data.loc[
							halo_id, ['cx_kpc', 'cy_kpc', 'cz_kpc']
						])
						halo_m200 = m200_data.loc[halo_id, 'm200_ms']
						halo_r200 = m200_data.loc[halo_id, 'r200_kpc']
						print(
							f'\tHalo ID {halo_id}: Position {halo_pos} '
							f'({halo_pos / cell_len_kpc - 0.5}),\n'
							f'\t\tM_200 = {halo_m200:e}, R_200 = {halo_r200}'
						)
		# load density profiles from file and calculate mean density profiles
		# for each mass bin
		profile_data = process_halo_profiles(
			snap, path, groups, mode, mass_bins, bin_halos
		)
		mark_indices = []
		if mode == 'individual' and args.plot_individual_mark_percentile:
			assert sum(len(b.ids) for b in bin_halos) == len(profile_data)
			line_mark = None
			bin_halo_masses = numpy.concatenate([b.m200_ms for b in bin_halos])
			sortind = numpy.argsort(bin_halo_masses)
			sortind_inv = numpy.zeros_like(sortind)
			sortind_inv[sortind] = numpy.arange(len(sortind))
			bin_halo_masses_sorted = bin_halo_masses[sortind]
			percentiles = numpy.percentile(
				bin_halo_masses_sorted, args.plot_individual_mark_percentile
			)
			sorted_indices = numpy.searchsorted(
				bin_halo_masses_sorted, percentiles
			)
			mark_indices = sortind_inv[sorted_indices]
		# plot density profiles
		r_bins_fit = numpy.geomspace(
			args.xlim[0] * FIT_XLIM_FACTORS[0],
			args.xlim[1] * FIT_XLIM_FACTORS[1], FIT_R_BINS
		)
		global profile_gidx, profile_gidx_mark
		for profile_idx, [
			mass_bin, r_bins, bin_profile, bin_profile_sphere
		] in enumerate(profile_data):
			assert not numpy.any(numpy.isnan(bin_profile))
			m_symbol = 'M_{200}' if use_m200 else 'M_{\mathrm{FoF}}'
			plot_mask = bin_profile > 0
			if args.hmf_units == 'rho_m':
				bin_profile_rel = bin_profile / rho_avg_ms_kpc
			elif args.hmf_units == 'Ms/kpc3':
				bin_profile_rel = bin_profile
			else: assert False
			line_style = (
				args.line_styles[profile_gidx] if args.line_styles else '-'
			)
			profile_gidx += 1
			# TODO: doesn’t work (default Axes colors are set on creation!)
			if False: #if mode == 'individual':
				plot_context = plt.style.context('grayscale')
			else:
				plot_context = contextlib.nullcontext()
			with plot_context:
				plot_opt = dict(linewidth=0.4)
				if mode == 'individual':
					plot_opt['color'] = 'gray'
					plot_opt['linewidth'] = 0.2
					plot_opt['alpha'] = 0.75
				# for stacked:
				# plot outskirts (outside virial radius) with thin lines
				line, = plt.plot(
					r_bins[plot_mask], bin_profile_rel[plot_mask], line_style,
					**plot_opt
				)
				# for individual: optionally mark some lines
				if mode == 'individual' and profile_idx in mark_indices:
					line_style = (
						args.plot_individual_mark_line_styles[profile_gidx_mark]
						if args.plot_individual_mark_line_styles else '-'
					)
					plot_opt = dict(linewidth=0.4)
					if line_mark and line_style != '-':
						plot_opt['color'] = line_mark.get_color()
					line, = plt.plot(
						r_bins[plot_mask], bin_profile_rel[plot_mask],
						line_style, **plot_opt
					)
					line_mark = line
					profile_gidx_mark += 1
				label = ''
				if not args.labels and mode == 'stacked':
					mbl, mbh = mass_bin.low, mass_bin.high
#					label = (
#						f'${float_to_scientific_notation(mbl, digits=0)} '
#						fr'\,h^{{-1}}\,\mathrm{{M}}_\odot \leq {m_symbol} < '
#						f'{float_to_scientific_notation(mbh, digits=0)} '
#						r'\,h^{{-1}}\,\mathrm{{M}}_\odot$'
#					)
					label = (
						'$'
#						fr'{m_symbol} \in'
						f'[{float_to_scientific_notation(mbl, digits=0)}, '
						f'{float_to_scientific_notation(mbh, digits=0)}] '
						fr'\,h^{{-1}}\,\mathrm{{M}}_\odot$'
					)
					label_suffix = []
					if not args.equal_time:
						label_suffix.append(fr'$z = {format_z(z)}$')
					if mass_bin.num_halos > 0:
						label_suffix.append(
							fr'''$\# = {
								float_to_scientific_notation(
									mass_bin.num_halos, digits=1, integer=True
								)
							}$'''
						)
					if label_suffix:
						label += '\n'
						label += r'\quad' if args.plot_format == 'pgf' else '  '
						label += f'({", ".join(label_suffix)})'
				# plot inner halos (inside virial radius, but above resolution
				# limit) with thick lines
				overdense, = numpy.nonzero(
					bin_profile_sphere >=
					m200_util.VIRIAL_OVERDENSITY_FACTOR * rho_avg_ms_kpc
				)
				r200_bin = len(overdense)
				assert numpy.all(overdense == numpy.arange(r200_bin))
				assert r200_bin < len(r_bins)
				r200 = r_bins[r200_bin]
				plot_mask_inner = plot_mask & (r_bins <= r200)
				plot_opt = dict(
					color=line.get_color(), label=label,
				)
				if not args.no_fits:
					plot_opt['linewidth'] = 2
				if mode != 'individual' or profile_idx in mark_indices:
					with warnings.catch_warnings():
						warnings.simplefilter('ignore')
						plt.plot(
							r_bins[plot_mask_inner],
							bin_profile_rel[plot_mask_inner], line_style,
							**plot_opt
						)
			if (
				args.no_fits or
				(mode == 'individual' and profile_idx not in mark_indices)
			):
				continue
			# fit to soliton profile in log space
			# arXiv:1406.6586, arXiv:1407.7762
			if 'FDM_PSEUDOSPECTRAL' in snap.config:
				# get parameters
				a = snap.header['Time']
				m_ev = snap.param['AxionMassEv']
				h = snap.hubble_param
				#TODO
				p = str(path)
				if not (
					'_cm_' in p or '_correctmass_' in p or
					'_axionic_' in p
				):
					if m_ev in (1e-22, 5e-23, 2.5e-23) or '_wm_' in p:
						m_ev *= snap.hubble_param
						print(
							'WARNING: Assuming incorrect axion mass, '
							f'correcting by h (m = {m_ev})'
						)
				rc_kpc = None
				# TODO: factors of h for arguments to expected_soliton_radius_kpc?
				rc_guess_kpc = h * expected_soliton_radius_kpc(
					m200_util.VIRIAL_OVERDENSITY_FACTOR * rho_avg_ms_kpc *
						4/3 * numpy.pi * r200**3,
					a, m_ev
				)
				r_soliton_max = 3 * rc_guess_kpc
				print('rc_bins[0] =', r_bins[0], 'kpc/h')
				print('rc_guess_kpc =', rc_guess_kpc, 'kpc/h')
				print('r_soliton_max =', r_soliton_max, 'kpc/h')
				# potentially do fit
				plot_mask_soliton = plot_mask & (r_bins <= r_soliton_max)
				if (
					r_soliton_max > r_bins[0] and
					numpy.sum(plot_mask_soliton) <= 3
				):
					plot_mask_soliton = plot_mask & (
						numpy.cumsum(plot_mask) < 5
					)
				if numpy.sum(plot_mask_soliton) > 3:
					soliton_fit_log = lambda r_kpc, rc_kpc: numpy.log(
						rho_soliton(r_kpc, a, m_ev, rc_kpc)
					)
					fit_param, _ = scipy.optimize.curve_fit(
						soliton_fit_log, r_bins[plot_mask_soliton] / h,
						numpy.log(bin_profile[plot_mask_soliton] * h**2),
						bounds=(1e-100, box_size_kpc / h),
						p0=(rc_guess_kpc / h,)
					)
					rho0 = numpy.inf

# 					soliton_fit_log = lambda r_kpc, rc_kpc, rho0: numpy.log(
# 						rho_soliton(r_kpc, a, m_ev, rc_kpc, rho0)
# 					)
# 					fit_param, _ = scipy.optimize.curve_fit(
# 						soliton_fit_log, r_bins[plot_mask_soliton] / h,
# 						numpy.log(rho_bin_shell[plot_mask_soliton] * h**2),
# 						bounds=(1e-100, numpy.inf)
# 					)
# 					rho0 = fit_param[1] / h**2 / rho_avg_ms_kpc
# 					print0('rho0 =', rho0)

# 					soliton_fit = lambda r_kpc, rc_kpc: (
# 						rho_soliton(r_kpc, a, m_ev, rc_kpc)
# 					)
# 					fit_param, _ = scipy.optimize.curve_fit(
# 						soliton_fit, r_bins[plot_mask_soliton] / h,
# 						rho_bin_shell[plot_mask_soliton] * h**2,
# 						bounds=(1e-100, box_size_kpc / h),
# 						p0=(rc_guess_kpc,)
# 					)
# 					print(
# 						'rho0 = ',
# 						rho0_soliton_ms_kpc(a, m_ev, *fit_param) / h**2 /
# 						rho_avg_ms_kpc
# 					)

					print('rc =', fit_param[0] * h, 'kpc/h')
					rho0_schive = (
						rho0_soliton_ms_kpc(a, m_ev, fit_param[0]) / h**2 /
						rho_avg_ms_kpc
					)
					print(f'rho0 (Schive) = {rho0_schive} ({rho0_schive/rho0})')
					rc_kpc = fit_param[0] * h
					plt.plot(
						r_bins_fit, rho_soliton(
							r_bins_fit / h, a, m_ev, rc_kpc / h
						) / h**2 / rho_avg_ms_kpc,
						'k-.', linewidth=0.5
					)
					SOLITON_LABEL = True
				# exclude core from NFW fit for FDM case
				if rc_kpc is None:
					plot_mask_inner &= r_bins > 5 * r_bins[0]
				else:
					plot_mask_inner &= r_bins > 3 * rc_kpc
			# fit to NFW profile in log space (see arXiv:0706.2919)
			if numpy.sum(plot_mask_inner) > 2:
				rho_fit_log = lambda r, rho_0, R_s: numpy.log(
					rho_nfw(r, rho_0, R_s)
				)
				fit_param, _ = scipy.optimize.curve_fit(
					rho_fit_log, r_bins[plot_mask_inner],
					numpy.log(bin_profile[plot_mask_inner]),
					bounds=(1e-100, numpy.inf)
				)
				print(f'NFW parameters: {fit_param}')
				plt.plot(
					r_bins_fit,
					rho_nfw(r_bins_fit, *fit_param) / rho_avg_ms_kpc,
					'k--', linewidth=0.5
				)
				NFW_LABEL = True
	return z_check, {
		'NFW_LABEL': NFW_LABEL, 'SOLITON_LABEL': SOLITON_LABEL, 'r0': r0,
		'resolution_label': resolution_label,
	}

def process_halo_profiles(snap, fof_path, groups, mode, mass_bins, bin_halos):
	result_profiles = []
	if args.halo_profiles_paths:
		profiles_path = args.halo_profiles_paths[fof_path]
	else:
		profiles_path = fof_path.with_name(
			f'{fof_path.name}_halo_profiles.hdf5'
		)
	print(f'Opening halo profiles file “{profiles_path}”')
	with h5py.File(profiles_path, 'r') as pf:
		assert len(pf['r_bins_kpc']) == pf['rho_ms_kpc3'].shape[-1] + 1
		assert pf['rho_ms_kpc3'].shape == pf['count'].shape
		if args.no_fof:
			assert len(mass_bins) == 1
			mass_bins.loc[0, 'num_halos'] = len(pf['rho_ms_kpc3'])
		else:
			assert len(pf['rho_ms_kpc3']) == len(groups)
		if args.no_fof or len(groups) < 10_000:  #TODO
			# TODO: make this check more memory-efficient
			halos_done = numpy.any(pf['count'][...] > 0, axis=1)
			print(
				f'\t{numpy.sum(halos_done)} out of {len(pf["rho_ms_kpc3"])} '
				'halo profiles computed in total'
			)
			del halos_done
		r_bins = pf['r_bins_kpc'][...]
		for mass_bin in mass_bins.itertuples():
			# TODO: do chunk iterations to limit peak memory
			bin_idx, bin_low, bin_high, num_halos, *_ = mass_bin
			bin_ids = (
				... if args.no_fof else numpy.array(bin_halos[bin_idx].ids)
			)
			bin_profiles = pf['rho_ms_kpc3'][bin_ids]
			bin_counts = pf['count'][bin_ids]
			#TODO: use same check as above
			done_mask = numpy.isfinite(bin_profiles[:, -1])
			bin_profiles = bin_profiles[done_mask]
			bin_counts = bin_counts[done_mask]
			print(
				f'\tMass bin ({bin_low:g}, {bin_high:g}) M☉/h: '
				f'{len(bin_profiles)} halo profiles present out of '
				f'{num_halos} to process'
			)
			assert len(bin_profiles) > 0
			assert bin_profiles.shape[-1] == len(r_bins) - 1
			bin_profiles[numpy.isnan(bin_profiles)] = 0
			counts = bin_counts[0, :]
			if snap.get_type(args.prefer_type) == 'grid':
				assert numpy.all(bin_counts == counts)
			elif snap.get_type(args.prefer_type) == 'particles':
				counts = None
			else: assert False
			if mode == 'individual':
				# use individual profiles
				profiles = bin_profiles
			elif mode == 'stacked':
				# stack profiles
				stacked_profile = numpy.mean(bin_profiles, axis=0)
				profiles = [stacked_profile]
			else: assert False
			for i in range(len(profiles)):
				result_r_bins, result_profile, result_profile_sphere = rebin(
					r_bins, profiles[i], counts, args.r_bins,
					snap.get_type(args.prefer_type)
				)
				assert numpy.all(numpy.isfinite(result_profile))
				assert numpy.all(numpy.isfinite(result_profile_sphere))
				result_profiles.append((
					mass_bin, result_r_bins, result_profile,
					result_profile_sphere
				))
	return result_profiles

def rho_nfw(r, rho_0, R_s):
	return rho_0 / (r / R_s * (1 + r / R_s)**2)

def rho_einasto(r, rho_0, A, alpha):
	return rho_0 * numpy.exp(-A * r**alpha)

# result is in M☉/kpc³ (no h!)
def rho0_soliton_ms_kpc(a, m_ev, rc_kpc):
	return 1.9e9 / a * (m_ev / 1e-23)**-2 * rc_kpc**-4

def rho_soliton(r_kpc, a, m_ev, rc_kpc):
	rho0 = rho0_soliton_ms_kpc(a, m_ev, rc_kpc)
	return rho0 / (1 + 0.091 * (r_kpc / rc_kpc)**2)**8

# core–halo mass relation, see arXiv:1407.7762
# result is in kpc (no h!)
def expected_soliton_radius_kpc(M_halo_ms, a, m_ev):
	# approximate ζ(z) ≈ const. = 200
	return 1.6 / (m_ev / 1e-22) * numpy.sqrt(a) * (1e9 / M_halo_ms)**(1/3)


if __name__ == '__main__':
	main()
