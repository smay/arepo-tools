#!/usr/bin/env python3
import argparse
import re
import subprocess
from pathlib import Path
import h5py
import numba
import numpy
import numpy.fft
import matplotlib.pyplot as plt
import scipy.optimize
from read_snapshot.read_psi import read_psi

parser = argparse.ArgumentParser(description='Check linear growth of the '
	'density contrast')
parser.add_argument('--method',
	choices=('fft', 'max'), default='max',
	help='the method to use to determine the spectral power')
parser.add_argument('--snapshot-format',
	choices=('gadget', 'hdf5', 'npy', 'raw'), default='hdf5',
	help='the output snapshot format (default: hdf5)')
parser.add_argument('--time-digits', type=int, default=4,
	help='how many decimal digits to use for the time in the file name and '
		'plot title (default: 4)')
parser.add_argument('path', nargs='+', type=Path,
	help='the paths to the snapshot files; any normal files are interpreted '
		'as single-file snapshots, while all files in any given directory '
		'are interpreted as a single multi-file snapshot')
args = parser.parse_args()

MPC_IN_CM = 3.085678e24

a_values = []
delta_k_values = []
delta_k_values_m = []
delta_k_values_p = []

for path in args.path:
	print(path)
	if path.is_dir():
		snap_files = sorted(path.iterdir())
	else:
		snap_files = [path]
	# read wave function and meta data from snapshot
	meta, psi = read_psi(snap_files, args.snapshot_format)
	config, param, header = meta
	PMGRID = int(config['PMGRID'])
	time = None
	if header is not None:
		time = header['Time']
		box_size = header['BoxSize']
		box_size_Mpc = box_size * header['UnitLength_in_cm'] / MPC_IN_CM
	# try to parse time for formats without header information
	if time is None:
		time = re.match(
			args.time_from_filename_format, path.name
		).group(1)
	# determine power
	a_values.append(time)
	rho = numpy.abs(psi)**2
	rho_avg = numpy.mean(rho)
	print(f'rho_avg = {rho_avg}')
	delta = rho / rho_avg - 1
	# TODO: don’t hard-code mode
	if args.method == 'fft':
		ylabel = r'$|\delta_k|^2$'
		delta_k = numpy.fft.fftn(delta)
		delta_k_values.append(numpy.abs(delta_k[0, PMGRID//5, 0])**2)
		delta_k_values_m.append(numpy.abs(delta_k[0, PMGRID//5 - 1, 0])**2)
		delta_k_values_p.append(numpy.abs(delta_k[0, PMGRID//5 + 1, 0])**2)
	elif args.method == 'max':
		ylabel = r'$\max(\delta)$'
		power = numpy.max(delta)
		delta_k_values.append(power)
		# plot snapshot
		delta_slice = delta[PMGRID//2, :, PMGRID//2]
		plt.figure()
		plt.xlabel(r'$y$ / Mpc')
		plt.ylabel(r'$\delta$')
		X = numpy.linspace(0, box_size_Mpc, PMGRID)
		plt.plot(X, delta_slice, '.-')
		plt.xlim(numpy.min(X), numpy.max(X))
		plt.savefig(
			f'linear_growth_slice_{time:.{args.time_digits}f}.png',
			dpi=200, bbox_inches='tight')
		plt.close()
	else:
		raise AssertionError

plt.figure()
plt.xlabel('$a$')
plt.ylabel(ylabel)
#plt.xscale('log')
#plt.yscale('log')

def fit_func(x, a): return a * x
[a,], _ = scipy.optimize.curve_fit(fit_func, a_values, delta_k_values)

#plt.plot(a_values, delta_k_values_m, label=r'$k_y - 1$')
plt.plot(a_values, delta_k_values, '.-')
#plt.plot(a_values, delta_k_values_p, label='$k_y + 1$')

X_fit = numpy.linspace(numpy.min(a_values), numpy.max(a_values))
plt.plot(X_fit, fit_func(X_fit, a), label='fit')

plt.xlim(0, numpy.max(a_values))
if args.method == 'max':
	plt.ylim(bottom=0)
plt.legend()

plt.savefig('linear_growth.png', dpi=200, bbox_inches='tight')
plt.close()

