#!/usr/bin/env python3
"""
This module contains functions used to assign grid cells or particles to radial
bins, used by calculate_m200.py and plot_stacked_halo_profile.py.
"""
import collections
import time
import numba
import numpy
import numpy.linalg
import numpy.random
import pandas


#TODO: move wrapping functions somewhere else?
@numba.njit
def coord_periodic_wrap(box_size, x):
	while x >= box_size:
		x -= box_size
	while x < 0:
		x += box_size
	return x

@numba.njit
def coord_periodic_wrap_vec(box_size, x):
	#TODO: x.view not supported by numba
#	x_flat = x.view()
#	x_flat.shape = x_flat.size
	x_flat = x.reshape(-1)
	for i in range(len(x_flat)):
		x_flat[i] = coord_periodic_wrap(box_size, x_flat[i])
	return x

@numba.njit
def dist_periodic_wrap(box_size, x):
	while x < -box_size / 2:
		x += box_size
	while x >= box_size / 2:
		x -= box_size
	return x

@numba.njit
def dist_periodic_wrap_vec(box_size, x):
	#TODO: x.view not supported by numba
#	x_flat = x.view()
#	x_flat.shape = x_flat.size
	x_flat = x.reshape(-1)
	for i in range(len(x_flat)):
		x_flat[i] = dist_periodic_wrap(box_size, x_flat[i])
	return x


def bin_edges(num_bins, r0, r1, binfac=None, central_bin=True, spacing='log'):
	offset = int(central_bin)
	num_bins -= offset
	result_len = num_bins + 1
	result = numpy.zeros(offset + result_len, dtype='f8')
	if spacing == 'log':
		binfac_expected = num_bins / numpy.log(r1 / r0)
		if binfac is None:
			binfac = binfac_expected
		result[offset:] = r0 * numpy.exp(numpy.arange(result_len) / binfac)
	elif spacing == 'linear':
		binfac_expected = num_bins / (r1 - r0)
		if binfac is None:
			binfac = binfac_expected
		result[offset:] = r0 + numpy.arange(result_len) / binfac
	else: assert False
	assert binfac is None or numpy.isclose(binfac, binfac_expected)
	assert numpy.isclose(result[-1], r1)
	return result


# keep compatibility with an iterate_grid-like interface, but particles are
# actually entirely in memory
ParamParticles = collections.namedtuple(
	'ParamParticles',
	'center, mass, box_size, numpart_total, softening_length, '
	'r_min, r_max, binlog, central_bin'
)

# assumes that all particles have the same mass
@numba.njit
def bin_particles(pos, param, data):
	binlog = param.binlog
	central_bin = param.central_bin
	if central_bin:
		bin_center = data[:1]
		bin_data = data[1:]
	else:
		bin_data = data
	num_bins = len(bin_data)
	if param.r_min is None:
		r0 = param.softening_length if binlog else 0.
	else:
		r0 = param.r_min
	if param.r_max is None:
		r1 = param.box_size / 2
	else:
		r1 = param.r_max
	binfac = (
		num_bins / numpy.log(r1 / r0) if binlog else
			num_bins / (r1 - r0)
	)
	assert binfac > 0
	for idx in range(len(pos)):
		r_vec_unwrapped = pos[idx] - param.center
		r_vec = dist_periodic_wrap_vec(param.box_size, r_vec_unwrapped)
		r = numpy.sqrt(numpy.sum(r_vec**2))
		# cell is closer to the center than any of the bins, but
		# must be counted towards the spherical average
		if central_bin and r < r0:
			# TODO: numba error for “bin_center += 1” with bin_center dtype u8
			bin_center[0] += 1
		# cell is in one of the bins
		elif r0 <= r < r1:
			# regularly-spaced (logarithmic or linear) bins
			if binlog:
				r_bin = int(numpy.log(r / r0) * binfac)
			else:
				r_bin = int((r - r0) * binfac)
			assert 0 <= r_bin < num_bins
			# update data in r_bin
			bin_data[r_bin] += 1
	return r0, r1, binfac


ParamGrid = collections.namedtuple(
	'ParamGrid', 'center_g3, ir_min, ir_max, binlog, central_bin, mpi_rank'
)

@numba.njit
def rho_bin_cells_bulk(rho, pmgrid, ix_info, data_slice, param, data):
	assert len(param) == len(data)
	with numba.objmode(t0='f8'):
		t0 = time.perf_counter()
	for halo_idx in range(len(param)):
		with numba.objmode(t0='f8'):
			t1 = time.perf_counter()
			if param[halo_idx].mpi_rank == 0 and (
				halo_idx == 0 or t1 - t0 > 30
			):
				print(
					(
						'\tCurrently processing halo {: 4} (of {: 4}) on rank '
						'{}…'
					).format(halo_idx, len(param), param[halo_idx].mpi_rank),
					flush=True
				)
				t0 = t1
		rho_bin_cells(
			rho, pmgrid, ix_info, data_slice, param[halo_idx], data[halo_idx]
		)


@numba.njit
def rho_bin_cells(rho, pmgrid, ix_info, data_slice, param, data):
	ix_min, ix_start, ix_end = ix_info
	#TODO
	assert ix_min == 0 and rho.shape[1:] == (pmgrid, pmgrid)
	binlog = param.binlog
	central_bin = param.central_bin
	if central_bin:
		bin_center = data[:1]
		bin_data = data[1:]
	else:
		bin_data = data
	num_bins = len(bin_data)
	offset_ix, offset_iy, offset_iz = param.center_g3
	if param.ir_min is None:
		ir0 = 1. if binlog else 0.
	else:
		ir0 = param.ir_min
	if param.ir_max is None:
		ir1 = pmgrid / 2
	else:
		ir1 = param.ir_max
	binfac = (
		num_bins / numpy.log(ir1 / ir0) if binlog else
			num_bins / (ir1 - ir0)
	)
	assert binfac > 0
	for ix in range(ix_start, ix_end):
		dix = dist_periodic_wrap(pmgrid, ix - offset_ix)
		for iy in range(pmgrid):
			diy = dist_periodic_wrap(pmgrid, iy - offset_iy)
			for iz in range(pmgrid):
				diz = dist_periodic_wrap(pmgrid, iz - offset_iz)
				ir = numpy.sqrt(dix**2 + diy**2 + diz**2)
				rho_val = rho[ix - ix_start, iy, iz]
				# cell is closer to the center than any of the bins, but
				# must be counted towards the spherical average
				if central_bin and ir < ir0:
					# TODO: numba error for
					#       “bin_center['rho_sum_l'] += rho_val”
					bin_center[0]['rho_sum_l'] += rho_val
					bin_center[0]['rho_num_l'] += 1
				# cell is in one of the bins
				elif ir0 <= ir < ir1:
					# regularly-spaced (logarithmic or linear) bins
					if binlog:
						r_bin = int(numpy.log(ir / ir0) * binfac)
					else:
						r_bin = int((ir - ir0) * binfac)
					assert 0 <= r_bin < num_bins
					# update data in r_bin
					bin_data[r_bin]['rho_sum_l'] += rho_val
					bin_data[r_bin]['rho_num_l'] += 1


ParamFilament = collections.namedtuple(
	'ParamFilament',
	'filament, box_size, mpi_rank, r_min, r_max, binlog, central_bin, '
	'offsets, mass, numpart_total, softening_length',
	defaults=[None] * 8
)
assert ParamFilament._field_defaults == dict.fromkeys(
	[
		'r_min', 'r_max', 'binlog', 'central_bin', 'offsets', 'mass',
		'numpart_total', 'softening_length',
	], None
)

#TODO: offsets for grid filament profiles
@numba.njit
def rho_bin_cells_filament_bulk(rho, pmgrid, ix_info, data_slice, param, data):
	assert len(param) == len(data)
	with numba.objmode(t0='f8'):
		t0 = time.perf_counter()
	for fil_idx in range(len(param)):
		with numba.objmode(t0='f8'):
			t1 = time.perf_counter()
			if param[fil_idx].mpi_rank == 0 and (
				fil_idx == 0 or t1 - t0 > 30
			):
				print(
					(
						'\tCurrently processing filament {: 4} (of {: 4}) on '
						'rank {}…'
					).format(fil_idx, len(param), param[fil_idx].mpi_rank),
					flush=True
				)
				t0 = t1
		rho_bin_cells_filament(
			rho, pmgrid, ix_info, data_slice, param[fil_idx], data[fil_idx]
		)

@numba.njit
def segment_distance(p, p1, p2, box_size):
	# parameterize line: l = p1 + t (p2 - p1)
	dp = dist_periodic_wrap_vec(box_size, p2 - p1)
	t = (dist_periodic_wrap_vec(box_size, p - p1) @ dp) / numpy.sum(dp**2)
	if t < 0 or 1 < t:
		return numpy.nan
	intersect = p1 + t * dp
	dist_vec = dist_periodic_wrap_vec(box_size, p - intersect)
	return numpy.linalg.norm(dist_vec)

@numba.njit
def rho_bin_cells_filament(rho, pmgrid, ix_info, data_slice, param, data):
	ix_min, ix_start, ix_end = ix_info
	Nx, Ny, Nz = rho.shape
	#TODO
	assert Ny == Nz == pmgrid
	box_size = param.box_size
	cell_len = box_size / pmgrid
	filament = param.filament
	assert len(filament) > 1
	assert filament.shape[1:] == (3,)
	binlog = param.binlog
	central_bin = param.central_bin
	if central_bin:
		bin_center = data[:1]
		bin_data = data[1:]
	else:
		bin_data = data
	num_bins = len(bin_data)
	if param.r_min is None:
		r0 = cell_len if binlog else 0.
	else:
		r0 = param.r_min
	if param.r_max is None:
		r1 = box_size / 2
	else:
		r1 = param.r_max
	binfac = (
		num_bins / numpy.log(r1 / r0) if binlog else
			num_bins / (r1 - r0)
	)
	assert binfac > 0
	p = numpy.empty(3, dtype='f8')
	for ix_offset in range(Nx):
		ix = ix_start + ix_offset
		p[0] = ix * cell_len
		for iy in range(Ny):
			p[1] = iy * cell_len
			for iz in range(Nz):
				p[2] = iz * cell_len
				p1 = filament[0]
				for p2 in filament[1:]:
					r = segment_distance(p, p1, p2, box_size)
					if r < numpy.inf:				
						rho_val = rho[ix_offset, iy, iz]
						# cell is closer to the center than any of the bins, but
						# must be counted towards the spherical average
						if central_bin and r < r0:
							bin_center[0]['rho_sum_l'] += rho_val
							bin_center[0]['rho_num_l'] += 1
						# cell is in one of the bins
						elif r0 <= r < r1:
							# regularly-spaced (logarithmic or linear) bins
							if binlog:
								r_bin = int(numpy.log(r / r0) * binfac)
							else:
								r_bin = int((r - r0) * binfac)
							# apparently, this can happen due to rounding with
							# log?
							if r_bin == num_bins:
								r_bin -= 1
							assert 0 <= r_bin < num_bins
							# update data in r_bin
							bin_data[r_bin]['rho_sum_l'] += rho_val
							bin_data[r_bin]['rho_num_l'] += 1
					p1 = p2


@numba.njit
def bin_particles_filament_bulk(
	pdata, numpart_total, part_start, part_end, param, data_tuple
):
	data, info = data_tuple
	assert len(param) == len(data)
	assert len(info) == 1
	with numba.objmode(t0='f8'):
		t0 = time.perf_counter()
	for fil_idx in range(len(param)):
		with numba.objmode(t0='f8'):
			t1 = time.perf_counter()
			if param[fil_idx].mpi_rank == 0 and (
				fil_idx == 0 or t1 - t0 > 30
			):
				print(
					(
						'\tCurrently processing filament {: 4} (of {: 4}) on '
						'rank {}…'
					).format(fil_idx, len(param), param[fil_idx].mpi_rank),
					flush=True
				)
				t0 = t1
		bin_particles_filament(
			pdata, numpart_total, part_start, part_end, param[fil_idx],
			(data[fil_idx], info)
		)

# assumes that all particles have the same mass
@numba.njit
def bin_particles_filament(
	pdata, numpart_total, part_start, part_end, param, data_tuple
):
	pos = pdata.Coordinates
	assert pos.shape == (part_end - part_start, 3)
	data, info = data_tuple
	filament = param.filament
	assert len(filament) > 1
	assert filament.shape[1:] == (3,)
	offsets = param.offsets
	if offsets is not None:
		assert len(offsets) == len(filament) - 1
		assert offsets.shape[1:] == (3,)
	box_size = param.box_size
	binlog = param.binlog
	central_bin = param.central_bin
	if central_bin:
		bin_center = data[:1]
		bin_data = data[1:]
	else:
		bin_data = data
	num_bins = len(bin_data)
	if param.r_min is None:
		r0 = param.softening_length if binlog else 0.
	else:
		r0 = param.r_min
	if param.r_max is None:
		r1 = box_size / 2
	else:
		r1 = param.r_max
	binfac = (
		num_bins / numpy.log(r1 / r0) if binlog else
			num_bins / (r1 - r0)
	)
	assert binfac > 0
	info[0]['r0'] = r0
	info[0]['r1'] = r1
	info[0]['binfac'] = binfac
	for p_idx in range(len(pos)):
		for fil_pt in range(len(filament) - 1):
			o = offsets[fil_pt]
			p1 = filament[fil_pt] + o
			p2 = filament[fil_pt + 1] + o
			r = segment_distance(pos[p_idx], p1, p2, box_size)
			if r < numpy.inf:
				# cell is closer to the center than any of the bins, but
				# must be counted towards the cylindrical average
				if central_bin and r < r0:
					# TODO: numba error for “bin_center += 1” with bin_center
					#       dtype u8
					bin_center[0] += 1
				# cell is in one of the bins
				elif r0 <= r < r1:
					# regularly-spaced (logarithmic or linear) bins
					if binlog:
						r_bin = int(numpy.log(r / r0) * binfac)
					else:
						r_bin = int((r - r0) * binfac)
					assert 0 <= r_bin < num_bins
					# update data in r_bin
					bin_data[r_bin] += 1
	return r0, r1, binfac


def density_mean_division(rho_sum_g, rho_num_g):
	rho_mean = numpy.zeros_like(rho_sum_g)
	nonzero = rho_num_g > 0
	numpy.divide(rho_sum_g, rho_num_g, out=rho_mean, where=nonzero)
	return rho_mean, nonzero


def bin_to_r_linear(num_bins, r_bin, r_min, r_max):
	return (
		r_min + r_bin / num_bins * (r_max - r_min),
		r_min + (r_bin + 1) / num_bins * (r_max - r_min)
	)

# TODO: move the following to m200_util.py
BinHaloData = collections.namedtuple('BinHaloData', 'ids, halos, m200_ms')

def create_mass_bins(mass_bins_tuples):
	result = pandas.DataFrame(numpy.zeros(
		len(mass_bins_tuples), dtype=[
			('low', 'f8'), ('high', 'f8'), ('num_halos', 'u8'),
			('num_halos_unfiltered', 'u8')
		]
	))
	result[['low', 'high']] = mass_bins_tuples
	return result

def find_bin_halos(
	mass_bins, groups, m200_data, units, use_m200, param, RNG=None
):
	if use_m200:
		mass_ms = m200_data['m200_ms']
		fof_len = m200_data['fof_len']
	else:
		mass_ms = groups.tot_mass * units.mass_in_solar_mass
		fof_len = groups.tot_len
	bin_halos = []
	halo_ids = m200_data['id'].array if use_m200 else numpy.arange(len(groups))
	base_mask = (
		(halo_ids >= param.halo_id_offset) &
		(fof_len >= param.fof_group_min_len) &
		getattr(param, 'mask', True)
	)
	for bin_idx, bin_low, bin_high, *_ in mass_bins.itertuples():
		mass_mask = (bin_low <= mass_ms) & (mass_ms < bin_high)
		sample_mask = numpy.full_like(mass_mask, True)
		if RNG and param.mass_bin_sample_max > 0:
			# select random sample of mass_bin_sample_max halos
			sample_mask[...] = False
			sample_mask[:param.mass_bin_sample_max] = True
			RNG.shuffle(sample_mask)
		m = base_mask & mass_mask & sample_mask
		mass_bins.loc[bin_idx, 'num_halos_unfiltered'] = numpy.sum(mass_mask)
		mass_bins.loc[bin_idx, 'num_halos'] = numpy.sum(m)
		selected_ids = halo_ids[m]
		bin_halos.append(BinHaloData(
			ids=selected_ids,
			halos=groups[selected_ids],
			m200_ms=m200_data['m200_ms'][m].array if use_m200 else None,
		))
	return bin_halos

