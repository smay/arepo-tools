#!/usr/bin/env python3
import argparse
import re
import sys
import tempfile
from pathlib import Path
import astropy.constants
import astropy.units
import h5py
import matplotlib
import matplotlib.pyplot as plt
import numpy
import pandas
import scipy.interpolate
import scipy.optimize
import io_util
import m200_util
import read_snapshot
import read_snapshot.hdf5 as read_hdf5
import units_util
import matplotlib_settings

DELTA_C = 3/5 * (3 * numpy.pi / 2)**(2/3)  # = 1.686
M200_FILE_NAME = '{}_m200.csv'

parser = argparse.ArgumentParser(description='Plot the halo mass functions '
	'for fuzzy dark matter or cold dark matter simulation snapshots')
parser.add_argument('--hist-bins', type=int, default=100,
	help='the number of histogram bins (default: %(default)d)')
#TODO: actually check equal time!
parser.add_argument('--equal-time', action='store_true',
	help='whether all snapshots correspond to the same time (redshift)')
parser.add_argument('--with-relative', type=Path,
	help='plot mass functions relative to the given snapshot in a separate '
		'panel')
parser.add_argument('--only-relative', type=Path,
	help='plot mass functions relative to the given snapshot')
parser.add_argument('--panel-ratio', type=float, default=1/3,
	help='ratio of second (relative plot) row/panel to the first (default: '
		'%(default)g')
parser.add_argument('--relative-label', default='0',
	help='denominator subscript for the label on the y-axis of the relative '
		'plot panel')
parser.add_argument('--relative-label-suffix', default='',
	help='additional content after the y label for the relative plot panel')
parser.add_argument('--relative-label-fraction', choices=('frac', '/'),
	default='frac',
	help='notation to use for the fraction for the relative plot panel')
parser.add_argument('--relative-pos', type=int, default=0,
	help='where to insert the reference mass function')
parser.add_argument('--relative-exclude', type=int, nargs='+', default=[],
	help='don’t include the given paths in the relative plot')
parser.add_argument('--m200', type=int, nargs='+',
	help='use M_200 instead of FoF mass to bin the halos, for the '
		'corresponding file')
parser.add_argument('--m200-csv', type=int, nargs='+',
	help='force use of M_200 data from CSV file for the given files')
parser.add_argument('--min-radius', type=float,
	help='only halos with R_200 larger than this radius will be included')
parser.add_argument('--only-ids', type=int, nargs='+',
	help='only include halos with the given IDs for the corresponding file '
		'(assumes file name <path>_keep.npy with a boolean array of which '
		'halos to keep; e.g. to discard overlapping halos as found by '
		'find-duplicate-halos.py; default: false)')
parser.add_argument('--only-bound', type=int, nargs='+',
	help='only consider gravitationally bound halos for the corresponding file '
		'(assumes file name <path>_halo_energy.hdf5; default: false)')
parser.add_argument('--max-virial-ratio', type=float, nargs='+',
	help='only consider halos with a virial ratio less than the given value '
		'(assumes file name <path>_halo_energy.hdf5; default: no bound)')
parser.add_argument('--show-poisson-error', type=int, nargs='+', default=[],
	help='show statistical (Poisson) uncertainty for the given lines')
parser.add_argument('--line-type', choices=('line', 'step'), default='line',
	help='type of line to draw histograms with')
parser.add_argument('--line-styles', nargs='+',
	help='line styles for the given plots')
parser.add_argument('--mass-function-type', choices=('dn/dM', 'dn/dlnM'),
	default='dn/dM',
	help='units/representation of the mass function values (default: '
		'%(default)s)')
parser.add_argument('--legend-order', default=[], nargs='+',
	help='the order that functions/fits will show up in the legend (after '
		'lines with regular data)')
parser.add_argument('--press-schechter', choices=('cdm', 'fdm'), nargs='+',
	help='add a Press–Schechter mass function curve of the given type(s)')
parser.add_argument('--press-schechter-function',
	choices=('press-schechter', 'sheth-tormen'), nargs='+',
	default=['sheth-tormen'],
	help='type of function f(σ) to use with Press–Schechter formalism '
		'(default: %(default)s)')
parser.add_argument('--press-schechter-backend', choices=('manual', 'colossus'),
	nargs='+', default=['manual'],
	help='method used to compute the Press–Schechter mass function (default: '
		'%(default)s)')
parser.add_argument('--schive-fit', type=float,
	help='add fit by Schive et al. (2016) for FDM mass function, specifying '
		'the FDM mass')
parser.add_argument('--schive-fit-label',
	help='legend label for fit by Schive et al. (2016)')
parser.add_argument('--schive-fit-linestyle', default=':',
	help='line style for fit by Schive et al. (2016)')
parser.add_argument('--schive-fit-linewidth', type=float,
	help='line width for fit by Schive et al. (2016)')
parser.add_argument('--schive-fit-analog', type=float,
	help='add fit similar to Schive et al. (2016) (but with free parameters) '
		'for FDM mass function, specifying the FDM mass')
parser.add_argument('--schive-fit-analog-parameters', type=float, nargs=2,
	help='specify parameters to use for the function similar to Schive et al. '
		'(2016) (instead of doing a fit)')
parser.add_argument('--change-style-interval', nargs='+',
	help='list of arguments of the form (plot_item, interval_low, '
		'interval_high, style) which modifies the given item’s plot style in '
		'the given interval')
parser.add_argument('--title', help='a title for the plot')
parser.add_argument('--no-title', action='store_true',
	help='disable plot title')
parser.add_argument('--no-legend', action='store_true',
	help='disable plot legend')
parser.add_argument('--labels', nargs='+',
	help='legend labels (used instead of automatic legend)')
#parser.add_argument('--legend-entries', nargs='+',
#	help='legend line colors/styles (used instead of automatic legend); '
#		'number of values determines number of legend entries (must be used '
#		'with --labels)')
parser.add_argument('--xlim', nargs=2, type=float, default=(1e7, 1e14),
	help='the mass range to plot (default: %(default)s)')
parser.add_argument('--ylim', nargs=2, type=float,
	help='the mass function range to plot')
parser.add_argument('--ylim-relative', type=float, nargs=2,
	help='the relative mass function range to plot')
parser.add_argument('--plot-label',
	help='add a label to the plot (similar to legend)')
parser.add_argument('--plot-file-prefix', default='',
	help='prefix for the output file name')
parser.add_argument('--plot-file-name', help='override output file name')
parser.add_argument('--plot-format', default='png',
	help='the file format of the generated plot (default: %(default)s)')
parser.add_argument('--dpi', type=int, default=200,
	help='the resolution of the generated plot (default: %(default)d)')
parser.add_argument('--figaspect', type=float,
	help='the aspect ratio height/width of the figure')
parser.add_argument('path', nargs='+', type=Path,
	help='the paths to the fof_tab files; any normal files are interpreted '
		'as single files, while all files in any given directory are '
		'interpreted as a single multi-file output')
args = parser.parse_args()

if args.show_poisson_error:
	#TODO
	from matplotlib.axes._base import _process_plot_format


#TODO: de-duplicate these formatting functions
def float_to_scientific_notation(x, digits=None, integer=True):
	sign = '-' if x < 0 else ''
	exponent = int(numpy.floor(numpy.log10(numpy.abs(x))))
	significand = x / 10**exponent
	if numpy.isclose(significand, 1):
		return fr'{sign} 10^{{{exponent}}}'
	else:
		if integer and numpy.isclose(significand / round(significand), 1):
			significand = round(significand)
		if digits is None:
			significand = f'{significand}'
		else:
			significand = f'{significand:.{digits}f}'
		return fr'{sign}{significand} \times 10^{{{exponent}}}'


# set up figure
plt.rc('figure.constrained_layout', hspace=0)
figsize = plt.rcParams['figure.figsize']
if args.figaspect:
	figsize = (figsize[0], figsize[0] * args.figaspect)
if args.with_relative:
	gridspec_kw = {
		'wspace': 0.06,
		'hspace': 0,
		'height_ratios': [1, args.panel_ratio]
	}
	fig, axes = plt.subplots(
		2, 1, sharex='col', sharey='row', figsize=(
			figsize[0], figsize[1] * 1.05 + gridspec_kw['height_ratios'][1]
		), gridspec_kw=gridspec_kw
	)
	axes[0].tick_params(which='both', bottom=False)
	axes[1].tick_params(which='both', top=False)
else:
	fig = plt.figure(figsize=figsize)
	axes = [plt.gca()]
# set up axes
relative = args.only_relative or args.with_relative
#TODO: label units
massfunc_str = r'\mathrm{d}n/\mathrm{d}M'
massfunc_unit_str = r'$h^4$ Mpc$^{-3}$ $\mathrm{M}_\odot^{-1}$'
if args.mass_function_type == 'dn/dlnM':
	massfunc_str = fr'M \, {massfunc_str}'
	massfunc_unit_str = r'$h^3$ Mpc$^{-3}$'
massfunc_unit_str += ' (comoving)'
for ax in axes:
	# TODO?
	ax.tick_params(which='minor', labelleft=False, labelright=False)
	# prevent top/bottom x ticks from overlapping into panels above/below
	if ax is not axes[0]:
		ax.tick_params(which='both', top=False)
	if ax is not axes[-1]:
		ax.tick_params(which='both', bottom=False)
	if ax is axes[-1]:
		ax.set_xlabel(r'$M$ / $h^{-1} \mathrm{M}_\odot$')
	if args.only_relative or (len(axes) > 1 and ax == axes[-1]):
		if args.relative_label_fraction == 'frac':
			rylabel = (
				r'$\textstyle\frac{' fr'({massfunc_str})' '}{'
				fr'({massfunc_str})_{{{args.relative_label}}}' '}$'
			)
		elif args.relative_label_fraction == '/':
			rylabel = (
				fr'$({massfunc_str})$ / '
				fr'$({massfunc_str})_{{{args.relative_label}}}$'
			)
		else: assert False
		rylabel += f'{args.relative_label_suffix}'
		ax.set_ylabel(rylabel)
	else:
		ax.set_ylabel(fr'${massfunc_str}$ / {massfunc_unit_str}')
	ax.set_xscale('log')
	ax.set_yscale('log')

# setup for relative plotting
hist_ref = 1
if relative:
	args.path.insert(args.relative_pos, relative)
	args.path = [relative] + args.path

# set up bins
if len(args.path) > 1 and args.line_type == 'step':
	shift = numpy.linspace(-1, 1, len(args.path)) * 1e-2 * len(args.path)
else:
	shift = numpy.array([0.] * len(args.path))
bin_edges = numpy.logspace(
	numpy.log10(0.8 * args.xlim[0]), numpy.log10(1.2 * args.xlim[1]),
	args.hist_bins
)
bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2
bin_centers_orig = bin_centers
# make sure to include all halos in the binning by adding additional bins
# at the low and high end so that the normalization of the histogram
# (density=True) will be correct
bin_edges_hist = numpy.zeros(len(bin_edges) + 2)
bin_edges_hist[0] = 1e-10
bin_edges_hist[-1] = 1e100
bin_edges_hist[1:-1] = bin_edges


# process group catalogs
if not args.labels:
	args.labels = [p.name for p in args.path][bool(relative):]
if not args.line_styles:
	args.line_styles = ['-'] * (len(args.path) - bool(relative))
if not args.m200:
	args.m200 = [1] * (len(args.path) - bool(relative))
if not args.m200_csv:
	args.m200_csv = [0] * (len(args.path) - bool(relative))
if not args.only_ids:
	args.only_ids = [0] * (len(args.path) - bool(relative))
if not args.only_bound:
	args.only_bound = [0] * (len(args.path) - bool(relative))
if not args.max_virial_ratio:
	args.max_virial_ratio = [numpy.inf] * (len(args.path) - bool(relative))
assert (
	len(args.path) - bool(relative) == len(args.labels) ==
	len(args.line_styles) == len(args.m200) == len(args.m200_csv) ==
	len(args.only_ids) == len(args.only_bound) == len(args.max_virial_ratio)
)
#TODO
change_style_interval = {}
if args.change_style_interval:
	assert len(args.change_style_interval) % 4 == 0
	for i in len(args.change_style_interval) / 4:
		idx = int(args.change_style_interval[i * 4])
		assert idx < len(args.labels)
		low = float(args.change_style_interval[i * 4 + 1])
		high = float(args.change_style_interval[i * 4 + 2])
		style = args.change_style_interval[i * 4 + 3]
		if idx not in change_style_interval:
			change_style_interval[idx] = []
		change_style_interval[idx].append(low, high, style)
for x in change_style_interval.values():
	x.sort()
z_check = None
legend_entries = []
for path_idx, path in enumerate(args.path):
	bin_centers = numpy.copy(bin_centers_orig)
	reference = relative and path_idx == 0
	pprint = (lambda *args, **kwargs: None) if reference else print
	if not reference:
		path_idx -= bool(relative)
		pprint(path)
	#TODO
	galacticus = re.match('HMF_.+.hdf5', path.name)
	kulkarni = path.name.endswith('.npy')
	DO_NORMAL_HMF = not galacticus and not kulkarni
	# PRE-COMPUTED HMF
	if galacticus:
		redshifts, masses, hmf = io_util.read_galacticus_hmf(path)
		assert z_check is not None
		for idx, z in enumerate(redshifts):
			if numpy.isclose(z, z_check, atol=1e-9, rtol=1e-2):
				break
		else:
			assert False, redshifts
		bin_centers = masses[idx]
		hist = hmf[idx]
	elif kulkarni:
		z = z_check
		path_m = path.with_stem(f'{path.stem}_M')
		path_hmf = path.with_stem(f'{path.stem}_hhf')
		bin_centers, hist = io_util.read_npy_hmf(path_m, path_hmf, h=0.7)

	# “NORMAL” FOF HMF
	if DO_NORMAL_HMF:
		# read data
		fof_tab_files = read_snapshot.snapshot_files(path)
		fof_tab_data = read_hdf5.read_fof_tab(fof_tab_files, read_ids=False)
		[config, param, header], groups, _ = fof_tab_data
		units = units_util.get_units(param)
		box_size = header['BoxSize'] * (1e-3 * units.length_in_kpc)
		hubble = param['HubbleParam']
		snaptime = header['Time']
		z = 1 / snaptime - 1
		if z_check is None:
			z_check = z
		pprint(f'\tz = {z}')
		assert numpy.isclose(z, z_check, atol=1e-9, rtol=1e-2), (z_check, z)
		# read data and plot
		if args.m200[path_idx]:
			m200_data = m200_util.read_m200_data(
				path, fof_data=fof_tab_data,
				prefer='csv' if args.m200_csv[path_idx] else 'hdf5'
			)
			g_mass = m200_data['m200_ms']
			r200_kpc = m200_data['r200_kpc']
#			assert len(groups) == len(g_mass), f'{len(groups)} != {len(g_mass)}'
			mask = g_mass > 0
			mask_orig = mask
			num_valid = numpy.sum(mask_orig)
			num_invalid = numpy.sum(~mask_orig)
			pprint(f'\tConsidering {num_valid} halos')
			if args.min_radius:
				min_radius_mask = r200_kpc > args.min_radius
				num_excluded = (
					numpy.sum(~(min_radius_mask & mask_orig)) - num_invalid
				)
				num_excluded_new = (
					numpy.sum(~(min_radius_mask & mask)) - numpy.sum(~mask)
				)
				mask &= min_radius_mask
				pprint(
					f'\tExcluded {num_excluded} halos with R_200 ≤ '
					f'{args.min_radius} kpc/h (of which {num_excluded_new} were '
					'not already excluded)'
				)
			if args.only_ids[path_idx]:
				keep = numpy.load(path.with_name(f'{path.name}_keep.npy'))
				assert len(mask) == len(keep)
				num_excluded = numpy.sum(~(keep & mask_orig)) - num_invalid
				num_excluded_new = numpy.sum(~(keep & mask)) - numpy.sum(~mask)
				mask &= keep
				pprint(
					f'\tExcluded {num_excluded} overlapping halos (of which '
					f'{num_excluded_new} were not already excluded)'
				)
			load_energies = (
				args.only_bound[path_idx] or
				args.max_virial_ratio[path_idx] < numpy.inf
			)
			if load_energies:
				if numpy.any(numpy.isnan(groups.kinegy)):
					with h5py.File(
						path.with_name(f'{path.name}_halo_energy.hdf5'), 'r'
					) as f:
						energy_kin = (
							f['KineticEnergy'][...] + f['QuantumEnergy'][...]
						)
				else:
					energy_kin = groups.kinegy + groups.gradegy
				energy_pot = groups.potegy
				assert energy_kin.shape == energy_pot.shape
				energy_tot = energy_kin + energy_pot
			if args.only_bound[path_idx]:
				bound = energy_tot < 0
				assert len(mask) == len(bound)
				num_excluded = numpy.sum(~(bound & mask_orig)) - num_invalid
				num_excluded_new = numpy.sum(~(bound & mask)) - numpy.sum(~mask)
				mask &= bound
				pprint(
					f'\tExcluded {num_excluded} unbound halos (of which '
					f'{num_excluded_new} were not already excluded)'
				)
			if args.max_virial_ratio[path_idx] < numpy.inf:
				virial_ratio = -2 * energy_kin / energy_pot
				virial_ratio_mask = virial_ratio < args.max_virial_ratio[path_idx]
				assert len(mask) == len(virial_ratio_mask)
				num_excluded = numpy.sum(
					~(virial_ratio_mask & mask_orig)
				) - num_invalid
				num_excluded_new = numpy.sum(
					~(virial_ratio_mask & mask)
				) - numpy.sum(~mask)
				mask &= virial_ratio_mask
				pprint(
					f'\tExcluded {num_excluded} unbound halos (of which '
					f'{num_excluded_new} were not already excluded)'
				)
			g_mass = g_mass[mask]
			num_remaining = numpy.sum(mask)
			pprint(f'\t{num_remaining} halos remain in HMF after filtering')
			if num_remaining < num_valid:
				with open('filtered-halos.txt', 'w') as f:
					for group_nr in m200_data['id'][mask]:
						print(group_nr, file=f)
		else:
			g_mass = groups.tot_mass * units.mass_in_solar_mass
		hist_counts, _ = numpy.histogram(g_mass, bins=bin_edges_hist)
		# normalize y-axis such that the values show the number density of halos
		# with respect to volume and mass (dn/dM, n = N / V), i.e. such that
		# ∫ dn/dM dM = N / V
		hist, _ = numpy.histogram(g_mass, bins=bin_edges_hist, density=True)
		# discard dummy low and high bins
		hist = hist[1:-1]
		hist *= len(g_mass) / box_size**3
		if args.mass_function_type == 'dn/dlnM':
			# dn/dln(M) = M dn/dM
			hist *= bin_centers
		# apply normalization to Poisson errors
		hist_counts = hist_counts[1:-1]
		poisson_error = numpy.sqrt(hist_counts)
		m = hist_counts > 0
		norm = (hist[m] / hist_counts[m])[0]
		#TODO: what is the correct check in other cases?
		assert args.mass_function_type != 'dn/dlnM' or numpy.allclose(
			hist[m] / hist_counts[m], norm
		)
		poisson_error *= norm
		# remember reference data and start processing actual entries
		if reference:
			hist_ref = hist
			continue

	# plot
	label = args.labels[path_idx]
	# optionally slightly shift for multiple lines to improve visibility
	bin_centers_plot = bin_centers * (1 + shift[path_idx])
	for ax in axes:
		if relative and ax is axes[-1]:
			if path_idx in args.relative_exclude:
				continue
			if DO_NORMAL_HMF:
				hist_ref_ax = hist_ref
			else:
				hist_ref_interp = scipy.interpolate.interp1d(
					bin_centers_orig, hist_ref, bounds_error=False
				)
				hist_ref_ax = hist_ref_interp(bin_centers)
		else:
			hist_ref_ax = 1
		with numpy.errstate(divide='ignore', invalid='ignore'):
			if args.line_type == 'line':
				#TODO
				if args.change_style_interval and path_idx == 3:
					mask_low = bin_centers_plot < 5e9
					mask_high = ~mask_low
					if numpy.any(mask_low):
						mask_low = numpy.roll(mask_low, 1)
						mask_low[0] = True
						if numpy.any(mask_high):
							mask_high = numpy.roll(mask_high, -1)
							mask_high[-1] = True
						ax.plot(
							bin_centers_plot[mask_low],
							hist[mask_low] / (
								hist_ref_ax if isinstance(hist_ref_ax, int) else
								hist_ref_ax[mask_low]
							), alpha=0.8, linewidth=0.5, linestyle='dotted',
							color=f'C{path_idx}'
						)
					line, = ax.plot(
						bin_centers_plot[mask_high],
						hist[mask_high] / (
							hist_ref_ax if isinstance(hist_ref_ax, int) else
							hist_ref_ax[mask_high]
						), label=label
					)
					if ax is axes[0]:
						legend_entries.append((f'data-{path_idx}', line))
				else:
					hist_plot = hist / hist_ref_ax
					if path_idx in args.show_poisson_error:
						linestyle, marker, color = _process_plot_format(
							args.line_styles[path_idx]
						)
						ref_fac = 1
						if not isinstance(hist_ref_ax, int) or hist_ref_ax != 1:
							ref_fac = hist_plot
						ax.fill_between(
							bin_centers_plot,
							hist_plot - ref_fac * poisson_error,
							hist_plot + ref_fac * poisson_error,
							facecolor=color, alpha=0.5
						)
					line, = ax.plot(
						bin_centers_plot, hist_plot,
						args.line_styles[path_idx], label=label
					)
					if ax is axes[0]:
						legend_entries.append((f'data-{path_idx}', line))
			elif args.line_type == 'step':
				if path_idx in args.show_poisson_error:
					pass  #TODO
				line, = ax.step(
					bin_centers_plot, hist / hist_ref_ax,
					args.line_styles[path_idx], where='mid', alpha=0.8,
					label=label
				)
				if ax is axes[0]:
					legend_entries.append((f'data-{path_idx}', line))
			else: assert False
	#TODO save values for fit analogous to Schive (2016)
	#TODO: make idx configurable
	if path_idx == 1:
		hist_for_schive_fit = hist

# plot (semi-)analytic functions
bin_centers = bin_centers_orig
for ax in axes:
	if relative and ax is axes[-1]:
		if path_idx in args.relative_exclude:
			continue
		hist_ref_ax = hist_ref
	else:
		hist_ref_ax = None
	# compute and plot Press–Schechter function(s)
	if args.press_schechter:
		if 'manual' in args.press_schechter_backend:
			#TODO
			# growth factor table output from GADGET-4’s N-GenIC
			data = pandas.read_csv(
				Path(sys.path[0], 'gadget4_growthfac_z127.txt'),
				names=['a', 'Dplus'], sep=r'\s+'
			)
			a = data['a']
			Dplus = data['Dplus']
			growthfac = numpy.exp(
				scipy.interpolate.interp1d(numpy.log(a), numpy.log(Dplus))(
					numpy.log(snaptime)
				)
			)
			# σ table output from GADGET-4’s N-GenIC
			data = pandas.read_csv(
				Path(sys.path[0], 'gadget4_variance_z127.txt'),
				names=['M_ms', 'M_1e10ms', 'sigma2', 'sigma'], sep=r'\s+'
			)
			M = data['M_1e10ms']
			sigma = data['sigma']
			# scale back to the redshift we are looking at
			sigma *= growthfac
			# create xaxes as a uniform table in terms of ln(1/σ)
			dloginv =  0.001
			mi = -3.0
			ma = 2.0
			xaxes = numpy.linspace(
				mi, ma, round((ma - mi) / dloginv), endpoint=False
			)
			# get the corresponding variance and mass;
			# shift the table slightly, so that we can numerically
			# differentiate
			sig_list = []
			maxes_list = []
			for i in range(2):
				xax = xaxes + i * 0.1 * dloginv
				sig_list.append(1 / numpy.exp(xax))
				maxes_list.append(
					numpy.exp(
						scipy.interpolate.interp1d(
							-numpy.log(sigma), numpy.log(M),
							kind='quadratic', bounds_error=False
						)(xax)
					)
				)
			sig, sig1 = sig_list
			maxes, maxes1 = maxes_list
			# compute logarithmic derivative dln(σ)/dln(M)
			dlnsigma_dlnmass = (
				(numpy.log(sig1) - numpy.log(sig)) /
				(numpy.log(maxes1) - numpy.log(maxes))
			)
			# Press & Schechter mass function: f_ps = f(σ) = dF/dln(σ)
			f_ps = (
				numpy.sqrt(2 * numpy.pi) * DELTA_C / sig *
				numpy.exp(-DELTA_C**2 / (2 * sig**2))
			)
			# Sheth & Tormen mass function:    f_st = f(σ) = dF/dln(σ)
			A  = 0.3222
			aa = 0.707
			p  = 0.3
			f_st = (
				A * numpy.sqrt(2 * aa / numpy.pi) *
				(1 + (sig**2 / (aa * DELTA_C**2))**p) * DELTA_C / sig *
				numpy.exp(-aa * DELTA_C**2 / (2 * sig**2))
			)
			# dn/dM = ρ_m/M |dF/dM| = ρ_m/M² |dF/dln(M)|
			# dn/dln(M) = ρ_m/M |dF/dln(M)|
			#           = ρ_m/M |dF/dln(σ)| |dln(σ)/ln(M)|
			#           = ρ_m/M f(σ) |dln(σ)/dln(M)|
			G = astropy.constants.G.to('1e-10 * Mpc**3/(Msun * s**2)').value
			H0 = astropy.units.Quantity(100, 'km/(s * Mpc)').to('1/s').value
			rho_m = header['Omega0'] * 3 * H0**2/(8 * numpy.pi * G)
			maxes_ms = maxes * 1e10
			ps_M_ms = maxes_ms
			# dn/dln(M) = M dn/dM
			ps_normfac = 1
			if args.mass_function_type == 'dn/dM':
				ps_normfac = 1 / ps_M_ms
			for ps_function in args.press_schechter_function:
				if ps_function == 'press-schechter':
					f = f_ps
					label = 'Press–Schechter (CDM)'
				elif ps_function == 'sheth-tormen':
					f = f_st
					label = 'Sheth–Tormen (CDM)'
				else: assert False
				ps_mass_func = (
					ps_normfac * rho_m / maxes * f *
					numpy.abs(dlnsigma_dlnmass)
				)
				# plot
				hist_ref_interp = scipy.interpolate.interp1d(
					bin_centers, hist_ref_ax, bounds_error=False
				)(maxes_ms) if relative and hist_ref_ax is not None else 1
				with numpy.errstate(divide='ignore', invalid='ignore'):
					line, = ax.plot(
						ps_M_ms, ps_mass_func / hist_ref_interp,
						color='gray', linewidth=0.5, label=label
					)
					if ax is axes[0]:
						legend_entries.append(('ps-manual', line))
		if 'colossus' in args.press_schechter_backend:
			from colossus.cosmology import cosmology
			from colossus.lss import mass_function
			cosmology.setCosmology(
				'MyAREPO',
				params=dict(
					Om0=header['Omega0'], Ode0=header['OmegaLambda'],
					# TODO: don’t hardcode values for Ob0, sigma8 and ns
					Ob0=0, H0=100 * hubble, sigma8=0.9, ns=1.0
				)
			)
			ps_M_ms = numpy.logspace(*numpy.log10(args.xlim), 200) / hubble
			# dn/dln(M) = M dn/dM
			ps_normfac = 1
			if args.mass_function_type == 'dn/dM':
				ps_normfac = 1 / ps_M_ms
			ps_mass_func = mass_function.massFunction(
				ps_M_ms, z, mdef='fof', model='press74', q_out='dndlnM',
				ps_args={'model': 'eisenstein98_zb'}
			) * ps_normfac
			hist_ref_interp = scipy.interpolate.interp1d(
				bin_centers, hist_ref_ax, bounds_error=False
			)(ps_M_ms) if relative and hist_ref_ax is not None else 1
			line, = ax.plot(
				ps_M_ms, ps_mass_func / hist_ref_interp, '--',
				color='gray', linewidth=0.5,
				label='Press–Schechter HMF (CDM) [colossus]',
			)
			if ax is axes[0]:
				legend_entries.append(('ps-colossus', line))
	# plot Schive et al. (2016) FDM HMF fit
	if args.schive_fit:
		m_ax = args.schive_fit
		M0_ms = 1.6e10 * (m_ax / 1e-22)**(-4/3)
		hmf_fdm = ps_mass_func * (1 + (ps_M_ms / M0_ms)**-1.1)**-2.2
		linewidth_opt = {}
		if args.schive_fit_linewidth:
			linewidth_opt['linewidth'] = args.schive_fit_linewidth
		line, = ax.plot(
			ps_M_ms, hmf_fdm / hist_ref_interp,
			args.schive_fit_linestyle.strip(), color='gray', **linewidth_opt,
			label=args.schive_fit_label if args.schive_fit_label else
				'Schive et al. (2016) HMF (FDM,\n'
				fr'$\qquad mc^2 = {float_to_scientific_notation(m_ax)}\,$eV)',
		)
		if ax is axes[0]:
			legend_entries.append(('schive-fit', line))
	##TODO fit analogous to Schive (2016)
	if args.schive_fit_analog:
		m_ax = args.schive_fit_analog
		ps_interp = scipy.interpolate.interp1d(ps_M_ms, ps_mass_func)
		def mass_func_like_schive(M, M0, a=-1.1, b=-2.2):
			return (
				ps_interp(M) * (1 + (M / (M0 * (m_ax / 1e-22)**(-4/3)))**a)**b
			)
		if args.schive_fit_analog_parameters:
			fit_param = args.schive_fit_analog_parameters
		else:
			#hmf_fit = lambda M, M0, a, b: numpy.log(mass_func_like_schive(M, M0, a, b))
			hmf_fit = (
				lambda M, M0, a: numpy.log(mass_func_like_schive(M, M0, a))
			)
			hist_mask = hist_for_schive_fit > 0
			fit_param, _ = scipy.optimize.curve_fit(
				hmf_fit, bin_centers[hist_mask],
				numpy.log(hist_for_schive_fit[hist_mask]),
			#	p0=(1.6e10, -1.1, -2.2),
				p0=(1.6e10 * header['HubbleParam'], -1.1),
			)
		pprint(f'\t{fit_param}')
		hist_ref_interp_schive_analog = scipy.interpolate.interp1d(
			bin_centers, hist_ref_ax, bounds_error=False
		)(bin_centers) if relative and hist_ref_ax is not None else 1
		line, = ax.plot(
			bin_centers,
			mass_func_like_schive(bin_centers, *fit_param) /
				hist_ref_interp_schive_analog,
			'--', color='gray', linewidth=0.8,
			#TODO
			label=r'\providecommand{\plothookfit}{Fit with}\plothookfit'
				'\n'
				r'$M_0 = '
				f'{float_to_scientific_notation(fit_param[0], digits=2)} '
#				r'(mc^2 / (10^{-22}\,\mathrm{eV}))^{-\sfrac{4}{3}} '
				r'\,h^{-1} \mathrm{M}_\odot$,'
				'\n'
				fr'$\alpha = {fit_param[1]:.2f}$',
# 			r'$\textstyle\left[1 + \left(\frac{M}{'
# 				f'{float_to_scientific_notation(fit_param[0], digits=2)} '
# 				r'h^{-1} \mathrm{M}_\odot \, m_{22}^{-4/3}}\right)^{'
# 				f'{fit_param[1]:.2f}' r'}\right]^{-2.2} \!\!\!\!\!\times$'
# 				'\n'
# 				r'$\textstyle\qquad\left. '
# 				r'M\frac{\mathrm{d}n}{\mathrm{d}M}\right|_{\mathrm{CDM}}$',
		)
		if ax is axes[0]:
			legend_entries.append(('schive-fit-analog', line))

legend = None
for ax in axes:
	# axis limits
	ax.set_xlim(*args.xlim)
	if relative and ax is axes[-1]:
		if args.ylim_relative:
			ax.set_ylim(args.ylim_relative)
	elif args.ylim:
		ax.set_ylim(args.ylim)
	# legend
	if ax is axes[0] and not args.no_legend:
		sort_key = {}
		for i, name in enumerate(args.legend_order):
			sort_key[name] = i
		legend_entries_data = [
			e for e in legend_entries if e[0].startswith('data-')
		]
		#TODO
#		legend_entries_other = [
#			e for e in legend_entries if not e[0].startswith('data-')
#		]
		legend_entries.sort(key=lambda tup: sort_key.get(
			tup[0], -numpy.inf if tup[0].startswith('data-') else numpy.inf
		))
#		if legend_entries_other:
#			legend_other = ax.legend(
#				handles=[line for name, line in legend_entries_other],
##				bbox_to_anchor=(0.995, 0.7), loc='upper right'
#				loc='lower left'
#			)
#			ax.add_artist(legend_other)
		legend = ax.legend(
			handles=[line for name, line in legend_entries],
#			loc='upper right' if legend_entries_other else 'best'
		)

# title
title = args.title if args.title else ''
if z >= 0:
	title_time = f'$z = {z:.2f}$'
else:
	title_time = f'$a = {snaptime:.4f}$'
title += title_time
if title and not args.no_title:
	fig.suptitle(title)


# redshift text (lower left/upper right corner)
# Needs to be done as late as possible so that the rest of the figure is
# already in place!
if args.equal_time:
	loc = 'lower left'
	# check if legend is already in lower left corner, and put redshift
	# text in upper right corner if this is the case
	if legend:
		# save figure to a temporary file so that figure layout is done
		# with the correct backend
		with tempfile.TemporaryDirectory() as tmp, numpy.errstate(
			divide='ignore'
		):
			fig.savefig(
				Path(tmp) / f'tmp.{args.plot_format}', transparent=True
			)
		legend_bbox = legend.get_window_extent().transformed(
			axes[0].transAxes.inverted()
		)
		if matplotlib.transforms.Bbox.intersection(
			legend_bbox, matplotlib.transforms.Bbox([[0, 0], [1/4, 1/4]])
		):
			loc = 'upper right'
	axes[0].add_artist(matplotlib.offsetbox.AnchoredText(
		f'$z = {round(z, 1):.1f}$', loc=loc
	))
if args.plot_label:
	loc = 'lower left'
	axes[0].add_artist(matplotlib.offsetbox.AnchoredText(
		args.plot_label, loc=loc
	))


# save plot
savefig_opts = {}
if args.plot_format != 'pdf':
	savefig_opts['dpi'] = args.dpi
plot_file_name = args.plot_file_prefix
plot_file_name += f'mass_function_{snaptime:.4f}'
if args.only_relative:
	plot_file_name += '_ratio'
if args.plot_file_name:
	plot_file_name = args.plot_file_name
plot_file_name += f'.{args.plot_format}'
fig.savefig(plot_file_name, transparent=True, **savefig_opts)
