#!/usr/bin/env python3
import time
START_WALL_TIME = time.perf_counter()
import argparse
import collections
import gc
import os
import shlex
import subprocess
import sys
from pathlib import Path
import numba
import numexpr
import numpy
import pandas
import bin_density
import m200_util
import mpi_util
import read_snapshot
import read_snapshot.hdf5 as read_hdf5
import snapshot
import units_util
from mpi_util import MPI, mpi_comm, mpi_rank, mpi_size, print0
from snapshot import Snapshot


# constants
PARTICLE_TYPE = 1
HALO_CENTER_SPHERE_FACTOR = 0.9

DAY_IN_SECONDS = 24 * 60**3
MAX_RUNTIME_FACTOR = 23.6 / 24

parser = argparse.ArgumentParser(description='Calculate virial masses and '
	'radii using the spherical overdensity algorithm for halos in fuzzy dark '
	'matter or cold dark matter simulation snapshots')
parser.add_argument('--prefer-type', choices=('grid', 'particles'),
	default='grid',
	help='the type of snapshot data that should be preferred '
		'(default: %(default)s)')
parser.add_argument('--max-runtime', type=float, default=23.6 * 60**2,
	help='maximum wall clock time in seconds before execution is stopped '
		'(default: 23.6h)')
parser.add_argument('--only-halo-center', action='store_true',
	help='only calculate halo centers during M_200 calculation')
parser.add_argument('--resume', action='store_true',
	help='resume processing an incomplete M_200 CSV file')
parser.add_argument('--overwrite', action='store_true',
	help='overwrite existing M_200 CSV file')
parser.add_argument('--m200-save-interval', type=int, default=500,
	help='save M_200 output file each time after this number of halos has '
		'been processed (default: %(default)d)')
parser.add_argument('--resubmit', action='store_true',
	help='resubmit SLURM job if computation is not complete before '
		'--max-runtime')
parser.add_argument('--halo-id-offset', type=int, default=0,
	help='do not process halos with IDs smaller than the given ID '
		'(default: %(default)d)')
parser.add_argument('--fof-group-min-len', type=int, default=8,
	help='minimum FoF length to be processed (default: %(default)d)')
parser.add_argument('--group-id-data-size-limit', type=float, default=0,
	help='limit for the size of FoF ID data to keep in memory (in MiB)')
parser.add_argument('path', type=Path, nargs='+',
	help='the path to the fof_tab catalog; any normal files are interpreted '
		'as single files, while all files in any given directory are '
		'interpreted as a single multi-file output')
args = parser.parse_args()

# global variables
group_gids = None
group_gids_range = None


def r200_bin_from_rho_mean(
	rho_mean_binned, rho_avg_global, m_fof, r_min, r_max
):
	overdense_bins, = numpy.nonzero(
		rho_mean_binned > m200_util.VIRIAL_OVERDENSITY_FACTOR * rho_avg_global
	)
	assert len(overdense_bins) < len(rho_mean_binned)
	r200_bin = None
	if len(overdense_bins) > 0:
		# make sure that we don’t end up with a huge fake halo if the density
		# never reaches ρ_200 in the middle, but then later at large distances
		# due to neighboring halos: the first time reaching ρ_200 should happen
		# at a radius smaller than if we had M_FoF = M_200
		_, r_first_overdense = bin_density.bin_to_r_linear(
			len(rho_mean_binned), overdense_bins[0], r_min, r_max
		)
		_, r_first_bin = bin_density.bin_to_r_linear(
			len(rho_mean_binned), 0, r_min, r_max
		)
		r_first_overdense_max = 1/2 * m200_util.r200_from_m200(
			rho_avg_global, m_fof
		)
		if (
			r_first_overdense < r_first_overdense_max or
			# for the second iteration, r_min will be large so we cannot do
			# this check, but then make sure that the lower bins are overdense
			(
				r_first_bin >= r_first_overdense_max and
				overdense_bins[0] == 0
			)
		):
			# make sure to only include a contiguous halo (no low-density
			# “gaps”) (shift by overdense_bins[0] in case the central bins
			# don’t contain any particles)
			contiguous = numpy.arange(len(overdense_bins)) + overdense_bins[0]
			overdense_bins = overdense_bins[overdense_bins == contiguous]
			r200_bin = overdense_bins[-1]
	return r200_bin, rho_mean_binned[r200_bin] if r200_bin is not None else 0.

def calculate_m200_particles(
	particle_data, particle_mass, numpart_total, box_size, center, m_fof
):
	num_bins = int(16 * numpart_total**(1/3))
	r_min_abs = min(
		particle_data.softening,
		1/4 * m200_util.r200_from_m200(particle_data.rho_avg, m_fof)
	)
	r_max_abs = 0.4 * box_size
	r200_bin, _ = calculate_r200_bin_particles(
		particle_data.pos, particle_mass, numpart_total, box_size, num_bins,
		center, m_fof, particle_data.rho_avg,
		# use a small minimal radius to avoid the case where a few particles
		# at the center cause an overdensity > ρ_200, which then drops below
		# ρ_200 before the next particles are reached
		r_min=r_min_abs, r_max=r_max_abs
	)
	if r200_bin is None:
		return 0., 0., 0.
	estimate_interval = (r_max_abs - r_min_abs) / num_bins
	# iterate on the estimate on R_200 by using bounds from the first estimate
	_, r200_estimate = bin_density.bin_to_r_linear(
		num_bins, r200_bin, r_min_abs, r_max_abs
	)
	r_min = max(r200_estimate - estimate_interval, r_min_abs)
	r_max = min(r200_estimate + estimate_interval, r_max_abs)
	r200_bin, rho_mean = calculate_r200_bin_particles(
		particle_data.pos, particle_mass, numpart_total, box_size, num_bins,
		center, m_fof, particle_data.rho_avg, r_min=r_min, r_max=r_max
	)
	r200 = (
		bin_density.bin_to_r_linear(
			num_bins, r200_bin, r_min, r_max
		)[1] if r200_bin is not None else 0.
	)
	# rho_mean[r200_bin] should be approximately equal to 200 * rho_avg
	m200 = rho_mean * 4/3 * numpy.pi * r200**3
	return m200, r200, r200_estimate

# TODO: r_min/r_max=None doesn’t actually work
def calculate_r200_bin_particles(
	pos, particle_mass, numpart_total, box_size, num_bins, center, m_fof,
	rho_avg, r_min=None, r_max=None
):
	# use one additional data bin at the beginning to store sums for distances
	# < r_min
	bin_sums = numpy.zeros(num_bins + 1, dtype='i8')
	param = bin_density.ParamParticles(
		center=center, mass=particle_mass, box_size=box_size,
		numpart_total=numpart_total, softening_length=None,
		r_min=r_min, r_max=r_max, binlog=False, central_bin=True
	)
	bin_density.bin_particles(pos, param, bin_sums)
	# element 0 of data contains sum for distances < r_min
	bin_sums_g = numpy.cumsum(mpi_comm.allreduce(bin_sums, op=MPI.SUM))[1:]
	_, r_bins = bin_density.bin_to_r_linear(
		num_bins, numpy.arange(num_bins), r_min, r_max
	)
	rho_mean_binned = particle_mass * bin_sums_g / (4/3 * numpy.pi * r_bins**3)
	return r200_bin_from_rho_mean(
		rho_mean_binned, rho_avg, m_fof, r_min, r_max
	)

def calculate_m200_grid(grid_data, snap, center_g3, m_fof):
	box_size = snap.header['BoxSize']
	cell_len = box_size / snap.pmgrid
	num_bins = 2 * int(snap.pmgrid)
	ir_max_abs = 0.4 * snap.pmgrid
	r200_bin, _ = calculate_r200_bin_grid(
		grid_data.rho, grid_data.rho_avg, grid_data.first_slab_x,
		grid_data.nslab_x, snap.pmgrid, box_size, num_bins, center_g3, m_fof,
		ir_min=0., ir_max=ir_max_abs
	)
	if r200_bin is None:
		return 0., 0., 0.
	# iterate on the estimate on R_200 by using bounds from the first estimate
	_, ir200 = bin_density.bin_to_r_linear(num_bins, r200_bin, 0, ir_max_abs)
	r200_estimate = ir200 * cell_len
	ir_min = max(ir200 - 2, 0)
	ir_max = min(ir200 + 2, ir_max_abs)
	r200_bin, rho_mean = calculate_r200_bin_grid(
		grid_data.rho, grid_data.rho_avg, grid_data.first_slab_x,
		grid_data.nslab_x, snap.pmgrid, box_size, num_bins, center_g3, m_fof,
		ir_min=ir_min, ir_max=ir_max
	)
	ir200 = (
		bin_density.bin_to_r_linear(num_bins, r200_bin, ir_min, ir_max)[1]
		if r200_bin is not None else 0.
	)
	r200 = ir200 * cell_len
	# rho_mean[r200_bin] should be approximately equal to 200 * rho_avg
	m200 = rho_mean * 4/3 * numpy.pi * r200**3
	return m200, r200, r200_estimate

def calculate_r200_bin_grid(
	rho, rho_avg, first_slab_x, nslab_x, PMGRID, box_size,
	num_bins, center_g3, m_fof, ir_min=None, ir_max=None
):
	# use one additional data bin at the beginning to store sums for distances
	# < ir_min
	data = numpy.zeros(
		num_bins + 1, dtype=[('rho_sum_l', 'f8'), ('rho_num_l', 'i8')]
	)
	param = bin_density.ParamGrid(
		center_g3=center_g3, ir_min=ir_min, ir_max=ir_max, binlog=False,
		central_bin=True, mpi_rank=mpi_rank
	)
	bin_density.rho_bin_cells(
		rho, PMGRID, first_slab_x, first_slab_x + nslab_x, param, data
	)
	# element 0 of data contains sums for distances < ir_min
	rho_sum_g = numpy.cumsum(
		mpi_comm.allreduce(data['rho_sum_l'], op=MPI.SUM)
	)[1:]
	rho_num_g = numpy.cumsum(
		mpi_comm.allreduce(data['rho_num_l'], op=MPI.SUM)
	)[1:]
	rho_mean_binned, _ = bin_density.density_mean_division(
		rho_sum_g, rho_num_g
	)
	cell_len = box_size / PMGRID
	return r200_bin_from_rho_mean(
		rho_mean_binned, rho_avg, m_fof, ir_min * cell_len, ir_max * cell_len
	)

def make_slab_to_task(PMGRID, nslab_x):
	slabs_x_per_task = mpi_comm.allgather(nslab_x)
	slab_to_task = numpy.zeros(PMGRID, dtype='u4')
	curr_slab = 0
	for task in range(mpi_size):
		task_nslabs = slabs_x_per_task[task]
		for task_slab in range(task_nslabs):
			slab_to_task[curr_slab + task_slab] = task
		curr_slab += task_nslabs
	assert curr_slab == PMGRID
	return slab_to_task

@numba.njit
def scatter_halo_ids_get_send_counts(slab_to_task, slabs, send_counts):
	for slab in slabs:
		send_counts[slab_to_task[slab]] += 1

def scatter_halo_ids(PMGRID, slab_to_task, send_ids):
	# determine send counts
	send_counts = None
	send_displs = None
	if mpi_rank == 0:
		assert send_ids.dtype == numpy.dtype('u8')
		send_counts = numpy.zeros(mpi_size, dtype='u4')
		slabs = send_ids // PMGRID**2
		scatter_halo_ids_get_send_counts(slab_to_task, slabs, send_counts)
		send_displs = numpy.zeros_like(send_counts)
		send_displs[1:] = numpy.cumsum(send_counts)[:-1]
		assert numpy.sum(send_counts) == len(send_ids)
		assert send_displs[-1] + send_counts[-1] == len(send_ids)
		assert send_displs[0] == 0
		assert numpy.all(send_displs[:-1] <= send_displs[1:])
		print0('\tID counts for Scatterv() have been determined')
	# allocate local ID arrays
	num_ids = mpi_comm.scatter(send_counts, root=0)
	ids = numpy.zeros(num_ids, dtype='u8')
	# distribute IDs
	gc.collect()
	mpi_comm.Scatterv((send_ids, (send_counts, send_displs)), ids, root=0)
	return ids

def get_type(snap_types):
	if args.prefer_type in snap_types:
		return args.prefer_type
	elif 'particles' in snap_types:
		return 'particles'
	elif 'grid' in snap_types:
		return 'grid'
	else: assert False

def main():
	mpi_util.default_print_args['flush'] = True
	if args.resume:
		args.overwrite = True
	if args.group_id_data_size_limit:
		args.group_id_data_size_limit = (
			round(args.group_id_data_size_limit * 1024**2)
		)
		if args.group_id_data_size_limit == 0:
			args.group_id_data_size_limit = 128
	# process paths
	for path in args.path:
		path_result = process_path(path)
		if path_result == 'max-runtime':
			# optionally resubmit if max. runtime was exceeded
			if mpi_rank == 0:
				# {shlex.join(sys.argv[1:])}
				sbatch_script = f'''#!/bin/bash -l
					srun python3 ~/arepo-tools/{shlex.quote(sys.argv[0])} \
						{' '.join(shlex.quote(x) for x in sys.argv[1:])} \
						--resume \
						--max-runtime {parser.get_default("max_runtime")}
				'''
				sbatch_cmd = [
					'sbatch', '--time=24:00:00',
					f'--partition={os.environ["SLURM_JOB_PARTITION"]}',
					f'--nodes={os.environ["SLURM_JOB_NUM_NODES"]}',
					f'--ntasks-per-node={os.environ["SLURM_TASKS_PER_NODE"]}',
					f'--job-name={os.environ["SLURM_JOB_NAME"]}',
					'--output=/u/smay/slurm/%j_%x.out',
					'--error=/u/smay/slurm/%j_%x.out',
					'--mail-type=ALL', '--mail-user=smay@mpa-garching.mpg.de',
				]
				print0('Re-submitting batch job:')
				print0(sbatch_cmd)
				print0(f'input={sbatch_script}')
				subprocess.run(sbatch_cmd, input=sbatch_script, check=True)
			break

def process_path(path):
	print0(path)
	m200_warnings = []
	wrote_csv = False
	m200_file = m200_util.m200_csv_path(path)
	# find fof_tab files
	if path.is_dir():
		fof_tab_files = sorted(
			path.iterdir(), key=read_snapshot.arepo_filename_key
		)
		assert fof_tab_files
	else:
		fof_tab_files = [path]
	# read fof_tab metadata
	fof_tab_data = None
	if mpi_rank == 0:
		*fof_tab_data, _ = read_hdf5.read_fof_tab(
			fof_tab_files, read_ids=False
		)
	# broadcast fof_tab data to other ranks
	if mpi_size > 1:
		fof_tab_data = mpi_comm.bcast(fof_tab_data, root=0)
	[fof_config, fof_param, fof_header], groups = fof_tab_data
	if len(groups) == 0:
		print0('fof_tab contains no groups')
		return 'normal'
	# read snapshot metadata
	print0('Reading snapshot metadata…')
	snap = Snapshot(read_snapshot.output_path(path, fof_config, fof_param))
	units = units_util.get_units(snap.header)
	snaptime = snap.header['Time']
	z = 1 / snaptime - 1
	print0(f'\tRedshift: z = {z}')
	# read fof_tab files on rank 0
	global group_gids, group_gids_range
	print0('Reading fof_tab files…')
	t0 = time.perf_counter()
	if mpi_rank == 0:
		# read fof_tab data
		*_, group_gids = read_hdf5.read_fof_tab(
			fof_tab_files, read_ids=get_type(snap.types) == 'grid',
			max_data_size=args.group_id_data_size_limit
		)
		# group_gids is only stored on rank 0 because it can be large
		if get_type(snap.types) == 'grid':
			assert group_gids is not None
			assert len(group_gids) > 0
			group_gids_range = range(len(group_gids))
			group_gids_mem = sum(gids.nbytes for gids in group_gids)
			print0(
				f'\tRead ID data for groups {min(group_gids_range)} '
				f'to {max(group_gids_range)} '
				f'({group_gids_mem / 1024**2} MiB)'
			)
	groups_nr = numpy.arange(len(groups))
	# use dtype i8 because the result of numpy.cumsum has dtype u8 by
	# default if groups.tot_len has an unsigned integer dtype, and
	# combining any signed integer with u8 in an operation causes the
	# resulting dtype to be f8
	groups_first_particle = numpy.zeros(len(groups), dtype='i8')
	groups_first_particle[1:] = numpy.cumsum(groups.tot_len)[:-1]
	t1 = time.perf_counter()
	print0(f'\tTook {t1 - t0} sec.')
	print0(f'Processing fof_tab with {len(groups)} groups')
	# read density (grid or particle) data
	print0('Reading snapshot density data…')
	t0 = time.perf_counter()
	if get_type(snap.types) == 'grid':
		# determine data to read on each rank (first_slab_x and nslab_x)
		ix_chunks = snap.determine_chunks(mpi_rank, mpi_size)
		if ix_chunks:
			first_slab_x = ix_chunks[0][0]
			nslab_x = ix_chunks[-1][-1] - first_slab_x
		else:
			first_slab_x = snap.pmgrid
			nslab_x = 0
		ix_slice = numpy.s_[first_slab_x:first_slab_x + nslab_x]
		# read grid
		psi_re = snap.read_snapshot('/FuzzyDM/PsiRe', data_slice=ix_slice)
		rho = psi_re**2
		del psi_re
		psi_im = (
			snap.read_snapshot('/FuzzyDM/PsiIm', data_slice=ix_slice)
			if snap.dataset_present('/FuzzyDM/PsiIm') else 0
		)
		rho += psi_im**2
		del psi_im
		assert len(rho) == nslab_x
		slab_to_task = make_slab_to_task(snap.pmgrid, nslab_x)
		# calculate average density
		rho_sum_l = numpy.sum(rho)
		rho_sum_g = mpi_comm.allreduce(rho_sum_l, op=MPI.SUM)
		rho_avg = rho_sum_g / snap.pmgrid**3
		grid_data = collections.namedtuple(
			'GridData', 'first_slab_x, nslab_x, rho, rho_avg'
		)(
			first_slab_x=first_slab_x, nslab_x=nslab_x,
			rho=rho, rho_avg=rho_avg
		)
		data = grid_data
	elif get_type(snap.types) == 'particles':
		# determine data to read on each rank
		numpart_total = read_snapshot.numpart_total(snap.header)
		part_chunks = snap.determine_chunks(
			mpi_rank, mpi_size, num_slabs=numpart_total, slab_size=1
		)
		first_part = part_chunks[0][0]
		numpart = part_chunks[-1][-1] - first_part
		part_slice = numpy.s_[first_part:first_part + numpart]
		# read positions and mass
		pos = snap.read_snapshot(
			f'/PartType{PARTICLE_TYPE}/Coordinates', data_slice=part_slice
		)
		# calculate average density (assumes fixed particle mass)
		particle_mass = snap.header['MassTable'][PARTICLE_TYPE]
		rho_avg = (
			particle_mass * numpart_total / snap.header['BoxSize']**3
		)
		softening = snap.param[
			'SofteningComovingType'
			f'{snap.param["SofteningTypeOfPartType{PARTICLE_TYPE}"]}'
		]
		particle_data = collections.namedtuple(
			'ParticleData', 'first_part, numpart, softening, pos, rho_avg'
		)(
			first_part=first_part, numpart=numpart, softening=softening,
			pos=pos, rho_avg=rho_avg
		)
		data = particle_data
	print0(f'\t〈ρ〉 = {rho_avg}')
	t1 = time.perf_counter()
	print0(f'\tTook {t1 - t0} sec.')
	# enforce minimum FoF length and select halos
	fof_len_mask = groups.tot_len
	halo_mask = (
		(groups_nr >= args.halo_id_offset) &
		(fof_len_mask >= args.fof_group_min_len)
	)
	halo_ids = groups_nr[halo_mask]
	selected_halos = collections.namedtuple(
		'HaloData', 'ids, halos, first_particle'
	)(
		ids=halo_ids,
		halos=groups[halo_ids],
		first_particle=groups_first_particle[halo_ids],
	)
	# initialize M_200 DataFrame
	m200_data = pandas.DataFrame(
		numpy.zeros(len(selected_halos.ids), dtype=m200_util.M200_DTYPE)
	)
	m200_data['id'] = halo_ids
	m200_data['fof_len'] = selected_halos.halos.tot_len
	m200_data['m_fof_ms'] = (
		selected_halos.halos.tot_mass * units.mass_in_solar_mass
	)
	calc_cols = ['m200_ms', 'r200_kpc', 'cx_kpc', 'cy_kpc', 'cz_kpc']
	for col in calc_cols:
		m200_data[col] = numpy.nan
	halo_offset = 0
	if args.resume:
		try:
			existing_data = pandas.read_csv(m200_file)
			for col in calc_cols:
				if col in existing_data:
					m200_data[col] = existing_data[col]
			# skip M_200 values which are already present
			halo_offset = len(m200_data.dropna())
		except IOError as e:
			print0(
				f'Note: Error reading existing M_200 file {m200_file}:\n'
				f'\t{e}'
			)
	print0(
		f'Processing {len(selected_halos.ids)} halos '
		f'(min. length = {args.fof_group_min_len})'
	)
	# determine M_200 for each selected halo
	halos_processed = 0
	for halo_idx in range(halo_offset, len(selected_halos.ids)):
		process_halo(
			snap, fof_tab_files, selected_halos, halo_idx, m200_data, data,
			slab_to_task, units
		)
		# sanity check for M_200 result/data
		if m200_data is not None:
			halo_mass_ratio = (
				m200_data.loc[halo_idx, 'm200_ms'] /
				m200_data.loc[halo_idx, 'm_fof_ms']
			)
			if not (1/2 < halo_mass_ratio < 1.5):
				m200_warnings.append((path, m200_data.loc[halo_idx]))
			assert halo_mass_ratio < 1e3
		# potentially save current M_200 progress
		halos_processed += 1
		if mpi_rank == 0 and halos_processed % args.m200_save_interval == 0:
			wrote_csv, msg = m200_util.save_m200_csv_data(
				path, m200_data, overwrite=args.overwrite or wrote_csv
			)
			print0(msg)
		if check_max_runtime(START_WALL_TIME):
			return 'max-runtime'
	print0('=' * 50)
	print0()
	# save computed data
	if mpi_rank == 0:
		m200_util.save_m200_csv_data(
			path, m200_data, overwrite=args.overwrite or wrote_csv
		)
		# write warnings to a log file
		warnings = [w[1] for w in m200_warnings if w[0] == path]
		with open(f'{path}_warnings', 'wt') as warn_file:
			print0(f'halo_offset = {halo_offset}', file=warn_file)
			for warning in warnings:
				print0(
					f'ID {int(warning["id"])}, '
					f'M_FoF = {warning["m_fof_ms"]:e} M☉/h, '
					f'M_200 = {warning["m200_ms"]:e} M☉/h',
					file=warn_file
				)
		# write warnings to console
		warnings = [w for w in m200_warnings if w[1]['m200_ms'] > 0]
		if warnings:
			print0()
			print0(
				'WARNING: Discrepancies between M_FoF and M_200 in the '
				'following halos!'
			)
			for warning_path, warning in warnings:
				assert warning_path == path
				print0(
					f'ID {int(warning["id"])}, '
					f'M_FoF = {warning["m_fof_ms"]:e} M☉/h, '
					f'M_200 = {warning["m200_ms"]:e} M☉/h'
				)
	return 'normal'

def process_halo(
	snap, fof_tab_files, halos, halo_idx, m200_data, data, slab_to_task, units
):
	halo_id = halos.ids[halo_idx]
	halo = halos.halos[halo_idx]
	assert args.halo_id_offset + halo_idx == halo_id
	print0(
		f'Processing halo {halo_id} (M_FoF = '
		f'{m200_data.loc[halo_idx, "m_fof_ms"]:e} M☉/h)'
	)
	print0(f'\t{halo}')
	# get IDs for this group from rank 0
	global group_gids, group_gids_range
	gids = None
	if mpi_rank == 0:
		if group_gids and halo_id not in group_gids_range:
			# read more IDs if necessary
			print0(
				f'\tIDs for halo {halo_id} are not loaded '
				f'({min(group_gids_range)} to {max(group_gids_range)}) – '
				'loading more IDs'
			)
			*_, group_gids = read_hdf5.read_fof_tab(
				fof_tab_files, read_ids=True, start_group=halo_id,
				max_data_size=args.group_id_data_size_limit
			)
			del _
			group_gids_range = range(halo_id, halo_id + len(group_gids))
			group_gids_mem = sum(gids.nbytes for gids in group_gids)
			print0(
				f'\tRead ID data for groups {min(group_gids_range)} to '
				f'{max(group_gids_range)} ({group_gids_mem / 1024**2} MiB)'
			)
			assert all(
				numpy.all(g < snap.pmgrid**3) for g in group_gids
			), [
				(len(g), len(g[g >= snap.pmgrid**3]), g[g >= snap.pmgrid**3])
				for g in group_gids
			]
		gids = (
			group_gids[halo_id - group_gids_range[0]] if group_gids else None
		)
		# IDs should already be sorted, but make sure
		gids.sort()
	have_gids = False
	have_gids = mpi_comm.bcast(gids is not None, root=0)
	if have_gids:
		gids = scatter_halo_ids(snap.pmgrid, slab_to_task, gids)
		gc.collect()
	# process grid data
	if get_type(snap.types) == 'grid':
		assert gids is None or numpy.all(gids < snap.pmgrid**3), (
			gids[gids >= snap.pmgrid**3]
		)
		m200, r200, halo_center = process_grid(
			snap, fof_tab_files, halo_idx, halo, m200_data, data, gids
		)
	# process particle data
	elif get_type(snap.types) == 'particles':
		m200, r200, halo_center = process_particles(
			snap, fof_tab_files, halo_idx, halo,
			halos.first_particle[halo_idx], m200_data, data
		)
	else: assert False
	# store results
	m200_data.loc[halo_idx, 'm200_ms'] = m200 * units.mass_in_solar_mass
	m200_data.loc[halo_idx, 'r200_kpc'] = r200 * units.length_in_kpc
	m200_data.loc[halo_idx, 'cx_kpc'] = halo_center[0] * units.length_in_kpc
	m200_data.loc[halo_idx, 'cy_kpc'] = halo_center[1] * units.length_in_kpc
	m200_data.loc[halo_idx, 'cz_kpc'] = halo_center[2] * units.length_in_kpc
	print0(
		f'\tM_200 = {m200_data.loc[halo_idx, "m200_ms"]:e} M☉/h, '
		f'R_200 = {m200_data.loc[halo_idx, "r200_kpc"]:g} kpc/h'
	)
	print0()

def process_grid(
	snap, fof_tab_files, halo_idx, halo, m200_data, grid_data, gids
):
	cell_ids = gids
	box_size = snap.header['BoxSize']
	cell_len = box_size / snap.pmgrid
	# determine halo center by finding the densest cell within the FoF group
	print0('Determining halo center…')
	t0 = time.perf_counter()
	# get indices of FoF cells
	cells_fof_l3 = numpy.array(numpy.unravel_index(
		cell_ids, (snap.pmgrid,) * 3)
	)
	cell_x = cells_fof_l3[0]
	cells_fof_l3 = cells_fof_l3[
		:,
		(grid_data.first_slab_x <= cell_x) &
		(cell_x < grid_data.first_slab_x + grid_data.nslab_x)
	]
	cells_fof_l3[0] -= grid_data.first_slab_x
	density_max = 0
	density_max_g3 = numpy.array([-1] * 3)
	if cells_fof_l3.shape[1] > 0:
		# find the densest cell within the FoF group on this rank
		rho_fof = grid_data.rho[tuple(cells_fof_l3)]
		max_idx = numpy.argmax(rho_fof)
		density_max = rho_fof[max_idx]
		density_max_g3 = cells_fof_l3[:, max_idx].copy()
		density_max_g3[0] += grid_data.first_slab_x
	# combine results globally
	density_max = mpi_comm.allgather(density_max)
	density_max_g3 = mpi_comm.allgather(density_max_g3)
	max_rank = numpy.argmax(density_max)
	halo_center_g3 = density_max_g3[max_rank]
	halo_center = halo_center_g3 * cell_len
	print0(f'\tHalo center is at {halo_center_g3} ({halo_center}),')
	density_max_rel = density_max[max_rank] / (
		grid_data.rho_avg * m200_util.VIRIAL_OVERDENSITY_FACTOR
	)
	print0(f'\t\tρ/ρ_200 = {density_max_rel:g}')
	print0(f'\t(CoM: {halo.cm})')
	t1 = time.perf_counter()
	print0(f'\tTook {t1 - t0} sec.')
	# calculate M_200
	if args.only_halo_center:
		print0('Skipping M_200 calculation')
		m200 = m200_data.loc[halo_idx, 'm200_ms']
		r200 = m200_data.loc[halo_idx, 'r200_kpc']
	else:
		print0('Calculating M_200…')
		t0 = time.perf_counter()
		if density_max_rel >= 1:
			m200, r200, r200_estimate = calculate_m200_grid(
				grid_data, snap, halo_center_g3, halo.tot_mass
			)
			print0(f'\tFirst estimate for R_200: {r200_estimate:g}')
		# discard this halo if none of the FoF cells have a density above
		# ρ_200 to avoid double counting
		else:
			print0('\tNo FoF cell with ρ ≥ ρ_200!')
			m200 = r200 = 0.
	t1 = time.perf_counter()
	print0(f'\tTook {t1 - t0} sec.')
	return m200, r200, halo_center

# Note: This is the same implementation as in AREPO’s fof.c. This method only
# works reliably if the FoF group spans less than half (?) the box size
# (which should normally be the case).
def center_of_mass_particles(pos, mass, box_size, firstpos=None):
	if firstpos is None:
		firstpos = mpi_comm.bcast(pos[0], root=0)
	pos_relative_to_firstpos = bin_density.dist_periodic_wrap_vec(
		box_size, pos - firstpos
	)
	# all particles have the same mass here, so the mass cancels
	center_sum_l = numpy.sum(pos_relative_to_firstpos, axis=0)
	center_num_l = len(pos)
	center_sum_g = mpi_comm.allreduce(center_sum_l, op=MPI.SUM)
	center_num_g = mpi_comm.allreduce(center_num_l, op=MPI.SUM)
	com = bin_density.coord_periodic_wrap_vec(
		box_size, center_sum_g / center_num_g + firstpos
	) if center_num_g != 0 else firstpos
	return com, center_num_g, pos_relative_to_firstpos

def process_particles(
	snap, fof_tab_files, halo_idx, halo, halo_first_particle, m200_data,
	particle_data
):
	numpart_total = read_snapshot.numpart_total(snap.header)
	box_size = snap.header['BoxSize']
	particle_mass = snap.header['MassTable'][PARTICLE_TYPE]
	# read particles within the FoF group
	part_chunks = snap.determine_chunks(
		mpi_rank, mpi_size, num_slabs=halo.tot_len, slab_size=1
	)
	if part_chunks:
		halo_first_part_thistask = part_chunks[0][0] + halo_first_particle
		halo_numpart_thistask = part_chunks[-1][-1] - part_chunks[0][0]
		halo_part_slice = numpy.s_[
			halo_first_part_thistask:
			halo_first_part_thistask + halo_numpart_thistask
		]
		halo_particle_pos = snap.read_snapshot(
			f'/PartType{PARTICLE_TYPE}/Coordinates', data_slice=halo_part_slice
		)
	else:
		# more particles than number of tasks
		halo_particle_pos = numpy.zeros((0, 3))
	# find the halo’s center using the shrinking sphere method
	# TODO: skip this if halo center is known in m200_data?
	print0('Determining halo center…')
	t0 = time.perf_counter()
	pos = halo_particle_pos
	# use start radius which includes all FoF particles
	radius = box_size / 2
	halo_center, num_particles_g, _ = center_of_mass_particles(
		pos, particle_mass, box_size, firstpos=halo.cm
	)
	assert numpy.allclose(
		halo_center, halo.cm, rtol=1e-2, atol=1e-3
	), f'{halo_center}, {halo.cm}'
	num_iterations = 0
	while num_particles_g > 1:
		# discard particles outside the sphere radius
		if len(pos) > 0:
			distances_vec = bin_density.dist_periodic_wrap_vec(
				box_size, pos - halo_center
			)
			distances2 = numexpr.evaluate('sum(distances_vec**2, axis=1)')
			# this would change the shape of pos if len(pos) == 0
			pos = pos[distances2 < radius**2]
		# new center is the center of mass of the remaining particles
		halo_center, num_particles_g, _ = center_of_mass_particles(
			pos, particle_mass, box_size, firstpos=halo_center
		)
		# shrink sphere
		radius *= HALO_CENTER_SPHERE_FACTOR
		num_iterations += 1
	print0(f'\tPerformed {num_iterations} shrinking sphere iterations.')
	assert not numpy.any(numpy.isnan(halo_center))
	print0(f'\tHalo center is at {halo_center}')
	print0(f'\t(CoM: {halo.cm})')
	t1 = time.perf_counter()
	print0(f'\tTook {t1 - t0} sec.')
	# calculate M_200
	if args.only_halo_center:
		print0('Skipping M_200 calculation')
		m200 = m200_data.loc[halo_idx, 'm200_ms']
		r200 = m200_data.loc[halo_idx, 'r200_kpc']
	else:
		print0('Calculating M_200…')
		t0 = time.perf_counter()
		# spherically averaged densities
		m200, r200, r200_estimate = calculate_m200_particles(
			particle_data, particle_mass, numpart_total, box_size, halo_center,
			halo.total_mass
		)
		print0(f'\tFirst estimate for R_200: {r200_estimate:g}')
	t1 = time.perf_counter()
	print0(f'\tTook {t1 - t0} sec.')
	return m200, r200, halo_center

def check_max_runtime(start_wall_time):
	current_wall_time = time.perf_counter()
	stop_run = current_wall_time - start_wall_time > args.max_runtime
	if stop_run:
		print0(f'Exceeded maximum runtime of {args.max_runtime}s; terminating')
	return stop_run

if __name__ == '__main__':
	main()
