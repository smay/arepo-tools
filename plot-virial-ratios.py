#!/usr/bin/env python3
import argparse
import sys
from pathlib import Path
import h5py
import numpy
import matplotlib.pyplot as plt
import pandas
import scipy.interpolate
import scipy.optimize
import m200_util
import read_snapshot
import read_snapshot.hdf5 as read_hdf5
import units_util
import matplotlib_settings


parser = argparse.ArgumentParser(description='Plot the virial ratios -2T/V '
	'for fuzzy dark matter group catalog')
parser.add_argument('--min-radius', type=float,
	help='only halos with R_200 larger than this radius will be included')
parser.add_argument('--only-ids', type=int, nargs='+',
	help='only include halos with the given IDs for the corresponding file '
		'(assumes file name <path>_keep.npy with a boolean array of which '
		'halos to keep; e.g. to discard overlapping halos as found by '
		'find-duplicate-halos.py; default: false)')
parser.add_argument('--mark-ids', nargs='+',
	help='like --only-ids, but instead mark the given haloes with the given '
		'symbol while still plotting all haloes (default: off)')
parser.add_argument('--mark-max-ratio', type=float,
	help='shade vertical region in plot above given value for the ratio')
parser.add_argument('--mark-bound', nargs='+',
	help='mark gravitationally bound halos (i.e. -2T/V ≥ 2) for the '
		'corresponding file with a different symbol (default: off)')
parser.add_argument('--mark-bound-labels', nargs='+',
	help='legend labels for --mark-bound')
parser.add_argument('--x-variable', choices=('m200'), default='m200',
	help='variable to use on the horizontal axis (default: %(default)s)')
parser.add_argument('--line-styles', nargs='+',
	help='line styles for the given plots')
parser.add_argument('--title', help='a title for the plot')
parser.add_argument('--no-title', action='store_true',
	help='disable plot title')
parser.add_argument('--no-legend', action='store_true',
	help='disable plot legend')
parser.add_argument('--labels', nargs='+',
	help='legend labels (used instead of automatic legend)')
parser.add_argument('--xlim', nargs=2, type=float, default=(1e7, 1e14),
	help='the mass range to plot (default: %(default)s)')
parser.add_argument('--ylim', nargs=2, type=float, default=(0.8, 2.5),
	help='the mass function range to plot')
parser.add_argument('--plot-file-name', help='override output file name')
parser.add_argument('--plot-format', default='png',
	help='the file format of the generated plot (default: %(default)s)')
parser.add_argument('--dpi', type=int, default=200,
	help='the resolution of the generated plot (default: %(default)d)')
parser.add_argument('--figsize', type=float, nargs=2,
	help='the figure size in inches')
parser.add_argument('path', nargs='+', type=Path,
	help='the paths to the fof_tab files; any normal files are interpreted '
		'as single files, while all files in any given directory are '
		'interpreted as a single multi-file output (also assumes file name '
		'<path>_halo_energy.hdf5 to be present)')
args = parser.parse_args()


#TODO: de-duplicate these formatting functions
def float_to_scientific_notation(x, digits=None, integer=True):
	sign = '-' if x < 0 else ''
	exponent = int(numpy.floor(numpy.log10(numpy.abs(x))))
	significand = x / 10**exponent
	if numpy.isclose(significand, 1):
		return fr'{sign} 10^{{{exponent}}}'
	else:
		if integer and numpy.isclose(significand / round(significand), 1):
			significand = round(significand)
		if digits is None:
			significand = f'{significand}'
		else:
			significand = f'{significand:.{digits}f}'
		return fr'{sign}{significand} \times 10^{{{exponent}}}'


def main():
	# set up figure
	if args.figsize:
		plt.rc('figure', figsize=args.figsize)
	plt.figure()
	# mark ratio 1.0
	plt.axhline(1.0, linestyle='-', linewidth=0.2, color='gray')
	# mark bound/unbound line (ratio 2.0)
	plt.axhline(2.0, linestyle='--', linewidth=0.2, color='gray')
	# mark excluded area
	if args.mark_max_ratio:
		plt.fill_between(
			[0.9 * args.xlim[0], 1.1 * args.xlim[1]], args.mark_max_ratio,
			1.1 * args.ylim[1], linewidth=0.4, facecolor='none', hatch='//'
		)
	# process command line arguments
	if not args.labels:
		args.labels = [p.name for p in args.path]
	if not args.line_styles:
		args.line_styles = ['.'] * len(args.path)
	if not args.only_ids:
		args.only_ids = [0] * len(args.path)
	if not args.mark_ids:
		args.mark_ids = [''] * len(args.path)
	if not args.mark_bound:
		args.mark_bound = [''] * len(args.path)
	if not args.mark_bound_labels:
		args.mark_bound_labels = [''] * len(args.path)
	assert (
		len(args.path) == len(args.labels) == len(args.line_styles) ==
		len(args.only_ids) == len(args.mark_ids) == len(args.mark_bound) ==
		len(args.mark_bound_labels)
	)
	# process group catalogs
	z_check = None
	for path_args in zip(
		args.path, args.labels, args.line_styles, args.only_ids,
		args.mark_ids, args.mark_bound, args.mark_bound_labels,
	):
		z_check = process_path(*path_args, z_check)
	# axis limits
	plt.xlim(*args.xlim)
	plt.ylim(*args.ylim)
	# axis scale
	plt.xscale('log')
	# legend
	if not args.no_legend:
		plt.legend()
	# labels
	plt.xlabel(r'$M$ / $h^{-1} M_\odot$')
	plt.ylabel(r'$-2 T / V$')
	# title
	if not args.no_title:
		title = args.title if args.title else ''
		title += f'$z = {z_check:.2f}$'
		plt.title(title)
	# save plot
	savefig_opts = {}
	if args.plot_format != 'pdf':
		savefig_opts['dpi'] = args.dpi
	plot_file_name = f'virial_ratios_{z_check:.2f}'
	if args.plot_file_name:
		plot_file_name = args.plot_file_name
	plot_file_name += f'.{args.plot_format}'
	plt.savefig(plot_file_name, transparent=True, **savefig_opts)
	

def process_path(
	path, label, line_style, only_ids, mark_ids, mark_bound, mark_bound_label,
	z_check
):
	print(path)
	# read data
	fof_tab_files = read_snapshot.snapshot_files(path)
	fof_tab_data = read_hdf5.read_fof_tab(fof_tab_files, read_ids=False)
	[config, param, header], groups, _ = fof_tab_data
	units = units_util.get_units(param)
	z = 1 / header['Time'] - 1
	if z_check is None:
		z_check = z
	print(f'\tz = {z}')
	assert numpy.isclose(z, z_check, atol=1e-9, rtol=1e-2)
	# read data and plot
	m200_data = m200_util.read_m200_data(path, fof_data=fof_tab_data)
	g_mass = m200_data['m200_ms']
	r200_kpc = m200_data['r200_kpc']
	mask = g_mass > 0
	mask_orig = mask
	num_valid = numpy.sum(mask_orig)
	num_invalid = numpy.sum(~mask_orig)
	print(f'\tConsidering {num_valid} halos')
	if args.min_radius:
		min_radius_mask = r200_kpc > args.min_radius
		num_excluded = (
			numpy.sum(~(min_radius_mask & mask_orig)) - num_invalid
		)
		num_excluded_new = (
			numpy.sum(~(min_radius_mask & mask)) - numpy.sum(~mask)
		)
		mask &= min_radius_mask
		print(
			f'\tExcluded {num_excluded} halos with R_200 <= '
			f'{args.min_radius} kpc/h (of which {num_excluded_new} were '
			'not already excluded)'
		)
	if only_ids:
		keep = numpy.load(path.with_name(f'{path.name}_keep.npy'))
		assert len(mask) == len(keep)
		num_excluded = numpy.sum(~(keep & mask_orig)) - num_invalid
		num_excluded_new = numpy.sum(~(keep & mask)) - numpy.sum(~mask)
		mask &= keep
		print(
			f'\tExcluded {num_excluded} overlapping halos (of which '
			f'{num_excluded_new} were not already excluded)'
		)
	energy_kin = groups.kinegy + groups.gradegy
	energy_pot = groups.potegy
	assert energy_kin.shape == energy_pot.shape
	with numpy.errstate(divide='ignore'):
		virial_ratio = -2 * energy_kin / energy_pot
	if mark_bound:
		mask_bound = virial_ratio >= 2
		assert len(mask) == len(mask_bound)
	with numpy.errstate(divide='ignore'):
		virial_ratio = -2 * energy_kin / energy_pot
	num_remaining = numpy.sum(mask)
	print(f'\t{num_remaining} halos remain after filtering')
	# plot
	mask_lim = virial_ratio <= args.ylim[1]
	if mark_bound:
		m = mask & mask_lim & mask_bound
		if numpy.any(virial_ratio[m] < args.ylim[0]):
			print(f'\tWARNING: Some points are below the ylim range!')
		plt.plot(
			g_mass[m], virial_ratio[m], mark_bound, markersize=2.0,
			label=mark_bound_label
		)
		m = mask & mask_lim & ~mask_bound
	else:
		m = mask & mask_lim
		if numpy.any(
			numpy.isfinite(virial_ratio[m]) & (virial_ratio[m] < args.ylim[0])
		):
			print(f'\tWARNING: Some points are below the ylim range!')
	plt.plot(
		g_mass[m], virial_ratio[m], line_style, markersize=2.0, label=label
	)
#	for m, r in zip(g_mass[~mask_lim], virial_ratio[~mask_lim]):
#		plt.arrow(m, 0.96 * args.ylim[1], m, 0.99 * args.ylim[1])
	return z_check


if __name__ == '__main__':
	main()

