#!/usr/bin/env python3
import argparse
import io
import os.path
import re
from pathlib import Path

# see also prepare-config.py in AREPO

COMMENT_CHAR = '#'
ASSIGNMENT_CHAR = '='
INCLUDE_DIRECTIVE = '!include'
DUPLICATES_CHOICES = ('warn', 'ignore')
INPUT_FILE_SUFFIX = '_incl'
OUTPUT_FILE_SUFFIX = '_merged'

def read_config_file(
	path, duplicates='warn', sep=ASSIGNMENT_CHAR, comment_char=COMMENT_CHAR
):
	with open(path) as file:
		return read_config_file_f(
			file, duplicates=duplicates, sep=sep, comment_char=comment_char
		)

def read_config_file_f(
	file, duplicates='warn', sep=ASSIGNMENT_CHAR, comment_char=COMMENT_CHAR
):
	assert duplicates in DUPLICATES_CHOICES
	result = {}
	# Note that this method of parsing implies that there must always be
	# whitespace between any configuration option and a potential comment
	# following it!
	# Further, comments after a configuration option (on the same line) do
	# not technically need to be introduced by a special character;
	# everything after the first whitespace is simply thrown away. By
	# corollary, there must not be any whitespace between a configuration
	# option, the equals sign, and/or its value; configuration options
	# cannot contain any whitespace in general.
	for line_num, line in enumerate(file, start=1):
		fields = line.split()
		# ignore whitespace-only lines and comment lines
		if not fields or fields[0].startswith(comment_char):
			continue
		option = fields[0]
		# set key/value pairs in dict
		if option == INCLUDE_DIRECTIVE:
			if len(fields) < 2:
				warnings.warn(
					f'Found !include directive without value in “{path}”, '
					f'line {line_num}'
				)
				continue
			line_options = read_config(
				fields[1], duplicates=duplicates, sep=sep,
				comment_char=comment_char
			)
		else:
			subfields = option.split(sep)
			if len(subfields) < 2:
				subfields.append(None)
			sub_key, sub_val = subfields[:2]
			line_options = {sub_key: sub_val}
		if duplicates == 'warn':
			for key in line_options:
				if key in result:
					warnings.warn(
						f'Config option “{key}” is multiply defined\n'
						f'(old value: “{result[key]}”, new value: '
							f'“{line_options[key]}”)\n'
						f'in “{path}”, line {line_num}'
					)
		result.update(line_options)
	return result

def write_config_file(path, config, sep=ASSIGNMENT_CHAR):
	with open(path, 'w') as f:
		for key, value in config.items():
			line = f'{key}'
			if value is not None:
				line += f'{sep}{value}'
			line += '\n'
			f.write(line)

def read_ngenic_config_file(
	path, duplicates='warn', sep=ASSIGNMENT_CHAR, comment_char=COMMENT_CHAR
):
	data = Path(path).read_text()
	data = re.sub(r'OPT\s*\+=\s*-D', '', data)
	return read_config_file_f(
		io.StringIO(data), duplicates=duplicates, sep=sep,
		comment_char=comment_char
	)

def write_ngenic_config_file(path, config, sep=ASSIGNMENT_CHAR):
	config = {
		f'OPT += -D{key}': val for key, val in config.items()
	}
	write_config_file(path, config, sep=sep)


# if called as a program (not loaded as a module)
if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='read GADGET/AREPO (and '
		'N-GenIC) configuration files and process included files')
	parser.add_argument('--config-type', choices=('GADGET', 'N-GenIC'),
		default='GADGET',
		help='Config file type to process')
	parser.add_argument('--duplicates', choices=DUPLICATES_CHOICES,
		default='warn',
		help='how to treat multiply defined options (note: currently, the '
			'last-specified value “wins” regardless of this option’s value)')
	parser.add_argument('--print', action='store_true',
		help='print each processed file')
	parser.add_argument('--output-suffix',
		help=f'suffix to append to the output file name (default: strip '
			f'“{INPUT_FILE_SUFFIX}” if input file name ends with it, else '
			f'append “{OUTPUT_FILE_SUFFIX}”))')
	parser.add_argument('path', nargs='+', type=Path,
		help='the paths to the config files')
	args = parser.parse_args()

	for path in args.path:
		print(path)
		if args.config_type == 'GADGET':
			config = read_config_file(path, args.duplicates)
		elif args.config_type == 'N-GenIC':
			config = read_ngenic_config_file(path, args.duplicates)
		else: assert False
		if args.print:
			for key, value in config.items():
				line = f'{key}'
				if value is not None:
					line += f'={value}'
				print(line)
			print()
		# determine output path and write to output file
		stem = path.stem
		ext = path.suffix
		if args.output_suffix is None:
			if stem.endswith(INPUT_FILE_SUFFIX):
				stem = stem[:-len(INPUT_FILE_SUFFIX)]
			else:
				stem += OUTPUT_FILE_SUFFIX
		else:
			stem += args.output_suffix
		if args.config_type == 'GADGET':
			write_config_file(path.with_name(stem + ext), config)
		elif args.config_type == 'N-GenIC':
			write_ngenic_config_file(path.with_name(stem + ext), config)
		else: assert False
