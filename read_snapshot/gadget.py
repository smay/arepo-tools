import collections.abc
import gc
import numpy
from . import common

BYTE_BITS = 8
N_TYPES = 6
PARTICLE_TYPE_GAS = 0
HEADER_LEN = 256
HEADER_DTYPE_LIST = [
	('npart',                 '6u4'),
	('mass',                  '6f8'),
	('time',                   'f8'),
	('redshift',               'f8'),
	('flag_sfr',               'i4'),
	('flag_feedback',          'i4'),
	('npartTotal',            '6u4'),
	('flag_cooling',           'i4'),
	('num_files',              'i4'),
	('BoxSize',                'f8'),
	('Omega0',                 'f8'),
	('OmegaLambda',            'f8'),
	('HubbleParam',            'f8'),
	('flag_stellarage',        'i4'),
	('flag_metals',            'i4'),
	('npartTotalHighWord',    '6u4'),
	('flag_entropy_instead_u', 'i4'),
]
HEADER_GADGET_TO_AREPO = {
	'npart':              'NumPart_ThisFile',
	'mass':               'MassTable',
	'time':               'Time',
	'redshift':           'Redshift',
	'flag_sfr':           'Flag_Sfr',
	'flag_feedback':      'Flag_Feedback',
	'npartTotal':         'NumPart_Total',
	'flag_cooling':       'Flag_Cooling',
	'num_files':          'NumFilesPerSnapshot',
	'BoxSize':            'BoxSize',
	'Omega0':             'Omega0',
	'OmegaLambda':        'OmegaLambda',
	'HubbleParam':        'HubbleParam',
	'flag_stellarage':    'Flag_StellarAge',
	'flag_metals':        'Flag_Metals',
	'npartTotalHighWord': 'NumPart_Total_HighWord',
}
HEADER_DTYPE = numpy.dtype(HEADER_DTYPE_LIST)
PARTICLE_DTYPE_SINGLE = numpy.dtype([
	('type',     'i4'),
	('pos',     '3f4'),
	('vel',     '3f4'),
	('id',       'u8'),
	('mass',     'f4'),
	('U',        'f4'),
	('rho',      'f4'),
	('temp',     'f4'),
	('Ne',       'f4'),
	('num_file', 'u4'),
])
PARTICLE_DTYPE_DOUBLE = numpy.dtype([
	('type',     'i4'),
	('pos',     '3f8'),
	('vel',     '3f8'),
	('id',       'u8'),
	('mass',     'f8'),
	('U',        'f8'),
	('rho',      'f8'),
	('temp',     'f8'),
	('Ne',       'f8'),
	('num_file', 'u4'),
])

def read_block_len(snapshot_file):
	a = numpy.fromfile(snapshot_file, 'u4', 1)
	assert len(a) in (0, 1)
	return a[0] if len(a) == 1 else None

#TODO: use seek instead of fromfile to skip data in file (also below)
def read_standard_block(
	snap_part, header, block_data, dtype, ptype, npart_read, npart_all
):
	block_len_1 = read_block_len(snap_part)
	offset = npart_read
	for pt in range(N_TYPES):
		npart = header.npart[pt]
		file_data = numpy.fromfile(snap_part, dtype, npart)
		if pt in ptype:
			block_data[offset:offset + npart] = file_data
		del file_data
		gc.collect()
		offset += npart
	block_len_2 = read_block_len(snap_part)
	block_len_expected = numpy.dtype(dtype).itemsize * npart_all
	assert block_len_1 == block_len_2 == block_len_expected, (
		f'{block_len_1}, {block_len_2}, {block_len_expected}'
	)

def header_array_to_dict(header):
	result = {
		key_arepo: header[key_gadget]
		for key_gadget, key_arepo in HEADER_GADGET_TO_AREPO.items()
	}
	return result


def read_header_array(snapshot_files, single_file=False):
	# the longids or double_precision settings don’t affect the header
	header, _ = read_snapshot(snapshot_files, read_particles=False)
	return header

def read_header(snap_part):
	block_len_1 = read_block_len(snap_part)
	header = numpy.rec.array(
		numpy.fromfile(snap_part, HEADER_DTYPE, 1)
	)[0]
	# skip padding
	snap_part.read(HEADER_LEN - HEADER_DTYPE.itemsize)
	block_len_2 = read_block_len(snap_part)
	assert block_len_1 == block_len_2 == HEADER_LEN
	return header

def read_snapshot(
	snapshot_files, longids=False, double_precision=False, read_particles=True,
	ptype=range(N_TYPES), single_file=False
):
	assert snapshot_files
	assert not single_file or len(snapshot_files) == 1
	if not isinstance(ptype, collections.abc.Iterable):
		ptype = [ptype]
	if double_precision:
		particle_dtype = PARTICLE_DTYPE_DOUBLE
		float_dtype = 'f8'
	else:
		particle_dtype = PARTICLE_DTYPE_SINGLE
		float_dtype = 'f4'
	first_header = None
	particles = None
	npart_read = 0
	npart_seen = 0
	for num_file, file_name in enumerate(
		sorted(snapshot_files, key=common.arepo_filename_key)
	):
		with open(file_name, 'rb') as snap_part:
			# block ID 1: HEAD
			header = read_header(snap_part)
			if first_header is None:
				first_header = header
				first_header_check = header.copy()
				first_header_check.npart = 0
				assert single_file or (
					first_header.num_files == len(snapshot_files)
				), f'{first_header.num_files=} != {len(snapshot_files)=}'
				npart_total_all = common.numpart_total(
					header_array_to_dict(header)
				)
				if read_particles:
					if single_file:
						npart_total_ptype = 0
						for pt in ptype:
							npart_total_ptype += header.npart[pt]
					else:
						npart_total_ptype = common.numpart_total(
							header_array_to_dict(header), ptype=ptype
						)
					particles = numpy.rec.array(numpy.full(
						npart_total_ptype, numpy.nan, dtype=particle_dtype
					))
			# consistency checks
			if not read_particles:
				header_check = header.copy()
				header_check.npart = 0
				assert first_header_check == header_check
				continue
			# read particle data
			npart_all = sum(header.npart)
			npart_ptypes = sum(header.npart[pt] for pt in ptype)
			assert npart_total_all >= npart_all
			assert not read_particles or npart_total_ptype >= npart_ptypes
			# particle types and fixed masses
			offset = npart_read
			for pt in range(N_TYPES):
				npart = header.npart[pt]
				if pt in ptype:
					particles.type[offset:offset + npart] = pt
					particles.mass[offset:offset + npart] = header.mass[pt]
				offset += npart
			# store which file a particle was contained in
			particles.num_file[npart_read:npart_read + npart_ptypes] = num_file
			# block ID 2: POS
			read_standard_block(
				snap_part, header, particles.pos, f'3{float_dtype}', ptype,
				npart_read, npart_all
			)
			# block ID 3: VEL
			read_standard_block(
				snap_part, header, particles.vel, f'3{float_dtype}', ptype,
				npart_read, npart_all
			)
			# block ID 4: ID
			id_dtype = 'u8' if longids else 'u4'
			read_standard_block(
				snap_part, header, particles.id, id_dtype, ptype, npart_read,
				npart_all
			)
			# block ID 5: MASS
			npart_withmasses_all = sum(
				header.npart[pt] for pt in range(N_TYPES)
				if header.mass[pt] == 0
			)
			npart_withmasses_ptype = sum(
				header.npart[pt] for pt in ptype
				if header.mass[pt] == 0
			)
			if npart_withmasses_all:
				block_len_1 = read_block_len(snap_part)
				mass_data = numpy.fromfile(
					snap_part, float_dtype, npart_withmasses_all
				)
				offset_p = npart_read
				offset_m = 0
				for pt in range(N_TYPES):
					npart = header.npart[pt]
					if pt in ptype:
						if header.mass[pt] == 0:
							particles.mass[offset_p:offset_p + npart] = (
								mass_data[offset_m:offset_m + npart]
							)
						offset_p += npart
					if header.mass[pt] == 0:
						offset_m += npart
				del mass_data
				gc.collect()
				block_len_2 = read_block_len(snap_part)
				block_len_expected = (
					numpy.dtype(float_dtype).itemsize * npart_withmasses_all
				)
				assert block_len_1 == block_len_2 == block_len_expected, (
					f'{block_len_1}, {block_len_2}, {block_len_expected}'
				)
			# gas blocks
			npart_gas = header.npart[PARTICLE_TYPE_GAS]
			if npart_gas and PARTICLE_TYPE_GAS in ptype:
				# block ID 6, 7, 8: U, RHO, HSML
				for col in 'U', 'rho', 'Ne':
					block_len_1 = read_block_len(snap_part)
					if block_len_1 is None:
						# file may not contain all gas blocks
						break
					getattr(particles, col)[npart_read:npart_read + npart_gas] = (
						numpy.fromfile(snap_part, float_dtype, npart_gas)
					)
					block_len_2 = read_block_len(snap_part)
					block_len_expected = (
						numpy.dtype(float_dtype).itemsize * npart_gas
					)
					assert block_len_1 == block_len_2 == block_len_expected, (
						f'{block_len_1}, {block_len_2}, {block_len_expected}'
					)
		# update current particle offset
		npart_read += npart_ptypes
		npart_seen += npart_all
	if read_particles:
		if single_file:
			assert npart_seen == npart_all, (
				f'{npart_seen} != {npart_all}'
			)
		else:
			assert npart_seen == npart_total_all, (
				f'{npart_seen} != {npart_total_all}'
			)
#		for block in ['type', 'pos', 'vel', 'id', 'mass', 'num_file']:
#			assert not numpy.any(numpy.isnan(particles[block]))
	return header_array_to_dict(first_header), particles

