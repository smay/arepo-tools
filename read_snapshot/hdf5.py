import h5py
import numba
import numba.typed
import numpy
import numpy.testing
from . import common

GROUP_DTYPE_SINGLE = numpy.dtype([
	('len',     '6u4'),
	('offset',  '6u8'),
	('mass',    '6f4'),
	('cm',      '3f4'),
	('pos',     '3f4'),
	('vel',     '3f4'),
	('tot_len',  'u4'),
	('tot_mass', 'f4'),
	('m200',     'f4'),
	('r200',     'f4'),
	('kinegy',   'f4'),
	('gradegy',  'f4'),
	('potegy',   'f4'),
	('num_file', 'u4'),
])

def read_metadata(snapshot_files, otype=common.OUTPUT_TYPES[0]):
	if otype == 'snapshot':
		meta, _ = read_snapshot(snapshot_files, None)
	elif otype == 'fof_tab':
		meta, _, _ = (
			read_fof_tab(snapshot_files, read_data=False, read_ids=False)
		)
	else: assert False
	return meta

# Note: A more advanced version of this functionality is available in the
#       Snapshot class!
def read_snapshot(snapshot_files, dataset, num_file=None):
	assert snapshot_files
	assert num_file is None or num_file < len(snapshot_files)
	result = None
	dataset_len = 0
	first_config = None
	first_param = None
	particle_data = False
	# determine total dataset length
	if dataset is not None:
		first_part = True
		for file_idx, file_name in enumerate(snapshot_files):
			with h5py.File(file_name, 'r') as snap_part:
				d = snap_part[dataset]
				if num_file is None:
					dataset_len += len(d)
				elif num_file == file_idx:
					dataset_len = len(d)
				dtype = d.dtype
				shape = d.shape
				if first_part:
					first_part = False
					dataset_dtype = dtype
					dataset_shape = shape
				assert dtype == dataset_dtype
				assert shape[1:] == dataset_shape[1:]
		dataset_shape = (dataset_len,) + dataset_shape[1:]
		particle_data = dataset.lstrip('/').startswith('PartType')
		if particle_data:
			ptype = int(dataset.lstrip('/').lstrip('PartType')[0])
		result = numpy.empty(dataset_shape, dtype=dataset_dtype)
	# read data
	len_read = 0
	first_part = True
	for file_idx, file_name in enumerate(snapshot_files):
		with h5py.File(file_name, 'r') as snap_part:
			config = snap_part.get('/Config')
			param = snap_part.get('/Parameters')
			header = snap_part['/Header']
			if first_part:
				first_part = False
				if config is not None:
					first_config = dict(config.attrs)
				if param is not None:
					first_param = dict(param.attrs)
				first_header = dict(header.attrs)
				del first_header['NumPart_ThisFile']
				first_header_check = dict(header.attrs)
				del first_header_check['NumPart_ThisFile']
				#TODO
#				if particle_data:
#					numpart_total = common.numpart_total(dict(header.attrs))
#					assert dataset_len == numpart_total
			# consistency checks
			header_check = dict(header.attrs)
			del header_check['NumPart_ThisFile']
			assert (
				header.attrs['NumFilesPerSnapshot'] == len(snapshot_files)
			), (
				'Mismatch of header with number of files: '
				f"{header.attrs['NumFilesPerSnapshot']} != "
				f"{len(snapshot_files)}"
			)
			if config is None:
				assert first_config is None
			else:
				numpy.testing.assert_equal(dict(config.attrs), first_config)
			if param is None:
				assert first_param is None
			else:
				numpy.testing.assert_equal(dict(param.attrs), first_param)
			numpy.testing.assert_equal(header_check, first_header_check)
			# read data
			if dataset is not None and (
				num_file is None or num_file == file_idx
			):
				d = snap_part[dataset]
				len_d = len(d)
				if particle_data:
					assert header.attrs['NumPart_ThisFile'][ptype] == len_d
				result[len_read:len_read + len_d] = d[...]
				len_read += len_d
	return (first_config, first_param, first_header), result

@numba.njit
def read_fof_ids(
	ids, groups, h5ids, curr_group, curr_id_in_group, start_group,
	max_data_size
):
	# determine size of ids list
	nbytes_ids = 0
	for gids in ids[1:]:
		nbytes_ids += len(gids) * gids.itemsize
	# start ID read loop
	curr_id_in_file = 0
	id_len_file = len(h5ids)
	while curr_id_in_file < id_len_file:
		# add 1 to account for dummy element in list “ids”
		lidx = curr_group + 1 - start_group
		group_len = groups.tot_len[curr_group]
		ids_left_in_group = group_len - curr_id_in_group
		ids_left_in_file = id_len_file - curr_id_in_file
		num_ids_to_read = min(ids_left_in_group, ids_left_in_file)
		# only read actual data after start group and while maximum data size
		# has not been reached, but always finish reading a started group
		read_data = curr_group >= start_group and (
			not max_data_size or nbytes_ids < max_data_size or lidx < len(ids)
			#TODO can this be made to work?
#			numpy.sum([len(gids) * gids.itemsize for gids in ids]) <
#				max_data_size
		)
		if read_data:
			if len(ids) == lidx:
				# start reading IDs for a new group
				ids.append(numpy.full(
					group_len, numpy.iinfo(numpy.uint64).max,
					dtype=numpy.uint64
				))
				nbytes_ids += group_len * ids[lidx].itemsize
			assert 0 < lidx < len(ids)
			ids[lidx][curr_id_in_group:curr_id_in_group + num_ids_to_read] = (
				h5ids[curr_id_in_file:curr_id_in_file + num_ids_to_read]
			)
		if curr_id_in_group + num_ids_to_read == group_len:
			# finished reading IDs for the current group
			assert not read_data or len(ids[lidx]) == group_len
			curr_group += 1
			curr_id_in_group = 0
		else:
			# file ended, but IDs for the current group are still incomplete
			curr_id_in_group += num_ids_to_read
		curr_id_in_file += num_ids_to_read
	assert curr_id_in_file == id_len_file
	return curr_group, curr_id_in_group

def check_fof_ids(ids, config):
	dummy = numpy.iinfo('u8').max
	assert not any(numpy.any(gids == dummy) for gids in ids)
	assert all(len(numpy.unique(gids)) == len(gids) for gids in ids)
	if 'FDM_PSEUDOSPECTRAL' in config:
		pmgrid = int(config['PMGRID'])
		assert all(numpy.all(gids < pmgrid**3) for gids in ids), [
			(
				group_idx, len(gids), len(gids[gids >= pmgrid**3]),
				gids, gids[gids < pmgrid**3], gids[gids >= pmgrid**3]
			) for group_idx, gids in enumerate(ids)
		]

def read_fof_tab(
	fof_tab_files, read_data=True, read_ids=True, dataset=None,
	read_auxiliary_files=False,
	# arguments limiting data size only apply to IDs
	start_group=0, max_data_size=0
):
	assert fof_tab_files
	assert not read_ids or (read_data and dataset is None)
	assert start_group >= 0
	assert max_data_size >= 0
	group_dtype = GROUP_DTYPE_SINGLE
	groups = None
	result = None
	# initialize list with dummy numpy array for numba typing
	if read_ids:
		ids = numba.typed.List()
		ids.append(numpy.array([numpy.iinfo('u8').max], dtype='u8'))
	else:
		ids = None
	curr_group = 0
	curr_group_for_ids = 0
	curr_id_in_group = 0
	tot_ids = 0
	ids_dataset_present = numpy.zeros(len(fof_tab_files), dtype='bool')
	first_part = True
	for num_file, file_name in enumerate(
		sorted(fof_tab_files, key=common.arepo_filename_key)
	):
		with h5py.File(file_name, 'r') as fof_tab_part:
			config = fof_tab_part['/Config']
			param = fof_tab_part['/Parameters']
			header = fof_tab_part['/Header']
			h5grp = fof_tab_part['/Group']
			h5ids = fof_tab_part['/IDs']
			if first_part:
				first_part = False
				first_config = dict(config.attrs)
				first_param = dict(param.attrs)
				first_header = dict(header.attrs)
				first_header_check = dict(header.attrs)
				del first_header_check['Ngroups_ThisFile']
				del first_header_check['Nsubgroups_ThisFile']
				del first_header_check['Nids_ThisFile']
				if dataset is None:
					#TODO: ~/.local/lib/python3.11/site-packages/numpy/core/numeric.py:330: RuntimeWarning: invalid value encountered in cast
					groups = numpy.rec.array(numpy.full(
						first_header['Ngroups_Total'], numpy.nan,
						dtype=group_dtype
					))
				else:
					assert dataset in fof_tab_part, dataset
					result = numpy.full(
						first_header['Ngroups_Total'], numpy.nan,
						dtype=fof_tab_part[dataset].dtype
					)
			# consistency checks
			header_check = dict(header.attrs)
			del header_check['Ngroups_ThisFile']
			del header_check['Nsubgroups_ThisFile']
			del header_check['Nids_ThisFile']
			assert header.attrs['NumFiles'] == len(fof_tab_files)
			numpy.testing.assert_equal(dict(config.attrs), first_config)
			numpy.testing.assert_equal(dict(param.attrs), first_param)
			numpy.testing.assert_equal(header_check, first_header_check)
			# read data
			ngroup = header.attrs['Ngroups_ThisFile']
			if read_data and len(h5grp) > 0 and dataset is None:
				groups.num_file[curr_group:curr_group + ngroup] = num_file
				groups.len[curr_group:curr_group + ngroup] = (
					h5grp['GroupLenType']
				)
				zeros = numpy.zeros_like(
					h5grp['GroupLenType'],
					shape=(1, *h5grp['GroupLenType'].shape[1:])
				)
				groups.offset[curr_group:curr_group + ngroup] = numpy.cumsum(
					numpy.concatenate([zeros, h5grp['GroupLenType'][...]])[:-1],
					axis=0, dtype='u8'
				) + (0 if curr_group == 0 else
					groups.offset[curr_group - 1] + groups.len[curr_group - 1]
				)
				groups.mass[curr_group:curr_group + ngroup] = (
					h5grp['GroupMassType']
				)
				groups.cm[curr_group:curr_group + ngroup] = h5grp['GroupCM']
				groups.pos[curr_group:curr_group + ngroup] = h5grp['GroupPos']
				groups.vel[curr_group:curr_group + ngroup] = h5grp['GroupVel']
				groups.tot_len[curr_group:curr_group + ngroup] = (
					h5grp['GroupLen']
				)
				groups.tot_mass[curr_group:curr_group + ngroup] = (
					h5grp['GroupMass']
				)
				if 'Group_M_Mean200' in h5grp:
					groups.m200[curr_group:curr_group + ngroup] = (
						h5grp['Group_M_Mean200']
					)
				if 'Group_R_Mean200' in h5grp:
					groups.r200[curr_group:curr_group + ngroup] = (
						h5grp['Group_R_Mean200']
					)
				if 'Group_Ekin_Mean200' in h5grp:
					groups.kinegy[curr_group:curr_group + ngroup] = (
						h5grp['Group_Ekin_Mean200']
					)
				if 'Group_Egrad_Mean200' in h5grp:
					groups.gradegy[curr_group:curr_group + ngroup] = (
						h5grp['Group_Egrad_Mean200']
					)
				if 'Group_Epot_Mean200' in h5grp:
					groups.potegy[curr_group:curr_group + ngroup] = (
						h5grp['Group_Epot_Mean200']
					)
				assert numpy.all(
					numpy.sum(h5grp['GroupLenType'], axis=1) ==
					h5grp['GroupLen']
				)
				assert numpy.all(numpy.isclose(
					numpy.sum(h5grp['GroupMassType'], axis=1),
					h5grp['GroupMass']
				))
			if dataset is not None and dataset in fof_tab_part:
				result[curr_group:curr_group + ngroup] = fof_tab_part[dataset]
			# read IDs
			ids_dataset_present[num_file] = 'ID' in h5ids
			if ids_dataset_present[num_file]:
				if read_ids:
					h5ids_a = h5ids['ID'][...]
					curr_group_for_ids, curr_id_in_group = read_fof_ids(
						ids, groups, h5ids_a, curr_group_for_ids,
						curr_id_in_group, start_group, max_data_size
					)
					del h5ids_a
				tot_ids += len(h5ids['ID'])
		# update current group offset
		curr_group += ngroup
	if not numpy.any(ids_dataset_present) and dataset is None:
		# skip check if the ID dataset was not present in any input file
		tot_ids = numpy.sum(groups.tot_len)
	assert curr_group == first_header['Ngroups_Total'], (
		f'{curr_group} != {first_header["Ngroups_Total"]}'
	)
	if read_data and dataset is None:
		assert numpy.sum(groups.tot_len) == tot_ids, (
			f'{numpy.sum(groups.tot_len)} != {tot_ids}'
		)
		# TODO: need snap header to verify
#		assert all(
#			s <= common.numpart_total(i)
#			for i, np in enumerate(numpy.sum(groups.len, axis=0, dtype='u8'))
#		)
		assert numpy.all(
			numpy.sum(groups.len, axis=0, dtype='u8') ==
			groups.offset[-1] + groups.len[-1]
		)
	if read_ids:
		# remove dummy element in list
		ids = ids[1:]
		assert len(ids) >= 1
		assert curr_group == curr_group_for_ids == len(groups), (
			f'{curr_group} {curr_group_for_ids} {len(groups)}'
		)
		assert start_group or max_data_size or len(groups) == len(ids), (
			f'{len(groups)} {len(ids)}'
		)
		range_num_ids = sum(len(gids) for gids in ids)
		range_tot_len = numpy.sum(
			groups.tot_len[start_group:start_group + len(ids)]
		)
		assert range_num_ids == range_tot_len, (
			f'{range_num_ids} ≠ {range_tot_len}'
		)
		if not (start_group or max_data_size):
			assert range_num_ids == tot_ids, f'{range_num_ids} ≠ {tot_ids}'
		ids = list(ids)
		check_fof_ids(ids, first_config)
	if groups is not None and len(groups) > 1:
		if len(fof_tab_files) > 1:
			base = fof_tab_files[0].parent
			if str(base) == '.':
				base = base.absolute()
		else:
			base = fof_tab_files[0]
		energy_file_path = base.with_name(f'{base.name}_halo_energy.hdf5')
		if read_auxiliary_files and energy_file_path.exists():
			with h5py.File(energy_file_path, 'r') as f:
				energy_kin = f['KineticEnergy'][...]
				energy_grad = f['QuantumEnergy'][...]
			assert len(energy_kin) == len(groups)
			assert len(energy_grad) == len(groups)
			groups.kinegy[...] = energy_kin
			groups.gradegy[...] = energy_grad
	if dataset is None:
		result = groups
	return (first_config, first_param, first_header), result, ids

