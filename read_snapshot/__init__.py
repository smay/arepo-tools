from . import gadget as snap_gadget
from . import hdf5 as snap_hdf5
from .common import *

class SnapshotError(Exception): pass

def snapshot_files(path, sort_paths=True):
	if not path.exists():
		raise SnapshotError(f'File/directory {path} does not exist')
	elif path.is_dir():
		dir_paths = [
			p for p in path.iterdir()
			if not p.is_dir() and not p.suffix == '.ewah'
		]
		if sort_paths:
			snap_files = sorted(dir_paths, key=arepo_filename_key)
		else:
			snap_files = dir_paths
		if not snap_files:
			raise SnapshotError(f'Directory {path} is empty')
	else:
		snap_files = [path]
	return snap_files

def read_metadata(snapshot_files, snapshot_format):
	# HDF5 snapshot
	if snapshot_format == 'hdf5':
		return snap_hdf5.read_metadata(snapshot_files)
	# GADGET snapshot
	elif snapshot_format == 'gadget':
		header = snap_gadget.read_header(snapshot_files)
		return None, None, header
	# numpy or raw binary files
	elif snapshot_format == 'npy' or snapshot_format == 'raw':
		return None, None, None
	else: assert False
