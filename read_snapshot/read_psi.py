import functools
import operator
import h5py
import numba
import numpy
from . import common
from . import gadget as snap_gadget
from . import hdf5 as snap_hdf5

FDM_PTYPE = 1

@numba.njit
def particles_to_grid(psi, coordinates, psi_snap, CELL_LEN):
	for i in range(len(coordinates)):
		# add 0.25 to prevent off-by-one error rounding down
		# this should work for both cases where the points are either in the
		# corners (<idx>.0) or in the centers (<idx>.5) of the cells
		idx_arr = (coordinates[i] / CELL_LEN + 0.25).astype(numpy.int64)
		idx = (idx_arr[0], idx_arr[1], idx_arr[2])
		psi[idx] = psi_snap[i]

def read_psi(
	snapshot_files, snapshot_format, PMGRID=None, num_file=None,
	output='wavefunction'
):
	assert output in ['wavefunction', 'density']
	config = param = header = None
	# HDF5 snapshot
	if snapshot_format == 'hdf5':
		# check if snapshot contains data as grid or in “particles”
		with h5py.File(snapshot_files[0], 'r') as first_snapshot:
			snapshot_grid = '/FuzzyDM' in first_snapshot
			snapshot_psi = '/FuzzyDM/PsiRe' in first_snapshot
			snapshot_rho_proj = '/FuzzyDM/RhoProjected' in first_snapshot
		# config and header
		config, param, header = snap_hdf5.read_metadata(snapshot_files)
		pmgrid = int(config['PMGRID'])
		if PMGRID is not None: assert pmgrid == PMGRID
		CELL_LEN = header['BoxSize'] / pmgrid
		# wave function as grid
		if snapshot_grid:
			if snapshot_psi:
				_, psi_re = snap_hdf5.read_snapshot(
					snapshot_files, '/FuzzyDM/PsiRe', num_file=num_file
				)
				num_cells = functools.reduce(operator.mul, psi_re.shape, 1)
				num_cells_re = num_cells
				assert num_cells % pmgrid**2 == 0
				assert num_file is not None or num_cells == pmgrid**3
				num_cells_x = num_cells // pmgrid**2
				if output == 'wavefunction':
					psi = numpy.zeros_like(psi_re, dtype='c16')
					psi.real = psi_re
				elif output == 'density':
					psi = psi_re**2
				del psi_re
				_, psi_im = snap_hdf5.read_snapshot(
					snapshot_files, '/FuzzyDM/PsiIm', num_file=num_file
				)
				num_cells_im = functools.reduce(operator.mul, psi_im.shape, 1)
				assert num_cells_re == num_cells_im
				if output == 'wavefunction':
					psi.imag = psi_im
				elif output == 'density':
					psi += psi_im**2
				del psi_im
				if len(psi.shape) == 1:
					psi = psi.reshape((num_cells_x,) + (pmgrid,) * 2)
			# Does the snapshot only contain the projected density?
			elif snapshot_rho_proj:
				_, rho_proj = snap_hdf5.read_snapshot(
					snapshot_files, '/FuzzyDM/RhoProjected'
				)
				assert rho_proj.shape == (pmgrid,) * 2
				psi = rho_proj
				header['RHO_PROJECTED'] = ''
			else: assert False
		# wave function in “particles”
		else:
			_, coordinates = snap_hdf5.read_snapshot(
				snapshot_files, f'/PartType{FDM_PTYPE}/Coordinates'
			)
			_, psi_re_snap = snap_hdf5.read_snapshot(
				snapshot_files, f'/PartType{FDM_PTYPE}/PsiRe'
			)
			_, psi_im_snap = snap_hdf5.read_snapshot(
				snapshot_files, f'/PartType{FDM_PTYPE}/PsiIm'
			)
			# consistency checks
			numpart_total = common.numpart_total(header)
			assert numpart_total == pmgrid**3 == len(coordinates) == \
				len(psi_re_snap) == len(psi_im_snap)
			psi = numpy.full((pmgrid,) * 3, numpy.nan, dtype='c16')
			particles_to_grid(
				psi, coordinates, psi_re_snap + 1j * psi_im_snap, CELL_LEN
			)
	# GADGET snapshot
	elif snapshot_format == 'gadget':
		header, particles = snap_gadget.read_snapshot(
			snapshot_files, longids=True
		)
		len_fdm_particles = len(particles[particles.type == FDM_PTYPE])
		if PMGRID is None:
			root_fdm_particles = len_fdm_particles**(1/3)
			PMGRID = round(root_fdm_particles)
			assert abs(PMGRID - root_fdm_particles) < 1e-2
		else:
			assert len(particles[particles.type == FDM_PTYPE]) == PMGRID**3
		CELL_LEN = header['BoxSize'] / PMGRID
		coordinates = particles.pos
		masses = particles.mass
		phases = particles.vel[:, 0]
		psi_snap = numpy.sqrt(masses / CELL_LEN**3) * numpy.exp(1j * phases)
		psi = numpy.full((PMGRID,) * 3, numpy.nan, dtype='c16')
		particles_to_grid(psi, coordinates, psi_snap, CELL_LEN)
	elif snapshot_format == 'npy':
		assert len(snapshot_files) == 1
		psi = numpy.load(snapshot_files[0])
	elif snapshot_format == 'raw':
		# TODO: read snapshots with more than one file
		assert len(snapshot_files) == 1
		assert PMGRID is not None
		psi = numpy.fromfile(
			str(snapshot_files[0]), dtype='c16'
		).reshape((PMGRID,) * 3)
	else:
		raise AssertionError
	assert not numpy.any(numpy.isnan(psi))
	return (config, param, header), psi

