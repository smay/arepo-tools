import collections.abc
import re
from pathlib import Path

OUTPUT_TYPES = ('snapshot', 'fof_tab')

def arepo_filename_key(s, num_re=re.compile(r'(\d+)')):
	if isinstance(s, Path):
		s = s.name
	return [
		int(text) if text.isdigit() else text.lower()
		for text in num_re.split(s)
	]

def output_path(path, config, param, kind='snapshot'):
	snapshot_base = _snapshot_name_base(param)
	fof_base = _fof_name_base(path, config)
	snap_format = int(param['SnapFormat'])
	suffix = '.hdf5' if snap_format == 3 and not path.is_dir() else ''
	if snapshot_base in path.name:
		snap_extra = re.fullmatch(
			fr'{re.escape(snapshot_base)}([-_](.*_)?)(\d+){suffix}', path.name
		)[1]
		snap_num = snapshot_number(path, param)
	elif fof_base in path.name:
		snap_extra = re.fullmatch(
			fr'{re.escape(fof_base)}([-_](.*_)?)(\d+){suffix}', path.name
		)[1]
		snap_num = snapshot_number_fof(path, config)
	else: assert False
	if snap_num is None:
		raise RuntimeError(f'Could not determine snap_num for path “{path}”')
	if kind == 'snapshot':
		result = path.with_name(f'{snapshot_base}_{snap_num:03}{suffix}')
		result_extra = path.with_name(
			f'{snapshot_base}{snap_extra}{snap_num:03}{suffix}'
		)
	elif kind == 'fof':
		result = path.with_name(f'{fof_base}_{snap_num:03}{suffix}')
		result_extra = path.with_name(
			f'{fof_base}{snap_extra}{snap_num:03}{suffix}'
		)
	else: assert False
	return (
		result if result.exists() or not result_extra.exists() else result_extra
	)

def snapshot_number(path, param):
	base = _snapshot_name_base(param)
	return _snapshot_number_impl(path, base)

def snapshot_number_fof(path, config):
	base = _fof_name_base(path, config)
	return _snapshot_number_impl(path, base)

def _snapshot_name_base(param):
	base = str(param['SnapshotFileBase'], 'utf-8')
	if param['NumFilesPerSnapshot'] > 1:
		base += 'dir'
	return base

def _fof_name_base(path, config):
	if path.is_dir():
		base = 'groups'
	elif 'SUBFIND' in config:
		base = 'fof_subhalo_tab'
	else:
		base = 'fof_tab'
	return base

def _snapshot_number_impl(path, base):
	name = path.name
	if base not in name:
		return None
	try:
		return int(re.fullmatch(
			fr'{re.escape(base)}[-_](.*_)?(\d+)(\.[^.]+)?', name
		)[2])
	except (TypeError, ValueError):
		return None

def numpart_total(header, ptype=None):
	if ptype is None:
		ptype_range = range(len(header['NumPart_Total']))
	elif isinstance(ptype, collections.abc.Iterable):
		ptype_range = ptype
	else:
		ptype_range = [ptype]
	num_bits = 8 * header['NumPart_Total'].itemsize
	result = sum(
		(int(header['NumPart_Total_HighWord'][ptype]) << num_bits) +
		int(header['NumPart_Total'][ptype])
		for ptype in ptype_range
	)
	return result

def get_run_name(path):
	p = path.resolve().parent
	if p.name == 'output':
		p = p.parent
	return p.name

def parse_run_name(path):
	run_name = get_run_name(path)
	re_int = r'\d+'
	re_float = r'\d+(\.\d+)?(e-?\d+)?'
	run_type = re.search(r'(^|_)(l?cdm|fdm)b?($|_)', run_name)[2]
	N = int(re.search(rf'(^|_)p({re_int})(-{re_int})?($|_)', run_name)[2])
	box_size = float(re.search(rf'(^|_)b({re_float})($|_)', run_name)[2])
	mass_result = re.search(rf'(^|_)m({re_float})($|_)', run_name)
	mass = float(mass_result[2]) if mass_result else None
	return run_type, N, box_size, mass
