#!/usr/bin/env python3
import argparse
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
import h5py as h5
import smoothing_library as SL
import Pk_library as PKL
import units_util
from snapshot import Snapshot


parser = argparse.ArgumentParser(description='Plot the density PDF of '
	'Schrödinger–Poisson simulations')
parser.add_argument('--plot-format', default='png',
	help='the file format of the generated plots (default: %(default)s)')
parser.add_argument('--dpi', type=float, default=200.,
	help='the resolution of the generated plots (default: %(default)s)')
parser.add_argument('path', type=Path, nargs='+',
	help='the paths to the snapshot files; any normal files are interpreted '
		'as single-file snapshots, while all files in any given directory '
		'are interpreted as a single multi-file snapshot')


def main():
	for snap_path in args.path:
		print(snap_path)
		snap = Snapshot(snap_path)
		units = units_util.get_units(snap.header)
		BoxSize = snap.box_size * 1e-3 * units.length_in_kpc  #Mpc/h
		grid    = snap.pmgrid

		Rlist   = [10., 15., 20.]  # Mpc/h, for accurate results the mesh is 1/10 of this
		Filter  = 'Top-Hat' # 'Top-Hat' or 'Gaussian'
		threads = 8

		W_k = {}
		for R in Rlist:
			W_k[R] = SL.FT_filter(BoxSize, R, grid, Filter, threads)

		rhomin=0
		rhomax=10
		Num_bins=1001
		drho=(rhomax-rhomin)/(Num_bins-1)
		bin_mids_m=np.linspace(rhomin,rhomax,Num_bins)
		# bin edges
		bins_m = np.linspace(rhomin-drho/2,rhomax+drho/2,Num_bins+1)

		# CREATE MATTER DENSITY FIELD
		density_m = (
			snap.read_snapshot('/FuzzyDM/PsiRe')**2 +
			snap.read_snapshot('/FuzzyDM/PsiIm')**2
		)
		density_m /= np.mean(density_m)

		# SMOOTH MATTER DENSITY FIELD
		for R_i, R in enumerate(Rlist):
			density_m_smoothed = SL.field_smoothing(
				density_m.astype(np.float32), W_k[R], threads
			)
			# make joint PDF
			hist, bin_edges = np.histogram(
				density_m_smoothed.flatten(), bins=bins_m, density=True
			)
			bin_mids = (bin_edges[:-1]+bin_edges[1:]) / 2
			plt.figure()
			plt.plot(bin_mids, hist)
			plt.xlim(rhomin, rhomax)
			plt.ylim(0, 1.5)
			plt.xlabel(r'$\rho$ / $\langle \rho \rangle$')
			plt.ylabel(r'PDF')
			plt.savefig(
				f'matter_PDF_linbins_R{R}.{args.plot_format}',
				dpi=args.dpi, bbox_inches='tight', transparent=True
			)
			np.savetxt(
				f'matter_PDF_linbins_R{R}.txt',
				np.column_stack((bin_mids, hist)),
				header='1+delta, P(delta)'
			)


if __name__ == '__main__':
	args = parser.parse_args()
	main()

