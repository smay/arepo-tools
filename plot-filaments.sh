#!/bin/sh
set -e

trap 'kill 0' EXIT


PARALLEL_MAX=10
nnice='nice -n +19'


get_filament_limits() {
python3 - "$@" <<'PYTHON'
import sys
import numpy
import bin_density
import io_util

path = sys.argv[1]
box_size = float(sys.argv[2])
fil_id = int(sys.argv[3])

ngrid, filaments = io_util.read_NDskl_ascii(path)
coord_fac = box_size / ngrid
filament = filaments[fil_id]
filament *= coord_fac

def get_filament_extent(box_size, filament, radius):
	#TODO: more optimal range by actually checking cylinders around the segments
	rmin = numpy.full(3, numpy.inf, dtype='f8')
	rmax = numpy.full_like(rmin, -numpy.inf)
	# use first filament point as a reference to handle periodic wrapping of
	# coordinates
	p0 = filament[0]
	it_range = numpy.array([-radius, radius])
	for p_orig in filament:
		p = bin_density.dist_periodic_wrap_vec(box_size, p_orig - p0)
		for i in range(3):
			for d in it_range:
				x = p[i] + d
				if x < rmin[i]:
					rmin[i] = x
				if x > rmax[i]:
					rmax[i] = x
	# transform back to original coordinate system
	rmin += p0
	rmax += p0
	for i in range(3):
		if rmax[i] >= box_size:
			rmin[i] -= box_size
			rmax[i] -= box_size
	return rmin, rmax

rmin, rmax = get_filament_extent(box_size, filament, 4 * box_size / 200)
for imin, imax in zip(rmin, rmax):
	print(f'{imin:.12f}', f'{imax:.12f}', end=' ')
PYTHON
}


get_filament_normals() {
python3 - "$@" <<'PYTHON'
import sys
import numpy
import bin_density
import io_util

DEFAULT_NORMAL = numpy.array([0., 0., 1.])
DEFAULT_NORMAL_BACKUP = numpy.array([0., 1., 0.])

path = sys.argv[1]
box_size = float(sys.argv[2])
fil_id = int(sys.argv[3])

ngrid, filaments = io_util.read_NDskl_ascii(path)
coord_fac = box_size / ngrid
fil = filaments[fil_id]
fil *= coord_fac
dp = bin_density.dist_periodic_wrap_vec(box_size, fil[-1] - fil[0])
dp_unit = dp / numpy.linalg.norm(dp)
normal1 = numpy.zeros_like(dp)
if numpy.isclose(numpy.abs(dp_unit @ DEFAULT_NORMAL), 1):
	normal1[...] = DEFAULT_NORMAL_BACKUP
else:
	normal1[...] = DEFAULT_NORMAL
normal1 -= normal1 @ dp_unit * dp_unit
normal1 /= numpy.linalg.norm(normal1)
normal2 = numpy.cross(dp, normal1)
for v in (normal1, normal2):
	for i in range(3):
		print(f'{v[i]:.12f}', end=' ')
PYTHON
}


get_filament_vec() {
python3 - "$@" <<'PYTHON'
import sys
import numpy
import bin_density
import io_util

path = sys.argv[1]
box_size = float(sys.argv[2])
fil_id = int(sys.argv[3])

ngrid, filaments = io_util.read_NDskl_ascii(path)
coord_fac = box_size / ngrid
fil = filaments[fil_id]
fil *= coord_fac
dp = bin_density.dist_periodic_wrap_vec(box_size, fil[-1] - fil[0])
for i in range(3):
	print(f'{dp[i]:.12f}', end=' ')
print(f'{numpy.linalg.norm(dp):.12f}', end=' ')
PYTHON
}


zero_pad() {
	python3 -c \
		'import sys; print(f"{int(sys.argv[2]):0{int(sys.argv[1])}}", end="")' \
		"$@"
}


path=~/ptmp/r-workspace/cdm_axionic_p2048_b10000_m7e-23/
#ndskl_path="$path/disperse/snapdir_010_cic260.fits_c5e-07.up.NDskl.S005.a.NDskl"
path_2=~/ptmp/r-workspace/fdm_axionic_p8640_b10000_m7e-23/
ndskl_path="$path_2/disperse/snapdir_008_downscale288.fits_c5e-07.up.NDskl.S005.a.NDskl"
echo '…'
~/arepo-tools/filament-straightness.py \
	--box-size 10000 --min-length 500 "$ndskl_path" \
	| tail -50
counter=0
#sort -n filtered-filaments.txt | while read -r fil_id; do
echo 52 | while read -r fil_id; do
#sort -n filament-pairs-2.txt | while read -r fil_id; do
	echo "Filament #$fil_id"
	lim=$(get_filament_limits "$ndskl_path" 10000 "$fil_id" -1)
	xmin=$(echo "$lim" | awk '{print $1}')
	xmax=$(echo "$lim" | awk '{print $2}')
	ymin=$(echo "$lim" | awk '{print $3}')
	ymax=$(echo "$lim" | awk '{print $4}')
	zmin=$(echo "$lim" | awk '{print $5}')
	zmax=$(echo "$lim" | awk '{print $6}')
	echo "xmin=$xmin" "xmax=$xmax"
	echo "ymin=$ymin" "ymax=$ymax"
	echo "zmin=$zmin" "zmax=$zmax"
	normals=$(get_filament_normals "$ndskl_path" 10000 "$fil_id")
	n1x=$(echo "$normals" | awk '{print $1}')
	n1y=$(echo "$normals" | awk '{print $2}')
	n1z=$(echo "$normals" | awk '{print $3}')
	n2x=$(echo "$normals" | awk '{print $4}')
	n2y=$(echo "$normals" | awk '{print $5}')
	n2z=$(echo "$normals" | awk '{print $6}')
	north=$(get_filament_vec "$ndskl_path" 10000 "$fil_id")
	north_x=$(echo "$north" | awk '{print $1}')
	north_y=$(echo "$north" | awk '{print $2}')
	north_z=$(echo "$north" | awk '{print $3}')
	north_len=$(echo "$north" | awk '{print $4}')
	height=$(echo "$north_len + 10." | bc -l)
	buff_height=$(echo "scale = 0; ($height * 1.5) / 1" | bc -l)
#	for normal_vec in 1 2; do
	for normal_vec in 1; do
		eval nx=\$n${normal_vec}x
		eval ny=\$n${normal_vec}y
		eval nz=\$n${normal_vec}z
		echo "normal = $nx $ny $nz"

		output_name="nbody-fdm_$(zero_pad 4 "$fil_id")_off_$normal_vec.pdf"
		num_files=$(set -- "$output_name"*; echo "$#")
		if [ "$num_files" -gt 1 ] || [ -f "$output_name"* ]; then
			echo 'Plot output(s) already exist, skipping!'
			echo "$output_name"*
			echo
			continue
		fi
		$nnice ~/arepo-tools/yt-off-axis-plot.py \
			--slice-x "$xmin" "$xmax" \
			--slice-y "$ymin" "$ymax" \
			--slice-z "$zmin" "$zmax" \
			--normal "$nx" "$ny" "$nz" \
			--north-vector "$north_x" "$north_y" "$north_z" \
			--width 300 "$height" --depth 50 --buff-size 500 "$buff_height" \
			--dpi 400 --output-path "$output_name" \
			--cic-path "nbody-fdm_$fil_id.h5" \
			-- "$path/snapdir_010/" \
			&
		echo
		# wait when maximum number of jobs is running in parallel
		counter=$((counter + 1))
		if [ "$counter" -eq "$PARALLEL_MAX" ]; then
			echo "Reached $PARALLEL_MAX parallel jobs; waiting for completion"
			wait
			echo "$PARALLEL_MAX parallel jobs completed; resuming iterations"
			counter=0
		fi
	done
	wait
done

path=~/ptmp/r-workspace/fdm_axionic_p8640_b10000_m7e-23/
path2=~/ptmp/r-workspace/fdm_p8640_b10000_m1e-22/
ndskl_path="$path/disperse/snapdir_008_downscale288.fits_c5e-07.up.NDskl.S005.a.NDskl"
echo '…'
~/arepo-tools/filament-straightness.py \
	--box-size 10000 --min-length 350 "$ndskl_path" \
	| tail -50
counter=0
#sort -n filtered-filaments.txt | while read -r fil_id; do
printf "%s\n" 52 | while read -r fil_id; do
	echo "Filament #$fil_id"
	lim=$(get_filament_limits "$ndskl_path" 10000 "$fil_id" -1)
	xmin=$(echo "$lim" | awk '{print $1}')
	xmax=$(echo "$lim" | awk '{print $2}')
	ymin=$(echo "$lim" | awk '{print $3}')
	ymax=$(echo "$lim" | awk '{print $4}')
	zmin=$(echo "$lim" | awk '{print $5}')
	zmax=$(echo "$lim" | awk '{print $6}')
	echo "xmin=$xmin" "xmax=$xmax"
	echo "ymin=$ymin" "ymax=$ymax"
	echo "zmin=$zmin" "zmax=$zmax"
	normals=$(get_filament_normals "$ndskl_path" 10000 "$fil_id")
	n1x=$(echo "$normals" | awk '{print $1}')
	n1y=$(echo "$normals" | awk '{print $2}')
	n1z=$(echo "$normals" | awk '{print $3}')
	n2x=$(echo "$normals" | awk '{print $4}')
	n2y=$(echo "$normals" | awk '{print $5}')
	n2z=$(echo "$normals" | awk '{print $6}')
	north=$(get_filament_vec "$ndskl_path" 10000 "$fil_id")
	north_x=$(echo "$north" | awk '{print $1}')
	north_y=$(echo "$north" | awk '{print $2}')
	north_z=$(echo "$north" | awk '{print $3}')
	north_len=$(echo "$north" | awk '{print $4}')
	height=$(echo "$north_len + 10." | bc -l)
	buff_height=$(echo "scale = 0; ($height * 1.5) / 1" | bc -l)
	for normal_vec in 1 2; do
#	for normal_vec in 0 1 2; do
		eval nx=\$n${normal_vec}x
		eval ny=\$n${normal_vec}y
		eval nz=\$n${normal_vec}z
		echo "normal = $nx $ny $nz"

		output_name="sp-cdm_$(zero_pad 4 "$fil_id")_off_$normal_vec.pdf"
		num_files=$(set -- "$output_name"*; echo "$#")
		if [ "$num_files" -gt 1 ] || [ -f "$output_name"* ]; then
			echo 'Plot output(s) already exist, skipping!'
			echo "$output_name"*
			echo
			continue
		fi
		$nnice ~/arepo-tools/yt-off-axis-plot.py \
			--slice-x "$xmin" "$xmax" --no-colorbar \
			--slice-y "$ymin" "$ymax" \
			--slice-z "$zmin" "$zmax" \
			--normal "$nx" "$ny" "$nz" \
			--north-vector "$north_x" "$north_y" "$north_z" \
			--width 300 "$height" --depth 50 --buff-size 500 "$buff_height" \
			--dpi 400 --output-path "$output_name" \
			-- "$path2/snapdir_008/" \
			&
		echo
		# wait when maximum number of jobs is running in parallel
		counter=$((counter + 1))
		if [ "$counter" -eq "$PARALLEL_MAX" ]; then
			echo "Reached $PARALLEL_MAX parallel jobs; waiting for completion"
			wait
			echo "$PARALLEL_MAX parallel jobs completed; resuming iterations"
			counter=0
		fi
	done
	wait
done

#sort -n filtered-filaments.txt | while read -r fil_id; do
#	for proj_axis in 2 1 0; do
#		lim=$(get_filament_limits "$ndskl_path" 10000 "$fil_id" "$proj_axis")
#		xmin=$(echo "$lim" | awk '{print $1}')
#		xmax=$(echo "$lim" | awk '{print $2}')
#		ymin=$(echo "$lim" | awk '{print $3}')
#		ymax=$(echo "$lim" | awk '{print $4}')
#		zmin=$(echo "$lim" | awk '{print $5}')
#		zmax=$(echo "$lim" | awk '{print $6}')
#		echo "xmin=$xmin" "xmax=$xmax"
#		echo "ymin=$ymin" "ymax=$ymax"
#		echo "zmin=$zmin" "zmax=$zmax"
#		case "$proj_axis" in
#			0) xmi="$ymin"; xma="$ymax"
#			   ymi="$zmin"; yma="$zmax"
#			   pmi="$xmin"; pma="$xmax"
#			;;
#			1) xmi="$xmin"; xma="$xmax"
#			   ymi="$zmin"; yma="$zmax"
#			   pmi="$ymin"; pma="$ymax"
#			;;
#			2) xmi="$xmin"; xma="$xmax"
#			   ymi="$ymin"; yma="$ymax"
#			   pmi="$zmin"; pma="$zmax"
#			;;
#		esac
#		$nnice mpiexec -n 8 python3 ~/arepo-tools/plot-snapshot.py \
#			--dpi 600 --transparent 0 --lim-absolute --project-absolute \
#			--xlim "$xmi" "$xma" --ylim "$ymi" "$yma" --project "$pmi" "$pma" \
#			--project-axis "$proj_axis" \
#			--plot-file-name "$(zero_pad 4 "$fil_id")_$proj_axis" \
#			--mark-filaments "$ndskl_path" \
#			"$path/snapdir_008/" \
#			</dev/null
#		echo
#	done
#done

