#!/usr/bin/env python3
import argparse
import collections
import gc
import itertools
import sys
import time
from pathlib import Path
import h5py
import numba
import numpy
import bin_density
import iterate_grid
import io_util
import mpi_util
import snapshot
import units_util
from mpi_util import MPI, mpi_comm, mpi_rank, mpi_size, print0


# constants
# use at most 1 GiB/core for local filament profile data (40 GiB for 40 cores)
PROFILES_MAX_MEM_SIZE_MB = 1024
SPHERE_SHRINK_FACTOR = 0.9
CYLINDER_RADIUS = 150.  # code units (normally kpc/h)
SHRINK_PARTICLE_THRESHOLD = 3
PARTICLES_CAPACITY_DEFAULT = 100_000
PARTICLES_CAPACITY_FACTOR = 2


ParamGrid = collections.namedtuple(
	'ParamGrid', 'filament, imin, imax, iradius, icenters_orig, mpi_rank'
)
ParamParticlesRegion = collections.namedtuple(
	'ParamParticlesRegion',
	'filament, left_edge, right_edge, radius, box_size, mpi_rank'
)
ParamParticles = collections.namedtuple(
	'ParamParticles', 'filament, radius, centers_orig, box_size, mpi_rank'
)

def pairwise_action(suffix1, suffix2):
	class PairwiseAction(argparse.Action):
		def __call__(self, parser, namespace, values, option_string=None):
			assert len(values) % 2 == 0
			setattr(namespace, f'{self.dest}{suffix1}', values[::2])
			setattr(namespace, f'{self.dest}{suffix2}', values[1::2])
	return PairwiseAction


parser = argparse.ArgumentParser(description='Calculate filament density '
	'profiles for fuzzy dark matter or cold dark matter simulation snapshots')
parser.add_argument('--prefer-type', choices=('grid', 'particles'),
	default='particles',
	help='the type of snapshot data that should be preferred '
		'(default: %(default)s)')
parser.add_argument('--ptype', type=int, default=1,
	help='particle type to use (default: %(default)d)')
parser.add_argument('--snap-chunk-mem-size', type=float,
	default=snapshot.CHUNK_MEM_SIZE_DEFAULT / 1024**2,
	help='maximum amount of memory to use for snapshot data per core, in MiB '
		'(default: %(default)g)')
parser.add_argument('--output-path', nargs='+', type=Path,
	help='override output file names')
parser.add_argument('path', nargs='+', type=Path,
	action=pairwise_action('_snap', '_ndskl'),
	help='the paths to the snapshot files and NDskl ASCII files (generated by '
		'DisPerSE) to process (for snapshots, any normal files are interpreted '
		'as single-file snapshots, while all files in any given directory '
		'are interpreted as a single multi-file snapshot); paths must be given '
		'in pairs: <snap_path> <ndskl_path> [<snap_path> <ndskl_path>…]')


def main():
	mpi_util.default_print_args['flush'] = True
	# process arguments
	if not args.output_path:
		args.output_path = [None] * len(args.path_snap)
	assert len(args.path_snap) == len(args.output_path)
	# process paths
	for path_args in zip(args.path_snap, args.path_ndskl, args.output_path):
		process_path(*path_args)
		print0()


def process_path(snap_path, ndskl_path, output_path):
	print0(snap_path)
	print0(ndskl_path)
	if not output_path:
		output_path = ndskl_path.with_name(
			f'{ndskl_path.name}_shrinking_shift.hdf5'
		)
	# read snapshot metadata
	print0('Reading snapshot metadata…')
	snap = snapshot.Snapshot(snap_path)
	units = units_util.get_units(snap.header)
	snaptime = snap.header['Time']
	z = 1 / snaptime - 1
	print0(f'\tRedshift: z = {z}')
	# read filament data
	ndskl_ngrid = None
	filaments = None
	if mpi_rank == 0:
		ndskl_ngrid, filaments = io_util.read_NDskl_ascii(
			ndskl_path,
			snap.pmgrid if snap.get_type(args.prefer_type) == 'grid' else None
		)
		coord_fac = snap.box_size / ndskl_ngrid
		for filament in filaments:
			filament *= coord_fac
	# broadcast filament data to other ranks
	if mpi_size > 1:
		ndskl_ngrid = mpi_comm.bcast(ndskl_ngrid, root=0)
		filaments = mpi_comm.bcast(filaments, root=0)
	print0(f'Processing NDskl with {len(filaments)} filaments')
	if len(filaments) == 0:
		print0('NDskl contains no filaments')
	# check existing data
	fil_ids_todo = None
	if mpi_rank == 0:
		if output_path.exists():
			print0(f'Not recomputing existing data in file “{output_path}”')
			with h5py.File(output_path, 'r') as of:
				for i in range(len(filaments)):
					if f'{i}' in of:
						fil_shape = filaments[i].shape
						assert of[f'{i}'].shape == (
							(fil_shape[0] - 1,) + fil_shape[1:]
						)
		else:
			print0(
				f'“{output_path}” not found as existing file, starting from '
				'scratch'
			)
			# initialize HDF5 file
			with h5py.File(output_path, 'w') as of:
				pass
		# determine which filaments are both requested and missing
		fil_ids_todo = []
		with h5py.File(output_path, 'r') as of:
			for i in range(len(filaments)):
				if f'{i}' not in of:
					fil_ids_todo.append(i)
		fil_ids_todo = numpy.array(fil_ids_todo)
		print0(
			f'{len(fil_ids_todo)} filaments left to process for file '
			f'“{ndskl_path}”'
		)
	fil_ids_todo = mpi_comm.bcast(fil_ids_todo, root=0)
	if len(fil_ids_todo) == 0:
		return
	# process filaments
	for fil_id in fil_ids_todo:
		process_filament(snap, fil_id, filaments[fil_id], units, output_path)


def process_filament(snap, fil_id, filament, units, output_path):
	print0(f'Processing filament #{fil_id}')
	print0(f'{filament}')
	#TODO:
#	if snap.path.name == 'snapdir_008_downscale216.fits_c5e-07.up.NDskl.S005.a.NDskl' and fil_id == 2546:
#		return
	# load enclosing rectangular region of cylinder around filament
	rmin, rmax = get_filament_extent(snap.box_size, filament, CYLINDER_RADIUS)
	# prepare for grid
	if snap.get_type(args.prefer_type) == 'grid':
		cell_len = snap.box_size / snap.pmgrid
		filament_grid = filament / cell_len
		imin = numpy.floor(rmin / cell_len).astype('i4')
		imax = numpy.ceil(rmax / cell_len).astype('i4')
		data_slice = ()
		for mi, ma in zip(imin, imax):
			data_slice += numpy.index_exp[mi:ma]
		icenters = numpy.full_like(filament_grid, numpy.nan)[:-1]
		for i in range(len(filament_grid) - 1):
			# use mid-point of segment (i.e. zero distance from original
			# cylinder axis) as initial guess for center
			icenters[i] = bin_density.coord_periodic_wrap_vec(
				snap.pmgrid, filament_grid[i] +
				bin_density.dist_periodic_wrap_vec(
					snap.pmgrid, filament_grid[i + 1] - filament_grid[i]
				) / 2
			)
		icenters_orig = numpy.copy(icenters)
		icenters_prev = numpy.copy(icenters)
		rho_sum = numpy.zeros(len(filament_grid) - 1, dtype='f8')
		num_cells = numpy.zeros_like(rho_sum, dtype='u8')
	# prepare for particles
	elif snap.get_type(args.prefer_type) == 'particles':
		numpart_total = snap.numpart_total(args.ptype)
		centers = numpy.full_like(filament, numpy.nan)[:-1]
		for i in range(len(filament) - 1):
			# use mid-point of segment (i.e. zero distance from original
			# cylinder axis) as initial guess for center
			centers[i] = bin_density.coord_periodic_wrap_vec(
				snap.box_size, filament[i] + bin_density.dist_periodic_wrap_vec(
					snap.box_size, filament[i + 1] - filament[i]
				) / 2
			)
		centers_orig = numpy.copy(centers)
		centers_prev = numpy.copy(centers)
		num_part = numpy.full(len(filament) - 1, -1, dtype='u8')
		# get particles in rectangular region given by (rmin, rmax)
		param = ParamParticlesRegion(
			filament=filament, left_edge=rmin, right_edge=rmax,
			radius=CYLINDER_RADIUS, box_size=snap.box_size, mpi_rank=mpi_rank
		)
		pos_region = numba.typed.List()
		pos_region.append(numpy.full(
			(PARTICLES_CAPACITY_DEFAULT, 3), numpy.nan, dtype='f8'
		))
		numpart_region = numba.typed.List()
		numpart_region.append(0)
		iterate_grid.iterate_particles(
			snap, collect_particles_region, (pos_region, numpart_region),
			param=param, ptype=args.ptype,
			chunk_mem_size=round(args.snap_chunk_mem_size * 1024**2),
		)
		pos_region = pos_region[0][:numpart_region[0]]
		t0 = time.perf_counter()
		mpi_comm.barrier()
		t1 = time.perf_counter()
		print0(f'Waiting for all ranks to complete took {t1 - t0} sec.')
		# exchange particles to obtain even particle load
		numpart_ranks = mpi_comm.allgather(len(pos_region))
		last_rank_with_particles = mpi_size - 1
		for rank in range(mpi_size - 1, 0, -1):
			if numpart_ranks[rank] > 0:
				last_rank_with_particles = rank
				break
		numpart_region_all = numpy.sum(numpart_ranks)
		numpart_ranks_target = [numpart_region_all // mpi_size] * mpi_size
		for rank in range(numpart_region_all % mpi_size):
			numpart_ranks_target[rank] += 1
		assert numpy.sum(numpart_ranks_target) == numpart_region_all
		numpart_region = numpart_ranks_target[mpi_rank]
		send_counts = [0] * mpi_size
		rank_send = 0
		for rank_recv in range(mpi_size):
			assert rank_send < mpi_size
			while numpart_ranks_target[rank_recv] > 0:
				numpart_send = min(
					numpart_ranks[rank_send], numpart_ranks_target[rank_recv]
				)
				if rank_send == mpi_rank:
					send_counts[rank_recv] += numpart_send
				numpart_ranks[rank_send] -= numpart_send
				numpart_ranks_target[rank_recv] -= numpart_send
				assert numpart_ranks[rank_send] >= 0
				assert numpart_ranks_target[rank_recv] >= 0
				if numpart_ranks[rank_send] == 0:
					rank_send += 1
		assert rank_send == last_rank_with_particles + 1
		for rank in range(mpi_size):
			assert numpart_ranks[rank] == 0
			assert numpart_ranks_target[rank] == 0
		assert numpy.sum(send_counts) == len(pos_region)
		send_counts = [3 * x for x in send_counts]
		recv_counts = mpi_comm.alltoall(send_counts)
		pos_region_old = pos_region
		pos_region = numpy.full((numpart_region, 3), numpy.nan, dtype='f8')
		mpi_comm.Alltoallv(
			[pos_region_old, send_counts], [pos_region, recv_counts]
		)
		assert numpy.all(numpy.isfinite(pos_region))
		del pos_region_old
		gc.collect()
		pdata = iterate_grid.ParticleData(Coordinates=pos_region)
		print0(
			f'Processing {len(pos_region):,} particles in region '
			f'({rmin}, {rmax})'
		)
	else: assert False
	# apply shrinking spheres for each segment
	radius = CYLINDER_RADIUS
	t0 = time.perf_counter()
	it = 0
	while True:
		print0(f'Shrinking iteration {it: 3} ({radius = })')
		# process grid
		if snap.get_type(args.prefer_type) == 'grid':
			iradius = radius / cell_len
			if iradius < 1.1 * cell_len:
				break
			param = ParamGrid(
				filament=filament_grid, imin=imin, imax=imax, iradius=iradius,
				icenters_orig=icenters_orig, mpi_rank=mpi_rank,
			)
			# iterate to get new COM
			iterate_grid.iterate(
				snap, center_of_mass_grid, (icenters, rho_sum, num_cells),
				param=param, use_rho=True, data_slice=data_slice,
				chunk_mem_size=round(args.snap_chunk_mem_size * 1024**2)
			)
			# get global COM
			icenters = mpi_comm.allreduce(icenters, op=MPI.SUM)
			rho_sum = mpi_comm.allreduce(rho_sum, op=MPI.SUM)
			num_cells = mpi_comm.allreduce(num_cells, op=MPI.SUM)
			for i in range(len(filament_grid) - 1):
				# finish COM and add starting point to get final new center
				# location
				if num_cells[i] > 0:
					icenters[i] /= rho_sum[i]
				icenters[i] = bin_density.coord_periodic_wrap_vec(
					snap.pmgrid, icenters_prev[i] + icenters[i]
				)
				# consistency checks
				dip = bin_density.dist_periodic_wrap_vec(
					snap.pmgrid, filament_grid[i + 1] - filament_grid[i]
				)
				dic = bin_density.dist_periodic_wrap_vec(
					snap.pmgrid, icenters[i] - icenters_orig[i]
				)
				assert numpy.isclose(dip @ dic, 0, atol=1e-6, rtol=0), (
					dip @ dic, dip, dic
				)
			icenters_prev[...] = icenters
		# process particles
		elif snap.get_type(args.prefer_type) == 'particles':
			if (
				numpy.all(num_part < SHRINK_PARTICLE_THRESHOLD) or
				radius < snap.box_size / numpart_total**(1/3) / 10
			):
				break
			param = ParamParticles(
				filament=filament, radius=radius, centers_orig=centers_orig,
				box_size=snap.box_size, mpi_rank=mpi_rank,
			)
			# get new COM using local particle data
			center_of_mass_particles(
				pdata, len(pdata.Coordinates), 0, len(pdata.Coordinates),
				param, (centers, num_part)
			)
			# get global COM
			centers = mpi_comm.allreduce(centers, op=MPI.SUM)
			num_part = mpi_comm.allreduce(num_part, op=MPI.SUM)
			for i in range(len(filament) - 1):
				# finish COM and add starting point to get final new center
				# location
				if num_part[i] > 0:
					centers[i] /= num_part[i]
				centers[i] = bin_density.coord_periodic_wrap_vec(
					snap.box_size, centers_prev[i] + centers[i]
				)
				# consistency checks
				dp = bin_density.dist_periodic_wrap_vec(
					snap.box_size, filament[i + 1] - filament[i]
				)
				dc = bin_density.dist_periodic_wrap_vec(
					snap.box_size, centers[i] - centers_orig[i]
				)
				assert numpy.isclose(dp @ dc, 0, atol=1e-6, rtol=0), (
					dp @ dc, dp, dc
				)
			centers_prev[...] = centers
		else: assert False
		# shrink sphere
		it += 1
		radius *= SPHERE_SHRINK_FACTOR
	sys.stdout.flush()
	t1 = time.perf_counter()
	print0(f'Took {t1 - t0} sec.')
	# save data
	if mpi_rank == 0:
		if snap.get_type(args.prefer_type) == 'grid':
			centers_orig = icenters_orig * cell_len
			centers = icenters * cell_len
		#TODO units?
		offsets = numpy.full_like(centers, numpy.nan)
		for i in range(len(offsets)):
			offsets[i] = bin_density.dist_periodic_wrap_vec(
				snap.box_size, centers[i] - centers_orig[i]
			)
		print0(f'Writing to file “{output_path}”')
		with h5py.File(output_path, 'r+') as of:
			of[f'{fil_id}'] = offsets
		del offsets


#@numba.njit
def get_filament_extent(box_size, filament, radius):
	#TODO: more optimal range by actually checking cylinders around the segments
	rmin = numpy.full(3, numpy.inf, dtype='f8')
	rmax = numpy.full_like(rmin, -numpy.inf)
	# use first filament point as a reference to handle periodic wrapping of
	# coordinates
	p0 = filament[0]
	it_range = numpy.array([-radius, radius])
	for p_orig in filament:
		p = bin_density.dist_periodic_wrap_vec(box_size, p_orig - p0)
		for i in range(3):
			for d in it_range:
				x = p[i] + d
				if x < rmin[i]:
					rmin[i] = x
				if x > rmax[i]:
					rmax[i] = x
	# transform back to original coordinate system
	rmin += p0
	rmax += p0
	for i in range(3):
		if rmax[i] >= box_size:
			rmin[i] -= box_size
			rmax[i] -= box_size
	return rmin, rmax


# calculate center of mass “in projection” along the cylinder, i.e. only use
# vector components orthogonal to cylinder axis in COM calculation
@numba.njit
def center_of_mass_grid(rho, pmgrid, ix_info, data_slice, param, data):
	ix_min, ix_start, ix_end = ix_info
	Nx, Ny, Nz = rho.shape
	imin = param.imin
	imax = param.imax
	assert ix_min == imin[0] % pmgrid
	ix_range_spans_origin = ix_start > ix_end
	icenters, rho_sum, num_cells = data
	fil = param.filament
	ir = param.iradius
	icenters_orig = param.icenters_orig
	ip = numpy.empty(3, dtype='u4')
	for i in range(len(fil) - 1):
		warning_y_printed = warning_z_printed = False
		rho_sum[i] = 0
		num_cells[i] = 0
		ic = icenters[i]
		icom_sum = numpy.zeros(3, dtype='f8')
		ip1 = numpy.copy(fil[i])
		ip2 = numpy.copy(fil[i + 1])
		dip = bin_density.dist_periodic_wrap_vec(pmgrid, ip2 - ip1)
		dip_len = numpy.linalg.norm(dip)
		# shift cylinder axis to current center (ic)
		io = bin_density.dist_periodic_wrap_vec(pmgrid, ic - icenters_orig[i])
		ip1 += io
		ip2 += io
#		max_length = int(numpy.ceil(numpy.max([dip_len, ir])))
		if dip_len > ir:
			max_length_f = dip_len
		else:
			max_length_f = ir
		max_length_f = numpy.ceil(max_length_f)
		max_length = int(max_length_f)
		for dix in range(-max_length, max_length + 1):
			ix = int(ic[0] + dix) % pmgrid
			if ix_range_spans_origin:
				if ix_end <= ix < ix_start:
					continue
			else:
				if ix < ix_start or ix_end <= ix:
					continue
			ip[0] = ix
			for diy in range(-max_length, max_length + 1):
				iy = int(ic[1] + diy) % pmgrid
				ip[1] = iy
				for diz in range(-max_length, max_length + 1):
					iz = int(ic[2] + diz) % pmgrid
					ip[2] = iz
					# get orthogonal distance between p and line segment,
					# discarding points located outside the cylinder
					ivdist = segment_distance(ip, ip1, dip, dip_len, pmgrid)
					idist = numpy.linalg.norm(ivdist)
					if idist < ir:
						lix = ix - ix_start
						if lix < 0:
							lix += pmgrid
						liy = iy - imin[1]
						if liy >= pmgrid:
							liy -= pmgrid
						liz = iz - imin[2]
						if liz >= pmgrid:
							liz -= pmgrid
						assert 0 <= lix < Nx
						if not (0 <= liy < Ny):
							if not warning_y_printed:
								print(
									'WARNING! imin= ', imin, 'imax =', imax,
									'Ny =', Ny, 'liy =', liy
								)
								warning_y_printed = True
							continue
						if not (0 <= liz < Nz):
							if not warning_z_printed:
								print(
									'WARNING! imin= ', imin, 'imax =', imax,
									'Ny =', Nz, 'liy =', liz
								)
								warning_z_printed = True
							continue
						rho_val = rho[lix, liy, liz]
						# can use density instead of mass because all cells have
						# the same volume (i.e. just leaving out a constant
						# factor)
						icom_sum += ivdist * rho_val
						rho_sum[i] += rho_val
						num_cells[i] += 1
		icenters[i] = icom_sum


@numba.njit
def collect_particles_region(
	pdata, numpart_total, part_start, part_end, param, data
):
	pos = pdata.Coordinates
	data_pos, data_idx = data
	pos_out = data_pos[0]
	idx_out = data_idx[0]
	filament = param.filament
	radius = param.radius
	box_size = param.box_size
	le = param.left_edge
	re = param.right_edge
	de = re - le
	assert numpy.all(de > 0)
	for p in pos:
		dp = bin_density.dist_periodic_wrap_vec(box_size, p - le)
		if numpy.all(dp > 0) and numpy.all(dp < de):
			inside = False
			for i in range(len(filament) - 1):
				p1 = filament[i]
				p2 = filament[i + 1]
				# get orthogonal distance between p and line segment,
				# discarding points located outside the cylinder
				dist = bin_density.segment_distance(p, p1, p2, box_size)
				if dist < radius:
					inside = True
					break
			if inside:
				assert idx_out <= len(pos_out)
				if idx_out == len(pos_out):
					# enlarge output array
					out_new = numpy.full(
						(PARTICLES_CAPACITY_FACTOR * len(pos_out),) +
							pos_out.shape[1:],
						numpy.nan, dtype='f8'
					)
					out_new[:len(pos_out)] = pos_out
					pos_out = out_new
				pos_out[idx_out] = p
				idx_out += 1
	data_pos[0] = pos_out
	data_idx[0] = idx_out

# Calculate center of mass “in projection” along the cylinder, i.e. only use
# vector components orthogonal to cylinder axis in COM calculation.
# This isn’t actually used with the iterate_particles() machinery, but was
# originally and retains the corresponding interface.
@numba.njit
def center_of_mass_particles(
	pdata, numpart_total, part_start, part_end, param, data
):
	pos = pdata.Coordinates
	centers, num_part = data
	fil = param.filament
	box_size = param.box_size
	radius = param.radius
	centers_orig = param.centers_orig
	for i in range(len(fil) - 1):
		#TODO: make this work (difficult because of global reduction operations
		#      in each loop iteration!)
#		if num_part[i] < SHRINK_PARTICLE_THRESHOLD:
#			continue
		num_part[i] = 0
		c = centers[i]
		com_sum = numpy.zeros(3, dtype='f8')
		p1 = numpy.copy(fil[i])
		p2 = numpy.copy(fil[i + 1])
		dp = bin_density.dist_periodic_wrap_vec(box_size, p2 - p1)
		dp_len = numpy.linalg.norm(dp)
		# shift cylinder axis to current center (c)
		o = bin_density.dist_periodic_wrap_vec(box_size, c - centers_orig[i])
		p1 += o
		p2 += o
		for p in pos:
			# get orthogonal distance between p and line segment,
			# discarding points located outside the cylinder
			vdist = segment_distance(p, p1, dp, dp_len, box_size)
			dist = numpy.linalg.norm(vdist)
			if dist < radius:
				# can omit particle masses when assuming that all particles have
				# the same mass (which cancels out)
				com_sum += vdist
				num_part[i] += 1
		centers[i] = com_sum

def particles_callback(
	snap, it_start, part_chunks, part_start, part_end, tdiff
):
	if it_start:
		print0(f'Reading particles…', flush=True)
	else:
		print0(f'Processed {part_end: 10,} particles in subregion', flush=True)
	if tdiff > 0.1:
		print0(f'\tTook {tdiff} sec.', flush=True)


@numba.njit
def segment_distance(p, p1, dp, dp_len, box_size):
	# parameterize line: l = p1 + t (p2 - p1)
#	dp = bin_density.dist_periodic_wrap_vec(box_size, p2 - p1)
	t = (
		(bin_density.dist_periodic_wrap_vec(box_size, p - p1) @ dp) /
		dp_len**2
	)
	if t < 0 or 1 < t:
		return dp * numpy.nan
	intersect = p1 + t * dp
	dist_vec = bin_density.dist_periodic_wrap_vec(box_size, p - intersect)
	return dist_vec


if __name__ == '__main__':
	args = parser.parse_args()
	main()

