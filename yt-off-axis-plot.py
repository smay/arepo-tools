#!/usr/bin/env python3
import argparse
import itertools
from pathlib import Path
import matplotlib.cm
import matplotlib.pyplot as plt
#import mpl_toolkits.axes_grid1
import numpy
import yt
import io_util
import units_util
import bin_density
import snapshot
import matplotlib_settings


GROUP_NAME = '/FuzzyDM'
DATASET_NAME_RE = f'{GROUP_NAME}/PsiRe'
DATASET_NAME_IM = f'{GROUP_NAME}/PsiIm'
NPROCS = 8
LOG_ZERO_VALUE = 1e-50

def vec(dim, eltype, coll_type=list):
	def vec(s):
		try:
			result = coll_type(map(eltype, s.strip().split(',')))
			if len(result) != dim:
				raise ValueError(
					f'Invalid length {len(result)} (expected {dim})'
				)
			return result
		except Exception:
			raise argparse.ArgumentTypeError(
				f'vector of length {dim} must be given as “v₁,v₂,…” '
				f'(problematic input: “{s}”)'
			)
	return vec

parser = argparse.ArgumentParser(description='Create an off-axis projection '
	'plot of a sub-region of an FDM or CDM snapshot’s density field')
parser.add_argument('--prefer-type', choices=('grid', 'particles'),
	default='particles',
	help='the type of snapshot data that should be preferred '
		'(default: %(default)s)')
parser.add_argument('--slice-x', type=vec(2, float), nargs='+',
	help='data slice to use in the x direction (default: (None, None))')
parser.add_argument('--slice-y', type=vec(2, float), nargs='+',
	help='data slice to use in the y direction (default: (None, None))')
parser.add_argument('--slice-z', type=vec(2, float), nargs='+',
	help='data slice to use in the z direction (default: (None, None))')
parser.add_argument('--normal', type=vec(3, float), nargs='+',
	help='the “normal” argument to yt.OffAxisProjectionPlot (default: '
		'[0, 0, 1])')
parser.add_argument('--north-vector', type=vec(3, float), nargs='+',
	help='the “north_vector” argument to yt.OffAxisProjectionPlot')
parser.add_argument('--right-handed', type=bool, nargs='+',
	help='the “right_handed” argument to yt.OffAxisProjectionPlot')
#TODO
#parser.add_argument('--center', type=float,
#	help='the “center” argument to yt.OffAxisProjectionPlot')
parser.add_argument('--width', type=vec(2, float, coll_type=tuple), nargs='+',
	help='the “width” argument to yt.OffAxisProjectionPlot')
parser.add_argument('--depth', type=float, nargs='+',
	help='the “depth” argument to yt.OffAxisProjectionPlot')
parser.add_argument('--buff-size', type=vec(2, int), nargs='+',
	help='the “buff_size” argument to yt.OffAxisProjectionPlot')
parser.add_argument('--mark-filaments', type=Path, nargs='+',
	help='mark filaments found by DisPerSE in the given NDskl_ascii file '
		'using lines')
parser.add_argument('--mark-single-filament', type=int, nargs='+',
	help='only mark filament with the given ID')
parser.add_argument('--mark-single-segment', type=int, nargs='+',
	help='only mark filament segment with the given ID')
parser.add_argument('--clim', nargs=2, type=float, default=(1e0, 1e3),
	help='the plot’s range in units of the average density (default: '
		'%(default)s)')
parser.add_argument('--colorbar-extend',
	help='setting to force for the colorbar “extend” option')
parser.add_argument('--colorbar-fraction', type=float,
	help='setting to force for the colorbar “fraction” option')
parser.add_argument('--colorbar-shrink', type=float,
	help='setting to force for the colorbar “shrink” option')
parser.add_argument('--no-colorbar', action='store_true',
	help='disable colorbar')
parser.add_argument('--mpl-bbox',
	help='matplotlib rc property savefig.bbox')
parser.add_argument('--figsize', type=float, nargs=2,
	help='the figure size in inches')
parser.add_argument('--dpi', type=float, default=200.,
	help='the resolution of the generated plot(s) (default: %(default)s)')
parser.add_argument('--cic-path', type=Path, nargs='+',
	help='paths to read/write the CIC-binned particle data from/to')
parser.add_argument('--plot-file-name', type=Path, default=Path('filament_off'),
	help='path to write the plot output to (without file extension)')
parser.add_argument('--plot-format', default='png',
	help='the file format of the generated plot(s) (default: %(default)s)')
parser.add_argument('path', type=Path, nargs='+',
	help='the paths to the snapshot files; any normal files are interpreted '
		'as single-file snapshots, while all files in any given directory '
		'are interpreted as a single multi-file snapshot')


# the color gets brighter when amount > 1 and darker when amount < 1
def adjust_lightness(color, amount):
	import matplotlib.colors
	import colorsys
	c = colorsys.rgb_to_hls(*matplotlib.colors.to_rgb(color))
	return colorsys.hls_to_rgb(c[0], max(0, min(1, amount * c[1])), c[2])


def main():
	if args.figsize:
		plt.rc('figure', figsize=args.figsize)
	if args.mpl_bbox:
		plt.rc('savefig', bbox=args.mpl_bbox)
	if not args.cic_path:
		args.cic_path = [
			p.with_name(f'{p.stem}_cic.h5') for p in args.path
		]
	if not args.slice_x:
		args.slice_x = [(None,) * 2] * len(args.path)
	if not args.slice_y:
		args.slice_x = [(None,) * 2] * len(args.path)
	if not args.slice_z:
		args.slice_x = [(None,) * 2] * len(args.path)
	if not args.normal:
		args.normal = [[0., 0., 1.]] * len(args.path)
	for key in (
		'north_vector', 'right_handed', 'width', 'depth', 'buff_size',
		'mark_filaments', 'mark_single_filament', 'mark_single_segment'
	):
		if getattr(args, key) is None:
			setattr(args, key, [None] * len(args.path))
	# process paths
	plots = []
	for path_args in zip(
		args.path, args.cic_path, args.slice_x, args.slice_y, args.slice_z,
		args.normal, args.north_vector, args.right_handed,
		args.width, args.depth, args.buff_size, args.mark_filaments,
		args.mark_single_filament, args.mark_single_segment,
		strict=True
	):
		plots.append(process_path(*path_args))
	# combined plot
	fig, grid = plt.subplots(1, len(plots), layout='constrained', squeeze=False)
	figsize = fig.get_size_inches()
	cbar = None
	# need to go through setup twice: first so that image axes are sized, and
	# second after adding the colorbar based on the already correctly-sized
	# image axes
	for _ in range(2):
		for ax, (proj, field) in zip(grid.flat, plots, strict=True):
			plot = proj.plots[field]
			plot.figure = fig
			plot.axes = ax
			if cbar:
				plot.cax = cbar.ax
			proj.render()
		# yt manually overrides figure and axes positions (set_position) and sizes
		# in proj.render(), so reset size and re-activate constrained layout to get
		# the requested layout
		fig.set_size_inches(figsize)
		for ax in grid.flat:
			ax.set_in_layout(True)
		if cbar:
			cbar.ax.tick_params(
				which='both', direction=plt.rcParams['ytick.direction']
			)
			cbar.ax.set_in_layout(True)
		else:
			# create dummy colorbar to get axes for colorbar
			cbar = fig.colorbar(
				matplotlib.cm.ScalarMappable(), ax=list(grid.flat),
#				fraction=args.colorbar_fraction if args.colorbar_fraction
#					else 0.1,
			)
#	for ax, (proj, field) in zip(grid.flat, plots, strict=True):
#		plot = proj.plots[field]
#		plot.figure = fig
#		plot.axes = ax
#		proj.render()
#	# yt manually overrides figure and axes positions (set_position) and sizes
#	# in proj.render(), so reset size and re-activate constrained layout to get
#	# the requested layout
#	fig.set_size_inches(figsize)
#	for ax in grid.flat:
#		ax.set_in_layout(True)
	#TODO
	cbar_opt = {'pad': 0.02}
	if args.colorbar_extend:
		cbar_opt['extend'] = args.colorbar_extend
	if args.colorbar_fraction:
		cbar_opt['fraction'] = args.colorbar_fraction
	if args.colorbar_fraction:
		cbar_opt['shrink'] = args.colorbar_shrink
	proj, field = plots[-1]
#	cbar = fig.colorbar(proj.plots[field].image, ax=grid.flat, **cbar_opt)
	cbar = fig.colorbar(proj.plots[field].image, cax=cbar.ax, **cbar_opt)
	cbar_label_opt = {}
	if numpy.log10(min(args.clim)) < 0:
		cbar_label_opt['labelpad'] = -2
	cbar.set_label(r'$\rho/\langle \rho \rangle$', **cbar_label_opt)
	# save plot
	fig.savefig(f'{args.plot_file_name}.{args.plot_format}', dpi=args.dpi)


def process_path(
	path, cic_path, slice_x, slice_y, slice_z, normal,
	north_vector, right_handed, width, depth, buff_size, mark_filaments_path,
	mark_single_filament, mark_single_segment
):
	print(path)
	snap = snapshot.Snapshot(path)
	units = units_util.get_units(snap.header)
	ds = None
	field = None
	lim = None
	rho_avg = (
		snap.header['Omega0'] *
		snap.constant_in_internal_units('critical_density0')
	)
	# load grid data
	if snap.get_type(args.prefer_type) == 'grid':
		cell_len = snap.box_size / snap.pmgrid
		data_slice = ()
		for mi, ma in (slice_x, slice_y, slice_z):
			data_slice += numpy.index_exp[
				int(numpy.floor(mi / cell_len)) if mi else mi:
				int(numpy.ceil(ma / cell_len)) if ma else ma
			]
		print(f'Using region {data_slice}')
		_, indices = snap.read_snapshot_indices(DATASET_NAME_RE, data_slice)
		for ax_indices in indices:
			if ax_indices.flat[-1] <= ax_indices.flat[0]:
				ax_indices.flat[0] -= snap.pmgrid
			assert ax_indices.flat[-1] > ax_indices.flat[0]
		rho = snap.read_snapshot(DATASET_NAME_RE, data_slice=data_slice)**2
		rho += snap.read_snapshot(DATASET_NAME_IM, data_slice=data_slice)**2
		rho *= units.mass_in_solar_mass / units.length_in_kpc**3
		rho_avg *= units.mass_in_solar_mass / units.length_in_kpc**3
#		data = {'density': (rho, '(Msun / h) / (kpccm / h)**3')}
		data = {'density': (rho, 'Msun / kpc**3')}
#		data = {'density': (rho, '1e10 * Msun / kpc**3')}
		bbox = numpy.array([
			[ax_indices.flat[0] * cell_len, ax_indices.flat[-1] * cell_len]
			for ax_indices in indices
		])
		ds = yt.load_uniform_grid(
			data, rho.shape,
#			length_unit=(1., 'kpccm / h'), mass_unit=(1., 'Msun / h'),
			length_unit=(1., 'kpc'), mass_unit=(1., 'Msun'),
#			length_unit=(units.length_in_kpc, 'kpc'),
#			mass_unit=(units.mass_in_solar_mass, 'Msun'),
			bbox=bbox, periodicity=(False,) * 3,
			nprocs=NPROCS,
		)
		field = ('stream', 'density')
		lim = bbox
	# load particle data
	elif snap.get_type(args.prefer_type) == 'particles':
		if path.is_dir():
			yt_path = snap.paths[0]
		else:
			yt_path = path
		print(yt_path)
		ds_particles = yt.load(yt_path)
		slice_xyz = [slice_x, slice_y, slice_z]
		left_edge = [
			slice_xyz[i][0] if slice_xyz[i] is not None
				else ds_particles.domain_left_edge[i]
			for i in range(len(slice_xyz))
		]
		right_edge = [
			slice_xyz[i][1] if slice_xyz[i] is not None
				else ds_particles.domain_right_edge[i]
			for i in range(len(slice_xyz))
		]
		lim = [(le, re) for le, re in zip(left_edge, right_edge)]
		if not cic_path.exists():
			numpart_tot = sum(ds_particles.particle_type_counts.values())
			numpart_tot_per_dim = numpart_tot**(1/3)
			ag = ds_particles.arbitrary_grid(
				left_edge=left_edge.copy(), right_edge=right_edge.copy(),
				dims=[
					int(
						(4 * numpart_tot_per_dim * (re - le) / snap.box_size) +
						1
					) for le, re in zip(left_edge, right_edge)
				]
			)
			cic_path = ag.save_as_dataset(
				filename=cic_path, fields=[('deposit', 'all_cic')]
			)
		print(cic_path)
		ds = yt.load(cic_path)
		field = ('grid', 'all_cic')
	else: assert False
	assert ds is not None
	assert field is not None
	assert lim is not None
	# create projection plot
	proj_args = {}
	loc = locals()
	for key in (
		#TODO: center
#		'north_vector', 'right_handed', 'center', 'width', 'depth', 'buff_size',
		'north_vector', 'right_handed', 'width', 'depth', 'buff_size',
	):
		args_val = loc[key]
		if args_val is not None:
			proj_args[key] = args_val
	proj = yt.OffAxisProjectionPlot(
		ds, normal, field, fontsize=plt.rcParams['font.size'], **proj_args
	)
	# workaround to avoid showing background color
	frb_data = proj.frb[field]
	frb_data[frb_data == 0] = LOG_ZERO_VALUE
	# plot settings
	zdist = proj.zlim[1] - proj.zlim[0]
	proj.set_unit(
		field,
		f'{rho_avg} * code_mass/code_length**3 * {zdist.value} * {zdist.units}'
	)
	proj.set_zlim(field, *args.clim)
	proj.set_colorbar_label(field, r'$\rho/\langle \rho \rangle$')
	proj.set_cmap(field, plt.get_cmap())
	proj.annotate_scale(
		# self.corner == "lower_right": self.pos = (0.89, 0.052)
		pos=(0.8, 0.052), coeff=50,
		#TODO: don’t hard-code code_length as kpc
		unit='code_length', scale_text_format='{scale}$\,h^{{-1}}\,$kpc'
	)
	proj.hide_axes(draw_frame=True)
	if args.no_colorbar:
		proj.hide_colorbar()
#	lim = [proj.xlim, proj.ylim, proj.zlim]
	if mark_filaments_path:
		mark_filaments(
			snap, mark_filaments_path, units, lim, proj,
			mark_single_filament, mark_single_segment
		)
	orienter = proj.data_source.orienter
	print(f'NORMAL VECTOR = {orienter.normal_vector}')
	print(f'NORTH VECTOR  = {orienter.north_vector}')
	# save plot
	#TODO
#	proj.save(
#		f'{output_path}.{args.plot_format}', mpl_kwargs=dict(dpi=args.dpi)
#	)
	return proj, field


def mark_filaments(
	snap, path, units, lim, proj, mark_single_filament, mark_single_segment
):
	print(f'Marking filaments in {path} with lines')
	ndskl_ngrid, filaments = io_util.read_NDskl_ascii(
		path,
		snap.pmgrid if snap.get_type(args.prefer_type) == 'grid' else None
	)
	coord_fac = snap.box_size / ndskl_ngrid
	def plot_points(
		snap, lim, proj, fil_id, points, shifts=(-snap.box_size, 0),
		color=None, linewidth=None
	):
		shifts = (0,) #TODO
		linewidth = 1.5 if linewidth is None else linewidth
		if color is None:
			color_idx = fil_id
			if color_idx == 2:
				color = 'k'
			else:
				color = f'C{color_idx}'
			if mark_single_filament is not None:
				color = 'white'
		for p1, p2 in zip(points[:-1], points[1:]):
			for p in (p1, p2):
				for i in range(len(p)):
					if lim[i][0] < 0 and p[i] > snap.box_size / 2:
						p[i] -= snap.box_size
			for shift in itertools.product(shifts, repeat=3):
				shift = numpy.array(shift)
				proj.annotate_line(
					p1 + shift, p2 + shift,
					color=color, linewidth=linewidth,
				)
	shrinking_shift_path = path.with_name(f'{path.name}_shrinking_shift.hdf5')
	if shrinking_shift_path.exists():
		import h5py
		f = h5py.File(shrinking_shift_path, 'r')
		print(f'OFFSET FILE “{shrinking_shift_path}” FOUND!')
	else:
		f = {}
		print(f'OFFSET FILE “{shrinking_shift_path}” NOT FOUND!')
	for fil_id, fil in enumerate(filaments):
		fil *= coord_fac
		if mark_single_filament is not None:
			if fil_id != mark_single_filament:
				continue
			print('Filament data:')
			print(fil)
		if f'{fil_id}' in f:
			offsets = f[f'{fil_id}'][...] * units.length_in_kpc
		else:
			offsets = None
		# shifted lines
		for i in range(len(fil) - 1):
			if offsets is None: break
			if mark_single_segment is not None:
				if i != mark_single_segment:
					continue
			p = numpy.copy(fil[i])
			dp = bin_density.dist_periodic_wrap_vec(
				snap.box_size, fil[i + 1] - fil[i]
			)
			if offsets is None:
				o = numpy.zeros_like(p)
			else:
				o = offsets[i]
				if mark_single_filament is not None:
					print(f'OFFSET {i: >3}: {o}')
			assert numpy.isclose(o @ dp, 0, atol=1e-6, rtol=0), (
				fil_id, i, o, dp, o @ dp
			)
			plot_points(
				snap, lim, proj, fil_id, [p + o, p + o + dp],
				shifts=(-snap.box_size, 0, snap.box_size)
			)
		# original lines
		points = [numpy.copy(fil[0])]
		for point in numpy.copy(fil[1:]):
			color = None
			linewidth = None
			if mark_single_filament is None:
				if offsets is not None: break
			else:
				if fil_id == mark_single_filament:
					if offsets is not None:
						color = adjust_lightness('gray', 1.4)
						linewidth = 0.75
				else:
					continue
			if numpy.linalg.norm(points[-1] - point) > 0.9 * snap.box_size:
				plot_points(
					snap, lim, proj, fil_id, points, color=color,
					linewidth=linewidth
				)
				dp = bin_density.dist_periodic_wrap_vec(
					snap.box_size, point - points[-1]
				)
				# need to plot crossing line at both -L and +L,
				# since we don’t know what side the line is on
				plot_points(
					snap, lim, proj, fil_id, [points[-1], points[-1] + dp],
					shifts=(-snap.box_size, 0, snap.box_size),
					color=color, linewidth=linewidth
				)
				points = []
			points.append(point)
		plot_points(
			snap, lim, proj, fil_id, points, color=color, linewidth=linewidth
		)
	if f:
		f.close()


if __name__ == '__main__':
	args = parser.parse_args()
	main()

