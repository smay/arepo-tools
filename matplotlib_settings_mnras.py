import matplotlib.pyplot as plt

plt.rc('xtick', top=True)
plt.rc('ytick', right=True)
plt.rc('xtick.minor', visible=True)
plt.rc('ytick.minor', visible=True)
plt.rc('savefig', bbox='tight', pad_inches=0.05, transparent=True)
plt.rc('figure', dpi=600)
plt.rc('figure.constrained_layout', use=True, h_pad=0.01, w_pad=0.01)

plt.rc('text', usetex=True)
plt.rc('pgf', texsystem='pdflatex', rcfonts=False)

# MNRAS: \normalsize=9pt, captions: 8pt
plt.rc('font', family='serif', size=8)
plt.rc('legend', fontsize='small')
# default:
#plt.rc('lines', linewidth=1.5)
plt.rc('lines', linewidth=1.0)
plt.rc('mathtext', fontset='dejavuserif')
plt.rc('pgf', preamble=r'\usepackage{newtxtext,newtxmath}')
# default:
# figure.figsize:     6.4, 4.8  # figure size in inches
plt.rc('figure', figsize=(3.375, 2.53))
