#!/usr/bin/env python3
import argparse
import astropy.constants as constants
import astropy.units as units
import numba
import numexpr
import numpy
import numpy.random
from pathlib import Path
from read_snapshot.gadget import \
	HEADER_DTYPE, HEADER_LEN, PARTICLE_DTYPE_SINGLE
PARTICLE_DTYPE = PARTICLE_DTYPE_SINGLE

parser = argparse.ArgumentParser(description='Generate the initial conditions '
	'for arXiv:1807.04037 fig. 2 (in GADGET format)')
parser.add_argument('--PMGRID', type=int,
	help='the number N of grid points per dimension')
parser.add_argument('--BoxSize', type=float,
	help='the simulation box size')
parser.add_argument('--AxionMassEv', type=float,
	help='FDM particle mass in eV')
parser.add_argument('--total-mass', type=float,
	help='sum of generated soliton masses')
parser.add_argument('--num-solitons', type=int,
	help='number of solitons to generate')
parser.add_argument('--target-radius', type=float,
	help='radius around the box center within which to place solitons')
parser.add_argument('--seed', type=int, default=numpy.random.randint(2**32),
	help='seed for numpy.random')
parser.add_argument('output', nargs='?', default='fdm.dat',
	help='the output path for the IC file')
args = parser.parse_args()

assert args.target_radius <= args.BoxSize / 2

numpy.random.seed(args.seed)

# for writing an IC file in GADGET format
PTYPE_HALO = 1
# maximum number of particles which fit into a single file in GADGET format due
# to use of 32-bit block lengths
LONGEST_FIELD_LEN = numpy.dtype('3f4').itemsize
NPART_MAX = 2**31 // LONGEST_FIELD_LEN

# Needs to match resolution of soliton profile array file. Default = 0.00001
DELTA_X = 0.000001
# α = M²/3.883²
SOLITON_ALPHA_M = 1 / 3.8827652756**2
SOLITON_BETA = 2.453887
MAX_DIST = 5.6
MASS_CORRECTION_FAC = 1.00474

# Unit conversion from/to PyUltraLight units, which use ψ' = T √(mG) ψ
# (with T given below: PU_UnitTime).
# The exact values of the physical constants technically don’t matter because
# units are converted to the target units in the output.
PU_hbar = 1.0545718e-34 * (units.m**2 * units.kg / units.s)
PU_G = 6.67e-11 * (units.N * units.m**2 / units.kg**2)
PU_omega_m0 = 0.31
PU_H_0 = 67.7 * units.km / units.Mpc / units.s
PU_axion_mass = args.AxionMassEv * 1.783e-36 * units.kg
PU_UnitLength = ((
	8 * numpy.pi * PU_hbar**2 /
	(3 * PU_axion_mass**2 * PU_H_0**2 * PU_omega_m0)
)**0.25).to('kpc')
PU_UnitTime = ((3 * PU_H_0**2 * PU_omega_m0 / (8 * numpy.pi))**-0.5).to('Gyr')
PU_UnitMass = (
	(3 * PU_H_0**2 * PU_omega_m0 / (8 * numpy.pi))**0.25 * PU_hbar**1.5 /
	(PU_axion_mass**1.5 * PU_G)
).to('solMass')

target_UnitLength = 1.0 * units.kpc
target_UnitVelocity = 1e5 * (units.cm / units.s)
target_UnitMass = 1e10 * units.solMass
target_UnitTime = target_UnitLength / target_UnitVelocity

PU_density_factor = (PU_UnitTime**2 * PU_G).decompose([
	units.Unit(target_UnitLength),
	units.Unit(target_UnitTime),
	units.Unit(target_UnitMass),
])

OUTPUT_FILE_NAME = args.output
PMGRID = args.PMGRID
BOX_SIZE_target = args.BoxSize * target_UnitLength
CELL_LEN_target = BOX_SIZE_target / PMGRID
BOX_SIZE = BOX_SIZE_target.to_value(PU_UnitLength)
TIME_BEGIN = 0.

# cf. https://stackoverflow.com/a/35608701
def ndindex_array(dimensions, dtype=int):
	return numpy.stack(numpy.indices(dimensions, dtype=dtype), -1).reshape(
		-1, len(dimensions)
	)

def write_block(output, block, block_len):
	assert block.nbytes == block_len
	output.write(block_len)
	block.tofile(output)
	output.write(block_len)

@numba.njit
def coord_periodic_wrap(box_size, x):
	while x >= box_size:
		x -= box_size
	while x < 0:
		x += box_size
	return x

@numba.njit
def coord_periodic_wrap_vec(box_size, x):
	x_flat = x.reshape(-1)
	for i in range(len(x_flat)):
		x_flat[i] = coord_periodic_wrap(box_size, x_flat[i])
	return x

@numba.njit
def dist_periodic_wrap(box_size, x):
	while x < -box_size / 2:
		x += box_size
	while x >= box_size / 2:
		x -= box_size
	return x

@numba.njit
def dist_periodic_wrap_vec(box_size, x):
	x_flat = x.reshape(-1)
	for i in range(len(x_flat)):
		x_flat[i] = dist_periodic_wrap(box_size, x_flat[i])
	return x

def soliton_radius(mass):
	#TODO: numerical values
#	return (
#		(5.5e9 * units.solMass / mass) * (1e-23 / args.AxionMassEv)**2 *
#		units.kpc
#	).to('kpc')
	# arXiv:1606.05151 eq. (5)
	return (
		(8.64e6 * units.solMass / (0.237 * mass)) *
		(2.5e-22 / args.AxionMassEv)**2 * units.kpc
	).to('kpc')

def get_soliton_com(solitons, soliton_masses):
	com = numpy.zeros(3)
	for i in range(3):
		soliton_pos_i = numpy.array([pos[i] for _, pos, *_ in solitons])
		# get (angular) COM
		# see https://en.wikipedia.org/wiki/Center_of_mass#Systems_with_periodic_boundary_conditions
#		#TODO
#		angles = soliton_pos_i * 2 * numpy.pi / BOX_SIZE
#		angles_mean_x = (
#			numpy.sum(soliton_masses * numpy.cos(angles)) / args.total_mass
#		)
#		angles_mean_y = (
#			numpy.sum(soliton_masses * numpy.sin(angles)) / args.total_mass
#		)
#		com_target_angle = COM_TARGET[i] * 2 * numpy.pi / BOX_SIZE
#		com_target_angle_x = numpy.cos(com_target_angle)
#		com_target_angle_y = numpy.sin(com_target_angle)
#		pos_angle_x = (
#			args.total_mass / mass_remaining *
#			(com_target_angle_x - angles_mean_x)
#		)
#		pos_angle_y = (
#			args.total_mass / mass_remaining *
#			(com_target_angle_y - angles_mean_y)
#		)
#		pos_angle = numpy.arctan2(-pos_angle_y, -pos_angle_x) + numpy.pi
#		pos[i] = pos_angle * BOX_SIZE / (2 * numpy.pi)
		com[i] = COM_TARGET[i] + numpy.sum(
			soliton_masses *
			dist_periodic_wrap_vec(BOX_SIZE, soliton_pos_i - COM_TARGET[i])
		) / args.total_mass
#		tmp = coord_periodic_wrap(BOX_SIZE,
#			com[i] * args.total_mass / numpy.sum(soliton_masses)
#		)
#		print(f'{com[i]=}')
	return com


def final_soliton_pos(solitons, soliton_masses, mass_remaining):
	pos = numpy.zeros(3)
	com = get_soliton_com(solitons, soliton_masses)
	for i in range(3):
		pos[i] = args.total_mass / mass_remaining * dist_periodic_wrap(
			BOX_SIZE, COM_TARGET[i] - com[i]
		)
	return pos


# compare code in PyUltraLight
def overlap_check(candidate, soliton):
	for i in range(len(soliton)):
		m = max(candidate[0], soliton[i][0])
		d_sol = 5.35854 / m
		c_pos = numpy.array(candidate[1])
		s_pos = numpy.array(soliton[i][1])
		displacement = dist_periodic_wrap_vec(BOX_SIZE, c_pos - s_pos)
		distance = numpy.sqrt(
			displacement[0]**2 + displacement[1]**2 + displacement[2]**2
		)
		if distance < 2 * d_sol:
			return False
	return True

@numba.jit
def initsoliton(funct, xarray, yarray, zarray, position, alpha, f, delta_x):
	for index in numpy.ndindex(funct.shape):
		# Note also that this distfromcenter is here to calculate the distance
		# of every gridpoint from the center of the soliton, not to calculate
		# the distance of the soliton from the center of the grid
		distfromcenter = (
			(dist_periodic_wrap(BOX_SIZE, xarray[index[0], 0, 0] - position[0]))**2 +
			(dist_periodic_wrap(BOX_SIZE, yarray[0, index[1], 0] - position[1]))**2 +
			(dist_periodic_wrap(BOX_SIZE, zarray[0, 0, index[2]] - position[2]))**2
		)**0.5
		# Utilizes soliton profile array out to dimensionless radius 5.6.
		if (numpy.sqrt(alpha) * distfromcenter <= MAX_DIST):
			funct[index] = alpha * f[
				int(numpy.sqrt(alpha) * (distfromcenter / delta_x + 1))
			]
		else:
			funct[index] = 0
	return funct

def add_soliton(s, psi):
	mass, position, velocity, phase = s
	position = numpy.array(position, dtype=float)
	# Note that alpha and beta parameters are computed when the initial_f.npy
	# soliton profile file is generated.
	alpha = mass**2 * SOLITON_ALPHA_M
	beta = SOLITON_BETA
	funct = initsoliton(
		numpy.zeros((PMGRID,) * 3, dtype='complex128'), xarray, yarray, zarray,
		position, alpha, f, DELTA_X
	)
	# impart velocity to solitons in Galilean-invariant way
	t0 = TIME_BEGIN
	velx, vely, velz = velocity
	funct = numexpr.evaluate(
		'exp(1j * (alpha * beta * t0 +'
		'  velx * xarray + vely * yarray + velz * zarray -'
		'  0.5 * (velx * velx + vely * vely + velz * velz) * t0 + phase)) *'
		'funct'
	) * numpy.sqrt(MASS_CORRECTION_FAC)
	psi += funct
	mass_field = numpy.sum(
		numpy.abs(funct)**2 * (
			CELL_LEN_target**3 / PU_density_factor
		).to_value()
	)
	return mass_field

# initialization
f = numpy.load(f'ic/initial_f_{DELTA_X:f}.npy')
psi = numpy.zeros((PMGRID,) * 3, dtype='complex128')

# TODO: should be endpoint=False?
gridvec = numpy.linspace(-BOX_SIZE / 2, BOX_SIZE / 2, PMGRID)
# alternative:
# xarray, yarray, zarray = numpy.meshgrid(
#	*((gridvec,) * 3), indexing='ij', sparse=True
# )
xarray = numpy.ones((PMGRID, 1, 1))
yarray = numpy.ones((1, PMGRID, 1))
zarray = numpy.ones((1, 1, PMGRID))
xarray[:, 0, 0] = gridvec
yarray[0, :, 0] = gridvec
zarray[0, 0, :] = gridvec

# generate solitons
# TODO: Soliton positions use box center as origin!
COM_TARGET = numpy.zeros(3) #numpy.ones(3) * BOX_SIZE / 2
POS_RANGE = (args.target_radius * target_UnitLength).to_value(PU_UnitLength)
print(f'PU units: {PU_UnitMass = }, {BOX_SIZE = :g}')
num_solitons = args.num_solitons #numpy.random.randint(16, 64 + 1)
mass_mean = args.total_mass / num_solitons
#mass_interval = (0.3 * mass_mean, 1.7 * mass_mean)
mass_interval = (0.8 * mass_mean, 1.2 * mass_mean)
#TODO: check soliton radii
# solitons: [mass, [x, y, z], [vx, vy, vz], phase]
print('Starting random soliton placements')
success = False
while not success:
	mass_remaining = args.total_mass
	SOLITONS = []
	soliton_masses = []
	for i in range(num_solitons - 1):
		if mass_remaining <= mass_interval[1]:
			break
		mass = numpy.random.uniform(*mass_interval)
		mass_pu = (mass * target_UnitMass).to_value(PU_UnitMass)
		phase = numpy.random.uniform(0., 2 * numpy.pi)
		pos = numpy.zeros(3)
		overlap = True
		while overlap:
			in_range = False
			while not in_range:
				for j in range(len(pos)):
					pos[j] = numpy.random.uniform(
						COM_TARGET[j] - POS_RANGE, COM_TARGET[j] + POS_RANGE
					)
				in_range = numpy.linalg.norm(
					dist_periodic_wrap_vec(BOX_SIZE, pos - COM_TARGET)
				) < POS_RANGE
			soliton = [mass_pu, pos, [0., 0., 0.], phase]
			overlap = not overlap_check(soliton, SOLITONS)
		SOLITONS.append(soliton)
		soliton_masses.append(mass)
		mass_remaining -= mass
	# determine last soliton
	if mass_remaining <= mass_interval[0]:
		print(
			f'\tFailure due to remaining mass (= {mass_remaining}) being below '
			f'minimum soliton mass (= {mass_interval[0]})'
		)
		continue
	pos = numpy.copy(COM_TARGET)
	if num_solitons > 1:
		pos = final_soliton_pos(
			SOLITONS, numpy.array(soliton_masses), mass_remaining
		)
	mass_pu = (mass_remaining * target_UnitMass).to_value(PU_UnitMass)
	phase = numpy.random.uniform(0., 2 * numpy.pi)
	soliton = [mass_pu, pos, [0., 0., 0.], phase]
	if numpy.any(numpy.abs(pos - COM_TARGET) >= BOX_SIZE):
		print('\tFailure due to final soliton being outside the box:')
		print(f'\t\t{soliton}')
		continue
	if numpy.linalg.norm(pos - COM_TARGET) > POS_RANGE:
		print('\tFailure due to final soliton being outside of target radius:')
		print(f'\t\t{soliton}')
		continue
	if not overlap_check(soliton, SOLITONS):
		print('\tFailure due to final soliton overlapping others:')
		print(f'\t\t{soliton}')
		continue
	SOLITONS.append(soliton)
	soliton_masses.append(mass_remaining)
	success = True
com_expected = get_soliton_com(SOLITONS, numpy.array(soliton_masses))
print(f'Expected distance of COM from target: {com_expected}')
assert numpy.all(numpy.abs(com_expected - COM_TARGET) / BOX_SIZE < 1e-2)

# add solitons to wave function
soliton_masses_field = []
for i, soliton in enumerate(SOLITONS[:-1]):
	mass = soliton_masses[i]
	radius = soliton_radius(mass * target_UnitMass)
	print(f'Soliton {i + 1:2}: {mass = :g} (radius = {radius:g})')
	print(f'\t{soliton}')
	mass_field = add_soliton(soliton, psi)
	print(f'\tActual mass in field: {mass_field:g} (ratio = {mass / mass_field})')
	soliton_masses_field.append(mass_field)
# final soliton
total_field_mass = numpy.sum(
	numpy.abs(psi)**2 * (CELL_LEN_target**3 / PU_density_factor).to_value()
)
mass_remaining = args.total_mass - total_field_mass
mass_pu = (mass_remaining * target_UnitMass).to_value(PU_UnitMass)
pos = SOLITONS[-1][1]
if num_solitons > 1:
	pos = final_soliton_pos(
		SOLITONS[:-1], numpy.array(soliton_masses_field), mass_remaining
	)
SOLITONS[-1][0] = mass_pu
SOLITONS[-1][1] = pos
radius = soliton_radius(mass_remaining * target_UnitMass)
print(f'Soliton {num_solitons:2}: {mass_remaining = :g} (radius = {radius:g})')
print(f'\t{SOLITONS[-1]}')
mass_field = add_soliton(SOLITONS[-1], psi)
soliton_masses_field.append(mass_field)
print(f'\tActual mass in field: {mass_field:g} (ratio = {mass_remaining / mass_field})')
print(f'Total mass in field (individual): {numpy.sum(soliton_masses_field):g}')
assert overlap_check(SOLITONS[-1], SOLITONS[:-1])
assert numpy.linalg.norm(
	dist_periodic_wrap_vec(BOX_SIZE, pos - COM_TARGET)
) <= POS_RANGE

# write IC file
NPART_TOTAL_ALL = PMGRID**3
assert NPART_TOTAL_ALL <= 2**63
if NPART_TOTAL_ALL <= NPART_MAX:
	output_file_names = [OUTPUT_FILE_NAME]
else:
	output_file_names = [
		f'{OUTPUT_FILE_NAME}.{i}' for i in range(
			int(numpy.ceil(NPART_TOTAL_ALL / NPART_MAX))
		)
	]
header = numpy.rec.array(numpy.zeros((1,), dtype=HEADER_DTYPE))[0]
header.time = TIME_BEGIN
# TODO: for cosmological simulations
#header.redshift = 1 / (TIME_BEGIN - 1)
# low word is truncated automatically since it is an unsigned 32-bit integer
header.npartTotal[PTYPE_HALO] = NPART_TOTAL_ALL
header.npartTotalHighWord[PTYPE_HALO] = NPART_TOTAL_ALL >> 32
header.num_files = len(output_file_names)
header.BoxSize = BOX_SIZE
header.Omega0 = 0
header.OmegaLambda = 0
header.HubbleParam = 0
npart_remaining = NPART_TOTAL_ALL
com_target = COM_TARGET + BOX_SIZE_target.to_value() / 2
mass_sum = 0
com_sum = numpy.zeros(3)
com_angle_sum_x = numpy.zeros(3)
com_angle_sum_y = numpy.zeros(3)
for file_name in output_file_names:
	npart_done = NPART_TOTAL_ALL - npart_remaining
	npart = NPART_MAX if npart_remaining > NPART_MAX else npart_remaining
	header.npart[PTYPE_HALO] = npart
	npart_remaining -= npart
	with open(file_name, 'wb') as output:
		# block ID 1: HEAD
		block_len = numpy.uint32(HEADER_LEN)
		output.write(block_len)
		header.tofile(output)
		output.write(b'\0' * (HEADER_LEN - header.nbytes))
		output.write(block_len)
		# block ID 2: POS
		block_len = numpy.uint32(npart * PARTICLE_DTYPE['pos'].itemsize)
		block = ndindex_array(
			(PMGRID,) * 3, dtype=PARTICLE_DTYPE['pos'].subdtype[0]
		)[npart_done:npart_done + npart] * CELL_LEN_target.to_value()
		write_block(output, block, block_len)
		block_pos = block
		# block ID 3: VEL
		# the wave function’s phase is written into the first velocity
		# component
		block_len = numpy.uint32(npart * PARTICLE_DTYPE['vel'].itemsize)
		block = numpy.zeros((npart, 3), dtype=PARTICLE_DTYPE['vel'].subdtype[0])
		block[:, 0] = numpy.ravel(numpy.angle(psi))[
			npart_done:npart_done + npart
		]
		write_block(output, block, block_len)
		# block ID 4: ID
		block_len = numpy.uint32(npart * PARTICLE_DTYPE['id'].itemsize)
		block = numpy.arange(
			1 + npart_done,
			1 + npart_done + npart, dtype=PARTICLE_DTYPE['id']
		)
		write_block(output, block, block_len)
		# block ID 5: MASS
		# the mass value is determined by the squared absolute value of the
		# wave function
		block_len = numpy.uint32(npart * PARTICLE_DTYPE['mass'].itemsize)
		block = numpy.ravel(numpy.abs(psi)**2).astype(PARTICLE_DTYPE['mass'])[
			npart_done:npart_done + npart
		] * CELL_LEN_target.to_value()**3
		# convert units from PyUltraLight to target
		block /= PU_density_factor.to_value()
		# TODO: fix reading 0 values in AREPO
		block[block == 0] = 1e-10 / PMGRID**3
		write_block(output, block, block_len)
		block_mass = block
		# statistics
		mass_sum += numpy.sum(block_mass)
		com_sum += numpy.sum(
			block_mass[:, numpy.newaxis] * dist_periodic_wrap_vec(
				BOX_SIZE_target.to_value(), block_pos - com_target
			),
			axis=0
		)
		pos_angles = block_pos * 2 * numpy.pi / BOX_SIZE_target.to_value()
		com_angle_sum_x += numpy.sum(
			block_mass[:, numpy.newaxis] * numpy.cos(pos_angles),
			axis=0
		)
		com_angle_sum_y += numpy.sum(
			block_mass[:, numpy.newaxis] * numpy.sin(pos_angles),
			axis=0
		)
# write random seed and arguments to text file
seed_path = Path(OUTPUT_FILE_NAME).with_suffix('.txt')
seed_path = seed_path.with_stem(f'{seed_path.stem}_info')
with open(seed_path, 'w') as f:
	print('Seed:', file=f)
	print(args.seed, file=f)
	print(file=f)
	print('Arguments:', file=f)
	print(args, file=f)

com_angle = numpy.arctan2(
	-com_angle_sum_y / mass_sum, -com_angle_sum_x / mass_sum
) + numpy.pi
com_angular = com_angle * BOX_SIZE_target.to_value() / (2 * numpy.pi)
com_standard = coord_periodic_wrap_vec(
	BOX_SIZE_target.to_value(), com_sum / mass_sum
)
print()
print('Final output:')
print(f'\tTotal mass: {mass_sum:g}')
print(f'\tCOM (angular):  {com_angular}')
print(f'\tCOM (standard): {com_standard}')

