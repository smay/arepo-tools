#!/usr/bin/env python3
import argparse
import functools
import itertools
import operator

MAX_EXPONENT = 16
MAX_PMGRID = 20000

parser = argparse.ArgumentParser(description='List grid sizes which have '
	'small prime factors')
parser.add_argument('--cores-per-node', type=int, nargs='+', default=[40],
	help='the number of CPU cores per cluster node; if several numbers are '
		'given, then the grid size must divide all of them '
		'(default: %(default)s)')
parser.add_argument('--num-nodes', type=int, default=1,
	help='use a fixed number of nodes (so that PMGRID must divide the product '
		'of --num-nodes and --cores-per-node)')
parser.add_argument('--factors', type=int, nargs='+', default=[2, 3, 5],
	help='the prime numbers which can appear as factors of the grid size '
		'(default: %(default)s)')
args = parser.parse_args()

factors = sorted(args.factors)
data = []
for exponents in itertools.product(range(MAX_EXPONENT), repeat=len(factors)):
	assert len(exponents) == len(factors)
	pmgrid = functools.reduce(
		operator.mul,
		[fac**exp for fac, exp in zip(factors, exponents)],
		1
	)
	if (
		any(
			pmgrid % (args.num_nodes * cores_per_node) != 0
			for cores_per_node in args.cores_per_node
		) or pmgrid > MAX_PMGRID
	):
		continue
	fac_str = ' * '.join(
		f'{fac: >2}^{exp: >2}' for fac, exp in zip(factors, exponents)
	)
	mem_gib = pmgrid**3 * 24 / 1024**3
	data.append((pmgrid, fac_str, mem_gib))

for pmgrid, fac_str, mem_gib in sorted(data):
	mem_per_node = ''
	if args.num_nodes != 1:
		mem_per_node = f', {mem_gib / args.num_nodes: >8,.1f} GiB/node'
	line = f'{pmgrid: 6} = {fac_str}  ({mem_gib: >9,.1f} GiB{mem_per_node})'
	print(line)

