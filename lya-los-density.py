#!/usr/bin/env python3
import argparse
from pathlib import Path
import matplotlib.pyplot as plt
import numpy
import snapshot
import matplotlib_settings


parser = argparse.ArgumentParser(description='Plot line of sight density '
	'calculated by “Absorptionspectrum”.')
parser.add_argument('--labels', nargs='+',
	help='plot labels (same length as “path”)')
parser.add_argument('--label-template',
	help='template for plot labels')
parser.add_argument('--ylim', nargs=2, type=float, default=(1e-2, 1e1),
	help='the plot’s vertical axisrange in units of the average matter density '
		'(default: %(default)s)')
parser.add_argument('--plot-format', default='png',
	help='the file format of the generated plot(s) (default: %(default)s)')
parser.add_argument('--dpi', type=float, default=300.,
	help='the resolution of the generated plot(s) (default: %(default)s)')
parser.add_argument('output_dir', type=Path,
	help='AREPO output_directory')
parser.add_argument('spec_path', nargs='+', type=Path,
	help='spec*.dat files (created by “Absorptionspectrum”) to plot')
args = parser.parse_args()


if args.labels:
	assert len(args.spec_path) == len(args.labels)
else:
	args.labels = [''] * len(args.spec_path)
for path, label in zip(args.spec_path, args.labels):
	print(path)
	_, snap_num, *_ = path.name.split('_')
	snap_path_single = args.output_dir / f'snap_{snap_num}.hdf5'
	snap_path_dir = args.output_dir / f'snapdir_{snap_num}'
	if snap_path_single.exists():
		snap_path = snap_path_single
	elif snap_path_dir:
		snap_path = snap_path_dir
	else:
		assert False
	snap = snapshot.Snapshot(snap_path)
	rho_mean = (
		snap.header['Omega0'] * snap.constant_in_internal_units(
			'critical_density0'
		)
	)
	BoxSize = snap.header['BoxSize']
	snaptime = snap.header['Time']
	with open(path, 'rb') as spec_file:
		pixels = numpy.fromfile(spec_file, 'u4', 1)[0]
		box_size = numpy.fromfile(spec_file, 'f8', 1)[0]
		assert box_size == BoxSize
		vel_scale = numpy.fromfile(spec_file, 'f8', 1)[0]
		time = numpy.fromfile(spec_file, 'f8', 1)[0]
		assert time == snaptime
		xpos = numpy.fromfile(spec_file, 'f8', 1)[0]
		ypos = numpy.fromfile(spec_file, 'f8', 1)[0]
		dir_flag = numpy.fromfile(spec_file, 'i4', 1)[0]
		tau = numpy.fromfile(spec_file, 'f8', pixels)
		temp = numpy.fromfile(spec_file, 'f8', pixels)
		vpec = numpy.fromfile(spec_file, 'f8', pixels)
		rho = numpy.fromfile(spec_file, 'f8', pixels)
		rho_neutral = numpy.fromfile(spec_file, 'f8', pixels)
		N_HI = numpy.fromfile(spec_file, 'f8', pixels)
		metallicity = numpy.fromfile(spec_file, 'f8', pixels)
		dens_temp = numpy.fromfile(spec_file, 'f8', pixels)
		tau_HeII = numpy.fromfile(spec_file, 'f8', pixels)
		rho_HeII = numpy.fromfile(spec_file, 'f8', pixels)
		N_HeII = numpy.fromfile(spec_file, 'f8', pixels)
		temp_HeII = numpy.fromfile(spec_file, 'f8', pixels)
		vpec_HeII = numpy.fromfile(spec_file, 'f8', pixels)
		assert spec_file.read(1) == b''
	dist = numpy.arange(pixels) * box_size / pixels
	plot_opts = {}
	if label:
		plot_opts['label'] = label
	elif args.label_template:
		plot_opts['label'] = args.label_template.format(**dict(
			pixels=pixels, box_size=box_size, time=time, dir_flag=dir_flag
		))
	rho_plot = rho / rho_mean
	rho_plot[rho_plot == 0] = 1e-50
	plt.plot(dist, rho_plot, **plot_opts)

# save figure
plt.ylim(args.ylim)
plt.yscale('log')
#TODO units
plt.xlabel(r'$r$ / $\mathrm{kpc} / h$')
plt.ylabel(r'$\rho$ / $\langle \rho \rangle$')
if any(args.labels):
	plt.legend()
savefig_opts = {'dpi': args.dpi}
plt.savefig(f'los_density.{args.plot_format}', **savefig_opts)

