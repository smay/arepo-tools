#!/usr/bin/env python3
import argparse
import collections
import time
from pathlib import Path
import h5py
import numba
import numpy
import numpy.random
import pandas
import bin_density
import iterate_grid
import m200_util
import mpi_util
import read_snapshot
import read_snapshot.hdf5 as read_hdf5
import snapshot
import units_util
from mpi_util import MPI, mpi_comm, mpi_rank, mpi_size, print0
from snapshot import Snapshot


# constants
PARTICLE_TYPE = 1
# use at most 1 GiB per core for local halo profile data (40 GiB for 40 cores)
PROFILES_MAX_MEM_SIZE_MB = 1024

def mass_bin(s):
	try:
		low, high = map(float, s.split(','))
		return low, high
	except:
		raise argparse.ArgumentTypeError(
			'low and high bound of bin must be given as “low,high” '
			f'(problematic input: “{s}”)'
		)

def point(s):
	try:
		x, y, z = map(float, s.split(','))
		return x, y, z
	except:
		raise argparse.ArgumentTypeError(
			f'coordinates must be given as “x,y,z” (problematic input: “{s}”)'
		)

parser = argparse.ArgumentParser(description='Calculate halo density profiles '
	'for fuzzy dark matter or cold dark matter simulation snapshots')
parser.add_argument('--prefer-type', choices=('grid', 'particles'),
	default='particles',
	help='the type of snapshot data that should be preferred '
		'(default: %(default)s)')
parser.add_argument('--r-bins', type=int, default=500,
	help='the number of bins to use for the distance from the halo center '
		'(default: %(default)d)')
parser.add_argument('--m200', type=int, nargs='+',
	help='use M_200 instead of FoF mass to bin the halos, for the '
		'corresponding file (default: all true)')
parser.add_argument('--m200-prefer', choices=('hdf5, csv'), default='hdf5',
	help='which M_200 data source to prefer (default: %(default)s)')
parser.add_argument('--halo-id-offset', type=int, default=0,
	help='do not process halos with IDs smaller than the given ID '
		'(default: %(default)d)')
parser.add_argument('--mass-bins', type=mass_bin, nargs='+',
	default=[(0, numpy.inf)],
	help='only do calculations for halos which fit into one of a set of '
		'specified mass bins (default: %(default)s)')
parser.add_argument('--halo-centers', type=point, nargs='+',
	help='use given points as halo centers instead of reading FOF files'
		'(in this case, the given paths are snapshot instead of FOF catalogs)')
parser.add_argument('--mass-bin-sample-max', type=int, default=-1,
	help='the maximum number of halos to use per mass bin; < 0 means no limit '
		'(default: %(default)d)')
parser.add_argument('--mass-bin-sample-seed', type=int, default=123456,
	help='random seed to use for sampling mass bins (default: %(default)d)')
parser.add_argument('--fof-group-min-len', type=int, default=8,
	help='minimum FoF length to be processed (default: %(default)d)')
parser.add_argument('--max-halos-per-iteration', type=int,
	help='maximum number of halos to process per iteration')
parser.add_argument('--only-bound', type=int, nargs='+',
	help='only calculate for gravitationally bound halos for the corresponding '
		'file (assumes file name <path>_halo_energy.hdf5; default: false)')
parser.add_argument('--max-virial-ratio', type=float, nargs='+',
	help='only consider halos with a virial ratio less than the given value '
		'(assumes file name <path>_halo_energy.hdf5; default: no bound)')
parser.add_argument('--snap-chunk-mem-size', type=float,
	default=snapshot.CHUNK_MEM_SIZE_DEFAULT / 1024**2,
	help='maximum amount of memory to use for snapshot data per core, in MiB '
		'(default: %(default)g)')
parser.add_argument('--load', type=Path,
	help='path to a previously-computed set of halo density profiles '
		'(default: <path>_halo_profiles.hdf5')
parser.add_argument('--output-file-prefix', default='',
	help='prefix for the output file names')
parser.add_argument('path', type=Path, nargs='+',
	help='the path to the fof_tab catalog; any normal files are interpreted '
		'as single files, while all files in any given directory are '
		'interpreted as a single multi-file output')
args = parser.parse_args()

# global variables
RNG = numpy.random.default_rng(args.mass_bin_sample_seed)


def main():
	assert args.r_bins > 1
	mpi_util.default_print_args['flush'] = True
	if args.load:
		assert len(args.path) == 1, (
			'--load doesn’t really make sense with more than 1 path'
		)
	# process paths
	if not args.m200:
		args.m200 = [args.halo_centers is None] * len(args.path)
	if not args.only_bound:
		args.only_bound = [False] * len(args.path)
	if not args.max_virial_ratio:
		args.max_virial_ratio = [numpy.inf] * len(args.path)
	for path_args in zip(
		args.path, args.m200, args.only_bound, args.max_virial_ratio
	):
		process_path(*path_args)
		print0()


def process_path(path, use_m200, only_bound, max_virial_ratio):
	print0(path)
	fof_tab_files = read_snapshot.snapshot_files(path)
	# read fof_tab data
	if args.halo_centers:
		groups = numpy.rec.array(numpy.zeros(
			len(args.halo_centers), dtype=read_hdf5.GROUP_DTYPE_SINGLE
		))
		for i in range(len(groups)):
			groups[i].pos = numpy.array(args.halo_centers[i])
		groups.tot_len = args.fof_group_min_len
	else:
		fof_tab_data = None
		if mpi_rank == 0:
			fof_tab_data = read_hdf5.read_fof_tab(fof_tab_files, read_ids=False)
		# broadcast fof_tab data to other ranks
		if mpi_size > 1:
			fof_tab_data = mpi_comm.bcast(fof_tab_data, root=0)
		[fof_config, fof_param, fof_header], groups, _ = fof_tab_data
		print0(f'Processing fof_tab with {len(groups)} groups')
		if len(groups) == 0:
			print0('fof_tab contains no groups')
	mass_bins = bin_density.create_mass_bins(args.mass_bins)
	print0(
		'Mass bins:',
		', '.join(f'({low:g}, {high:g})'
		for low, high, *_ in mass_bins.itertuples(index=False))
	)
	# read snapshot metadata
	print0('Reading snapshot metadata…')
	snap = Snapshot(
		path if args.halo_centers else
		read_snapshot.output_path(path, fof_config, fof_param)
	)
	units = units_util.get_units(snap.header)
	box_size_kpc = snap.header['BoxSize'] * units.length_in_kpc
	softening_kpc = snap.param[
		'SofteningComovingType'
		f'{snap.param[f"SofteningTypeOfPartType{PARTICLE_TYPE}"]}'
	] * units.length_in_kpc
	cell_len_kpc = box_size_kpc / snap.pmgrid
	if snap.get_type(args.prefer_type) == 'grid':
		r0_kpc = cell_len_kpc
	elif snap.get_type(args.prefer_type) == 'particles':
		r0_kpc = softening_kpc / 10
	else: assert False
	r1_kpc = box_size_kpc / 2
	r_lim_kpc = (r0_kpc, r1_kpc)
	snaptime = snap.header['Time']
	z = 1 / snaptime - 1
	print0(f'\tRedshift: z = {z}')
	# determine which halos are contained in which mass bin, enforcing
	# options such as minimum FoF lengths
	if use_m200:
		m200_data = m200_util.read_m200_data(
			path, fof_data=fof_tab_data, prefer=args.m200_prefer
		)
	else:
		m200_data = pandas.DataFrame(numpy.zeros(
			len(groups), dtype=numpy.dtype([
				('id',       'u8'),
				('cx_kpc',   'f8'),
				('cy_kpc',   'f8'),
				('cz_kpc',   'f8'),
			])
		))
		m200_data['id'] = numpy.arange(len(groups))
		for idim, sdim in enumerate(['cx_kpc', 'cy_kpc', 'cz_kpc']):
			m200_data[sdim] = groups.pos[:, idim] * units.length_in_kpc
	if not args.halo_centers:
		if use_m200:
			# TODO print M_200 data source
			print0('Using M_200 data')
		else:
			print0('Using FoF masses/center of mass')
	if args.mass_bin_sample_max > 0:
		print0(
			f'Sampling at most {args.mass_bin_sample_max} halos per mass bin'
		)
	load_energies = only_bound or max_virial_ratio < numpy.inf
	if load_energies:
		with h5py.File(
			path.with_name(f'{path.name}_halo_energy.hdf5'), 'r'
		) as f:
			energy_kin = f['KineticEnergy'][...] + f['QuantumEnergy'][...]
		energy_pot = groups.potegy
		energy_tot = energy_kin + energy_pot
		args.mask = numpy.ones_like(energy_tot, dtype=bool)
	if only_bound:
		args.mask &= energy_tot < 0
	if max_virial_ratio < numpy.inf:
		virial_ratio = -2 * energy_kin / energy_pot
		args.mask &= virial_ratio < max_virial_ratio
	bin_halos = bin_density.find_bin_halos(
		mass_bins, groups, m200_data, units, use_m200, args, RNG
	)
	num_halos_tot = mass_bins['num_halos'].sum()
	assert num_halos_tot == sum(len(b.halos) for b in bin_halos)
	assert (
		not use_m200 or
		num_halos_tot == sum(len(b.m200_ms) for b in bin_halos)
	)
	# print info about mass bins
	for bin_idx, bin_low, bin_high, *_ in mass_bins.itertuples():
		print0(
			f'Mass bin ({bin_low:g}, {bin_high:g}) M☉/h: containing '
			f'{mass_bins.at[bin_idx, "num_halos_unfiltered"]} halos '
			f'(processing {mass_bins.at[bin_idx, "num_halos"]}, '
			f'min. length = {args.fof_group_min_len})'
		)
	# check existing density profiles
	halo_ids_todo = None
	num_chunks = None
	output_path = args.load if args.load else path.with_name(
		f'{args.output_file_prefix}{path.name}_halo_profiles.hdf5'
	)
	if mpi_rank == 0:
		if output_path.exists():
			print0(f'Not recomputing existing data in file “{output_path}”')
			with h5py.File(output_path, 'r') as of:
				assert len(of['r_bins_kpc']) == of['rho_ms_kpc3'].shape[-1] + 1
				assert of['rho_ms_kpc3'].shape == of['count'].shape
				assert of['rho_ms_kpc3'].shape == (len(groups), args.r_bins), (
					f'“{output_path}” contains data with shape '
					f'{of["rho_ms_kpc3"].shape} (expected '
					f'{(len(groups), args.r_bins)})'
				)
		else:
			print0(
				f'“{output_path}” not found as existing file, starting from '
				'scratch'
			)
			assert not args.load
			# initialize HDF5 datasets
			with h5py.File(output_path, 'w') as of:
				of['r_bins_kpc'] = bin_density.bin_edges(
					args.r_bins, r0_kpc, r1_kpc, central_bin=True,
					spacing='log'
				)
				of.create_dataset(
					'rho_ms_kpc3', shape=(len(groups), args.r_bins),
					dtype='f8', fillvalue=numpy.nan
				)
				of.create_dataset_like(
					'count', of['rho_ms_kpc3'], dtype='u4', fillvalue=0
				)
		# determine which halo profiles are both requested and missing
		with h5py.File(output_path, 'r') as of:
			# TODO: make this check more memory-efficient
			halos_not_done = numpy.all(of['count'][...] == 0, axis=1)
			halo_ids_todo = []
			for b in bin_halos:
				for i in b.ids:
					if i >= args.halo_id_offset and halos_not_done[i]:
						halo_ids_todo.append(i)
			del halos_not_done
		print0(f'{len(halo_ids_todo)} halos left to process for file “{path}”')
		if len(halo_ids_todo) > 0:
			print0(f'(starting with halo {numpy.min(halo_ids_todo)})')
			# split halo_ids_todo into chunks to avoid allocating too much
			# memory for local halo profiles at once, i.e. iterate over the
			# grid/particles multiple times if too many halos are requested
			nbytes_per_halo = (
				numpy.dtype('f8').itemsize + numpy.dtype('u4').itemsize
			) * args.r_bins
			max_num_halos = int(
				PROFILES_MAX_MEM_SIZE_MB * 1024**2 / nbytes_per_halo
			)
			if args.max_halos_per_iteration:
				max_num_halos = min(
					max_num_halos, args.max_halos_per_iteration
				)
			assert max_num_halos > 0
			chunk_num_halos = min(max_num_halos, len(halo_ids_todo))
			num_chunks = int(numpy.ceil(len(halo_ids_todo) / chunk_num_halos))
			print0(
				f'Can process at most {max_num_halos} halos at once; will '
				f'have to perform {num_chunks} grid/particle iterations'
			)
			print0()
	num_chunks = mpi_comm.bcast(num_chunks, root=0)
	halo_ids_todo = mpi_comm.bcast(halo_ids_todo, root=0)
	if len(halo_ids_todo) == 0:
		return
	for num_iteration, halo_ids_chunk in enumerate(
		numpy.array_split(halo_ids_todo, num_chunks), start=1
	):
		print0(
			f'Starting grid/particle iteration {num_iteration}/{num_chunks}'
		)
		process_chunk(
			snap, m200_data, halo_ids_chunk, r_lim_kpc, units, output_path
		)


def process_chunk(
	snap, m200_data, halo_ids_chunk, r_lim_kpc, units, output_path
):
	rho_mean_new, rho_num_new = create_halo_profiles(
		snap, m200_data, halo_ids_chunk, r_lim_kpc, units
	)
	if mpi_rank == 0:
		# convert units
		to_ms_kpc3 = units.mass_in_solar_mass / units.length_in_kpc**3
		rho_mean_new *= to_ms_kpc3
		# sort because h5py requires indexing in order
		sort_idx = numpy.argsort(halo_ids_chunk)
		ids_sorted = halo_ids_chunk[sort_idx]
		# save density profiles
		print0(f'Writing to file “{output_path}”')
		with h5py.File(output_path, 'a') as of:
			of['rho_ms_kpc3'][ids_sorted] = rho_mean_new[sort_idx]
			of['count'][ids_sorted] = rho_num_new[sort_idx]


def create_halo_profiles(snap, m200_data, halo_ids, r_lim_kpc, units):
	r0_kpc, r1_kpc = r_lim_kpc
	box_size_kpc = snap.header['BoxSize'] * units.length_in_kpc
	cell_len_kpc = box_size_kpc / snap.pmgrid
	# create binned density profiles for the given halos
	halo_profiles = numpy.zeros(
		(len(halo_ids), args.r_bins), dtype=[
			#TODO: why not u4 for symmetry with HDF5 output?
			('rho_sum_l', 'f8'), ('rho_num_l', 'u8')
		]
	)
	# process grid
	if snap.get_type(args.prefer_type) == 'grid':
		print0(f'Constructing param list for {len(halo_ids)} halos…')
		t0 = time.perf_counter()
		param = numba.typed.List()
		for halo_id in halo_ids:
			halo_center_kpc = (
				groups.pos * units.length_in_kpc if m200_data is None else
				m200_data.loc[
					halo_id, ['cx_kpc', 'cy_kpc', 'cz_kpc']
				].to_numpy()
			)
			halo_center_g3 = numpy.floor(
				halo_center_kpc / cell_len_kpc + 0.1
			).astype('i8')
			param.append(bin_density.ParamGrid(
				center_g3=halo_center_g3, ir_min=None, ir_max=None,
				binlog=True, central_bin=True, mpi_rank=mpi_rank
			))
		t1 = time.perf_counter()
		print0(f'\tTook {t1 - t0} sec.')
		iterate_grid.iterate(
			snap, bin_density.rho_bin_cells_bulk, halo_profiles,
			param=param, use_rho=True,
			chunk_mem_size=round(args.snap_chunk_mem_size * 1024**2)
		)
	# process particles
	elif snap.get_type(args.prefer_type) == 'particles':
		# for particles: read all particles into memory
		# TODO: chunked particle processing like iterate_grid
		numpart_total = read_snapshot.numpart_total(snap.header)
		particle_mass = snap.header['MassTable'][PARTICLE_TYPE]
		print0('Reading particle data…')
		t0 = time.perf_counter()
		# determine data to read on each rank
		part_chunks = snap.determine_chunks(
			mpi_rank, mpi_size, num_slabs=numpart_total, slab_size=1
		)
		first_part = part_chunks[0][0]
		numpart = part_chunks[-1][-1] - first_part
		part_slice = numpy.s_[first_part:first_part + numpart]
		# read positions and mass
		particle_pos = snap.read_snapshot(
			f'/PartType{PARTICLE_TYPE}/Coordinates', data_slice=part_slice
		)
		t1 = time.perf_counter()
		print0(f'\tTook {t1 - t0} sec.')
		# process halos
		t0 = time.perf_counter()
		for halo_idx, halo_id in enumerate(halo_ids):
			t1 = time.perf_counter()
			if halo_idx == 0 or t1 - t0 > 30:
				print0(
					f'\tCurrently processing halo {halo_idx: 5} (ID '
					f'{halo_id: 5}) of {len(halo_ids)} on rank {mpi_rank}…',
					flush=True
				)
				t0 = t1
			halo_center_kpc = m200_data.loc[
				halo_id, ['cx_kpc', 'cy_kpc', 'cz_kpc']
			].to_numpy()
			assert not numpy.any(numpy.isnan(halo_center_kpc))
			# radially bin density in each shell
			param = bin_density.ParamParticles(
				center=halo_center_kpc, mass=particle_mass,
				box_size=box_size_kpc, numpart_total=numpart_total,
				softening_length=None,
				r_min=r0_kpc, r_max=None, binlog=True, central_bin=True
			)
			r0, r1, binfac = bin_density.bin_particles(
				particle_pos, param, halo_profiles['rho_num_l'][halo_idx, :]
			)
			assert r0 == r0_kpc
			assert r1 == r1_kpc
			bin_edges = bin_density.bin_edges(
				args.r_bins, r0, r1, binfac=binfac, central_bin=True,
				spacing='log'
			)
			halo_profiles['rho_sum_l'] = (
				halo_profiles['rho_num_l'] * particle_mass
			)
	else: assert False
	# get global halo profiles
	# can’t directly call reduce on halo_profiles because operations
	# (like addition) are not defined on structured arrays
	# (“UFuncTypeError: ufunc 'add' did not contain a loop with signature
	# matching types …”)
	rho_sum_g = mpi_comm.reduce(
		halo_profiles['rho_sum_l'], op=MPI.SUM, root=0
	)
	rho_num_g = mpi_comm.reduce(
		halo_profiles['rho_num_l'], op=MPI.SUM, root=0
	)
	rho_mean_g = None
	if mpi_rank == 0:
		if snap.get_type(args.prefer_type) == 'grid':
			rho_mean_g, nonzero = (
				bin_density.density_mean_division(rho_sum_g, rho_num_g)
			)
			rho_mean_g[~nonzero] = numpy.nan
		elif snap.get_type(args.prefer_type) == 'particles':
			rho_mean_g = rho_num_g * particle_mass / (
				4/3 * numpy.pi * (bin_edges[1:]**3 - bin_edges[:-1]**3)
			)
			rho_mean_g[rho_num_g == 0] = numpy.nan
		else: assert False
	return rho_mean_g, rho_num_g


if __name__ == '__main__':
	main()
