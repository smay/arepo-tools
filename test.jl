#!/usr/bin/env julia
using EllipsisNotation
using EllipsisNotation: (..) as …
using FilePaths
using FilePathsBase: / as ⊘
using HDF5
using Test
using Util
using WrappingIndices

push!(LOAD_PATH, @__DIR__)
using Snapshots


HDF5_TEMPLATE_PATH = p"template.hdf5"


function prepare_snap_files(files, data, pmgrid; reshape_1d=true)
	for (path, psi_re) in zip(files, data)
		h5open(path, "r+") do f
			attrs(f["/Config"])["PMGRID"] = pmgrid
			attrs(f["/Header"])["NumFilesPerSnapshot"] = length(files)
			attrs(f["/Parameters"])["NumFilesPerSnapshot"] = length(files)
			for i in eachindex(attrs(f["/Header"])["NumPart_Total"])
				attrs(f["/Header"])["NumPart_Total"][i] *= length(files)
			end
			delete_object(f, "/FuzzyDM/PsiRe")
			delete_object(f, "/FuzzyDM/PsiIm")
			if reshape_1d
				psi_re = reshape(psi_re, :)
			end
			f["/FuzzyDM/PsiRe"] = psi_re
			ds_psi_re = f["/FuzzyDM/PsiRe"]
			create_dataset(
				f, "/FuzzyDM/PsiIm", datatype(ds_psi_re), dataspace(ds_psi_re);
				fill_value=0.
			)
		end
	end
end


@testset "Snapshots" begin
	# test slices
	slices = reverse.([
		(:, …),  # TODO: no way to specify just “…”?
		(3¦26, …),
		(3¦26, :, :),
		(-17¦33, :, :),
		(-17¦3¦33, :, :),
		(-16¦3¦33, :, :),
		(-15¦3¦33, :, :),
		(3¦26, 80¦95, :),
		(3¦26, :, 33¦70),
		(:, 80¦95, 33¦70),
		(3¦26, 80¦95, 33¦70),
		(-40¦-25, 80¦95, 33¦70),
		(3¦26, 80¦95, -12¦8),
		(3¦26, -9¦17, 33¦70),
		(-5¦7, 80¦95, 33¦70),
		(-5¦7, -9¦17, -12¦8),
		(-5¦2¦17, -9¦3¦37, -12¦4¦28),
		(-5¦3¦17, -9¦4¦37, -12¦5¦28),
		(-4¦3¦17, -8¦4¦37, -11¦5¦28),
		(-3¦3¦17, -7¦4¦37, -10¦5¦28),
		(16¦15, 8¦7, 29¦28),
		(16¦5, 8¦3, 29¦11),
		(16¦3¦15, 8¦4¦7, 29¦5¦28),
		(-15¦-16, -7¦-8, -28¦-29),
		(-15¦-26, -7¦-12, -11¦-36),
		(-15¦4¦-16, -7¦3¦-8, -28¦5¦-29),
		(16¦115, 8¦107, 29¦128),
		(16¦105, 8¦103, 29¦111),
		(16¦3¦115, 8¦4¦107, 29¦5¦128),
		# TODO: check error in this case!
#		numpy.s_[-15:-5, -7:-3, -11:-29],
		# TODO: the following cases don’t work because they result in (e.g.)
#       numpy.s_[84:84, 92:92, 71:71] (for PMGRID = 100)
#		numpy.s_[-16:84, -8:92, -29:71],
#		numpy.s_[-16:69, -8:49, -29:53],
#		numpy.s_[-16:84:3, -8:92:4, -29:71:5],
		# similar cases, but only reading at most 99 elements per dimension
		(-16¦83, -8¦91, -29¦70),
		(-16¦69, -8¦49, -29¦53),
		(-16¦3¦83, -8¦4¦91, -29¦5¦70),
	])
	@testset "reshape_1d = $reshape_1d" for reshape_1d in (true, false)
		mktmpdir() do tdir
			# single file
			pmgrid = 100
			orig = collect(reshape((1.:pmgrid^3), ntuple(Returns(pmgrid), 3)))
			@testset "single file" begin
				d = tdir ⊘ "single"
				mkdir(d)
				s = cp_to_dir(HDF5_TEMPLATE_PATH, d)
				prepare_snap_files([s], [orig], pmgrid; reshape_1d)
				snap = Snapshot([s])
				for data_slice in slices
					data = read_snap_data(snap, "/FuzzyDM/PsiRe"; data_slice)
					@test data == orig[data_slice...]
				end
			end
			# multiple files, multiple slabs per file
			pmgrid = 100
			orig = collect(reshape((1.:pmgrid^3), ntuple(Returns(pmgrid), 3)))
			@testset "mult-mult, num_files = $num_files" for num_files in (17, 19, 20)
				d = tdir ⊘ "mult-mult-$num_files"
				mkdir(d)
				files = [
					cp(HDF5_TEMPLATE_PATH, d ⊘ "snap_000.$i.hdf5")
					for i in 0:num_files - 1
				]
				dx = pmgrid ÷ length(files)
				psi_re = [
					orig[
						…, 1 .+ (
							i * dx:
							(i + 1 < length(files) ? (i + 1) * dx : end) - 1
						)
					] for i in 0:length(files) - 1
				]
				prepare_snap_files(files, psi_re, pmgrid; reshape_1d)
				snap = Snapshot(files)
				for data_slice in slices
					data = read_snap_data(snap, "/FuzzyDM/PsiRe"; data_slice)
					@test data == orig[data_slice...]
				end
			end
			# multiple files, multiple slabs per file, unevenly distributed
			pmgrid = 117
			orig = collect(reshape((1.:pmgrid^3), ntuple(Returns(pmgrid), 3)))
			@testset "mult-mult uneven" begin
				d = tdir ⊘ "mult-mult-uneven"
				mkdir(d)
				slabs_x_per_file = [1, 12, 2, 1, 1, 1, 5, 1, 6, 64, 9, 13, 1]
				first_slab_x_of_file =
					cumsum([1, slabs_x_per_file[begin:end - 1]...])
				@assert sum(slabs_x_per_file) == pmgrid
				files = [
					cp(HDF5_TEMPLATE_PATH, d ⊘ "snap_000.$i.hdf5")
					for i in 0:length(slabs_x_per_file) - 1
				]
				psi_re = [
					orig[…, slabstart_x .+ (0:nslab_x - 1)]
					for (slabstart_x, nslab_x) in zip(
						first_slab_x_of_file, slabs_x_per_file
					)
				]
				prepare_snap_files(files, psi_re, pmgrid; reshape_1d)
				snap = Snapshot(files)
				for data_slice in slices
					data = read_snap_data(snap, "/FuzzyDM/PsiRe"; data_slice)
					@test data == orig[data_slice...]
				end
			end
			# multiple files, single slab per file
			pmgrid = 101
			orig = collect(reshape((1.:pmgrid^3), ntuple(Returns(pmgrid), 3)))
			@testset "mult-single" begin
				d = tdir ⊘ "mult-single"
				mkdir(d)
				files = [
					cp(HDF5_TEMPLATE_PATH, d ⊘ "snap_000.$i.hdf5")
					for i in 0:pmgrid - 1
				]
				psi_re = [orig[…, i:i] for i in 1:length(files)]
				prepare_snap_files(files, psi_re, pmgrid; reshape_1d)
				snap = Snapshot(files)
				for data_slice in slices
					data = read_snap_data(snap, "/FuzzyDM/PsiRe"; data_slice)
					@test data == orig[data_slice...]
				end
			end
		end
	end
end

