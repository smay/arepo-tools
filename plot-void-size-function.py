#!/usr/bin/env python3
import argparse
import re
import sys
from pathlib import Path
import astropy.constants
import astropy.units
import h5py
import numpy
import matplotlib.pyplot as plt
import pandas
import scipy.interpolate
import scipy.optimize
import io_util
import m200_util
import read_snapshot
import read_snapshot.hdf5 as read_hdf5
import units_util
#import matplotlib_settings

parser = argparse.ArgumentParser(description='Plot the void size functions for '
	'for Cartesian grid simulation snapshots')
parser.add_argument('--hist-bins', type=int, default=200,
	help='the number of histogram bins (default: %(default)d)')
parser.add_argument('--with-relative', type=Path,
	help='plot void size functions relative to the given snapshot in a '
		'separate panel')
parser.add_argument('--only-relative', type=Path,
	help='plot void size functions relative to the given snapshot')
parser.add_argument('--panel-ratio', type=float, default=1/3,
	help='ratio of second (relative plot) row/panel to the first (default: '
		'%(default)g')
parser.add_argument('--relative-label', default='0',
	help='denominator subscript for the label on the y-axis of the relative '
		'plot panel')
parser.add_argument('--relative-label-suffix', default='',
	help='additional content after the y label for the relative plot panel')
parser.add_argument('--relative-label-fraction', choices=('frac', '/'),
	default='frac',
	help='notation to use for the fraction for the relative plot panel')
parser.add_argument('--relative-pos', type=int, default=0,
	help='where to insert the reference void size function')
parser.add_argument('--relative-exclude', type=int, nargs='+', default=[],
	help='don’t include the given paths in the relative plot')
parser.add_argument('--min-size', type=float,
	help='only voids with size larger than this radius will be included')
parser.add_argument('--only-ids', type=int, nargs='+',
	help='only include voids with the given IDs for the corresponding file '
		'(assumes file name <path>_keep.npy with a boolean array of which '
		'voids to keep; default: false)')
parser.add_argument('--line-type', choices=('line', 'step'), default='line',
	help='type of line to draw histograms with')
parser.add_argument('--line-styles', nargs='+',
	help='line styles for the given plots')
parser.add_argument('--size-function-type', choices=('dn/dr', 'dn/dlnr'),
	default='dn/dlnr',
	help='units/representation of the size function values (default: '
		'%(default)s)')
parser.add_argument('--title', help='a title for the plot')
parser.add_argument('--no-title', action='store_true',
	help='disable plot title')
parser.add_argument('--no-legend', action='store_true',
	help='disable plot legend')
parser.add_argument('--labels', nargs='+',
	help='legend labels (used instead of automatic legend)')
parser.add_argument('--xlim', nargs=2, type=float, default=(0, 100),
	help='the void size range to plot (default: %(default)s)')
parser.add_argument('--ylim', nargs=2, type=float,
	help='the void size function range to plot')
parser.add_argument('--ylim-relative', type=float, nargs=2,
	help='the relative void size function range to plot')
parser.add_argument('--plot-file-name', help='override output file name')
parser.add_argument('--plot-format', default='png',
	help='the file format of the generated plot (default: %(default)s)')
parser.add_argument('--dpi', type=int, default=200,
	help='the resolution of the generated plot (default: %(default)d)')
parser.add_argument('--figaspect', type=float,
	help='the aspect ratio height/width of the figure')
parser.add_argument('path', nargs='+', type=Path,
	help='the paths to the fof_tab files; any normal files are interpreted '
		'as single files, while all files in any given directory are '
		'interpreted as a single multi-file output')
args = parser.parse_args()


#TODO: de-duplicate these formatting functions
def float_to_scientific_notation(x, digits=None, integer=True):
	sign = '-' if x < 0 else ''
	exponent = int(numpy.floor(numpy.log10(numpy.abs(x))))
	significand = x / 10**exponent
	if numpy.isclose(significand, 1):
		return fr'{sign} 10^{{{exponent}}}'
	else:
		if integer and numpy.isclose(significand / round(significand), 1):
			significand = round(significand)
		if digits is None:
			significand = f'{significand}'
		else:
			significand = f'{significand:.{digits}f}'
		return fr'{sign}{significand} \times 10^{{{exponent}}}'


# set up figure
plt.rc('figure.constrained_layout', hspace=0)
figsize = plt.rcParams['figure.figsize']
if args.figaspect:
	figsize = (figsize[0], figsize[0] * args.figaspect)
if args.with_relative:
	gridspec_kw = {
		'wspace': 0.06,
		'hspace': 0,
		'height_ratios': [1, args.panel_ratio]
	}
	fig, axes = plt.subplots(
		2, 1, sharex='col', sharey='row', figsize=(
			figsize[0], figsize[1] * 1.05 + gridspec_kw['height_ratios'][1]
		), gridspec_kw=gridspec_kw
	)
	axes[0].tick_params(which='both', bottom=False)
	axes[1].tick_params(which='both', top=False)
else:
	fig = plt.figure(figsize=figsize)
	axes = [plt.gca()]
# set up axes
relative = args.only_relative or args.with_relative
#TODO: label units
sizefunc_str = r'\mathrm{d}n_{\mathrm{V}}/\mathrm{d}r_{\mathrm{V}}'
sizefunc_unit_str = r'$h^4$ Mpc$^{-4}$'
if args.size_function_type == 'dn/dlnr':
	sizefunc_str = r'r_{\mathrm{V}} \, ' f'{sizefunc_str}'
	sizefunc_unit_str = r'$h^3$ Mpc$^{-3}$'
sizefunc_unit_str += ' (comoving)'
for ax in axes:
	# TODO?
	ax.tick_params(which='minor', labelleft=False, labelright=False)
	# prevent top/bottom x ticks from overlapping into panels above/below
	if ax is not axes[0]:
		ax.tick_params(which='both', top=False)
	if ax is not axes[-1]:
		ax.tick_params(which='both', bottom=False)
	if ax is axes[-1]:
		ax.set_xlabel(r'$r_{\mathrm{V}}$ / $h^{-1}$ Mpc (comoving)')
	if args.only_relative or (len(axes) > 1 and ax == axes[-1]):
		if args.relative_label_fraction == 'frac':
			rylabel = (
				r'$\textstyle\frac{' fr'({sizefunc_str})' '}{'
				fr'({sizefunc_str})_{{{args.relative_label}}}' '}$'
			)
		elif args.relative_label_fraction == '/':
			rylabel = (
				fr'$({sizefunc_str})$ / '
				fr'$({sizefunc_str})_{{{args.relative_label}}}$'
			)
		else: assert False
		rylabel += f'{args.relative_label_suffix}'
		ax.set_ylabel(rylabel)
	else:
		ax.set_ylabel(fr'${sizefunc_str}$ / {sizefunc_unit_str}')
	ax.set_yscale('log')

# setup for relative plotting
hist_ref = 1
if relative:
	args.path.insert(args.relative_pos, relative)
	args.path = [relative] + args.path

# set up bins
if len(args.path) > 1 and args.line_type == 'step':
	shift = numpy.linspace(-1, 1, len(args.path)) * 1e-2 * len(args.path)
else:
	shift = numpy.array([0.] * len(args.path))
bin_edges = numpy.linspace(args.xlim[0] - 1, args.xlim[1] + 1, args.hist_bins)
bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2
bin_centers_orig = bin_centers
# make sure to include all halos in the binning by adding additional bins
# at the low and high end so that the normalization of the histogram
# (density=True) will be correct
bin_edges_hist = numpy.zeros(len(bin_edges) + 2)
bin_edges_hist[0] = -numpy.inf
bin_edges_hist[-1] = numpy.inf
bin_edges_hist[1:-1] = bin_edges


# process group catalogs
if not args.labels:
	args.labels = [p.name for p in args.path][bool(relative):]
if not args.line_styles:
	args.line_styles = ['-'] * (len(args.path) - bool(relative))
if not args.only_ids:
	args.only_ids = [0] * (len(args.path) - bool(relative))
assert (
	len(args.path) - bool(relative) == len(args.labels) ==
	len(args.line_styles) == len(args.only_ids)
)
z_check = None
for path_idx, path in enumerate(args.path):
	bin_centers = numpy.copy(bin_centers_orig)
	reference = relative and path_idx == 0
	pprint = (lambda *args, **kwargs: None) if reference else print
	if not reference:
		path_idx -= bool(relative)
		pprint(path)
	# read data
	fof_tab_files = read_snapshot.snapshot_files(path)
	fof_tab_data = read_hdf5.read_fof_tab(fof_tab_files, read_ids=False)
	[config, param, header], groups, _ = fof_tab_data
	units = units_util.get_units(param)
	box_size = header['BoxSize'] * (1e-3 * units.length_in_kpc)
	pmgrid = config['PMGRID']
	cell_len = box_size / pmgrid
	hubble = param['HubbleParam']
	snaptime = header['Time']
	z = 1 / snaptime - 1
	if z_check is None:
		z_check = z
	pprint(f'\tz = {z}')
	assert numpy.isclose(z, z_check, atol=1e-9, rtol=1e-2), (z_check, z)
	# read data and plot
	sizes = (3 / (4 * numpy.pi) * groups.tot_len * cell_len)**(1/3)
	mask = numpy.ones_like(sizes, dtype=bool)
	mask_orig = mask
	num_valid = numpy.sum(mask_orig)
	num_invalid = numpy.sum(~mask_orig)
	pprint(f'\tConsidering {num_valid} voids')
	if args.min_size:
		min_size_mask = sizes > args.min_size
		num_excluded = numpy.sum(~(min_size_mask & mask_orig)) - num_invalid
		num_excluded_new = numpy.sum(~(min_size_mask & mask)) - numpy.sum(~mask)
		mask &= min_size_mask
		pprint(
			f'\tExcluded {num_excluded} voids with size ≤ '
			f'{args.min_size} Mpc/h (of which {num_excluded_new} were '
			'not already excluded)'
		)
	if args.only_ids[path_idx]:
		keep = numpy.load(path.with_name(f'{path.name}_keep.npy'))
		assert len(mask) == len(keep)
		num_excluded = numpy.sum(~(keep & mask_orig)) - num_invalid
		num_excluded_new = numpy.sum(~(keep & mask)) - numpy.sum(~mask)
		mask &= keep
		pprint(
			f'\tExcluded {num_excluded} voids via IDs (of which '
			f'{num_excluded_new} were not already excluded)'
		)
	sizes = sizes[mask]
	num_remaining = numpy.sum(mask)
	pprint(f'\t{num_remaining} voids remain after filtering')
	# normalize y-axis such that the values show the number density of halos
	# with respect to volume and size (dn/dr, n = N / V), i.e. such that
	# ∫ dn/dr dr = N / V
	hist, _ = numpy.histogram(sizes, bins=bin_edges_hist, density=True)
	# discard dummy low and high bins
	hist = hist[1:-1]
	hist *= len(sizes) / box_size**3
	if args.size_function_type == 'dn/dlnr':
		# dn/dln(r) = r dn/dr
		hist *= bin_centers
	# remember reference data and start processing actual entries
	if reference:
		hist_ref = hist
		continue

	# plot
	label = args.labels[path_idx].format(num_voids=num_remaining)
	# optionally slightly shift for multiple lines to improve visibility
	bin_centers_plot = bin_centers * (1 + shift[path_idx])
	for ax in axes:
		if relative and ax is axes[-1]:
			if path_idx in args.relative_exclude:
				continue
			hist_ref_ax = hist_ref
		else:
			hist_ref_ax = 1
		with numpy.errstate(divide='ignore', invalid='ignore'):
			if args.line_type == 'line':
				ax.plot(
					bin_centers_plot, hist / hist_ref_ax,
					args.line_styles[path_idx], label=label
				)
			elif args.line_type == 'step':
				ax.step(
					bin_centers_plot, hist / hist_ref_ax,
					args.line_styles[path_idx], where='mid', alpha=0.8,
					label=label
				)
			else: assert False

for ax in axes:
	# axis limits
	if args.xlim:
		ax.set_xlim(*args.xlim)
	if relative and ax is axes[-1]:
		if args.ylim_relative:
			ax.set_ylim(args.ylim_relative)
	elif args.ylim:
		ax.set_ylim(args.ylim)
	# legend
	if ax is axes[0] and not args.no_legend:
		ax.legend()
# title
title = args.title if args.title else ''
if z >= 0:
	title_time = f'$z = {z:.2f}$'
else:
	title_time = f'$a = {snaptime:.4f}$'
title += title_time
if title and not args.no_title:
	fig.suptitle(title)

# save plot
savefig_opts = {}
if args.plot_format != 'pdf':
	savefig_opts['dpi'] = args.dpi
plot_file_name = f'void_size_function_{snaptime:.4f}'
if args.only_relative:
	plot_file_name += '_ratio'
if args.plot_file_name:
	plot_file_name = args.plot_file_name
plot_file_name += f'.{args.plot_format}'
#TODO bbox_inches
fig.savefig(plot_file_name, transparent=True, bbox_inches='tight', **savefig_opts)
