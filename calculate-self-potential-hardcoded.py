from pathlib import Path
import numba
import numpy
import numpy.linalg
from mpi_util import MPI, mpi_comm, mpi_rank, mpi_size, print0
import read_snapshot as rs
import read_snapshot.hdf5 as rs5
from snapshot import Snapshot


#=================

@numba.njit
def pos_to_idx3(box_size, pmgrid, pos):
	cell_len = box_size / pmgrid
	idx_d = pos / cell_len
	idx_i = (idx_d + 0.1).astype('u4')
	idx_diff = numpy.abs(idx_d - idx_i)
	idx_diff_rel = idx_diff / (idx_i + 1)
	assert idx_diff_rel < 1e-6
	assert 0 <= idx_i < pmgrid
	return idx_i

@numba.njit
def idx3_to_pos(box_size, pmgrid, ix, iy, iz):
	cell_len = box_size / pmgrid
	result = numpy.array([ix, iy, iz], dtype='f8')
	return result * cell_len

@numba.njit
def icoord_periodic_wrap(ngrid, x):
	assert -1000000 < x < 1000000
	while x < 0:
		x += ngrid
	while x >= ngrid:
		x -= ngrid
	return x

#==============


@numba.njit
def dist_periodic_wrap(box_size, x):
	while x < -box_size / 2:
		x += box_size
	while x >= box_size / 2:
		x -= box_size
	return x

@numba.njit
def dist_periodic_wrap_vec(box_size, x):
	for i in range(len(x)):
		x[i] = dist_periodic_wrap(box_size, x[i])
	return x


@numba.njit
def calcpot_particles(pot, pos, mass, box_size, G, a):
	for i in range(len(pos)):
		p = 0
		for j in range(len(pos)):
			if i == j:
				continue
			dist = numpy.linalg.norm(
				dist_periodic_wrap_vec(box_size, pos[i] - pos[j])
			)
			p -= mass / dist
		pot[i] = (G / a) * mass * p / 2


@numba.njit
def calcpot(rho, pot, r, x_min, x_max, box_size, pmgrid, G, a):
	cell_len = box_size / pmgrid
	cell_vol = cell_len**3
	ir = int(r / cell_len)
	for ix1 in range(x_min, x_max):
		print('ix1 =', ix1)
		for iy1 in range(-ir, ir + 1):
			for iz1 in range(-ir, ir + 1):
				p = 0
				for ix2 in range(-ir, ir + 1):
					for iy2 in range(-ir, ir + 1):
						for iz2 in range(-ir, ir + 1):
							if ix1 == ix2 and iy1 == iy2 and iz1 == iz2:
								continue
							dist2 = 0
							dist2 += dist_periodic_wrap(
								box_size, (ix1 - ix2) * cell_len
							)**2
							dist2 += dist_periodic_wrap(
								box_size, (iy1 - iy2) * cell_len
							)**2
							dist2 += dist_periodic_wrap(
								box_size, (iz1 - iz2) * cell_len
							)**2
							dist = numpy.sqrt(dist2)
							if dist > r:
								continue
							mass2 = rho[ir + ix2, ir + iy2, ir + iz2] * cell_vol
							p -= mass2 / dist
				mass1 = rho[ir + ix1, ir + iy1, ir + iz1] * cell_vol
				pot[ir + ix1, ir + iy1, ir + iz1] = (G / a) * mass1 * p / 2


output_path = Path('/u/smay/runs/fdm_axionic_p8640_b10000_m7e-23/output/')

g=rs5.read_fof_tab(rs.snapshot_files(output_path / 'groups_008'), read_ids=False)[1]
halo = g[82]
snap = Snapshot(output_path / 'snapdir_008')
cell_len=snap.box_size/snap.pmgrid
pos=halo.pos
ipos=(pos/cell_len).astype('u4')
r200=halo.r200
ir200=int(r200/cell_len)
rho = None
if mpi_rank == 0:
	rho = snap.read_snapshot('/FuzzyDM/PsiRe', data_slice=numpy.s_[ipos[0]-ir200:ipos[0]+ir200+1, ipos[1]-ir200:ipos[1]+ir200+1, ipos[2]-ir200:ipos[2]+ir200+1])**2
	rho += snap.read_snapshot('/FuzzyDM/PsiIm', data_slice=numpy.s_[ipos[0]-ir200:ipos[0]+ir200+1, ipos[1]-ir200:ipos[1]+ir200+1, ipos[2]-ir200:ipos[2]+ir200+1])**2
rho = mpi_comm.bcast(rho)
pot = numpy.zeros_like(rho)
[ix_min, ix_max], = snap.determine_chunks(
	mpi_rank, mpi_size, chunk_mem_size=numpy.inf, num_slabs=2 * ir200 + 1
)
ix_min -= ir200
ix_max -= ir200
calcpot(rho, pot, r200, ix_min, ix_max, snap.box_size, snap.pmgrid, snap.constant_in_internal_units('G'), snap.header['Time'])
pot_g = None
mpi_comm.Reduce(MPI.IN_PLACE if mpi_rank == 0 else pot, pot, op=MPI.SUM, root=0)
if mpi_rank == 0:
	pot_g = pot
	numpy.save('pot.npy', pot_g)

