import matplotlib.pyplot as plt

plt.rc('xtick', top=True)
plt.rc('ytick', right=True)
plt.rc('xtick.minor', visible=True)
plt.rc('ytick.minor', visible=True)
plt.rc('legend', fontsize='x-small')
plt.rc('savefig', pad_inches=0.01)
plt.rc('figure.constrained_layout', use=True, h_pad=0.01, w_pad=0.01)

plt.rc('text', usetex=False)
plt.rc('pgf', texsystem='lualatex', rcfonts=False)

# TODO font, font size etc.
#plt.rc('xtick', labelsize='small')
#plt.rc('ytick', labelsize='small')
# MNRAS: \normalsize=9pt, captions: 8pt
#plt.rc('font', family='serif', size=8)
plt.rc('font', family='serif', size=7)
plt.rc('lines', linewidth=1.0)
plt.rc('mathtext', fontset='dejavuserif')
#plt.rc('pgf', preamble=r'\usepackage{newtxtext,newtxmath}')
plt.rc('pgf', preamble=''
	r'\usepackage{amsmath}'
	r'\usepackage{amssymb}'
	r'\usepackage{amsthm}'
	r'\usepackage{mathtools}'
	r'\usepackage[mathrm=sym, mathit=sym, mathsf=sym, mathbf=sym, mathtt=sym]{unicode-math}'
	'\setmainfont{UniversLTPro}['
#		'Path=fonts/,'
		'Extension=.ttf,'
		'UprightFont=*-55Roman,'
		'ItalicFont=*-55Oblique,'
		'BoldFont=*-65Bold,'
		'BoldItalicFont=*-65Bold,'
		'BoldItalicFeatures={FakeSlant},'
	']'
)
# default:
# figure.figsize:     6.4, 4.8  # figure size in inches
# TODO figure size
plt.rc('figure', figsize=(3.375, 2.53))
