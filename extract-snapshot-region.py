#!/usr/bin/env python3
import argparse
import concurrent.futures
import importlib
from pathlib import Path
import h5py
import numba
import numpy
import iterate_grid
import m200_util
psigrid_filter = importlib.import_module('psigrid-filter')
import snapshot
import units_util
import mpi_util
from mpi_util import HAS_MPI, MPI, mpi_comm, mpi_rank, mpi_size, print0

if HAS_MPI and mpi_size == 1:
	# deactivate MPI if mpi_size == 1 to be able to use fork()
	MPI.Finalize()
	globals().update(**mpi_util.get_shim_variables())

FDM_GROUP_NAME = '/FuzzyDM'
DATASET_NAME_RE = f'{FDM_GROUP_NAME}/PsiRe'
DATASET_NAME_IM = f'{FDM_GROUP_NAME}/PsiIm'
FDM_DENSITY_DATASET = f'{FDM_GROUP_NAME}/Density'


parser = argparse.ArgumentParser(description='Extract a cubic sub-region of a '
	'fuzzy dark matter snapshot')
parser.add_argument('--datasets', nargs='+', required=True,
	help='the datasets to read/write')
parser.add_argument('--slice-x', type=int, nargs=2, default=(None,) * 2,
	help='slice to extract in the x direction (default: %(default)s)')
parser.add_argument('--slice-y', type=int, nargs=2, default=(None,) * 2,
	help='slice to extract in the y direction (default: %(default)s)')
parser.add_argument('--slice-z', type=int, nargs=2, default=(None,) * 2,
	help='slice to extract in the z direction (default: %(default)s)')
parser.add_argument('--fof-ids', type=int, nargs='+',
	help='FOF groups to extract a region around')
parser.add_argument('--fof-ids-all', action='store_true',
	help='Like --fof-ids, but for all FOF groups')
parser.add_argument('--fof-ids-r200-fac', type=float, default=2.0,
	help='Radius to extract around FOF groups as a multiple of R_200')
parser.add_argument('--fof-ids-r200-min', type=float, default=2.5,
	help='Minimum R200 (in units of the cell length) of FOF groups to include')
parser.add_argument('--fof-ids-single-file', action='store_true',
	help='Write all output to a single file instead of one file per FOF group')
parser.add_argument('--fof-ids-single-file-group-name', default='halo_',
	help='HDF5 group name to use with --fof-ids-single-file')
parser.add_argument('--fof-downscale-pmgrid', type=int,
	help='Downscale output to (approximately) the given grid size for each FOF '
		'group')
parser.add_argument('--fof-downscale-threshold', type=int,
	help='Minimum grid size to downscale (default: --fof-downscale-pmgrid)')
parser.add_argument('--fof-downscale-max-per-iteration', type=int, default=200,
	help='Maximum number of halos to process per iteration for downscaling')
parser.add_argument('--max-workers', type=int,
	help='max_workers argument for ProcessPoolExecutor')
parser.add_argument('--output-path', type=Path, nargs='+',
	help='paths to write the outputs to')
parser.add_argument('path', type=Path, nargs='+',
	help='the paths to the snapshot files; any normal files are interpreted '
		'as single-file snapshots, while all files in any given directory '
		'are interpreted as a single multi-file snapshot')


def main():
	if not args.output_path:
		args.output_path = [None] * len(args.path)
	if args.fof_downscale_pmgrid and not args.fof_downscale_threshold:
		args.fof_downscale_threshold = args.fof_downscale_pmgrid
	assert len(args.path) == len(args.output_path)
	# process paths
	for path_args in zip(args.path, args.output_path):
		process_path(*path_args)


def process_path(path, output_path):
	print0(path)
	if not output_path:
		output_path = path.with_name(f'{path.name}_region.hdf5')
	snap = snapshot.Snapshot(path)
	cell_len = snap.box_size / snap.pmgrid
	if args.fof_ids or args.fof_ids_all:
		fof_data = snap.read_fof_tab(read_ids=False)
		_, groups, _ = fof_data
	if args.fof_ids_all:
		args.fof_ids = range(len(groups))
	if args.fof_ids:
		if not numpy.any(numpy.isfinite(groups.r200)):
			# backwards compat for old FOF data
			m200_data = m200_util.read_m200_data(snap.fof_path())
			units = units_util.get_units(snap.param)
			if args.fof_ids_all:
				args.fof_ids = m200_data.id
			pos = numpy.stack(
				(m200_data.cx_kpc, m200_data.cy_kpc, m200_data.cz_kpc),
				axis=-1
			) / units.length_in_kpc
			r200 = m200_data['r200_kpc'] / units.length_in_kpc
		else:
			pos = groups.pos
			r200 = groups.r200
		data_slices = []
#		msg_printed = False
		for fof_id in args.fof_ids:
			# fix center to grid coordinates to avoid off-by-one differences in
			# grid size in different dimensions
			# this is not an issue for FDM-only simulations, but can show up in
			# other cases, such as voids or FDM + particles
			p = numpy.round(pos[fof_id] / cell_len) * cell_len
			r = r200[fof_id]
			if r < args.fof_ids_r200_min * cell_len:
				continue
			r_target = min(
				numpy.ceil(args.fof_ids_r200_fac * r),
				snap.box_size / 2 - cell_len
			)
			idx_min = numpy.array(
				[int(x) for x in numpy.round((p - r_target) / cell_len)],
				dtype=object
			)
			idx_max = numpy.array(
				[int(x) for x in numpy.round((p + r_target) / cell_len)],
				dtype=object
			) + 1
			pmgrid = idx_max[0] - idx_min[0]
			assert numpy.all(idx_max - idx_min == pmgrid)
			scale_factor_target = None
			if args.fof_downscale_pmgrid and (
				pmgrid > args.fof_downscale_threshold
			):
				# ensure compatibility of desired scale factor and selected grid
				scale_factor_target = int(numpy.floor(get_scale_factor_target(
					pmgrid
				)))
				if pmgrid / scale_factor_target < args.fof_downscale_threshold:
					scale_factor_target = int(numpy.floor(
						pmgrid / args.fof_downscale_threshold
					))
				if scale_factor_target > 1:
					remainder = (
						scale_factor_target - pmgrid % scale_factor_target
					)
					if remainder != 0:
						# if increasing grid size would go beyond the entire
						# grid: subtract instead of add to get divisibility
						if pmgrid + remainder >= snap.pmgrid:
							remainder = -(pmgrid % scale_factor_target)
						idx_min -= remainder // 2 + remainder % 2
						idx_max += remainder // 2
#			if scale_factor_target and scale_factor_target > 1:
#				if mpi_size == 1:
#					if not msg_printed:
#						print0(
#							f'{mpi_size = }; skipping halos requiring '
#							f'downscaling (re-run with mpi_size > 1 to '
#							f'process these)'
#						)
#						msg_printed = True
#					continue
#			else:
#				if mpi_size > 1:
#					if not msg_printed:
#						print0(
#							f'{mpi_size = }; skipping halos not requiring '
#							f'downscaling (re-run with mpi_size = 1 to process '
#							f'these)'
#						)
#						msg_printed = True
#					continue
			#TODO
			if args.fof_downscale_pmgrid and (
				pmgrid <= args.fof_downscale_threshold
			):
				scale_factor_target = 1
			data_slices.append((fof_id, scale_factor_target, numpy.index_exp[
				idx_min[0]:idx_max[0],
				idx_min[1]:idx_max[1],
				idx_min[2]:idx_max[2],
			]))
	else:
		data_slices = [None, None, numpy.index_exp[
			args.slice_x[0]:args.slice_x[1],
			args.slice_y[0]:args.slice_y[1],
			args.slice_z[0]:args.slice_z[1],
		]]
	if len(data_slices) > 1:
		print0(f'Extracting {len(data_slices)} halos')
	if mpi_size > 1:
		num_chunks = int(numpy.ceil(
			len(data_slices) / args.fof_downscale_max_per_iteration
		))
		print0(
			f'Processing at most {args.fof_downscale_max_per_iteration} halos '
			f'at once; will have to perform {num_chunks} grid iterations'
		)
		# create output file
		if args.fof_ids_single_file and mpi_rank == 0:
			with h5py.File(output_path, 'w') as _:
				pass
		# process chunks
		for num_iteration, fof_chunk in enumerate(
			numpy.array_split(
				numpy.array(data_slices, dtype=object), num_chunks
			), start=1
		):
			print0(f'Starting grid iteration {num_iteration}/{num_chunks}')
			extract_halo_chunk(snap, fof_chunk, args.datasets, output_path)
	else:
		executor_args = {}
		if args.max_workers:
			executor_args['max_workers'] = min(
				len(data_slices), args.max_workers
			)
		with concurrent.futures.ProcessPoolExecutor(**executor_args) as executor:
			futures = [
				executor.submit(
					extract_halo,
					snap, r200, fof_id, scale_factor, data_slice, args.datasets,
					output_path
				) for fof_id, scale_factor, data_slice in data_slices
			]
			for future in concurrent.futures.as_completed(futures):
				# check for errors
				future.result()

def get_scale_factor_target(pmgrid_extract):
	# Δx_target = 2 * args.fof_ids_r200_fac * R200 / args.fof_downscale_pmgrid
	# scale_factor_target = N / N_target = Δx_target / Δx
	result = pmgrid_extract / args.fof_downscale_pmgrid
	return result

def get_output_path_full(output_path, fof_id):
	if fof_id is None:
		output_path_full = output_path
	else:
		output_path_full = output_path.with_stem(f'{output_path.stem}_{fof_id}')
	return output_path_full

def extract_halo_chunk(snap, fof_chunk, datasets, output_path):
	param = numba.typed.List()
	outputs = numba.typed.List()
	pmgrid_in = []
	pmgrid_out = []
	for fof_id, scale_factor, sub_slice in fof_chunk:
		if scale_factor is None:
			scale_factor = 1
		sub_slice_, indices = snap.read_snapshot_indices(
			f'{FDM_GROUP_NAME}/PsiRe' if datasets[0] == FDM_DENSITY_DATASET
			else datasets[0],
			sub_slice
		)
		shape = numpy.broadcast(*indices).shape
		assert all(s == shape[0] for s in shape)
		pmgrid_halo = shape[0]
		assert pmgrid_halo % scale_factor == 0
		pmgrid_in.append(pmgrid_halo)
		pmgrid_out.append(pmgrid_halo // scale_factor)
		param.append(psigrid_filter.Param(
			scale_factor=scale_factor, sub_data=True, sub_data_slice=sub_slice,
			mpi_rank=mpi_rank
		))
		out = psigrid_filter.downscale_allocate_output(
			snap, datasets, scale_factor, data_slice=sub_slice
		)
		assert len(out) == len(datasets)
		outputs.append(out)
	assert len(fof_chunk) == len(param) == len(outputs)
	# argmin/argmax
	pmgrid_min_idx = min(range(len(pmgrid_out)), key=pmgrid_out.__getitem__)
	pmgrid_max_idx = max(range(len(pmgrid_out)), key=pmgrid_out.__getitem__)
	print0(
		f'Min./max. output grid sizes: '
		f'{pmgrid_in[pmgrid_min_idx]}³ → {pmgrid_out[pmgrid_min_idx]}³, '
		f'{pmgrid_in[pmgrid_max_idx]}³ → {pmgrid_out[pmgrid_max_idx]}³'
	)
	iterate_grid.iterate(
		snap, psigrid_filter.downscale_grid_bulk, outputs,
		param=param, grid_type='dataset', datasets=datasets
	)
	# collect results and write output
	for (fof_id, scale_factor, sub_slice), out in zip(fof_chunk, outputs):
		for ds, data in zip(datasets, out):
			# global sum
			mpi_comm.Reduce(
				MPI.IN_PLACE if mpi_rank == 0 else data, data, op=MPI.SUM,
				root=0
			)
			if mpi_rank == 0:
				# normalize mean
				data /= scale_factor**3
		if mpi_rank == 0:
			if args.fof_ids_single_file:
				output_path_full = output_path
				file_mode = 'r+'
			else:
				output_path_full = get_output_path_full(output_path, fof_id)
				file_mode = 'w'
			with h5py.File(output_path_full, file_mode) as fout:
				if args.fof_ids_single_file:
					group_prefix = (
						f'/{args.fof_ids_single_file_group_name}{fof_id}'
					)
					fout.create_group(group_prefix)
				else:
					group_prefix = ''
				# metadata
				fout.attrs['{group_prefix}/scale_factor'] = scale_factor
				fout.attrs['{group_prefix}/data_slice_min'] = numpy.array([
					s.start for s in sub_slice
				])
				fout.attrs['{group_prefix}/data_slice_max'] = numpy.array([
					s.stop for s in sub_slice
				])
				for ds, data in zip(datasets, out):
					# write output
					fout[f'{group_prefix}{ds}'] = data

def extract_halo(
	snap, r200, fof_id, scale_factor, data_slice, datasets, output_path
):
	print0(
		('' if fof_id is None else f'(Halo {fof_id:6}:) ') +
		f'Extracting region {data_slice}'
	)
	output_path_full = get_output_path_full(output_path, fof_id)
#	if args.fof_downscale_pmgrid and scale_factor and scale_factor > 1:
#		data_slice_, indices = snap.read_snapshot_indices(
#			f'{FDM_GROUP_NAME}/PsiRe' if datasets[0] == FDM_DENSITY_DATASET
#			else datasets[0],
#			data_slice
#		)
#		shape = numpy.broadcast(*indices).shape
#		assert all(s == shape[0] for s in shape)
#		pmgrid = shape[0]
##		print0(f'\t{scale_factor=} {pmgrid=}')
#		assert pmgrid % scale_factor == 0
#		out = psigrid_filter.downscale(
#			snap, datasets, scale_factor, data_slice=data_slice,
#			print_messages=True
#		)
#		assert len(out) == len(datasets)
#		if mpi_rank == 0:
#			with h5py.File(output_path_full, 'w') as output_file:
#				for ds, data in zip(datasets, out):
#					output_file[ds] = data
#		del out
#	else:
	assert not scale_factor
	with h5py.File(output_path_full, 'w') as output_file:
		for ds in datasets:
			if ds == FDM_DENSITY_DATASET:
				data = snap.read_snapshot(
					DATASET_NAME_RE, data_slice=data_slice
				)**2
				data += snap.read_snapshot(
					DATASET_NAME_IM, data_slice=data_slice
				)**2
			else:
				data = snap.read_snapshot(ds, data_slice=data_slice)
			output_file[ds] = data
			del data


if __name__ == '__main__':
	args = parser.parse_args()
	main()

