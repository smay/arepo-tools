Base= "-BASE-"

Num=        -SNAPNUM-         ; snapshot number
Seed=      '34166'    ; random number seed
FluxFac=    1.0        ; flux rescale factor

Nspec=      6       ; number of spectra to be plotted in a concatenated way

fout = "-FOUT-"



TauConcat=[0]

for line_nr= 0, Nspec-1 do begin

    exts='000'
    exts=exts+strcompress(string(num),/remove_all)
    exts=strmid(exts,strlen(exts)-3,3)

    lexts='0000'
    lexts=lexts+strcompress(string(line_nr),/remove_all)
    lexts=strmid(lexts,strlen(lexts)-4,4)


    f= Base + '/spec_' + exts + '_seed' + seed + '_UVfac'+string(FluxFac, format='(F4.2)') + '_line' + lexts+ '.dat'
    f=strcompress(f,/remove_all)
    openr,1,f
    pixels=0L
    readu,1,Pixels

    BoxSize=0.0D   &    readu,1, BoxSize
    Wmax= 0.0D     &    readu,1, Wmax
    Time= 0.0D     &    readu,1, Time
    redshift = 1. * round(1. / Time - 1)
    Time= 1. / (redshift + 1.)

    Xpos= 0.0D
    Ypos= 0.0D
    readu,1,Xpos, Ypos

    DirFlag=0L
    readu,1,DirFlag
 
    Tau=        dblarr(PIXELS)
    Temp=       dblarr(PIXELS)
    Vpec=       dblarr(PIXELS)
    Rho=        dblarr(PIXELS)
    RhoNeutral= dblarr(PIXELS)
    NHI=        dblarr(PIXELS)

    readu, 1, Tau, Temp, Vpec, Rho, RhoNeutral, NHI

    close,1



    ; wrap spectrum periodically
    ind=where(Tau eq min(Tau))
    i= ind(0)
    if i ne 0 then begin
        Tau= [Tau(i:*), Tau(0:i-1)]
    endif

    ; approximately continuum normalize it by substracting the minimum
    Tau= Tau - min(Tau)


    TauConcat=[TauConcat, Tau]
endfor

TauConcat=TauConcat(1:*)


VelAxes=    Wmax*NSPEC*(lindgen(PIXELS*NSPEC)+0.5)/(PIXELS*NSPEC)

LambdaAxes= 1215.6 * 1.0/(Time) * (1+ VelAxes/ 2.9979e5) ; in Angstroem
LambdaMin=  1215.6 * 1.0/(Time) 
LAmbdaMax=  1215.6 * 1.0/(Time) * (1+ NSPEC*WMAX/2.9979e5)


set_plot,"PS"
device, filename = fout, /encapsulated, /color
;window, xsize=800, ysize=500
device,xsize=12.0,ysize=7.5
;!p.font=0
;device,/times,/italic,font_index=20
;device,/times
!p.font=1

!x.margin=[7,1]
!y.margin=[4,3]

plot, VelAxes, exp(-TauConCat ), xrange=[0, Wmax*NSPEC], xstyle=1+4, yrange = [0, 1.0], ystyle=1+4

lambda = "154B
tau = "164B

axis, xaxis=1, xrange=[0, Wmax*NSPEC], xstyle= 1, xtitle="!10v / km s!U-1!N"
axis, xaxis=0, xrange=[LambdaMin, LambdaMax], xstyle= 1, xtitle="!20" + String(lambda) + "!10 /!Z(00,C5)"

axis, yaxis=1, charsize=0.001, yrange = [0, 1.0], ystyle=1
axis, yaxis=0, ytitle= "!10exp(-!20" + String(tau) + "!10)" , yrange = [0, 1.0], ystyle=1


;for n=0, NSPEC-2 do begin
;    oplot, [1, 1]*(n+1)*Wmax, [0, 1.0], linestyle=1
;endfor

device,/close

end
