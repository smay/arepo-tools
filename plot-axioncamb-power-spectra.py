#!/usr/bin/env python3
import argparse
from pathlib import Path
import astropy.cosmology
import matplotlib.pyplot as plt
import numpy
import pandas
import scipy.integrate
import scipy.interpolate
import io_util
import matplotlib_settings

NGENIC_OMEGA_M0 = 0.3
NGENIC_UNIT_LENGTH_IN_MPC = 1e-3

parser = argparse.ArgumentParser(description='Plot axionCAMB output matter '
	'power spectra')
parser.add_argument('--relative', type=Path,
	help='plot relative to given spectrum')
parser.add_argument('--labels', nargs='+',
	help='labels for plotted data (must have length of len(paths) + 1)')
parser.add_argument('--xlim', type=float, nargs=2,
	help='limits for the k-axis (horizontal)')
parser.add_argument('--ylim', type=float, nargs=2,
	help='limits for the vertical axis')
parser.add_argument('--ngenic-inputspec-path', type=Path,
	default=Path('ngenic_inputspec_z127.txt'),
	help='Path to N-GenIC inputspec.txt CDM spectrum')
parser.add_argument('--plot-file-name', default='powerspec_axioncamb',
	help='Output plot file name')
parser.add_argument('path', nargs='*', type=Path,
	help='the paths to the snapshot files; any normal files are interpreted '
		'as single-file snapshots, while all files in any given directory '
		'are interpreted as a single multi-file snapshot')
args = parser.parse_args()

def growth_integrand(cosmo, a):
	if a == 0: return 0
	z = 1 / a - 1
	# efunc = sqrt(Ω_m0 / a³ + (1 - Ω_m0 - Ω_Λ) / a² + Ω_Λ)
	return 1 / (a * cosmo.efunc(z))**3

def growth(cosmo, a):
	z = 1 / a - 1
	# efunc = sqrt(Ω_m0 / a³ + (1 - Ω_m0 - Ω_Λ) / a² + Ω_Λ)
	hubble_a = cosmo.efunc(z)
	def integrand(a): return growth_integrand(cosmo, a)
	result, err = scipy.integrate.quad(integrand, 0, a, epsrel=1e-8)
	return hubble_a * result

def growth_factor(cosmo, a_start, a_end):
	return growth(cosmo, a_end) / growth(cosmo, a_start)


# make sure that mirrored ticks at the top are disabled (→ matplotlib_settings)
# to prevent duplicate/wrong ticks with top secondary_xaxis
plt.rc('xtick', top=False)

# plot setup
assert not args.labels or 1 + len(args.path) == len(args.labels)
plt.xlabel(r'$k$ / $h\,$ Mpc$^{-1}$')
with numpy.errstate(divide='ignore'):
	x2 = plt.gca().secondary_xaxis('top', functions=(
		lambda k: 2 * numpy.pi / k,
		lambda r: 2 * numpy.pi / r,
	))
x2.set_xlabel(r'$r$ / $h^{-1}\,$ Mpc')
if not args.relative:
	plt.ylabel(r'$P(k)$ / $h^{-3}$ Mpc$^3$')
if args.xlim:
	plt.xlim(args.xlim)
if args.ylim:
	plt.ylim(args.ylim)
plt.xscale('log')
plt.yscale('log')
plt.tick_params(which='both', left=True, right=True)

# N-GenIC CDM spectrum
zstart, growthfactor, ps = io_util.read_ngenic_inputspec_output(
	args.ngenic_inputspec_path
)
# calculate growth factor (value of efunc is independent of H0)
cosmo = astropy.cosmology.FlatLambdaCDM(70, NGENIC_OMEGA_M0)
# inputspec contains the power spectrum scaled to z = 0
a = 1.
D_plus = growth_factor(cosmo, 1, a)
assert D_plus == 1
K_inputspec = ps.k_linear / NGENIC_UNIT_LENGTH_IN_MPC
Delta2_inputspec = ps.d2_linear * D_plus**2
# CAMB matter power spectrum is defined such that Δ² = k³/(2π²) P
# see https://cosmologist.info/notes/CAMB.pdf
P_inputspec = Delta2_inputspec / K_inputspec**3 * 2 * numpy.pi**2
if args.relative:
	if args.relative.name == 'ngenic_inputspec_z127.txt':
		P_ref = scipy.interpolate.interp1d(K_inputspec, P_inputspec)
	else:
		ps = pandas.read_csv(args.relative, sep=r'\s+', names=['k', 'P'])
		P_ref = scipy.interpolate.interp1d(ps.k, ps.P)
else:
	P_ref = lambda k: 1
plt.plot(
	K_inputspec, P_inputspec / P_ref(K_inputspec),
#	'.-', linewidth=0.6, markersize=0.8,
	label=fr'N-GenIC CDM inputspec' if not args.labels else args.labels[0]
)
plt.axvline(8640*numpy.pi/10, color='gray', linestyle='--')

# Axion-CAMB spectra
paths = args.path if args.path else Path('matterpower').glob('*')
if args.relative and args.relative not in paths:
	paths = [args.relative] + paths
labels = (
	args.labels[1:] if args.labels else
	[path.name[len('_matterpower_'):-len('.dat')] for path in paths]
)
for idx, [path, label] in enumerate(zip(paths, labels)):
	print(path)
	ps = pandas.read_csv(path, sep=r'\s+', names=['k', 'P'])
	mask = (ps.k >= K_inputspec.min()) & (ps.k <= K_inputspec.max())
	k = ps.k[mask]
	P = ps.P[mask]
	if idx == 0:
		D_plus_127 = growth_factor(cosmo, 1, 1/(127+1))
		P/=D_plus_127**2
	elif idx == 1:
		D_plus_63 = growth_factor(cosmo, 1, 1/(63+1))
		P/=D_plus_63**2
	plt.plot(
		k, P / P_ref(k), #'.-', linewidth=0.4, markersize=0.6,
		label=label
	)

ax = plt.gca()
yloc_maj = ax.yaxis.get_major_locator()
yloc_min = ax.yaxis.get_minor_locator()
if hasattr(yloc_maj, '_base'):
	yticks = yloc_maj()
	# use minor ticks to mark major tick positions if axis starts skipping
	# major ticks
	if len(yticks) >= 2 and not numpy.isclose(
		yticks[1] / yticks[0], yloc_maj._base
	):
		yloc_min.set_params(numticks=100, subs=[1.0])
		# avoid automatic minor tick labels, since space is already limited
		# if we’re in this scenario
		ax.tick_params(which='minor', labelleft=False, labelright=False)


plt.legend()
with numpy.errstate(divide='ignore'):
	plt.savefig(f'{args.plot_file_name}.pgf')
#	plt.savefig(f'{args.plot_file_name}.pdf')
	plt.show()

