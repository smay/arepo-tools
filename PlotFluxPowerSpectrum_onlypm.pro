fout= "./fluxpowerspec_onlypm.eps"
fout2= "./fluxpowerspec-ratio_onlypm.eps"


BaseList=[   $
	'/u/smay/runs/lya-test/onlypm-1024/output/absorptionspectrum/',    $
	'/u/smay/runs/lya-test/onlypm-2048/output/absorptionspectrum/',    $
	'/u/smay/runs/lya-test/onlypm-3200/output/absorptionspectrum/',    $
	'/u/smay/runs/lya-test/normal-test128/output/absorptionspectrum/'  $
]

TagList =['ONLYPM 1024', 'ONLYPM 2048', 'ONLYPM 3200', 'TreePM']

ColorList =[1, 3, 4, 0, 0]

NumList = [3, 3, 3, 3, 3]
seed= '34166'

NSPEC= 1000

Power_ratio_ref = -1






set_plot,"PS"

!p.font=0
device,/times,/italic,font_index=20
device,xsize=14.0,ysize=12.0
!x.margin=[10,3]
!y.margin=[4,2]
!p.thick=2.5
!p.ticklen=0.03

device,filename=fout, /encapsulated, /color

 
v1=[255,  0,  0, 200  , 245]
v2=[  0,180,  0, 0, 245]
v3=[  0,  0, 255,200, 245]

tvlct,v1,v2,v3,1

    
plot, [1.0e-10], [1.0], psym=3, /xlog, /ylog, yrange=[1.0e-4,0.1], xrange=[0.003, 1.0], xstyle=1, $
  xthick=2.0, ythick=2.0, $
  xtitle="!20k!7  [ km!U-1!N s ]!3", $
  ytitle="!9p!7!U-1!N !20k!7  !20P!7!DF !N(!20k!7)!3", charsize=1.1



obs_k=[0.00284, 0.00358, 0.0045, 0.00566, 0.00713, 0.00898, 0.0113, 0.0142, 0.0179, 0.0225, 0.0284, 0.0357, 0.045, 0.0566, 0.0713, 0.0898, 0.113, 0.142]
obs_p=[20.8, 16.1, 14.7, 22.7, 10.8, 10.1, 9.08, 8.07, 5.61, 4.96, 3.59, 2.11, 1.53, 0.896, 0.435, 0.188, 0.0691, 0.0208]
obs_err=[3.9, 4, 2.8, 4.7, 1.6, 1.7, 1.9, 1.0, 0.35, 0.7, 0.43, 0.3, 0.14, 0.094, 0.051, 0.016, 0.007, 0.0022]


;oplot, obs_k, obs_k*Obs_p/!PI, psym=4 ,color=2, symsize=0.8

;errplot, obs_k, obs_k*(Obs_p-obs_err)/!PI, obs_k*(Obs_p+obs_err)/!PI, width=0.008, col=2


dp= [0.5, 0.3, 0.21, 0.16, 0.08, 0.02, 0.03]
fac= 10.0^(2.0* dp/4.3)

kp = reverse(obs_k)
kp = kp(0:n_elements(dp)-1)

pp= reverse(obs_p)
pp= pp(0:n_elements(dp)-1)


x=cos(2*!PI*indgen(16)/15.0)
y=sin(2*!PI*indgen(16)/15.0)
usersym,x,y, thick=3.0; ,/fill  ; filled circle

oplot, kp, fac*kp*pp/!PI, psym=8, color=2, symsize=0.8, thick=2.5



PowerList = []

for it=0, n_Elements(BaseList)-1 do begin

    num = Numlist(it)

    Sum=0.0D

    PowerSum=0

    for rep = 0, NSPEC-1 do begin

        exts='000'
        exts=exts+strcompress(string(num),/remove_all)
        exts=strmid(exts,strlen(exts)-3,3)

        lexts='0000'
        lexts=lexts+strcompress(string(rep),/remove_all)
        lexts=strmid(lexts,strlen(lexts)-4,4)


        f= BaseList(it) + '/spec_' + exts + '_seed' + seed + '_UVfac'+string(1.0, format='(F4.2)') + '_line' + lexts+ '.dat'
        f=strcompress(f,/remove_all)
        openr,1,f
        pixels=0L
        readu,1,Pixels

        BoxSize=0.0D   &    readu,1, BoxSize
        Wmax= 0.0D     &    readu,1, Wmax
        Time= 0.0D     &    readu,1, Time

        Xpos= 0.0D
        Ypos= 0.0D
        readu,1,Xpos, Ypos
        DirFlag=0L
        readu,1,DirFlag

        Tau=        dblarr(PIXELS)
        Temp=       dblarr(PIXELS)
        Vpec=       dblarr(PIXELS)
        Rho=        dblarr(PIXELS)
        RhoNeutral= dblarr(PIXELS)
        NHI=        dblarr(PIXELS)
        Metallicity=dblarr(PIXELS)
        DensTemp=   dblarr(PIXELS)

        readu, 1, Tau, Temp, Vpec, Rho, RhoNeutral, NHI, Metallicity, DensTemp
        close,1

        
        FluxFac=1.0D
        fluxfac_path = BaseList(it)+ '/fluxfactor_' + exts + '_' + seed + '.txt'
        if file_test(fluxfac_path) then begin
            openr,1, fluxfac_path
            readf, 1, FluxFac
            close,1
        endif

        Tau= Tau /FluxFac



  ;  Tau= Tau - min(Tau)
        
        Trans=  exp(-Tau) 


        Trans2= Trans - total(Trans, /double)/n_elements(Trans)

        Tk= FFT(Trans2, /double)


        Power = abs(Tk)^2

        print,  total(Trans2^2)/n_elements(Trans2) ,  total(Power)


        Power = Power * Wmax

        PowerSum= PowerSum+Power

    endfor

    PowerSum= PowerSum/NSPEC



    Np= PIXELS/2-2
    Power= PowerSum(1:Np)

    kaxes= (indgen(Np)+1) * (2*!PI)/Wmax


    oplot, kaxes, kaxes*Power/!PI , color=ColorList(it)


    xyouts, 0.24, 0.22+(it+1)*0.06, /normal, TagList(it), color= colorList(it)


    PowerList = [[PowerList], [Power]]

endfor


;    xyouts, 0.24, 0.22+0*0.06, /normal, '!7Mc Donald et al. (2000)!3', color= 2


    xyouts, 0.8, 0.8, /normal,"!20z!7 = 3!3", charsize=1.2

device,/close


set_plot,"PS"

!p.font=0
device,/times,/italic,font_index=20
device,xsize=14.0,ysize=6.0
!x.margin=[10,3]
!y.margin=[4,2]
!p.thick=2.5
!p.ticklen=0.03

device,filename=fout2, /encapsulated, /color

 
v1=[255,  0,  0, 200  , 245]
v2=[  0,180,  0, 0, 245]
v3=[  0,  0, 255,200, 245]

tvlct,v1,v2,v3,1

    
plot, [1.0e-10], [1.0], psym=3,   yrange=[0.7,1.1], xrange=[0.005, 1.0], xstyle=1,/xlog, $
  xthick=2.0, ythick=2.0, ystyle=1, yticks=4, $
  xtitle="!20k!7  [ km!U-1!N s ]!3", $
  ytitle="!7power ratio!3", charsize=1.1, xticklen=0.08

  for it=0, n_Elements(BaseList)-1 do begin
    oplot, kaxes, smooth(PowerList(*, it)/PowerList(*, Power_ratio_ref), 18), color=ColorList(it)
  endfor

  xyouts, 0.8, 0.74, /normal,"!20z!7 = 3!3", charsize=1.2

device,/close

end

