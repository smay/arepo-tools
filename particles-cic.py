#!/usr/bin/env python3
import argparse
from pathlib import Path
import h5py
import numba
import numpy
import read_snapshot
from snapshot import Snapshot
if __name__ == '__main__':
	from mpi_util import MPI, mpi_comm, mpi_rank, mpi_size, print0

parser = argparse.ArgumentParser(description='Create CiC binning of CDM '
	'particle data')
parser.add_argument('--pmgrid', type=int,
	help='CiC grid size to use')
parser.add_argument('--ptype', type=int, default=1, help='particle type to use')
parser.add_argument('--datasets', nargs='+', default=['Masses', 'Velocities'],
	help='the datasets to bin')
parser.add_argument('path', type=Path, nargs='+',
	help='the paths to the snapshot files; any normal files are interpreted '
		'as single-file snapshots, while all files in any given directory '
		'are interpreted as a single multi-file snapshot')


@numba.njit
def cic_assignment_generic(assign_func, grid, pos, cell_len, var):
	assert len(pos) == len(var) or len(var) == 1
	assert len(pos.shape) == 2
	assert len(var.shape) == 1
	Nvar = len(var)
	p_f = pos / cell_len
	p = p_f.astype(numpy.int64)
	p_c = p_f - p
	N = len(grid)
	fractions_idx = [numpy.array(o_tup) for o_tup in numpy.ndindex((2,) * 3)]
	for i in range(len(pos)):
		for o in fractions_idx:
			# numpy.sign(x - 0.5): 1 if x == 1, -1 if x == 0
			# if o[j] == 0: fractions[j] = 1 - p_c[j]
			# if o[j] == 1: fractions[j] = p_c[j]
			fractions = (1 - o) + numpy.sign(o - 0.5) * p_c[i]
			idx = (p[i] + o) % N
			assign_func(grid, idx, fractions, var[i % Nvar])

@numba.njit
def cic_assignment_func_default(grid, idx, fractions, var):
	grid[idx[0], idx[1], idx[2]] += numpy.prod(fractions) * var

def cic_assignment(pos, pmgrid, box_size, var, rtol=1e-6):
	assert len(pos) == len(var) or len(var) == 1
	cell_len = box_size / pmgrid
	grid = numpy.zeros((pmgrid,) * 3)
	cic_assignment_generic(
		cic_assignment_func_default, grid, pos, cell_len, var
	)
	sum_grid = numpy.sum(grid)
	sum_var = numpy.sum(var)
	if len(var) == 1:
		sum_var *= len(pos)
	assert numpy.isclose(sum_grid, sum_var, atol=0, rtol=rtol), (
	    f'{numpy.max(numpy.abs(sum_grid - sum_var))=}'
	)
	return grid


def main():
	for path in args.path:
		print0(path)
		path_out = path.with_name(f'{path.name}_cic{args.pmgrid}.hdf5')
		snap = Snapshot(path)
		# process particle data
		numpart_total = read_snapshot.numpart_total(snap.header)
		[part_first, part_stop], = snap.determine_chunks(
			mpi_rank, mpi_size, num_slabs=numpart_total, slab_size=1,
			chunk_mem_size=numpy.inf
		)
		# read positions and mass
		print0(f'Reading particles {part_first:,} to {part_stop:,}')
		pos = snap.read_snapshot(
			f'/PartType{args.ptype}/Coordinates',
			data_slice=numpy.s_[part_first:part_stop]
		)
		dataset_shape = []
		data_out = []
		for dataset in args.datasets:
			print0(f'\tLoading dataset {dataset}')
			if dataset == 'Masses':
				particle_mass = snap.header['MassTable'][args.ptype]
				assert particle_mass > 0
				data = numpy.array([particle_mass])
				shape = data.shape
			else:
				dataset_full = f'/PartType{args.ptype}/Velocities'
				data = snap.read_snapshot(
					dataset_full, data_slice=numpy.s_[part_first:part_stop]
				)
				shape = snap.dataset_shape[dataset_full]
			rtol = 1e-6
			if dataset == 'Velocities':
				rtol = 1e-1
			dataset_shape.append(shape)
			# do CIC binning
			print0(f'\tBinning to {args.pmgrid}³ grid')
			if len(shape) > 1:
				assert len(shape) == 2
				outputs = [
					cic_assignment(
						pos, args.pmgrid, snap.header['BoxSize'], data[:, i],
						rtol=rtol
					) for i in range(shape[-1])
				]
			else:
				outputs = [
					cic_assignment(
						pos, args.pmgrid, snap.header['BoxSize'], data,
						rtol=rtol
					)
				]
			for o in outputs:
				mpi_comm.Reduce(
					MPI.IN_PLACE if mpi_rank == 0 else o, o, op=MPI.SUM, root=0
				)
			data_out += outputs
			if mpi_rank == 0 and dataset == 'Masses':
				mass_out = data_out[-1]
				mass_out /= (snap.header['BoxSize'] / args.pmgrid)**3
				if args.ptype == 1:
					rho_avg_dm = (
						(snap.header['Omega0'] - snap.header['OmegaBaryon']) *
						snap.constant_in_internal_units('critical_density0')
					)
					assert numpy.isclose(
						numpy.mean(mass_out), rho_avg_dm, atol=0, rtol=1e-3
					)
		# write to output file
		if mpi_rank == 0:
			print0(f'\tWriting data to {path_out}')
			output_dset_base = f'/PartType{args.ptype}/CIC'
			output_datasets = []
			#TODO: Python 3.10+
#			for dataset, shape in zip(args.datasets, dataset_shape, strict=True):
			for dataset, shape in zip(args.datasets, dataset_shape):
				if dataset == 'Masses':
					output_datasets.append(f'{output_dset_base}/Density')
				elif len(shape) > 1:
					assert len(shape) == 2
					for i in range(shape[-1]):
						output_datasets.append(
							f'{output_dset_base}/{dataset}{i}'
						)
				else:
					output_datasets.append(f'{output_dset_base}/{dataset}')
			with h5py.File(path_out, 'a') as fout:
				#TODO: Python 3.10+
#				for ds, data in zip(output_datasets, data_out, strict=True):
				for ds, data in zip(output_datasets, data_out):
					fout[ds] = data


if __name__ == '__main__':
	args = parser.parse_args()
	main()
