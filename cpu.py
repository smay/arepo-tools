import numpy
import pandas

COLUMNS = ['operation', 'diff_abs', 'diff_rel', 'cum_abs', 'cum_rel']

def p2f(x): return x if x is None else float(x.rstrip('%')) / 100
PERCENT_COLUMNS = {
	'diff_rel': p2f, 'cum_rel': p2f,
}

# Absolute values in cpu.txt are wall-clock time given in seconds (i.e. total
# CPU time is diff_abs or cum_abs multiplied by the number of cores).
def parse_cpu_txt(path):
	df = pandas.read_table(
		path, names=COLUMNS, converters=PERCENT_COLUMNS,
		sep=r'\s\s+', skip_blank_lines=False, engine='python',
		# disable detection of NA values to avoid numpy warnings in pandas’
		# type inference (cf. https://github.com/numpy/numpy/issues/6784) and
		# since we don’t need it anyway (but see detail below)
		na_filter=False
	)
	# missing values in the first column become empty strings instead of None
	# for some reason, so they stay that way with na_filter=False; also,
	# df.replace('', None, inplace=True) doesn’t seem to have any effect
	df.replace('', numpy.nan, inplace=True)
	# cf. https://stackoverflow.com/a/47800806
	# add an index of the third dimension
	# empty lines are separators
	indices = df[df.isna().all(axis='columns')].index
	df['timestep'] = pandas.Series(range(len(indices)), index=indices)
	df['timestep'].fillna(method='bfill', inplace=True)
	df['timestep'] = df['timestep'].astype(int)
	# reset indices within each “group”
	df = df.groupby('timestep').apply(lambda x: x.reset_index())
	df = df.drop(['timestep', 'index'], axis='columns').dropna(how='all')
	# drop the first two rows (headers) in every time step
	df.drop([0, 1], level=1, inplace=True)
	# convert values to float
	df['diff_abs'] = df['diff_abs'].astype(float)
	# replace numerical index by names
	# rename duplicate rows
	insert_rows = df[
		df['operation'] == 'insert'
	].index.get_level_values(1).unique()
	assert len(insert_rows) == 2
	df.loc[
		df.index.get_level_values(1) == insert_rows[0], 'operation'
	] = 'treebuild_insert'
	df.loc[
		df.index.get_level_values(1) == insert_rows[1], 'operation'
	] = 'voronoi_insert'
	df.set_index(
		[df.index.get_level_values('timestep'), 'operation'], inplace=True
	)
	# reorder index/columns
	df.columns.name = 'stat'
	df = df.unstack().swaplevel(axis='columns')
	df.sort_index(axis='columns', inplace=True)
	return df


# Meaning of the columns in cpu.csv:
# CPU_X1 = diff_abs
# CPU_X2 = cum_abs
# CPU_X3 = cum_rel = CPU_X2 / CPU_ALL2
# Absolute values in cpu.csv are wall-clock time given in seconds (i.e. total
# CPU time is diff_abs or cum_abs multiplied by the number of cores).
def parse_cpu_csv(path):
	# read column names separately in order to handle files which have been
	# appended to (AREPO always writes the header row)
	columns = pandas.read_csv(path, skipinitialspace=True, nrows=3).columns
	# use comment='S' to ignore header rows written by AREPO (potentially in
	# the middle of the file), which start with “STEP, TIME, …”
	df = pandas.read_csv(
		path, skipinitialspace=True, comment='S', names=columns
	)
	return df

