#!/usr/bin/env python3
# standard modules
import argparse
import collections
import importlib
import itertools
import math
import re
import time
import types
import warnings
from pathlib import Path
# non-standard modules
import astropy
import astropy.constants
import astropy.units
import numba
import numba.typed
import numexpr
import numpy
import numpy.fft
import numpy.linalg
import matplotlib.colors
import matplotlib.patheffects
import matplotlib.pyplot as plt
import mpl_toolkits.axes_grid1.anchored_artists
import pandas
# own modules
import bin_density
import iterate_grid
import io_util
import matplotlib_settings
import m200_util
import read_snapshot
import snapshot
import units_util
try:
	# finalize manually if using MPI
	import mpi4py
	mpi4py.rc.finalize = False
except: pass
from mpi_util import MPI, mpi_rank, mpi_comm, print0
particles_cic = importlib.import_module('particles-cic')

PMGRID_DEFAULT = 256
BOX_SIZE_DEFAULT = 8.

class DetectDefaultAction(argparse.Action):
	def __call__(self, parser, namespace, values, option_string=None):
		setattr(namespace, self.dest, values)
		setattr(namespace, f'{self.dest}|set', True)

parser = argparse.ArgumentParser(description='Plot snapshots of fuzzy dark '
	'matter or cold dark matter simulations. By default, a 2D slice through '
	'the middle of the z-axis is shown.')
parser.add_argument('--chunk-mem-size', type=int,
	default=snapshot.CHUNK_MEM_SIZE_DEFAULT,
	help='max. amount of peak memory in bytes to use for grid data per core '
		'(default: %(default)d)')
parser.add_argument('--prefer-type', choices=('grid', 'particles'),
	default='particles',
	help='the type of snapshot data that should be preferred '
		'(default: %(default)s)')
parser.add_argument('--PMGRID', type=int,
	help='the number grid points per dimension (default: '
		f'{PMGRID_DEFAULT})')
parser.add_argument('--BoxSize', type=float,
	help=f'the box size (default: {BOX_SIZE_DEFAULT})')
parser.add_argument('--snapshot-format',
	choices=('gadget', 'hdf5', 'npy', 'raw'), default='hdf5',
	help='the output snapshot format (default: %(default)s)')
parser.add_argument('--longids', type=bool, default=True,
	help='whether to use 64-bit particle IDs instead of 32-bit ones '
		'(default: %(default)s)')
parser.add_argument('--double-precision', action='store_true',
	help='read snapshot data as 64-bit floating point numbers instead of '
		'32-bit ones')
parser.add_argument('--snapshot-plane', action='store_true',
	help='treat the snapshot data as a 2D plane '
		'(i.e. it is already a slice through 3D space)')
parser.add_argument('--snapshot-rho', action='store_true',
	help='treat the snapshot data as the (real) probability density '
		'instead of the (complex) wave function (only for “--snapshot-format '
		'npy”')
parser.add_argument('--ptype', type=int, default=1,
	help='particle type to plot (default: %(default)d)')
parser.add_argument('--plot-type',
	choices=('contour', 'color', 'dot'), default='color',
	help='the kind of plot to produce (default: %(default)s)')
parser.add_argument('--plot-scale', action=DetectDefaultAction,
	choices=('linear', 'log', 'symlog'), default='log',
	help='the plot’s color scale (default: %(default)s)')
parser.add_argument('--plot-range', action=DetectDefaultAction, nargs=2,
	type=float, default=(1e-2, 1e3),
	help='the plot’s range in units of the average density (default: '
		'%(default)s)')
parser.add_argument('--plot-range-absolute', action='store_true',
	help='use absolute values for the color scale')
parser.add_argument('--plot-range-mark-fof', nargs=2, type=float,
	default=(60, 1e4),
	help='the value range for the colormap used to mark FoF cells with '
		'--mark-fof-cells in units of the average density (default: '
		'%(default)s)')
parser.add_argument('--plot-range-auto', action='store_true',
	help='use automatic color scale instead of the value of --plot-range '
		'instead of minimum and maximum of data (automatic) for color scale')
parser.add_argument('--plot-min-cutoff', type=float,
	help='set grid values at all points which have a lower value than the '
		'given one (relative to the global average) to 0')
parser.add_argument('--subplots', action='store_true',
	help='plot all snapshots in one file using subplots')
parser.add_argument('--powerspec', action='store_true',
	help='also plot the matter power spectrum')
parser.add_argument('--phase', action='store_true',
	help='plot wave function phase instead of density')
parser.add_argument('--number-density', action='store_true',
	help='plot number density instead of density (for particles)')
parser.add_argument('--velocities', action='store_true',
	help='plot velocities derived from the wave function phase via '
		'u⃗ = ℏ/m ∇α instead of density (or particle velocities)')
parser.add_argument('--velocities-step', type=int, default=4,
	help='the spacing between drawn velocity arrows')
parser.add_argument('--potential', action='store_true',
	help='plot potential instead of density (for slice output)')
parser.add_argument('--mark-halos', action='store_true',
	help='mark halos contained within the plotted region by drawing the '
	'virial radius as a circle')
parser.add_argument('--no-mark-halos', action='store_true',
	help='disables --mark-halos')
parser.add_argument('--no-mark-zoom-halo', action='store_true',
	help='do not mark the halo given by --halo-zoom differently')
parser.add_argument('--mark-halos-use-fof-mass', action='store_true',
	help='use FoF mass to draw halo circles instead of R_200')
parser.add_argument('--mark-halos-label', action='store_true',
	help='label halo circle markers with FoF ID')
parser.add_argument('--mark-halos-label-r200', action='store_true',
	help='label halo circle markers with FoF ID')
parser.add_argument('--mark-halos-label-num', type=int, default=5,
	help='label the N largest halos with --mark-halos-label (default: '
		'%(default)d)')
parser.add_argument('--mark-halo-size', type=float, default=4e-3,
	help='minimal halo radius to be plotted with --mark-halos as a fraction '
		'of the total plot size (default: %(default)g)')
parser.add_argument('--mark-halos-overlapping-with', type=int,
	help='mark halos whose virial radii overlap with the given halo')
parser.add_argument('--mark-filaments', type=Path,
	help='mark filaments found by DisPerSE in the given NDskl_ascii file '
		'using lines')
parser.add_argument('--mark-filaments-label', action='store_true',
	help='label filaments with filament ID')
parser.add_argument('--mark-filaments-color',
	help='(Single) color to use when marking filaments')
parser.add_argument('--mark-filaments-no-offsets', action='store_true',
	help='Do not use filament shifts')
parser.add_argument('--time-from-filename-format',
	default=r'psigrid_(\d+\.\d+)\..+',
	help='if the time is not given in the snapshot file: try to parse it '
		'from the input file name using this format (regular expression)')
parser.add_argument('--time-digits', type=int, default=4,
	help='how many decimal digits to use for the time in the file name and '
		'plot title (default: %(default)d)')
parser.add_argument('--scalebar', action='store_true', help='show a scale bar')
parser.add_argument('--scalebar-size', type=float,
	help='the scale bar’s size (default: 25%% of the plot size)')
parser.add_argument('--scalebar-loc', default='lower right',
	help='the scale bar’s location (default: %(default)s)')
parser.add_argument('--scalebar-color', default='white',
	help='the scale bar color (default: %(default)s)')
parser.add_argument('--no-ticks', action='store_true',
	help='disable ticks and axis labels')
parser.add_argument('--title-template',
	help='format title according to the given template')
parser.add_argument('--v-max-in-title', action='store_true',
	help='show velocity criterion in plot title')
parser.add_argument('--subplots-title', action='store_true',
	help='use plot titles as with --subplots even when --subplots is not '
		'specified')
parser.add_argument('--no-title', action='store_true',
	help='disable plot title/suptitle')
parser.add_argument('--no-suptitle', action='store_true',
	help='disable plot suptitle')
parser.add_argument('--title-fontsize',
	help='font size to use for title')
parser.add_argument('--bbox',
	help='matplotlib rc property savefig.bbox')
parser.add_argument('--no-colorbar', action='store_true',
	help='disable colorbar')
parser.add_argument('--only-colorbar', action='store_true',
	help='only show colorbar (hide plot)')
parser.add_argument('--colorbar-extend',
	help='setting to force for the colorbar “extend” option')
parser.add_argument('--colorbar-orientation',
	choices=('horizontal', 'vertical'),
	help='setting for the colorbar “orientation” option')
parser.add_argument('--colorbar-digits', type=int,
	help='how many decimal digits to use for the colorbar scale')
parser.add_argument('--colorbar-label', default=r'$\rho/\langle \rho \rangle$',
	help='the colorbar label')
# TODO: unify projection and slicing
parser.add_argument('--slice-start', type=int, default=0,
	help='offset for the starting point with --slice-step '
		'(default: %(default)d)')
parser.add_argument('--slice-stop', type=int,
	help='offset for the end point with --slice-step '
		'(default: PMGRID)')
parser.add_argument('--slice-step', type=int,
	help='make slices through z-axis with the given distance')
parser.add_argument('--project', type=float, nargs=2,
	action=DetectDefaultAction,
	help='do a projection of the given range along --project-axis (as a '
		'fraction of the box size)')
parser.add_argument('--project-axis', type=int, choices=(0, 1, 2), default=2,
	help='axis along which to do the projection (default: %(default)d)')
parser.add_argument('--project-absolute', action='store_true',
	help='give projection range in absolute length units (instead of '
		'box size fraction)')
parser.add_argument('--xlim', nargs=2, type=float, default=(0., 1.),
	action=DetectDefaultAction,
	help='the x-coordinate range to plot (relative to the box size)')
parser.add_argument('--ylim', nargs=2, type=float, default=(0., 1.),
	action=DetectDefaultAction,
	help='the y-coordinate range to plot (relative to the box size)')
parser.add_argument('--lim-absolute', action='store_true',
	help='give --xlim/--ylim in absolute length units (instead of box size '
		'fraction)')
parser.add_argument('--zoom-inset', nargs=4, type=float,
	help='create a zoomed inset of a sub-region; arguments give (x0, x1, y0, '
		'y1) of the region to show in coordinates relative to the box size')
parser.add_argument('--zoom-inset-position', nargs=4, type=float,
	default=(0.6, 0.6, 0.3, 0.3),
	help='position of the inset in axes coordinates (arguments to inset_axes; '
		'default: %(default)s)')
parser.add_argument('--halo-zoom', type=int,
	help='plot a cubic region of space centered on the halo with the given '
		'GrNr, where the side length is given by the halo’s virial radius '
		'(requires M_200 data)')
parser.add_argument('--halo-zoom-factor', type=float, default=0.95,
	help='zoom factor for --halo-zoom to scale the shown region '
		'(default: %(default)d)')
parser.add_argument('--halo-zoom-offset', type=float, nargs=3,
	help='offset plot by the given vector (x y z) when using --halo-zoom '
		'(in kpc/h)')
parser.add_argument('--halo-zoom-project-width', type=float,
	help='override thickness of the projection when using --halo-zoom '
		'(in kpc/h)')
parser.add_argument('--halo-zoom-project-width-factor', type=float,
	help='override thickness of the projection when using --halo-zoom '
		'(by multiplying this factor to the original thickness)')
parser.add_argument('--halo-zoom-cube', type=float,
	help='plot a cube with the given half-length around the halo given by '
		'--halo-zoom (in units of kpc/h!)')
parser.add_argument('--halo-zoom-info', action='store_true',
	help='add an annotation with information about the zoomed halo in the '
		'upper left corner')
parser.add_argument('--halo-zoom-info-fontsize', default='xx-small',
	help='font size for --halo-zoom-info')
parser.add_argument('--only-ids', type=Path,
	help='only plot the particles with IDs given by the specified file '
		'(1D numpy array in .npy file)')
parser.add_argument('--mark-fof-cells', type=int,
	help='mark cells which belong to the given FoF group with a different '
		'color')
parser.add_argument('--mark-fof-cells-all', action='store_true',
	help='mark all FoF cells with a different color')
parser.add_argument('--fof-voids', action='store_true',
	help='indicate that the snapshot FoF groups correspond to voids (instead '
		'of halos) for all relevant FoF options')
parser.add_argument('--units-length',
	choices=('Mpc', 'kpc', 'code'), default='Mpc',
	help='the length units to use in the plot labels (default: %(default)s)')
#TODO
parser.add_argument('--save-result-data', type=Path,
	help='save the computed data to be plotted to a file so that it can be '
		'loaded using --read-result-data without recomputation')
parser.add_argument('--load-result-data', type=Path,
	help='plot the data from saved file(s) directly instead of '
		'computing/transforming from scratch')
parser.add_argument('--plot-file-prefix', default='',
	help='prefix for the output file names')
parser.add_argument('--plot-file-prefix-run-name', action='store_true',
	help='prefix the run name to the output file name')
parser.add_argument('--plot-file-name', help='override output file name')
parser.add_argument('--plot-format', default='png',
	help='the file format of the generated plot(s) (default: %(default)s)')
parser.add_argument('--figsize', type=float, nargs=2,
	help='the figure size in inches')
parser.add_argument('--dpi', type=float, default=300.,
	help='the resolution of the generated plot(s) (default: %(default)s)')
parser.add_argument('--transparent', type=int, default=1,
	help='whether to use a transparent background for the figure (default: '
		'%(default)d)')
parser.add_argument('--no-mass-h-fix', action='store_true',
	help='use the FDM mass in the snapshot metadata as-is')
parser.add_argument('--fof-path', nargs='+', type=Path,
	help='paths to FoF files corresponding to the snapshots, for use with '
		'--mark-halos, --mark-fof-*, etc.')
parser.add_argument('path', nargs='+', type=Path,
	help='the paths to the snapshot files; any normal files are interpreted '
		'as single-file snapshots, while all files in any given directory '
		'are interpreted as a single multi-file snapshot')
args = parser.parse_args()
# default settings for --phase
if args.phase:
	if 'plot_scale|set' not in args:
		args.plot_scale = 'linear'
	if 'plot_range|set' not in args:
		args.plot_range = (-numpy.pi, numpy.pi)

PS_BINS = 2000
FDM_HALO_FINDER_OVERDENSITY_DEFAULT = 60.
VIRIAL_OVERDENSITY = 200.
PMGRID = PMGRID_DEFAULT if args.PMGRID is None else args.PMGRID
PMGRID_LIST = []
BOX_SIZE = BOX_SIZE_DEFAULT if args.BoxSize is None else args.BoxSize
CELL_LEN = None
OFFSET = None
XLIM = None
YLIM = None
UNIT_IN_CM = {
	'Mpc': 3.085678e24,
	'kpc': 3.085678e21,
}
UNIT_IN_G = {
	'Ms': 1.989e33,
}
COORD_SYMBOLS = ('x', 'y', 'z')
CBAR_LABEL_OPT = {'labelpad': -2}
NUMBA_EMPTY_LIST_U8 = numba.typed.List()
NUMBA_EMPTY_LIST_U8.append(numpy.uint64(1))
NUMBA_EMPTY_LIST_U8.pop()
UnitLength_in_cm = UnitMass_in_g = UnitVelocity_in_cm_per_s = UnitTime_in_s = (
	None
)
units_length_factor = None
target_halo = None
target_halo_fof = None


# need to use tuple (instead of dict) for numba
Param = collections.namedtuple(
	'Param',
	'type, axis, min, max, cell_len, fof_ids, rho_fof_threshold, fof_voids, '
	'only_ids, particle_mass, box_size',
	defaults=[None] * 5
)
assert Param._field_defaults == dict.fromkeys(
	['rho_fof_threshold', 'fof_voids', 'only_ids', 'particle_mass', 'box_size'],
	None
)

def float_to_scientific_notation(x):
	exponent = math.floor(math.log10(x))
	significand = x / 10**exponent
	return fr'{significand} \times 10^{{{exponent}}}'

#TODO: de-duplicate these formatting functions
# Note: This is the same function as in plot-halo-profiles.py
def float_to_scientific_notation2(x, digits=2, integer=False):
	sign = '-' if x < 0 else ''
	if numpy.isinf(x):
		return fr'{sign}\infty'
	if x == 0:
		return '0'
	significand, exponent = units_util.split_num_exp(x)
	if integer and exponent == 0:
		return fr'{sign}{round(significand)}'
	elif numpy.isclose(significand, 1):
		return fr'{sign} 10^{{{exponent}}}'
	else:
		return fr'{sign}{significand:.{digits}f} \times 10^{{{exponent}}}'


def get_color_norm(interval):
	if args.plot_scale == 'log':
		norm = matplotlib.colors.LogNorm(*interval, clip=True)
	elif args.plot_scale == 'symlog':
		norm = matplotlib.colors.SymLogNorm(1, 1, *interval, clip=True)
	else:
		norm = matplotlib.colors.Normalize(*interval, clip=True)
	return norm


def set_globals(snap, path, config, param, header, **kw):
	global PMGRID, BOX_SIZE, CELL_LEN, OFFSET, XLIM, YLIM
	global UnitLength_in_cm, UnitMass_in_g, UnitVelocity_in_cm_per_s
	global UnitTime_in_s, units_length_factor, target_halo, target_halo_fof
	global PLOT_AXES
	# use/compare with snapshot values
	if snap.get_type(args.prefer_type) == 'grid':
		if args.PMGRID is None:
			PMGRID = snap.pmgrid
		assert (
			'FDM_PSEUDOSPECTRAL' not in config or
			('FDM_PMGRID' in config and PMGRID == config['FDM_PMGRID']) or
			PMGRID == config['PMGRID']
		)
	elif snap.get_type(args.prefer_type) == 'particles':
		if args.PMGRID is None:
			# set PMGRID³ to 2³ × number of particles for CDM snapshots
			numpart_total = read_snapshot.numpart_total(
				header, ptype=args.ptype
			)
			numpart_per_dim = round(numpart_total**(1/3))
			PMGRID = 2 * numpart_per_dim
		else:
			PMGRID = args.PMGRID
	else: assert False
	if header is not None:
		if args.BoxSize is None:
			BOX_SIZE = header['BoxSize']
		assert BOX_SIZE == header['BoxSize']
	# units
	assert (
		args.units_length == 'code' or (
			header and 'UnitLength_in_cm' in header
		)
	), 'Don’t know length unit'
	UnitLength_in_cm = header['UnitLength_in_cm']
	UnitMass_in_g = header['UnitMass_in_g']
	UnitVelocity_in_cm_per_s = header['UnitVelocity_in_cm_per_s']
	UnitTime_in_s = UnitLength_in_cm / UnitVelocity_in_cm_per_s
	units_length_factor = (
		1 if args.units_length == 'code'
		else UnitLength_in_cm / UNIT_IN_CM[args.units_length]
	)
	# arguments which imply others
	if args.snapshot_format in ('npy', 'raw'):
		args.no_ticks = True
	if args.halo_zoom is not None:
		if mpi_rank == 0:
			target_halo, target_halo_fof = get_target_halo(
				path_idx, path, config, param, args.halo_zoom
			)
		target_halo = mpi_comm.bcast(target_halo, root=0)
		target_halo_fof = mpi_comm.bcast(target_halo_fof, root=0)
		r_vir = target_halo['r200_kpc']
		if args.halo_zoom_cube:
			#TODO units
			r_lim_xy = args.halo_zoom_cube
			r_lim_z = r_lim_xy
		else:
			r_lim_xy = r_vir / args.halo_zoom_factor
			# project at most for R_200 along the projection axis
			r_lim_z = min(r_lim_xy, r_vir)
		if args.halo_zoom_project_width_factor:
			r_lim_z *= args.halo_zoom_project_width_factor
		if args.halo_zoom_project_width:
			r_lim_z = args.halo_zoom_project_width
		offset_x = offset_y = offset_z = 0
		if args.halo_zoom_offset:
			offset_x, offset_y, offset_z = args.halo_zoom_offset
		to_code_length_factor = UNIT_IN_CM['kpc'] / UnitLength_in_cm
		if 'xlim|set' not in args:
			args.xlim = tuple(x * to_code_length_factor for x in (
				target_halo['cx_kpc'] + offset_x - r_lim_xy,
				target_halo['cx_kpc'] + offset_x + r_lim_xy
			))
		if 'ylim|set' not in args:
			args.ylim = tuple(y * to_code_length_factor for y in (
				target_halo['cy_kpc'] + offset_y - r_lim_xy,
				target_halo['cy_kpc'] + offset_y + r_lim_xy
			))
		if 'project|set' not in args:
			args.project = tuple(z * to_code_length_factor for z in (
				target_halo['cz_kpc'] + offset_z - r_lim_z,
				target_halo['cz_kpc'] + offset_z + r_lim_z
			))
		print0(f'Plotting zoom on halo {args.halo_zoom}: {target_halo}')
		print0(
			f'Plot region: x = {args.xlim}, y = {args.ylim}, '
			f'z = {args.project}'
		)
		args.lim_absolute = True
		args.project_absolute = True
		args.mark_halos = True
	if args.no_mark_halos:
		args.mark_halos = False
	# set derived globals
	PMGRID_LIST.append(PMGRID)
	CELL_LEN = BOX_SIZE / PMGRID
	lim_fac = 1 / BOX_SIZE if args.lim_absolute else 1
	XLIM = tuple(int(round(x * lim_fac * PMGRID)) for x in args.xlim)
	YLIM = tuple(int(round(y * lim_fac * PMGRID)) for y in args.ylim)
	PLOT_AXES = tuple(sorted({0, 1, 2} - {args.project_axis}))


def set_plot_title(l):
	if not args.no_title:
		if l.snapshot_fdm and numpy.isfinite(l.mass):
			title = (
				f'${PMGRID}^3$, '
				f'$mc^2 = {float_to_scientific_notation(l.mass)}\,$eV'
			)
		else:
			title = f'${l.numpart_per_dim}^3$'
		if args.v_max_in_title:
			title += '\n'
			if l.snapshot_fdm:
				title += (
					r'$v_{\mathrm{max}} = ' f'{l.v_max_km_s:.1f}\,$km/s'
				)
			else:
				title += r'$v_{99} = ' f'{l.vel_99:.1f}\,$km/s'
		if title[-1] != '\n':
			title += ', '
		if l.comoving:
			if l.z >= 0:
				title += f'$z = {l.z:.2f}$'
			else:
				title += f'$a = {l.snaptime:.{args.time_digits}f}$'
		else:
			title += f'$t = {l.snaptime:.{args.time_digits}f}$'
		if args.subplots or args.subplots_title:
			title = f'{l.snapshot_type_str}: {title}'
			if not args.project:
				title += (
					'\n' r'$z_{\mathrm{slice}} = ' f'{l.z_slice:.2f}$'
				)
		else:
			if args.project:
				if args.project_absolute:
					project_fmt = '[' + ', '.join(
						f'{x * l.units_length_factor:.2f}'
						for x in args.project
					) + r']\, h^{-1}\,$' fr'{args.units_length}$\!'
				else:
					project_fmt = list(args.project)
				title += (
					f', ${COORD_SYMBOLS[args.project_axis]}'
					r'_{\mathrm{proj}} \in ' f'{project_fmt}$'
				)
			else:
				title += (
					f', ${COORD_SYMBOLS[args.project_axis]}_'
					r'{\mathrm{slice}} = '
					fr'{z_slice * CELL_LEN * l.units_length_factor:.2f} '
					r'\, h^{-1}\,$' f'{args.units_length}'
				)
		if args.title_template:
			l.N = PMGRID if l.snapshot_fdm else l.numpart_per_dim
			l.vel_max_str = r'v_{\mathrm{max}}' if l.snapshot_fdm else r'v_{99}'
			if args.v_max_in_title:
				l.vel_max = l.v_max_km_s if l.snapshot_fdm else l.vel_99
			title = args.title_template.format(**globals(), **locals())
		l.ax.set_title(title)


def get_target_halo(path_idx, path, config, param, gr_nr):
	if args.fof_path:
		fof_tab_path = args.fof_path[path_idx]
	else:
		fof_tab_path = read_snapshot.output_path(
			path, config, param, kind='fof'
		)
	m200_data, groups = m200_util.read_m200_data(
		fof_tab_path, m_fof_fallback=args.mark_halos_use_fof_mass,
		return_fof_groups=True
	)
	m200_data.dropna(inplace=True)
	assert gr_nr in m200_data['id']
	target_halo = m200_data.loc[gr_nr, :]
	assert target_halo['id'] == gr_nr
	return target_halo, groups[gr_nr]


@numba.njit
def FDM_FI(pmgrid, x, y, z):
	assert 0 <= x < pmgrid and 0 <= y < pmgrid and 0 <= z < pmgrid
	return pmgrid * (pmgrid * x + y) + z

@numba.njit
def project_grid(grid, pmgrid, ix_info, data_slice, param, data):
	ix_min, ix_start, ix_end = ix_info
	iy_start = data_slice[1].start
	iz_start = data_slice[2].start
	axis = param.axis
	Nx, Ny, Nz = grid.shape
	if ix_start + Nx == pmgrid:
		assert ix_end == pmgrid
	else:
		assert (ix_start + Nx) % pmgrid == ix_end
	g_phase = None
	if param.type == 'phase':
		g_phase = numpy.angle(grid)
	d, d_fof = data
	fof_ids = param.fof_ids
	fof_idx = 0
	for ix_offset in range(Nx):
		ix = (
			#TODO: why is coord_periodic_wrap correct here?
#			bin_density.dist_periodic_wrap(pmgrid, ix_start - ix_min) +
			bin_density.coord_periodic_wrap(pmgrid, ix_start - ix_min) +
			ix_offset
		)
		for iy_offset in range(Ny):
			iy = (iy_start + iy_offset) % pmgrid
			for iz_offset in range(Nz):
				# .real necessary for numba typing in phase computation
				grid_cell = (
					grid[ix_offset, iy_offset, iz_offset].real
					if g_phase is None else
					g_phase[ix_offset, iy_offset, iz_offset]
				)
				assert param.type != 'density' or grid_cell >= 0
				if axis == 0:
					data_idx = (iy_offset, iz_offset)
				elif axis == 1:
					data_idx = (ix, iz_offset)
				elif axis == 2:
					data_idx = (ix, iy_offset)
				else: assert False
				for i, N in zip(data_idx, d.shape):
					assert 0 <= i < N
				d[data_idx]['sum'] += grid_cell
				d[data_idx]['num_cells'] += 1
				# check for FoF cells
				if len(fof_ids) > 0:
					iz = (iz_start + iz_offset) % pmgrid
					grid_id = FDM_FI(pmgrid, ix + ix_min, iy, iz)
					while (
						fof_idx < len(fof_ids) and fof_ids[fof_idx] < grid_id
					):
						fof_idx += 1
					if fof_ids[fof_idx] == grid_id:
						d_fof[data_idx] = True
						assert (
							param.type != 'density' or (
								not param.fof_voids and
								grid_cell >= param.rho_fof_threshold
							) or (
								param.fof_voids and
								grid_cell <= param.rho_fof_threshold
							)
						)


@numba.njit
def get_fof_matches(fof_ids, pids_m):
	fof_idx = 0
	fof_matches = numpy.empty_like(pids_m, dtype=bool)
	for i in range(len(pids_m)):
		pid = pids_m[i]
		# note: particle IDs must be sorted!
		while fof_idx < len(fof_ids) and fof_ids[fof_idx] < pid:
			fof_idx += 1
		fof_matches[i] = fof_ids[fof_idx] == pid
	return fof_matches

def project_particles(pdata, numpart_total, part_start, part_end, param, data):
	cell_len = param.cell_len
	axis = param.axis
	fof_ids = param.fof_ids
	only_ids = param.only_ids
	pos = bin_density.coord_periodic_wrap_vec(param.box_size, pdata.Coordinates)
	grids = data[:-1]
	grid_fof = data[-1]
	numpart = len(pos)
	if 'masses' in param.type:
		mass = pdata.Masses
		assert mass.shape == (numpart,)
	if 'velocities' in param.type:
		vel = pdata.Velocities
		assert vel.shape == (numpart, 3)
	if len(only_ids) > 0 or len(fof_ids) > 0:
		pids = pdata.ParticleIDs
		assert pids.shape == (numpart,)
	assert part_start + numpart == part_end
	#TODO: wrapping across box boundary
	mask = (param.min <= pos[:, axis]) & (pos[:, axis] < param.max)
	if len(only_ids) > 0:
		mask &= numpy.isin(pids, only_ids)
	pos_m = pos[mask]
	if 'masses' in param.type:
		mass_m = mass[mask]
	else:
		mass_m = numpy.full(1, param.particle_mass)
	vars_to_bin = [mass_m]
	if 'velocities' in param.type:
		vel_m = vel[mask]
		vars_to_bin += [vel_m[:, i] for i in range(3)]
	assert len(vars_to_bin) == len(grids)
	for i, [particle_var, grid] in enumerate(zip(vars_to_bin, grids)):
		assert len(particle_var) == len(pos_m) or len(particle_var) == 1
		cic_assignment_project(grid, pos_m, cell_len, particle_var)
	if len(fof_ids) > 0:
		fof_matches = get_fof_matches(fof_ids, pids[mask])
		cic_assignment_project(grid_fof, pos_m, cell_len, fof_matches)


def cic_assignment_project(grid, pos, cell_len, var):
	sum_grid_initial = numpy.sum(grid)
	#TODO: projection axis!
	particles_cic.cic_assignment_generic(
		cic_assignment_func_project, grid, pos, cell_len, var
	)
	# consistency check
	sum_grid = numpy.sum(grid) - sum_grid_initial
	sum_var = numpy.sum(var)
	if len(var) == 1:
		sum_var *= len(pos)
	assert numpy.isclose(sum_grid, sum_var)

@numba.njit
def cic_assignment_func_project(grid, idx, fractions, var):
	grid[idx[0], idx[1]] += numpy.prod(fractions) * var


assert not args.fof_path or (
	len(args.path) == 1 or len(args.fof_path) == len(args.path)
)
# plot rc setup
if mpi_rank == 0:
	if args.figsize:
		plt.rc('figure', figsize=args.figsize)
	if args.title_fontsize:
		plt.rc('axes', titlesize=args.title_fontsize)
	if args.bbox:
		plt.rc('savefig', bbox=args.bbox)
	if args.no_title and args.no_ticks and args.no_colorbar:
		plt.rc('savefig', pad_inches=0.01)
# set up subplot grid
if mpi_rank == 0 and args.subplots:
	assert args.slice_step is None
	num_rows = 1 + int(args.powerspec)
	num_cols = len(args.path)
	gridspec_kw = {}
	if args.powerspec:
		gridspec_kw['height_ratios'] = [1.5, 1]
	rc_figsize = plt.rcParams['figure.figsize']
	fig, plt_grid = plt.subplots(
		num_rows, num_cols, squeeze=False, sharex='row', sharey='row',
		figsize=args.figsize if args.figsize else (
			num_cols * rc_figsize[0], (num_rows + 0.5) * rc_figsize[1]
		), gridspec_kw=gridspec_kw
	)
	#TODO
#	plot_pos = plt_grid[0, 0].get_position()
#	cbar_height = 0.95 * (plot_pos.ymax - plot_pos.ymin)
#	cbar_width = cbar_height / 40
#	cbar_left = 0.98 - cbar_width
#	plt.subplots_adjust(left=0.02, right=cbar_left - 0.02)
#	cbar_ax = fig.add_axes([cbar_left, plot_pos.ymin, cbar_width, cbar_height])
	# set up shared y axes for power spectra
	if args.powerspec:
		ps_twinx = [ax.twinx() for ax in plt_grid[1, :]]
		ps_twinx_first = ps_twinx[0]
		for i, ax in enumerate(ps_twinx):
			if i > 0:
				ps_twinx_first.get_shared_y_axes().join(ps_twinx_first, ax)
			if i < len(ps_twinx) - 1:
				ax.tick_params(which='both', labelright=False)
			else:
				ax.tick_params(labelright=True)
# process snapshots
for path_idx, path in enumerate(args.path):
	print0(path)
	t0_tot = time.perf_counter()
	# read metadata and determine if this is a FDM or CDM snapshot
	snapshot_args = {}
	if args.snapshot_plane is not None:
		snapshot_args['is_slice'] = args.snapshot_plane
	snap = snapshot.Snapshot(path, **snapshot_args)
	config = snap.config
	param = snap.param
	header = snap.header
	comoving = snaptime = None
	if header is not None:
		snaptime = header['Time']
		mass_table = header['MassTable']
	if param is not None:
		comoving = bool(param['ComovingIntegrationOn'])
	set_globals(snap, path, config, param, header)
	snap_num = read_snapshot.snapshot_number(path, param)
	project = args.project
	slice_stop = args.slice_stop if args.slice_stop else PMGRID
	snapshot_plane = args.snapshot_plane
	snapshot_rho = args.snapshot_rho
	# try to parse time for formats without header information
	if header is None:
		match = re.match(
			args.time_from_filename_format, path.name
		)
		if match:
			try:
				snaptime = float(match.group(1))
			except ValueError: pass
	z = None
	if snaptime is None:
		snaptime = path_idx
	elif comoving:
		z = 1 / snaptime - 1
		print0(f'z = {z}')
	box_size_plot = BOX_SIZE * units_length_factor
	#TODO
#	if snap.param['ComovingIntegrationOn']:
	rho_avg_matter = (
		snap.header['Omega0'] *
			snap.constant_in_internal_units('critical_density0')
	)
	rho_avg_expected = (
		(snap.header['Omega0'] - snap.header['OmegaBaryon']) *
			snap.constant_in_internal_units('critical_density0')
	)
	print0(f'Total matter background density: {rho_avg_matter}')
	print0(
		'Expected value for the dark matter background density: '
		f'{rho_avg_expected}'
	)
	grid_avg = rho_avg_expected
	# determine snapshot type
	snapshot_fdm = snap.get_type(args.prefer_type) == 'grid'
	if args.ptype != 1:
		#TODO
		snapshot_fdm = False
	snapshot_type_str = 'FDM' if snapshot_fdm else 'CDM'
	snapshot_type_str_alt = 'SP' if snapshot_fdm else '$N$-body'
	# special settings for FDM
	if snapshot_fdm:
		snapshot_plane = snap.slice
		hbar = astropy.constants.hbar
		mass = param.get('AxionMassEv', numpy.inf)
		p = str(path)
		if not args.no_mass_h_fix and not (
			'_cm_' in p or '_correctmass_' in p or '_axionic_' in p
		):
			if (
				snapshot_fdm and snap.param['ComovingIntegrationOn'] and
				mass in (1e-22, 5e-23, 2.5e-23)
			):
				mass *= snap.hubble_param
				print0(
					'WARNING: Assuming incorrect axion mass, correcting by h '
					f'(using value {mass})'
				)
		mass_u = astropy.units.Quantity(mass, 'eV') / astropy.constants.c**2
		cell_len_u = (
			astropy.units.Quantity(CELL_LEN * UnitLength_in_cm, 'cm') /
			snap.hubble_param
		)
		v_max_km_s = (hbar * numpy.pi / mass_u / cell_len_u).to('km/s').value
	# special settings for CDM
	else:
		snapshot_rho = True
	numpart_total = snap.numpart_total(args.ptype)
	numpart_per_dim = round(numpart_total**(1/3))
	# determine projection range
	if snapshot_plane: project = None
	if project:
		proj_min_pos, proj_max_pos = project
		if not args.project_absolute:
			proj_min_pos *= BOX_SIZE
			proj_max_pos *= BOX_SIZE
		proj_min = int(round(proj_min_pos / CELL_LEN))
		proj_max = int(round(proj_max_pos / CELL_LEN))
		if proj_max <= proj_min:
			proj_max = proj_min + 1
	# determine slices
	if args.slice_step and not snapshot_plane:
		z_slices = range(args.slice_start, slice_stop, args.slice_step)
	else:
		z_slices = [PMGRID // 2]
	# read FoF IDs
	fof_ids = None
	if args.mark_fof_cells is not None or args.mark_fof_cells_all:
		if args.mark_fof_cells_all:
			print0('Reading FoF IDs for all halos')
		else:
			print0(f'Reading FoF IDs for halo {args.mark_fof_cells}')
		t0 = time.perf_counter()
		fof_ids = None
		fof_ids_shape = None
		fof_ids_template = None
		if mpi_rank == 0:
			fof_opt = {}
			if args.fof_path:
				fof_opt['fof_path'] = args.fof_path[path_idx]
			*_, fof_ids = snap.read_fof_tab(
				**fof_opt, read_ids=True,
				# read only IDs for the specified group
				start_group=0 if args.mark_fof_cells_all else
					args.mark_fof_cells,
				max_data_size=0 if args.mark_fof_cells_all else 1
			)
			del _
			assert args.mark_fof_cells_all or len(fof_ids) == 1
			fof_ids = numpy.concatenate(fof_ids)
			fof_ids_shape = fof_ids.shape
			fof_ids_template = fof_ids[:1]
			if args.mark_fof_cells_all:
				print0(
					f'\tWill mark FoF cells of all halos ({len(fof_ids):,} '
					'cells)'
				)
			else:
				print0(
					f'\tWill mark FoF cells of halo {args.mark_fof_cells} (has '
					f'{len(fof_ids):,} cells)'
				)
			# IDs should already be sorted, but make sure
			fof_ids.sort()
		fof_ids_shape = mpi_comm.bcast(fof_ids_shape, root=0)
		fof_ids_template = mpi_comm.bcast(fof_ids_template, root=0)
		if mpi_rank != 0:
			fof_ids = numpy.zeros_like(fof_ids_template, shape=fof_ids_shape)
		mpi_comm.Bcast(fof_ids, root=0)
		t1 = time.perf_counter()
		print0(f'\tTook {t1 - t0} sec.')

	# plot slices
	for z_slice_idx, z_slice in enumerate(z_slices):
		if not args.project:
			print0(f'z_slice = {z_slice}')
		# plot setup
		if mpi_rank == 0:
			if args.subplots:
				ax = plt_grid[0, path_idx]
			else:
				fig = plt.figure()
				ax = plt.gca()
			if not args.only_colorbar:
				ax.set_aspect('equal')
			# set axis labels
			if args.no_ticks:
				ax.tick_params(
					which='both',
					bottom=False, top=False, left=False, right=False,
					labelbottom=False, labeltop=False,
					labelleft=False, labelright=False
				)
			else:
				xlabel = f'${COORD_SYMBOLS[PLOT_AXES[0]]}$'
				ylabel = f'${COORD_SYMBOLS[PLOT_AXES[1]]}$'
				if args.units_length != 'code':
					label_hubble = (
						'$h^{-1}\,$' if snap.hubble_param != 1 else ''
					)
					xlabel += f' / {label_hubble}{args.units_length}'
					ylabel += f' / {label_hubble}{args.units_length}'
				ax.set_xlabel(xlabel)
				if not args.subplots or path_idx == 0:
					ax.set_ylabel(ylabel)
			# set axis limits
			xlim = [(lim / PMGRID) * box_size_plot for lim in XLIM]
			ylim = [(lim / PMGRID) * box_size_plot for lim in YLIM]
			ax.set_xlim(xlim)
			ax.set_ylim(ylim)

		# FDM read (wave function)
		if snapshot_fdm:
			assert args.plot_type != 'dot'
			fof_threshold = snap.config.get(
				'FDM_HALO_FINDER_OVERDENSITY',
				FDM_HALO_FINDER_OVERDENSITY_DEFAULT
			)
			# compute (projected) density or phase from wave function
			pmgrid_read = (
				args.PMGRID if args.snapshot_format == 'raw' else PMGRID
			)
			# potentially load pre-computed result
			if args.load_result_data:
				load_path = args.load_result_data
				if len(args.path) > 1:
					load_path = load_path.with_stem(
						f'{load_path.stem}_{path_idx}'
					)
				grid = None
				if mpi_rank == 0:
					print0(f'Loading pre-computed grid data from “{load_path}”')
					grid = numpy.load(load_path)
					if fof_ids is not None:
						marked_cells_path = load_path.with_stem(
							f'{load_path.stem}_marked_cells'
						)
						marked_cells = numpy.load(marked_cells_path)
			# 2D data
			elif snapshot_plane and not args.potential and mpi_rank == 0:
				try:
					psi_re = snap.read_snapshot('/FuzzyDM/PsiReSlice')
					psi_im = snap.read_snapshot('/FuzzyDM/PsiImSlice')
				except snapshot.SnapshotError:
					psi_re = snap.read_snapshot('/FuzzyDM/PsiRe')
					psi_im = snap.read_snapshot('/FuzzyDM/PsiIm')
				psi = numexpr.evaluate('psi_re + 1j * psi_im')
				del psi_re, psi_im
				assert len(psi) == 1
				if args.phase:
					grid = numpy.angle(psi[0])
				else:
					grid = numpy.abs(psi[0])**2
					print0(f'2D grid: <ρ> = {numpy.mean(grid)}')
					print0(f'2D grid: v_max = {v_max_km_s} km/s')
				del psi
			# 3D data
			else:
				# project along given axis (--project-axis)
				if project:
					param_min, param_max = proj_min, proj_max
				else:
					param_min, param_max = z_slice, z_slice + 1
				data_slice = [None] * 3
				data_slice[PLOT_AXES[0]] = numpy.s_[XLIM[0]:XLIM[1]]
				data_slice[PLOT_AXES[1]] = numpy.s_[YLIM[0]:YLIM[1]]
				#TODO: fails if not --project (but should unify anyway)
				data_slice[args.project_axis] = numpy.s_[proj_min:proj_max]
				data_slice = tuple(data_slice)
				param_min_rel = param_min / PMGRID
				param_max_rel = param_max / PMGRID
				data_dtype = [('sum', 'f8'), ('num_cells', 'i8')]
				data = numpy.zeros(
					(XLIM[1] - XLIM[0], YLIM[1] - YLIM[0]), dtype=data_dtype
				)
				if fof_ids is None:
					data_fof = numpy.zeros((1,) * 2, dtype=bool)
				else:
					data_fof = numpy.zeros_like(data, dtype=bool)
				# get density or phase
				if args.phase:
					# phase from the interval [-π, π]
					it_param = Param(
						type='phase', axis=args.project_axis,
						min=param_min, max=param_max,
						fof_ids=NUMBA_EMPTY_LIST_U8 if fof_ids is None
							else fof_ids,
						rho_fof_threshold=fof_threshold * rho_avg_expected,
						fof_voids=args.fof_voids,
						cell_len=CELL_LEN
					)
					iterate_grid.iterate(
						snap, project_grid, (data, data_fof),
						param=it_param, use_rho=False, data_slice=data_slice
					)
					phase_sum_g = mpi_comm.reduce(
						data['sum'], op=MPI.SUM, root=0
					)
					num_cells_g = mpi_comm.reduce(
						data['num_cells'], op=MPI.SUM, root=0
					)
					if fof_ids is not None:
						marked_cells = mpi_comm.reduce(
							#TODO: need to convert to int array first?
							#      -> use int array in the first place?
							data_fof, op=MPI.SUM, root=0
						)
					if mpi_rank == 0:
						assert numpy.all(
							(num_cells_g == param_max - param_min) |
							(num_cells_g == 0)
						), (param_max, param_min, num_cells_g)
						num_cells_g[num_cells_g == 0] = 1
						phase = phase_sum_g / num_cells_g
						grid = phase
				elif args.potential and mpi_rank == 0:
					# potential “slice”
					phi = snap.read_snapshot('/FuzzyDM/PotentialSlice')
					assert len(phi) % 2 == 1
					# normalize to 0
					phi -= numpy.min(phi)
					it_param = Param(
						type='potential', axis=args.project_axis,
						# TODO: only plotting center slice at the moment
						min=len(phi) // 2, max=len(phi) // 2 + 1,
						fof_ids=NUMBA_EMPTY_LIST_U8 if fof_ids is None
							else fof_ids,
						rho_fof_threshold=fof_threshold * rho_avg_expected,
						fof_voids=args.fof_voids,
						cell_len=CELL_LEN,
					)
					project_grid(
						phi, PMGRID, 0, len(phi), it_param, (data, data_fof)
					)
					assert numpy.all(data['num_cells'] > 0)
					assert numpy.all(data['num_cells'] == 1) #TODO: see above
					phi = data['sum'] / data['num_cells']
					grid = phi
					grid_avg = numpy.mean(grid)
				else:
					# density
					it_param = Param(
						type='density', axis=args.project_axis,
						min=param_min, max=param_max,
						fof_ids=NUMBA_EMPTY_LIST_U8 if fof_ids is None
							else fof_ids,
						rho_fof_threshold=fof_threshold * rho_avg_expected,
						fof_voids=args.fof_voids,
						cell_len=CELL_LEN,
					)
					iterate_grid.iterate(
						snap, project_grid, (data, data_fof),
						param=it_param, data_slice=data_slice
					)
					print0('Iterations done, reducing data on rank 0')
					t0 = time.perf_counter()
					rho_sum_g = mpi_comm.reduce(
						data['sum'], op=MPI.SUM, root=0
					)
					num_cells_g = mpi_comm.reduce(
						data['num_cells'], op=MPI.SUM, root=0
					)
					if fof_ids is not None:
						marked_cells = mpi_comm.reduce(
							#TODO: need to convert to int array first?
							#      -> use int array in the first place?
							data_fof, op=MPI.SUM, root=0
						)
					if mpi_rank == 0:
						assert numpy.all(num_cells_g == param_max - param_min)
						rho = rho_sum_g / num_cells_g
						print0(
							f'3D Grid: <ρ> = {numpy.mean(rho)} (projection '
							f'axis ∈ [{param_min_rel:.2f}, '
							f'{param_max_rel:.2f}])'
						)
						print0(f'3D grid: v_max = {v_max_km_s} km/s')
						grid = rho
						t1 = time.perf_counter()
						print0(f'\tTook {t1 - t0} sec.')
		# CDM read (particles)
		# TODO: support GADGET snapshots in snapshot.py
		else:
			if args.load_result_data:
				load_path = args.load_result_data
				if len(args.path) > 1:
					load_path = load_path.with_stem(
						f'{load_path.stem}_{path_idx}'
					)
				grid = None
				if mpi_rank == 0:
					print0(f'Loading pre-computed grid data from “{load_path}”')
					grid = numpy.load(load_path)
			elif args.plot_type == 'dot':
				pos = snap.read_snapshot(f'/PartType{args.ptype}/Coordinates')
			else:
				# CDM snapshot: density grid assignment
				# TODO: remove need to always bin to a full box-sized grid
				data = (numpy.zeros((PMGRID,) * 2, dtype='f8'),)
				param_type = ('density',)
				datasets = ['Coordinates']
				if mass_table[args.ptype] == 0 and not args.number_density:
					datasets.append('Masses')
					param_type += ('masses',)
				v_grid = None
				if args.velocities:
					data += tuple(numpy.zeros_like(data[0]) for _ in range(3))
					datasets += [f'Velocities{i}' for i in range(3)]
					param_type += ('velocities',)
				if args.only_ids:
					only_ids = numpy.load(args.only_ids)
					# workaround in project_particles requires only_ids to be
					# sorted
					only_ids.sort()
				else:
					only_ids = numpy.empty(0, dtype='u8')
				if fof_ids is None:
					data_fof = numpy.zeros((1,) * 2, dtype=bool)
				else:
					data_fof = numpy.zeros_like(data[0], dtype=bool)
				if args.only_ids or fof_ids is not None:
					datasets.append('ParticleIDs')
				it_param = Param(
					type=param_type, axis=args.project_axis,
					min=proj_min_pos, max=proj_max_pos,
					fof_ids=NUMBA_EMPTY_LIST_U8 if fof_ids is None else fof_ids,
					cell_len=CELL_LEN, only_ids=only_ids,
					particle_mass=mass_table[args.ptype]
						if not args.number_density else 1.0,
					box_size=snap.box_size
				)
				iterate_grid.iterate_particles(
					snap, project_particles, data + (data_fof,), param=it_param,
					ptype=args.ptype, datasets=datasets
				)
				# globally reduce results
				for var_grid in data:
					mpi_comm.Reduce(
						MPI.IN_PLACE if mpi_rank == 0 else var_grid, var_grid,
						op=MPI.SUM, root=0
					)
				data_g = data
				if fof_ids is not None:
					marked_cells = mpi_comm.reduce(
						#TODO: need to convert to int array first?
						#      -> use int array in the first place?
						data_fof, op=MPI.SUM, root=0
					)
				if mpi_rank == 0:
					rho = data_g[0]
					rho /= CELL_LEN**3
					assert numpy.all(rho >= 0)
					v_grid = None
					if args.velocities:
						v_grid = data[1:4]
					rho_avg = numpy.mean(rho)
					#TODO: compute mean of all particles during iteration
					print0(f'Particles: <ρ> = {rho_avg}')
					if not snap.param['ComovingIntegrationOn']:
						grid_avg = rho_avg
					# set grid variable
					if project:
						grid = rho / (proj_max - proj_min)
					else:
						#TODO: CDM slice
#						grid = rho[:, :, z_slice]
						assert False
					# restrict to selected limits, accounting for potential
					# wrapping across the box boundary
					x_indices = numpy.arange(*XLIM)
					y_indices = numpy.arange(*YLIM)
					ix, iy = numpy.meshgrid(x_indices, y_indices, indexing='ij')
					grid = grid[ix % grid.shape[0], iy % grid.shape[1]]
			if args.v_max_in_title:
				#TODO: do this using grid iteration as well
				vel = snap.read_snapshot(f'/PartType{args.ptype}/Velocities')
				vel_max = numpy.max(numpy.linalg.norm(vel, axis=1))
				vel_99 = numpy.percentile(numpy.linalg.norm(vel, axis=1), 99)
				print0(f'Particles: v_max = {vel_max}, v_99 = {vel_99}')
		if mpi_rank == 0:
			# transpose grid for plot
			grid = grid.T
			if fof_ids is not None:
				marked_cells = marked_cells.T
			# messages about computed grids
			if fof_ids is not None:
				print0(f'Marked {numpy.sum(marked_cells > 0)} FoF cells')
			# optionally save computed grid data for future use
			if args.save_result_data:
				save_path = args.save_result_data
				if len(args.path) > 1:
					save_path = save_path.with_stem(
						f'{save_path.stem}_{path_idx}'
					)
				print0(f'Saving computed grid data to “{save_path}”')
				numpy.save(save_path, grid)
				if fof_ids is not None:
					marked_cells_path = save_path.with_stem(
						f'{save_path.stem}_marked_cells'
					)
					numpy.save(marked_cells_path, marked_cells)

		#TODO
			set_plot_title(types.SimpleNamespace(**locals()))

		# data has been read, remaining operations are serial
		if len(args.path) == 1 and not args.subplots and (
			args.plot_format == 'pgf'
		):
			# finalize MPI because matplotlib’s PGF backend creates LaTeX
			# child processes, which can cause issues with MPI libraries
			MPI.Finalize()
		if mpi_rank == 0:
			t0 = time.perf_counter()
			if args.plot_type == 'dot':
				print0('Plotting positions as dots')
			else:
				print0('Plotting computed grid')
			# hide axes if only saving colorbar
			if args.only_colorbar:
				ax.set_visible(False)
			# plot grid
			if args.plot_type == 'dot':
				pos = pos[
					(pos[:, args.project_axis] >= proj_min_pos) &
					(pos[:, args.project_axis] <= proj_max_pos)
				] * units_length_factor
				# plot dots, with z-order adjusted to the same value as the
				# other plotting types
				ax.plot(
					pos[:, PLOT_AXES[0]], pos[:, PLOT_AXES[1]], ',', zorder=1
				)
			elif args.plot_type == 'contour':
				ax.contour(grid, linewidths=0.5)
			elif args.plot_type == 'color':
				if args.plot_range_absolute or args.phase:
					grid_plot = grid
				else:
					grid_plot = grid / grid_avg
				if args.plot_min_cutoff:
					grid_plot[grid_plot < args.plot_min_cutoff] = 0
				# avoid points with zero density in log plot
				if args.plot_scale != 'linear':
					grid_plot[grid_plot == 0] = 1e-50
				norm = get_color_norm(
					[] if args.plot_range_auto else args.plot_range
				)
				plot_opt = {'rasterized': True, 'norm': norm}
				cbar_opt = {'pad': 0.02}
				if norm.vmin is not None and norm.vmax is not None:
					grid_min = numpy.min(grid_plot)
					grid_max = numpy.max(grid_plot)
					min_out_of_range = grid_min < norm.vmin
					max_out_of_range = grid_max > norm.vmax
					if min_out_of_range and max_out_of_range:
						cbar_opt['extend'] = 'both'
					elif min_out_of_range:
						cbar_opt['extend'] = 'min'
					elif max_out_of_range:
						cbar_opt['extend'] = 'max'
				if args.colorbar_extend:
					cbar_opt['extend'] = args.colorbar_extend
				if args.colorbar_orientation:
					cbar_opt['orientation'] = args.colorbar_orientation
				# note: grid_plot.shape values have to be swapped here because
				#       grid is transposed!
				X = numpy.linspace(*xlim, grid_plot.shape[1] + 1)
				Y = numpy.linspace(*ylim, grid_plot.shape[0] + 1)
				# check that pixels are square, i.e. that xlim, ylim match the
				# grid shape
				assert len(X) == XLIM[1] - XLIM[0] + 1
				assert len(Y) == YLIM[1] - YLIM[0] + 1
				cmesh = ax.pcolormesh(X, Y, grid_plot, **plot_opt)
				if args.colorbar_digits is not None:
					cbar_opt['format'] = f'%.{args.colorbar_digits}e'
				if not args.subplots:
					cbar = plt.colorbar(cmesh, **cbar_opt)
					cbar_label_opt = dict(CBAR_LABEL_OPT)
					if cbar_opt.get('orientation') == 'horizontal':
						del cbar_label_opt['labelpad']
					if args.phase:
						cbar.set_label(r'$\arg(\psi)$', **cbar_label_opt)
					else:
						cbar.set_label(args.colorbar_label, **cbar_label_opt)
					if args.no_colorbar:
						cbar.ax.set_visible(False)
				# print plot grid stats
				print0('Stats for the plotted grid:')
				print0(pandas.Series(grid_plot.reshape(-1)).describe())
			t1 = time.perf_counter()
			print0(f'\tTook {t1 - t0} sec.')
			# show marked FoF cells in a different color
			if args.plot_type == 'color' and fof_ids is not None:
				print0('Plotting marked FoF cells')
				t0 = time.perf_counter()
				marked_cells_mask = marked_cells.astype(bool)
				grid_plot[~marked_cells_mask] = numpy.nan
				marked_values = (grid / grid_avg)[marked_cells_mask]
				if not project:
					assert numpy.all(marked_values >= fof_threshold), (
						len(marked_values),
						numpy.sum(marked_values < fof_threshold),
						marked_values[marked_values < fof_threshold]
					)
				plot_opt['norm'] = get_color_norm(args.plot_range_mark_fof)
				ax.pcolormesh(X, Y, grid_plot, cmap='autumn', **plot_opt)
				t1 = time.perf_counter()
				print0(f'\tTook {t1 - t0} sec.')
			# optionally plot velocities
			if args.velocities:
				if not snapshot_fdm:
					vx, vy = v_grid
				s = args.velocities_step
				if args.no_ticks:
					X = Y = numpy.linspace(0, PMGRID, PMGRID)
				if project:
					idx = [numpy.s_[:]] * 3
					idx[args.project_axis] = numpy.s_[proj_min:proj_max]
					vx = numpy.sum(vx[tuple(idx)], axis=args.project_axis).T
					vy = numpy.sum(vy[tuple(idx)], axis=args.project_axis).T
				else:
					vx = vx[:, :, z_slice].T
					vy = vy[:, :, z_slice].T
				assert vx.shape == vy.shape == (PMGRID,) * 2
				ax.quiver(
					X[::s], Y[::s], vx[::s, ::s], vy[::s, ::s], color='w'
				)
			# optionally mark halos
			if args.mark_halos or args.mark_halos_overlapping_with is not None:
				if args.fof_path:
					fof_tab_path = args.fof_path[path_idx]
				else:
					fof_tab_path = read_snapshot.output_path(
						path, config, param, kind='fof'
					)
				print0('Marking halos with circles')
				t0 = time.perf_counter()
				m200_data = m200_util.read_m200_data(
					fof_tab_path, m_fof_fallback=args.mark_halos_use_fof_mass,
					fof_voids=args.fof_voids
				)
				# drop dummy rows
				m200_data.dropna(subset=['m200_ms'], inplace=True)
				kpc_to_length_factor = (
					UNIT_IN_CM['kpc'] / UnitLength_in_cm
					if args.units_length == 'code' else
					UNIT_IN_CM['kpc'] / UNIT_IN_CM[args.units_length]
				)
				Ms_to_mass_factor = UNIT_IN_G['Ms'] / UnitMass_in_g
				if args.mark_halos_use_fof_mass:
					r200 = (
						m200_data['m_fof_ms'] * Ms_to_mass_factor / (
							4/3 * numpy.pi * VIRIAL_OVERDENSITY *
							rho_avg_expected
						)
					)**(1/3) * units_length_factor
				else:
					r200 = m200_data['r200_kpc'] * kpc_to_length_factor
				assert (r200 < BOX_SIZE).all()
				# check if correct halo center is available
				assert len(m200_data.dropna()) == len(m200_data)
				halo_centers = numpy.zeros((len(m200_data), 3))
				halo_centers[:, 0] = m200_data['cx_kpc']
				halo_centers[:, 1] = m200_data['cy_kpc']
				halo_centers[:, 2] = m200_data['cz_kpc']
				halo_centers *= kpc_to_length_factor
				#TODO periodic wrapping
				c_proj = halo_centers[:, args.project_axis]
				mask = (
					(proj_min_pos * units_length_factor < c_proj + r200) &
					(c_proj - r200 < proj_max_pos * units_length_factor)
				)
				halo_centers = halo_centers[mask]
				r200 = r200[mask]
				halo_ids = m200_data['id'][mask]
			if args.mark_halos:
				# draw R_200 circles (but only if they’re bigger than a few
				# pixels)
				# TODO: does everything work with periodic wrapping?
				mark_halos_file = open('mark-halos.txt', 'w')
				x_range = xlim[1] - xlim[0]
				y_range = ylim[1] - ylim[0]
				largest_halos = []
				for halo_id, r, center in zip(halo_ids, r200, halo_centers):
					center_proj = center[list(PLOT_AXES)]
					if (
						xlim[0] - r < center_proj[0] < xlim[1] + r and
						ylim[0] - r < center_proj[1] < ylim[1] + r and
						r / x_range >= args.mark_halo_size and
						r / y_range >= args.mark_halo_size
					):
						ax.add_artist(
							plt.Circle(
								center_proj, radius=r, fill=False,
								color='k' if args.plot_type == 'dot'
									else 'white',
								linewidth=0.4
							)
						)
						assert mpi_rank == 0
						print(halo_id, file=mark_halos_file)
						if len(largest_halos) == args.mark_halos_label_num:
							r_threshold, *_ = largest_halos[-1]
							if r > r_threshold:
								largest_halos[-1] = (r, halo_id, center_proj)
								largest_halos.sort()
						else:
							largest_halos.append((r, halo_id, center_proj))
							largest_halos.sort()
						if (
							halo_id == args.halo_zoom and
							not args.no_mark_zoom_halo
						):
							ax.add_artist(
								plt.Circle(
									center[list(PLOT_AXES)], radius=r,
									fill=False,
									color='white' if args.plot_type == 'dot'
										else 'k',
									linewidth=0.2
								)
							)
				mark_halos_file.close()
				if args.mark_halos_label:
					for r, halo_id, center_proj in largest_halos:
						x, y = center_proj
						stroke = {} if args.plot_format == 'pgf' else {
							'path_effects': [
								matplotlib.patheffects.withStroke(
									linewidth=1.0, foreground='k'
								)
							]
						}
						label_text = f'{halo_id}'
						if args.mark_halos_label_r200:
							r200_kpc = r / kpc_to_length_factor
							label_text = (
								fr'$R_{{200}} = {r200_kpc:.1f}\,h^{{-1}}\,$kpc'
							)
						ax.text(
							x, y + 1.01 * r, label_text, color='white',
							size='x-small', verticalalignment='bottom',
							horizontalalignment='center', **stroke
						)
			if args.mark_halos_overlapping_with is not None:
				dup = pandas.read_csv(
					fof_tab_path.with_name(
						f'{fof_tab_path.name}_duplicates.csv'
					)
				)
				assert (dup['id1'] < dup['id2']).all()
				dup = dup[
					(dup['id1'] == args.mark_halos_overlapping_with) |
					(dup['id2'] == args.mark_halos_overlapping_with)
				]
				r200_target = m200_data.at[
					args.mark_halos_overlapping_with, 'r200_kpc'
				]
				print0(
					'Halos overlapping with halo '
					f'{args.mark_halos_overlapping_with} (R_200 = '
					f'{r200_target} kpc/h):'
				)
				print0(dup)
				for id1, id2, *_ in dup.itertuples(index=False):
#					dist_vec = bin_density.dist_periodic_wrap_vec(
#						box_size, center1 - center2
#					)
#					dist2 = numpy.dot(dist_vec, dist_vec)
#					if dist2 < (r200_1 + r200_2)**2:

					other_id = (
						id1 if id2 == args.mark_halos_overlapping_with else id2
					)
					other = m200_data.loc[other_id:other_id]
					assert len(other) == 1
					other = other.to_dict(orient='records')[0]
					assert other_id == other['id']
					or200 = other['r200_kpc'] * kpc_to_length_factor
					ocenter = numpy.array([
						other['cx_kpc'], other['cy_kpc'], other['cz_kpc']
					])
					ocenter *= kpc_to_length_factor
					ax.add_artist(
						plt.Circle(
							ocenter[list(PLOT_AXES)], radius=or200, fill=False,
							color='g', linewidth=0.4
						)
					)
			if args.mark_halos or args.mark_halos_overlapping_with is not None:
				t1 = time.perf_counter()
				print0(f'\tTook {t1 - t0} sec.')
			# optionally mark filaments
			if args.mark_filaments:
				print0(f'Marking filaments in {args.mark_filaments} with lines')
				ndskl_ngrid, filaments = io_util.read_NDskl_ascii(
					args.mark_filaments, PMGRID if snapshot_fdm else None
				)
				coord_fac = box_size_plot / ndskl_ngrid
				def plot_points(fil_id, points, shifts=(-box_size_plot, 0)):
					color_idx = fil_id
					if color_idx == 2:
						color = 'k'
					else:
						color = f'C{color_idx}'
					if args.mark_filaments_color:
						color = args.mark_filaments_color
					for x_shift, y_shift in itertools.product(shifts, repeat=2):
						plt.plot(
							[p[PLOT_AXES[0]] + x_shift for p in points],
							[p[PLOT_AXES[1]] + y_shift for p in points],
							'-', color=color, linewidth=0.4, alpha=0.7
						)
				plot_axes_l = list(PLOT_AXES)
				shrinking_shift_path = args.mark_filaments.with_name(
					f'{args.mark_filaments.name}_shrinking_shift.hdf5'
				)
				if (
					shrinking_shift_path.exists() and
					not args.mark_filaments_no_offsets
				):
					import h5py
					f = h5py.File(shrinking_shift_path)
					print0(f'OFFSET FILE “{shrinking_shift_path}” FOUND!')
				else:
					f = {}
					print0(f'OFFSET FILE “{shrinking_shift_path}” NOT FOUND!')
				for fil_id, fil in enumerate(filaments):
					fil *= coord_fac
					#TODO: to test shrinking sphere code
					if f'{fil_id}' in f:
						offsets = f[f'{fil_id}'][...] * units_length_factor
					else:
						offsets = None
					for i in range(len(fil) - 1):
						if offsets is None: break
						p = fil[i]
						dp = bin_density.dist_periodic_wrap_vec(
							box_size_plot, fil[i + 1] - fil[i]
						)
						if offsets is not None:
							o = offsets[i]
							assert numpy.isclose(
								o @ dp, 0, atol=1e-6, rtol=0
							), (fil_id, i, o, dp, o @ dp)
						else:
							o = numpy.zeros_like(p)
						plot_points(
							fil_id, [p + o, p + o + dp],
							shifts=(-box_size_plot, 0, box_size_plot)
						)
					#TODO: original code
					#TODO limit to projection range
					points = [fil[0]]
					for point in fil[1:]:
						#TODO
						if offsets is not None: break
						if (
							numpy.linalg.norm(
								points[-1][plot_axes_l] - point[plot_axes_l]
							) > 0.9 * box_size_plot
						):
							plot_points(fil_id, points)
							dp = bin_density.dist_periodic_wrap_vec(
								box_size_plot, point - points[-1]
							)
							# need to plot crossing line at both -L and +L,
							# since we don’t know what side the line is on
							plot_points(
								fil_id, [points[-1], points[-1] + dp],
								shifts=(-box_size_plot, 0, box_size_plot)
							)
							points = []
						points.append(point)
					plot_points(fil_id, points)
				if f:
					f.close()
				if args.mark_filaments_label:
					for fil_id, fil in enumerate(filaments):
						if len(fil) < 25:
							continue
						x, y = fil[1][list(PLOT_AXES)]
						stroke = {} if args.plot_format == 'pgf' else {
							'path_effects': [
								matplotlib.patheffects.withStroke(
									linewidth=1.0, foreground='k'
								)
							]
						}
						ax.text(
							x, y, f'{fil_id}', color='white', size='xx-small',
							**stroke
						)
			# optionally include scale bar
			if args.scalebar:
				if args.scalebar_size:
					size = args.scalebar_size
				else:
					plot_length = min(xlim[1] - xlim[0], ylim[1] - ylim[0])
					size = 0.25 * plot_length
				size_label = (
					f'{size:g}' r'$\,h^{-1}\,$' f'{args.units_length}'
				)
				scalebar = (
					mpl_toolkits.axes_grid1.anchored_artists.AnchoredSizeBar(
						ax.transData, size, size_label, args.scalebar_loc,
						size_vertical=size / 40, color=args.scalebar_color,
						frameon=False, pad=1.5, sep=3,
						fontproperties={'size': 'smaller'}
				))
				if args.plot_format != 'pgf':
					# unfortunately, path effects are done in PGF by
					# converting the text to paths, which somehow breaks text
					# alignment
					scalebar.size_bar.get_children()[0].set_path_effects([
						matplotlib.patheffects.withStroke(
							linewidth=2.0, foreground='k'
						)
					])
					scalebar.txt_label.get_children()[0].set_path_effects([
						matplotlib.patheffects.withStroke(
							linewidth=1.0, foreground='k'
						)
					])
				ax.add_artist(scalebar)
			# optionally print zoom halo info
			if args.halo_zoom is not None and args.halo_zoom_info:
				stroke = {} if args.plot_format == 'pgf' else {
					'path_effects': [
						matplotlib.patheffects.withStroke(
							linewidth=1.0, foreground='k'
						)
					]
				}
				virial_ratio = (
					-2 * (target_halo_fof.kinegy + target_halo_fof.gradegy) /
					target_halo_fof.potegy
				)
				label_text_top = (
					r'$M_{200} = '
					fr'{float_to_scientific_notation2(target_halo["m200_ms"], digits=2)}\,'
					r'h^{-1}\,M_\odot$' '\n'
					r'$R_{200} = ' fr'{target_halo["r200_kpc"]:.1f}\,'
					r'h^{-1}\,$kpc'
				)
				label_text_bottom = fr'$-2T/V = {virial_ratio:.2f}$'
				text_opt = dict(
					transform=ax.transAxes, horizontalalignment='left',
					color='white', size=args.halo_zoom_info_fontsize,
					in_layout=False, **stroke
				)
				ax.text(
					0.01, 0.99, label_text_top, verticalalignment='top',
					**text_opt
				)
				ax.text(
					0.01, 0.01, label_text_bottom, verticalalignment='bottom',
					**text_opt
				)
			# optionally plot power spectrum
			# TODO: improve this plot as in plot_power_spectra.py
			# TODO: factor out power spectrum plotting into common module
			if args.powerspec:
				path_parent = path.resolve().parent
				ps_path_snap = path_parent / f'powerspec_{snap_num:03}.txt'
				ps_path_combined = path_parent / 'powerspec.txt'
				ps = None
				if ps_path_snap.exists():
					ps_path = ps_path_snap
					ps = pandas.read_csv(
						ps_path_snap, sep=r'\s+',
						names=[
							'Kbin', 'DeltaUncorrected', 'PowerUncorrected',
							'CountModes', 'ShotLimit'
						],
					)
					ps.dropna(inplace=True)
				elif ps_path_combined.exists():
					ps_path = ps_path_combined
					ps = pandas.read_csv(ps_path_combined, comment='#')
					ps = ps[numpy.isclose(ps.Time, snaptime, atol=1e-2)]
				else:
					warnings.warn(
						f'Neither {ps_path_snap} nor {ps_path_combined} exist'
					)
				if ps is not None and len(ps) > 0 and len(ps) % PS_BINS == 0:
					ps = ps[:PS_BINS]
					ps = ps[ps.CountModes > 0]
					ps_ax = plt_grid[1, path_idx]
					ps_ax2 = ps_twinx[path_idx]
					if path_idx == 0:
						ps_ax.set_ylabel(r'$P(k)$')
					if path_idx == len(args.path) - 1:
						ps_ax2.set_ylabel(
							r'$\Delta(k)^2 = \frac{1}{2\pi^2} k^3 P(k)$'
						)
					ps_ax.set_xlabel('Kbin')
					P = ps.PowerUncorrected
					Delta2 = ps.Kbin**3 * P / (2 * numpy.pi**2)
					ps_ax.loglog(
						ps.Kbin, P, '.-', linewidth=0.3, markersize=0.5
					)
					ps_ax2.loglog(
						ps.Kbin, Delta2,
						'.-', color='C1', linewidth=0.3, markersize=0.5
					)
				elif ps is not None:
					warnings.warn(
						'Unexpected power spectrum file contents, ignoring:\n'
						f'{ps_path}, rows = {len(ps)}'
					)
			# optionally add zoom inset
			if args.zoom_inset:
				lim_abs = tuple(lim * box_size_plot for lim in args.zoom_inset)
				frame_color = 'white'
				ax_ins = ax.inset_axes(args.zoom_inset_position)
				ax_ins.set_aspect('equal')
				for s in ax_ins.spines.values():
					s.set_color(frame_color)
				ax_ins.tick_params(
					which='both',
					bottom=False, top=False, left=False, right=False,
					labelbottom=False, labeltop=False,
					labelleft=False, labelright=False
				)
				ax_ins.set_xlim(lim_abs[:2])
				ax_ins.set_ylim(lim_abs[2:])
				ax_ins.pcolormesh(X, Y, grid_plot, **plot_opt)
				ax.indicate_inset_zoom(ax_ins, alpha=1.0, edgecolor=frame_color)
			# save current snapshot as image file
			if not args.subplots:
				plot_file_name = ''
				if args.plot_file_prefix_run_name:
					plot_file_name += f'{read_snapshot.get_run_name(path)}_'
				if args.project:
					plot_file_name += f'project_{path.name}'
				else:
					plot_file_name += f'plane_{path.name}'
					if not snapshot_plane:
						plot_file_name += f'_{z_slice:03}'
				if args.plot_file_name:
					plot_file_name = args.plot_file_name
				savefig_path = (
					f'{args.plot_file_prefix}{plot_file_name}.'
					f'{args.plot_format}'
				)
				print0(f'Saving figure to {savefig_path}')
				t0 = time.perf_counter()
				savefig_opts = {'dpi': args.dpi}
				plt.savefig(
					savefig_path, transparent=args.transparent, **savefig_opts
				)
				plt.close()
				t1 = time.perf_counter()
				print0(f'\tTook {t1 - t0} sec.')
	t1_tot = time.perf_counter()
	print0(f'Total time taken for {path}: {t1_tot - t0_tot} sec.')

# save subplot grid as image file
if mpi_rank == 0 and args.subplots:
	title = ''
	if args.project and not args.no_suptitle and not args.no_title:
		title += (
			f'${COORD_SYMBOLS[args.project_axis]}'
			r'_{\mathrm{proj}} \in ' f'{args.project}$'
		)
		plt.suptitle(title, fontsize='xx-large')
	if not args.no_colorbar:
		# TODO: use a different method than ax=..., in order to make the
		#       colorbar the same size as the plot content (without
		#       titles/ticks!)
		cbar = plt.colorbar(cmesh, ax=plt_grid, **cbar_opt)
		cbar.set_label(args.colorbar_label, **CBAR_LABEL_OPT)
	plot_file_name = 'grid'
	if args.project:
		plot_file_name += '_project'
	if args.powerspec:
		plot_file_name += '_powerspec'
	if args.plot_file_name:
		plot_file_name = args.plot_file_name
	savefig_opts = {'dpi': args.dpi}
	plt.savefig(
		f'{args.plot_file_prefix}{plot_file_name}.{args.plot_format}',
		transparent=args.transparent, **savefig_opts
	)

if not MPI.Is_finalized():
	MPI.Finalize()
