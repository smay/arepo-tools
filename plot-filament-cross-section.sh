#!/bin/sh
set -e

trap 'kill 0' EXIT


nnice='nice -n +19'
nnice=


get_filament_limits() {
python3 - "$@" <<'PYTHON'
import sys
import numpy
import bin_density
import io_util

path = sys.argv[1]
box_size = float(sys.argv[2])
fil_id = int(sys.argv[3])
segment = int(sys.argv[4])

ngrid, filaments = io_util.read_NDskl_ascii(path)
coord_fac = box_size / ngrid
filament = filaments[fil_id]
filament *= coord_fac

def get_filament_extent(box_size, filament, radius, segment):
	assert segment < len(filament) - 1
	rmin = numpy.full(3, numpy.inf, dtype='f8')
	rmax = numpy.full_like(rmin, -numpy.inf)
	# use first filament point as a reference to handle periodic wrapping of
	# coordinates
	p0 = filament[0]
	it_range = numpy.array([-radius, radius])
	for p_orig in filament[segment:segment + 1]:
		p = bin_density.dist_periodic_wrap_vec(box_size, p_orig - p0)
		for i in range(3):
			for d in it_range:
				x = p[i] + d
				if x < rmin[i]:
					rmin[i] = x
				if x > rmax[i]:
					rmax[i] = x
	# transform back to original coordinate system
	rmin += p0
	rmax += p0
	for i in range(3):
		if rmax[i] >= box_size:
			rmin[i] -= box_size
			rmax[i] -= box_size
	return rmin, rmax

rmin, rmax = get_filament_extent(box_size, filament, box_size / 100, segment)
for imin, imax in zip(rmin, rmax):
	print(f'{imin:.12f}', f'{imax:.12f}', end=' ')
PYTHON
}


get_filament_vec() {
python3 - "$@" <<'PYTHON'
import sys
import numpy
import io_util

path = sys.argv[1]
box_size = float(sys.argv[2])
fil_id = int(sys.argv[3])
segment = int(sys.argv[4])

ngrid, filaments = io_util.read_NDskl_ascii(path)
coord_fac = box_size / ngrid
fil = filaments[fil_id]
fil *= coord_fac
assert segment < len(fil) - 1
dp = fil[segment + 1] - fil[segment]
for i in range(3):
	print(f'{dp[i]:.12f}', end=' ')
print(f'{numpy.linalg.norm(dp):.12f}', end=' ')
PYTHON
}


get_2d_vec() {
python3 - "$@" <<'PYTHON'
import sys
import math

angle_degree = float(sys.argv[1])
angle = math.radians(angle_degree)

print(f'{math.cos(angle):.12f}', end=' ')
print(f'{math.sin(angle):.12f}', end=' ')
PYTHON
}


get_rotation_vec() {
python3 - "$@" <<'PYTHON'
import sys
import numpy
from scipy.spatial.transform import Rotation

normal = numpy.array(list(map(float, sys.argv[1:4])))
north = numpy.array(list(map(float, sys.argv[4:7])))
filvec_2d = numpy.array(list(map(float, sys.argv[7:9])))
ratio = float(sys.argv[9])

west = numpy.cross(normal, north)
filvec = filvec_2d[0] * west + filvec_2d[1] * north
rotation_axis = numpy.cross(normal, filvec)
rotation_axis /= numpy.linalg.norm(rotation_axis)
rotation_angle = numpy.arccos(ratio) - numpy.pi / 2
rot = Rotation.from_rotvec(rotation_angle * rotation_axis)
result = rot.apply(normal)

for i in range(3):
	print(f'{result[i]:.12f}', end=' ')
PYTHON
}


zero_pad() {
	python3 -c \
		'import sys; print(f"{int(sys.argv[2]):0{int(sys.argv[1])}}", end="")' \
		"$@"
}


path=~/ptmp/r-workspace/fdm_axionic_p8640_b10000_m7e-23/
ndskl_path="$path/disperse/snapdir_008_downscale288.fits_c5e-07.up.NDskl.S005.a.NDskl"
echo '…'
~/arepo-tools/filament-straightness.py \
	--box-size 10000 --min-length 350 "$ndskl_path" \
	| tail -50
fil_id=52
segment=11

echo "Filament #$fil_id, segment #$segment"
lim=$(get_filament_limits "$ndskl_path" 10000 "$fil_id" "$segment")
xmin=$(echo "$lim" | awk '{print $1}')
xmax=$(echo "$lim" | awk '{print $2}')
ymin=$(echo "$lim" | awk '{print $3}')
ymax=$(echo "$lim" | awk '{print $4}')
zmin=$(echo "$lim" | awk '{print $5}')
zmax=$(echo "$lim" | awk '{print $6}')
echo "xmin=$xmin" "xmax=$xmax"
echo "ymin=$ymin" "ymax=$ymax"
echo "zmin=$zmin" "zmax=$zmax"
fil_vec=$(get_filament_vec "$ndskl_path" 10000 "$fil_id" "$segment")
normal="$fil_vec"
#normal=$(get_rotation_vec $fil_vec  \
#	0.94863586 0.         0.31637006  \
#	$(get_2d_vec "$(python3 -c 'print(360 - 32.3)')")  \
#	"$(python3 -c 'print(86 / 100)')"
#)
#normal=$(get_rotation_vec -0.24433346  0.53379808  0.80954355  \
#	0.33693088 -0.7360967   0.58705982  \
#	$(get_2d_vec "$(python3 -c 'print(360 - 16.1)')")  \
#	"$(python3 -c 'print(78.7 / 100)')"
#)
normal_x=$(echo "$normal" | awk '{print $1}')
normal_y=$(echo "$normal" | awk '{print $2}')
normal_z=$(echo "$normal" | awk '{print $3}')

output_name="$(zero_pad 4 "$fil_id")-$(zero_pad 3 "$segment")_off_cross.pdf"
$nnice ~/arepo-tools/yt-off-axis-plot.py \
	--slice-x "$xmin" "$xmax" \
	--slice-y "$ymin" "$ymax" \
	--slice-z "$zmin" "$zmax" \
	--normal "$normal_x" "$normal_y" "$normal_z" \
	--width 200 200 --depth 20 --buff-size 400 400 \
	--clim 5e-1 2e2 \
	--dpi 400 --output-path "$output_name" \
	-- "$path/snapdir_008/"
#--mark-filaments "$ndskl_path" --mark-single-filament "$fil_id" \
#--mark-single-segment "$segment" \
echo

