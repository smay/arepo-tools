#!/usr/bin/env python3
import argparse
import collections
import time
from pathlib import Path
import h5py
import numba
import numpy
import bin_density
import mpi_util
import read_snapshot
import read_snapshot.hdf5 as read_hdf5
import snapshot
from mpi_util import MPI, mpi_comm, mpi_rank, mpi_size, print0
from snapshot import Snapshot

parser = argparse.ArgumentParser(description='Calculate surface densities '
	'for fuzzy dark matter halos')
parser.add_argument('--r-max-factor', type=float, default=1.0,
	help='maximum radius to use for each halo, as a multiple of R_200 (default: '
		'%(default)g)')
parser.add_argument('--halo-id-offset', type=int, default=0,
	help='do not process halos with IDs smaller than the given ID '
		'(default: %(default)d)')
parser.add_argument('--fof-group-min-len', type=int, default=8,
	help='minimum FoF length to be processed (default: %(default)d)')
parser.add_argument('--fof-group-min-m200', type=float, default=0.,
	help='minimum FoF mass M_200 to be processed (default: %(default)d)')
parser.add_argument('--snap-chunk-mem-size', type=float,
	default=snapshot.CHUNK_MEM_SIZE_DEFAULT / 1024**2,
	help='maximum amount of memory to use for snapshot data per core, in MiB '
		'(default: %(default)g)')
#TODO
parser.add_argument('--load', type=Path,
	help='path to a previously-computed set of surface densities (default: '
		'<path>_surface_densities.hdf5')
parser.add_argument('--output-file-prefix', default='',
	help='prefix for the output file names')
parser.add_argument('path', type=Path, nargs='+',
	help='the path to the fof_tab catalog; any normal files are interpreted '
		'as single files, while all files in any given directory are '
		'interpreted as a single multi-file output')
args = parser.parse_args()


def main():
	mpi_util.default_print_args['flush'] = True
	if args.load:
		assert len(args.path) == 1, (
			'--load doesn’t really make sense with more than 1 path'
		)
	# process paths
	for path in args.path:
		process_path(path)
		print0()


def process_path(path):
	print0(path)
	fof_tab_files = read_snapshot.snapshot_files(path)
	# read fof_tab data
	fof_tab_data = None
	if mpi_rank == 0:
		fof_tab_data = read_hdf5.read_fof_tab(fof_tab_files, read_ids=False)
	# broadcast fof_tab data to other ranks
	if mpi_size > 1:
		fof_tab_data = mpi_comm.bcast(fof_tab_data, root=0)
	[fof_config, fof_param, fof_header], groups, _ = fof_tab_data
	print0(f'Processing fof_tab with {len(groups)} groups')
	if len(groups) == 0:
		print0('fof_tab contains no groups')
	# read snapshot metadata
	print0('Reading snapshot metadata…')
	snap = Snapshot(read_snapshot.output_path(path, fof_config, fof_param))
	snaptime = snap.header['Time']
	z = 1 / snaptime - 1
	print0(f'\tRedshift: z = {z}')
	cell_len = snap.box_size / snap.pmgrid
	# check existing surface density data
	halo_ids_todo = None
	num_chunks = None
	output_path = args.load if args.load else path.with_name(
		f'{args.output_file_prefix}{path.name}_surface_density.hdf5'
	)
	if mpi_rank == 0:
		if output_path.exists():
#			print0(f'Not recomputing existing data in file “{output_path}”')
#			with h5py.File(output_path, 'r') as of:
#				assert of['xi'].shape == of['count'].shape
#				assert of['xi'].shape == (len(groups), args.r_bins), (
#					f'“{output_path}” contains data with shape '
#					f'{of["xi"].shape} (expected '
#					f'{(len(groups), args.r_bins)})'
#				)
#				assert of.attrs['r_max_factor'] == args.r_max_factor
			pass
		else:
			print0(
				f'“{output_path}” not found as existing file, starting from '
				'scratch'
			)
			assert not args.load
			# initialize HDF5 datasets
			with h5py.File(output_path, 'w') as of:
#				of.create_dataset(
#					'xi', shape=(len(groups), args.r_bins),
#					dtype='f8', fillvalue=numpy.nan
#				)
#				of.create_dataset_like(
#					'count', of['xi'], dtype='u8', fillvalue=0
#				)
				of.attrs['r_max_factor'] = args.r_max_factor
		# determine which halos are both requested and missing
		with h5py.File(output_path, 'r') as of:
#			halos_not_done = (
#				numpy.all(numpy.isnan(of['xi'][...]), axis=1) &
#				~(groups.r200 < 2 * cell_len)
#			)
			halo_ids_todo = []
			for i in range(len(groups)):
				if (
					i >= args.halo_id_offset and
					groups[i].tot_len >= args.fof_group_min_len and
					groups[i].m200 >= args.fof_group_min_m200 #and
#					halos_not_done[i]
				):
					halo_ids_todo.append(i)
#			del halos_not_done
		print0(f'{len(halo_ids_todo)} halos left to process for file “{path}”')
		if len(halo_ids_todo) > 0:
			print0(f'(starting with halo {halo_ids_todo[0]})')
			#TODO chunk calculation
			max_num_halos = len(halo_ids_todo)
			chunk_num_halos = min(max_num_halos, len(halo_ids_todo))
			num_chunks = int(numpy.ceil(len(halo_ids_todo) / chunk_num_halos))
			print0(
				f'Can process at most {max_num_halos} halos at once; iteration '
				f'will be split into {num_chunks} chunks'
			)
			print0()
	num_chunks = mpi_comm.bcast(num_chunks, root=0)
	halo_ids_todo = mpi_comm.bcast(halo_ids_todo, root=0)
	if len(halo_ids_todo) == 0:
		return
	for num_iteration, halo_ids_chunk in enumerate(
		numpy.array_split(halo_ids_todo, num_chunks), start=1
	):
		print0(
			f'Starting grid iteration {num_iteration}/{num_chunks}'
		)
		process_chunk(snap, groups, halo_ids_chunk, output_path)
		print0()


def dist_pos_to_grid(box_size, pmgrid, r):
	cell_len = box_size / pmgrid
	dr = r / cell_len
	assert dr < pmgrid // 2
	return dr


def load_rho(snap, halo):
	cell_len = snap.box_size / snap.pmgrid
	assert not numpy.isnan(halo.r200)
	halo_center = halo.pos
	halo_center_g3 = numpy.floor(halo_center / cell_len + 0.1).astype('i8')
	dr200 = dist_pos_to_grid(
		snap.box_size, snap.pmgrid, halo.r200 * args.r_max_factor
	)
	di = min(int(dr200), snap.pmgrid // 2)
	sl = tuple(
		numpy.s_[
			#TODO
			i - di - snap.pmgrid if i + di + 1 > snap.pmgrid else i - di:
			i + di + 1 - snap.pmgrid if i + di + 1 > snap.pmgrid else i + di + 1
		] for i in halo_center_g3
	)
	rho = snap.read_snapshot('/FuzzyDM/PsiRe', data_slice=sl)**2
	rho += snap.read_snapshot('/FuzzyDM/PsiIm', data_slice=sl)**2
#	# filter out spherical region around halo center
#	ix, iy, iz = numpy.ogrid[numpy.index_exp[-di:di + 1] * 3]
#	ir2 = ix**2 + iy**2 + iz**2
#	rho[ir2 > di**2] = 0
	return rho


@numba.njit
def halo_surface_density(rho, rho_sum):
	assert rho.shape[:-1] == rho_sum.shape
	Nx, Ny, Nz = rho.shape
	for ix in range(Nx):
		for iy in range(Ny):
			for iz in range(Nz):
				rho_sum[ix, iy] += rho[ix, iy, iz]


def process_chunk(snap, groups, halo_ids, output_path):
	t0_tot = time.perf_counter()
	cell_len = snap.box_size / snap.pmgrid
	# iterate over halos: process using one rank per group
	halo_ids_idx = 0
	while halo_ids_idx < len(halo_ids):
		# assign groups to ranks
		thisrank_hid = None
		rank = 0
		while halo_ids_idx < len(halo_ids) and rank < mpi_size:
			if rank == mpi_rank:
				thisrank_hid = halo_ids[halo_ids_idx]
			rank += 1
			halo_ids_idx += 1
		halo_id_max = (
			halo_ids[halo_ids_idx] if halo_ids_idx < len(halo_ids)
			else halo_ids[halo_ids_idx - 1]
		)
		# process groups
		t0 = time.perf_counter()
		thisrank_halo = None
		rho_proj = None
		if thisrank_hid is not None:
			thisrank_halo = groups[thisrank_hid]
			print0(
				f'Starting surface density for GrNr = {thisrank_hid} to '
				f'{halo_id_max}'
			)
			# calculate surface density for all cells within cube of length r
			# for (up to) mpi_size halos in parallel
			rho = load_rho(snap, thisrank_halo)
			r = thisrank_halo.r200 * args.r_max_factor
			dr = dist_pos_to_grid(snap.box_size, snap.pmgrid, r)
			rho_proj = numpy.zeros_like(rho, shape=rho.shape[:-1])
			halo_surface_density(rho, rho_proj)
			rho_proj *= rho.shape[-1] * cell_len
		rho_proj_g = mpi_comm.gather(rho_proj, root=0)
		t1 = time.perf_counter()
		mpi_comm.Barrier()
		print0(f'\ttook {t1 - t0} sec.')
		# save results to file
		print0(f'\tSaving results to {output_path}…')
		rank_hids = mpi_comm.gather(thisrank_hid, root=0)
		if mpi_rank == 0:
			save_results(output_path, rank_hids[:rank], rho_proj_g[:rank])
	t1_tot = time.perf_counter()
	print0(f'Total surface density computation took {t1_tot - t0_tot} sec.')


def save_results(output_path, halo_ids, rho_proj):
	if mpi_rank == 0:
		with h5py.File(output_path, 'a') as of:
			for halo_id, halo_proj in zip(halo_ids, rho_proj):
				of[f'Halo{halo_id}'] = halo_proj


if __name__ == '__main__':
	main()
