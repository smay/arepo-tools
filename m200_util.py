from pathlib import Path
import numpy
import pandas
import read_snapshot
import read_snapshot.hdf5 as read_hdf5
import units_util

M200_DTYPE = numpy.dtype([
	('id',       'u8'),
	('fof_len',  'u8'),
	('m_fof_ms', 'f8'),
	('m200_ms',  'f8'),
	('r200_kpc', 'f8'),
	('cx_kpc',   'f8'),
	('cy_kpc',   'f8'),
	('cz_kpc',   'f8'),
])
M200_FILE_NAME = '{}_m200.csv'
VIRIAL_OVERDENSITY_FACTOR = 200


def m200_csv_path(path):
	return Path(M200_FILE_NAME.format(path))

def read_m200_data(
	path, fof_data=None, prefer='hdf5', m_fof_fallback=False, fof_voids=False,
	return_fof_groups=False
):
	assert prefer in ('hdf5', 'csv')
	# read HDF5 or CSV data
	csv_path = m200_csv_path(path)
	csv_present = csv_path.exists()
	hdf5_present = False
	if prefer == 'hdf5' or not csv_present:
		if fof_data is None:
			fof_tab_files = read_snapshot.snapshot_files(path)
			fof_data = read_hdf5.read_fof_tab(fof_tab_files, read_ids=False)
		[config, param, header], groups, *_ = fof_data
		hdf5_present = not numpy.any(numpy.isnan(groups.m200))
	use_hdf5 = (hdf5_present and prefer == 'hdf5') or not csv_present
	if use_hdf5:
		result_len = len(groups)
	else:
		csv_data = pandas.read_csv(csv_path)
		result_len = len(csv_data)
	# build result DataFrame
	m200_data = pandas.DataFrame(numpy.zeros(result_len, dtype=M200_DTYPE))
	for col, [dtype, _] in M200_DTYPE.fields.items():
		if 'float' in dtype.name:
			m200_data[col] = numpy.nan
	if use_hdf5:
		units = units_util.get_units(param)
		m200_data['id'] = numpy.arange(len(groups))
		m200_data['fof_len'] = groups.tot_len
		m200_data['m_fof_ms'] = groups.tot_mass * units.mass_in_solar_mass
		m200_data['m200_ms'] = groups.m200 * units.mass_in_solar_mass
		m200_data['r200_kpc'] = groups.r200 * units.length_in_kpc
		if m_fof_fallback and (
			pandas.isna(m200_data['m200_ms']).any() or
			pandas.isna(m200_data['r200_kpc']).any()
		):
			m200_data['m200_ms'] = m200_data['m_fof_ms']
			#TODO: doesn’t make sense for particles
			volumes = (
				m200_data['fof_len'] * (header['BoxSize'] / config['PMGRID'])**3
			)
			m200_data['r200_kpc'] = (
				(3 / (4 * numpy.pi) * volumes)**(1/3) * units.length_in_kpc
			)
		for idim, sdim in enumerate(['cx_kpc', 'cy_kpc', 'cz_kpc']):
			assert sdim in M200_DTYPE.names
			m200_data[sdim] = groups.pos[:, idim] * units.length_in_kpc
		assert not pandas.isna(m200_data).any(axis=None)
	else:
		for col in M200_DTYPE.names:
			if col in csv_data:
				m200_data[col] = csv_data[col]
	assert (m200_data['fof_len'] > 0).all(axis=None)
	if fof_voids:
		assert (m200_data['m_fof_ms'] >= 0).all(axis=None)
	else:
		assert (m200_data['m_fof_ms'] > 0).all(axis=None)
	if return_fof_groups:
		return m200_data, groups
	else:
		return m200_data

def save_m200_csv_data(path, m200_data, overwrite=False):
    csv_path = m200_csv_path(path)
    if not csv_path.exists() or overwrite:
        msg = f'Writing data to {csv_path}…\n'
        m200_data.to_csv(csv_path, index=False)
        wrote_csv = True
    else:
        msg = f'{csv_path} exists, not writing data\n'
        wrote_csv = False
    return wrote_csv, msg

def r200_from_m200(rho_avg_global, m200):
	return (
		m200 / (4/3 * numpy.pi * VIRIAL_OVERDENSITY_FACTOR * rho_avg_global)
	)**(1/3)
