#!/usr/bin/env python3
import argparse
from pathlib import Path
import h5py
import numpy


NTYPES = 6

parser = argparse.ArgumentParser(description='Merge N-body snapshot files')
parser.add_argument('--output-path', type=Path, nargs='+',
	help='paths to write the outputs to')
parser.add_argument('path', type=Path, nargs='+',
	help='the paths to the snapshot files; any normal files are interpreted '
		'as single-file snapshots, while all files in any given directory '
		'are interpreted as a single multi-file snapshot')


def main():
	if not args.output_path:
		args.output_path = [None] * len(args.path)
	assert len(args.path) == len(args.output_path)
	# process paths
	for path_args in zip(args.path, args.output_path):
		process_path(*path_args)


def process_path(path, output_path):
	print(path)
	paths = sorted(path.iterdir()) if path.is_dir() else [path]
	pdata = {}
	for ip in paths:
		with h5py.File(ip, 'r') as input_file:
			for ptype in range(NTYPES):
				if f'/PartType{ptype}' in input_file:
					if ptype not in pdata:
						pdata[ptype] = {}
					gdata = pdata[ptype]
					g = input_file[f'/PartType{ptype}']
					for ds in g:
						if ds not in gdata:
							gdata[ds] = []
						gdata[ds].append(g[ds][...])
	numpart_total = {}
	for ptype, ptype_data in pdata.items():
		for ds in ptype_data:
			ptype_data[ds] = numpy.concatenate(ptype_data[ds])
			numpart_total[ptype] = len(ptype_data[ds])
	assert all(
		len(d) == numpart_total[ptype]
		for ptype in pdata for d in pdata[ptype].values()
	)
	with h5py.File(output_path, 'a') as output_file:
		header = output_file['/Header'].attrs
		num_bits = 8 * header['NumPart_Total'][...].itemsize
		header['NumFilesPerSnapshot'] = oftype(
			header['NumFilesPerSnapshot'], 1
		)
		for ptype, ptype_data in pdata.items():
			# write header attributes
			if f'/PartType{ptype}' in output_file:
				del output_file[f'/PartType{ptype}']
			output_file.create_group(f'/PartType{ptype}')
			assert numpart_total[ptype] >> num_bits == 0
			header['NumPart_ThisFile'][ptype] = oftype(
				header['NumPart_ThisFile'][ptype], numpart_total[ptype]
			)
			header['NumPart_Total'][ptype] = oftype(
				header['NumPart_Total'][ptype], numpart_total[ptype]
			)
			header['NumPart_Total_HighWord'][ptype] = oftype(
				header['NumPart_Total_HighWord'][ptype], 0
			)
			# write data
			g = output_file[f'/PartType{ptype}']
			for ds in ptype_data:
				g[ds] = ptype_data[ds]

def oftype(obj, x): return numpy.array(x, dtype=obj.dtype)[()]


if __name__ == '__main__':
	args = parser.parse_args()
	main()

