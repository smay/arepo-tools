#!/usr/bin/env python3
# standard modules
import argparse
import contextlib
import ctypes
import itertools
import re
import tempfile
import warnings
from pathlib import Path
# non-standard modules
import astropy.constants
import astropy.cosmology
import astropy.units
import matplotlib.colors
import matplotlib.offsetbox
import matplotlib.transforms
import matplotlib.pyplot as plt
import numpy
import pandas
import scipy.integrate
import scipy.interpolate
# own modules
import io_util
import matplotlib_settings
import read_snapshot
from snapshot import Snapshot


INPUTSPEC_PATH = Path.home() / 'arepo-tools' / 'ngenic_inputspec_z127.txt'
NRSRC_LIB_PATH = Path.home() / 'arepo' / 'runs' / 'common' / 'nrsrc.so'
KPC_IN_CM = 3.085678e21
MPC_IN_CM = KPC_IN_CM * 1e3
OMEGA_M0_DEFAULT = 0.3
KBIN_START_IDX = 100
PLOT_OPT_SHOT_NOISE = {
	'ls': ':', 'linewidth': 0.2, 'color': 'gray',
}

parser = argparse.ArgumentParser(description='Plot power spectra of different '
	'simulations in one plot')
parser.add_argument('--BINS_PS', type=int, default=2000,
	help='the number of bins in wave number in the input file (default: '
		'%(default)d)')
parser.add_argument('--plot-bins', type=int, default=200,
	help='the number of bins in wave number to use for output/plotting; the '
		'input data will be rebinned to this number of bins (default: '
		'%(default)d)')
parser.add_argument('--snapshot-format',
	choices=('gadget', 'hdf5', 'npy', 'raw'), default='hdf5',
	help='the output snapshot format (default: hdf5)')
#TODO
parser.add_argument('--ptype', type=int, nargs='+',
	help='which particle type to plot the power spectrum for (default: all)')
parser.add_argument('--plot-vars', choices=('P', 'Delta2'), nargs='+',
	default=['P'],
	help='plot Δ² ~ k³ P(k) in addition to P(k)')
parser.add_argument('--mark-scale', nargs='+', default=[],
	choices=('jeans-fuzzy', 'jeans-fuzzy-eq', 'jeans-iso', 'manual'),
	help='mark FDM Jeans length in plots (legacy; use --mark-scale)')
parser.add_argument('--mark-scale-value', nargs='+', type=float,
	help='values to use for --mark-scale')
parser.add_argument('--mark-scale-label', nargs='+',
	help='label to add for each instance of --mark-scale')
parser.add_argument('--jeans-length', action='store_true',
	help='mark FDM Jeans length in plots (legacy; use --mark-scale)')
parser.add_argument('--jeans-length-eq', type=float,
	help='mark FDM Jeans length at z = 3300 for the given FDM mass (legacy; '
		'use  --mark-scale)')
parser.add_argument('--redshift-colorbar', action='store_true',
	help='assign line colors according to redshift and add a colorbar')
parser.add_argument('--redshift-colorbar-switch', type=float,
	help='switch between two colormaps at the specified redshift')
parser.add_argument('--redshift-colorbar-range', nargs=2, type=float,
	help='range for redshift colorbar')
parser.add_argument('--equal-time', action='store_true',
	help='whether all snapshots correspond to the same time (redshift)')
parser.add_argument('--equal-mass', action='store_true',
	help='assume that all FDM inputs use the same particle mass')
parser.add_argument('--plot-beyond-nyquist', action='store_true',
	help='keep data beyond the Nyquist frequency at high redshift')
parser.add_argument('--plot-scale',
	choices=('linear', 'loglog'), default='loglog',
	help='the plot’s scale (default: %(default)s)')
parser.add_argument('--plot-scale-relative',
	choices=('linear', 'loglog', 'logxliny', 'linxlogy'),
	help='the relative plot’s scale (default: the value of --plot-scale)')
parser.add_argument('--plot-xlim', type=float, nargs=2,
	help='the plot’s k range')
parser.add_argument('--plot-ylim', type=float, nargs=2,
	help='the plot’s P(k) range')
parser.add_argument('--plot-ylim-relative', type=float, nargs=2,
	help='the plot’s P(k)/P(k)_ref range')
parser.add_argument('--no-title', action='store_true',
	help='omit plot title')
parser.add_argument('--no-legend', action='store_true',
	help='omit legend')
parser.add_argument('--no-axlabels', action='append', default=[],
	choices=('left', 'right', 'top', 'bottom'),
	help='omit axis labels (including ticks) on the given sides of the plot')
parser.add_argument('--no-axlabels-mode', nargs='+', default=[],
	choices=('left-hide', 'right-hide', 'top-hide', 'bottom-hide',
		'left-collapse', 'right-collapse', 'top-collapse', 'bottom-collapse'),
	help='how to hide axis labels (not ticks!) on the given sides of the plot; '
		'see --no-axlabels (default: *-hide)')
parser.add_argument('--aux-legend', action='store_true',
	help='place auxiliary labels (shot noise, Jeans length) in separate legend')
parser.add_argument('--legend-ncols', type=int, default=1,
	help='number of columns for legend (default: %(default)d)')
parser.add_argument('--legend-loc', default='best',
	help='legend location')
parser.add_argument('--legend-box-size', action='store_true',
	help='show box sizes in legend')
parser.add_argument('--legend-fontsize',
	help='legend font size')
parser.add_argument('--styles', nargs='+',
	help='styles for the given lines (see matplotlib.style.available)')
parser.add_argument('--line-styles', nargs='+',
	help='line styles for the given plots')
parser.add_argument('--line-width', type=float,
	help='line width')
parser.add_argument('--line-color-maps', nargs='+',
	help='colormaps to create the color prop_cycle from for the given lines '
		'(see matplotlib.pyplot.colormaps)')
parser.add_argument('--line-color-maps-length', type=int, default=4,
	help='length of the color cycle for --line-color-maps')
parser.add_argument('--labels', nargs='+',
	help='legend labels (used instead of automatic legend)')
parser.add_argument('--legend-entries', nargs='+',
	help='legend line colors/styles (used instead of automatic legend); '
		'number of values determines number of legend entries (must be used '
		'with --labels)')
parser.add_argument('--with-relative', type=Path,
	help='plot power spectra relative to the given snapshot in a separate '
		'panel')
parser.add_argument('--only-relative', type=Path,
	help='plot power spectra relative to the given snapshot')
parser.add_argument('--panel-ratio', type=float, default=1/3,
	help='ratio of second (relative plot) row/panel to the first (default: '
		'%(default)g')
parser.add_argument('--relative-label-override',
	help='label on the y-axis for the relative plot panel')
parser.add_argument('--relative-label', default=r'P(k)_0',
	help='denominator for the label on the y-axis for the relative plot panel')
parser.add_argument('--relative-label-fraction', choices=('frac', '/'),
	default='frac',
	help='notation to use for the fraction for the relative plot panel')
parser.add_argument('--relative-pos', type=int, default=0,
	help='where to insert the reference power spectrum')
parser.add_argument('--relative-exclude', type=int, nargs='+', default=[],
	help='don’t include the given paths in the relative plot')
parser.add_argument('-z', type=float, nargs='+',
	help='list of redshift values for the given input paths (used for paths '
		'which represent text files)')
parser.add_argument('--powerspec-txt-format', choices=('cdm', 'fdm'),
	help='assume that all given powerspec*.txt files are in the given format '
		'(cdm: standard AREPO format; fdm: format emitted by FDM code)')
parser.add_argument('--include-missing', action='store_true',
	help='include misssing data in legend instead of raising an error')
parser.add_argument('--time-digits', type=int, default=4,
	help='how many decimal digits to use for the time in the file name and '
		'plot title (default: %(default)d)')
parser.add_argument('--plot-file-prefix', default='',
	help='prefix for the output file name')
parser.add_argument('--plot-file-name', default='',
	help='output file name')
parser.add_argument('--plot-format', default='png',
	help='the file format of the generated plot (default: %(default)s)')
parser.add_argument('--dpi', type=int, default=200,
	help='the resolution of the generated plot (default: %(default)d)')
parser.add_argument('--figsize', type=float, nargs=2,
	help='the figure size in inches')
parser.add_argument('--figaspect', type=float,
	help='the aspect ratio height/width of the figure')
parser.add_argument('--axionic-cdm-matterpower', nargs='+', type=Path,
	default=[Path.home() / 'arepo' / 'runs' / 'common' /
		'axioncamb_matterpower_omc0.1225_omnu0.dat'],
	help='for each axionCAMB matter power spectrum in path: the corresponding '
		'CDM reference power spectrum to use (if only one is given: always '
		'use the same reference spectrum)')
parser.add_argument('path', nargs='+', type=Path,
	help='the paths to the snapshot or power spectrum data files; any normal '
		'files are interpreted as single-file snapshots, while all files in '
		'any given directory are interpreted as a single multi-file snapshot')
args = parser.parse_args()


def float_to_scientific_notation(x, digits=None, integer=True):
	sign = '-' if x < 0 else ''
	exponent = int(numpy.floor(numpy.log10(numpy.abs(x))))
	significand = x / 10**exponent
	if numpy.isclose(significand, 1):
		return fr'{sign} 10^{{{exponent}}}'
	else:
		if integer and numpy.isclose(significand / round(significand), 1):
			significand = round(significand)
		if digits is None:
			significand = f'{significand}'
		else:
			significand = f'{significand:.{digits}f}'
		return fr'{sign}{significand} \times 10^{{{exponent}}}'

def format_z(z):
	return f'{round(z, 1):.1f}'

def set_ax_scale(ax, relative_ax):
	if relative_ax and args.plot_scale_relative:
		if args.plot_scale_relative == 'logxliny':
			assert len(axes) == 1 or args.plot_scale == 'loglog'
			ax.set_xscale('log')
		elif args.plot_scale_relative == 'linxlogy':
			assert len(axes) == 1 or args.plot_scale == 'linear'
			ax.set_yscale('log')
		elif args.plot_scale_relative == 'loglog':
			assert len(axes) == 1 or args.plot_scale == 'loglog'
			ax.set_xscale('log')
			ax.set_yscale('log')
	else:
		if args.plot_scale == 'loglog':
			ax.set_xscale('log')
			ax.set_yscale('log')

def growth_integrand(cosmo, a):
	if a == 0: return 0
	z = 1 / a - 1
	# efunc = sqrt(Ω_m0 / a³ + (1 - Ω_m0 - Ω_Λ) / a² + Ω_Λ)
	return 1 / (a * cosmo.efunc(z))**3

def growth(cosmo, a):
	z = 1 / a - 1
	# efunc = sqrt(Ω_m0 / a³ + (1 - Ω_m0 - Ω_Λ) / a² + Ω_Λ)
	hubble_a = cosmo.efunc(z)
	def integrand(a): return growth_integrand(cosmo, a)
	result, err = scipy.integrate.quad(integrand, 0, a, epsrel=1e-8)
	return hubble_a * result

def growth_factor(cosmo, a_start, a_end):
	return growth(cosmo, a_end) / growth(cosmo, a_start)

def powerspec_from_snap_num(path_parent, snapshot_fdm, snap_num, a):
	ps_path_snap = path_parent / f'powerspec_{snap_num:03}.txt'
	ps_path_combined = path_parent / 'powerspec.txt'
	if ps_path_snap.exists():
		ps_path = ps_path_snap
		if snapshot_fdm:
			ps = io_util.read_fdm_powerspec_csv(ps_path_snap)
		else:
			ps = io_util.read_arepo_powerspec_file(ps_path_snap)
	elif ps_path_combined.exists():
		ps_path = ps_path_combined
		ps = io_util.read_fdm_powerspec_csv(ps_path_combined)
	else:
		raise IOError(f'Neither {ps_path_snap} nor {ps_path_combined} exist')
	return ps_path, ps

def TopHatSigma2(R, PowerSpec):
	nrsrc = ctypes.cdll.LoadLibrary(NRSRC_LIB_PATH)
	# double qromb(double (*func)(double), double a, double b)
	qrombfunc = ctypes.CFUNCTYPE(ctypes.c_double, ctypes.c_double)
	nrsrc.qromb.argtypes = [qrombfunc, ctypes.c_double, ctypes.c_double]
	nrsrc.qromb.restype = ctypes.c_double
	sigma2_int = sigma2_int_func(R, PowerSpec)
	# note: 500/R ≈ ∞ (see N-GenIC source code)
	return nrsrc.qromb(qrombfunc(sigma2_int), 0, 500.0 / R)

def sigma2_int_func(R, PowerSpec):
	def sigma2_int(k):
		kr = R * k
		if kr < 1e-8:
			return 0
		w = 3 * (numpy.sin(kr) / kr**3 - numpy.cos(kr) / kr**2)
		x = 4 * numpy.pi * k**2 * w**2 * PowerSpec(k)
		return x
	return sigma2_int

def PowerSpec_Efstathiou_func(Norm, ShapeGamma):
	def PowerSpec(k):
		AA = 6.4 / ShapeGamma
		BB = 3.0 / ShapeGamma
		CC = 1.7 / ShapeGamma
		nu = 1.13
		return (
			Norm *
			k / (1 + (AA * k + (BB * k)**1.5 + CC**2 * k**2)**nu)**(2 / nu)
		)
	return PowerSpec

#TODO: fix this – can’t simply scale input spectrum in FDM case!
def linear_theory_axionic(path_axionic, path_cdmic, ic_param):
	# calculate ratio to reference run without axions and apply this as
	# a factor on top of N-GenIC’s Efstathiou spectrum
	camb_axion = io_util.read_axioncamb_matterpower(path_axionic)
	camb_cdm = io_util.read_axioncamb_matterpower(path_cdmic)
	assert (camb_axion.k == camb_cdm.k).all()
	ratio = camb_axion.P / camb_cdm.P
	# Efstathiou spectrum as calculated by N-GenIC
	# TODO: cosmological parameters
	ShapeGamma = 0.21 #float(ic_param['ShapeGamma'])
	Sigma8 = 0.9 #float(ic_param['Sigma8'])
	R8 = 8.0
	res = TopHatSigma2(R8, PowerSpec_Efstathiou_func(1.0, ShapeGamma))
	Norm = Sigma8**2 / res
	PowerSpec_Efstathiou = PowerSpec_Efstathiou_func(Norm, ShapeGamma)
	# apply axion/CDM factor
	# note: CAMB output is in Mpc/h
	k = camb_cdm.k
	# PowerSpec_Efstathiou is computed identically to N-GenIC’s code
	P = ratio * PowerSpec_Efstathiou(k)
	axionic_spec = pandas.DataFrame({
		# TODO: factor 2π² vs. 4π (4π is used in N-GenIC code)?
		'k_Mpc': k, 'P_Mpc3': P, 'Delta2': 2 * numpy.pi**2 * k**3 * P,
	})
	return axionic_spec

def bin_avg(arr, ModeCount):
	num_modes = numpy.sum(ModeCount)
	return numpy.sum(arr * ModeCount) / num_modes if num_modes > 0 else 0

def rebin(
	snapshot_fdm, piece, BoxSize, Ngrid, K, P, Delta2, ModeCount, FoldFac=16
):
	kmin = 2 * numpy.pi / BoxSize
	#TODO: where do these factors (/2.0/1.5, /2.0/1.65) for kmax/kmin come from?
	kmax = 2 * numpy.pi / BoxSize * Ngrid/2.0/1.5 * FoldFac**piece
	if piece > 0:
#		kmin = 2 * numpy.pi / BoxSize * Ngrid/2.0/1.65 * FoldFac**(piece - 1)
		kmin = 2 * numpy.pi / BoxSize * Ngrid/2.0/1.5 * FoldFac**(piece - 1)
	print('\trebin: Kmin =', kmin, '  Kmax = ', kmax)
	MinDlogK = (
		(numpy.log10(numpy.max(K)) - numpy.log10(numpy.min(K))) /
		args.plot_bins
	)
	k_list = []
	P_list = []
	Delta2_list = []
	count_list = []
	ind = [0]
	assert len(K) == len(P) == len(Delta2) == len(ModeCount) == args.BINS_PS
	for istart in range(1, len(K) + 1):
		K_ind = K[ind]
		count = ModeCount[ind]
		deltak = (
			numpy.log10(numpy.max(K_ind)) - numpy.log10(numpy.min(K_ind))
		)
		# continue combining bins until logarithmic bin width reaches MinDlogK
		# exception: keep sparsely-spaced bins on the largest scales
		#            (< 2 * kmin) intact
		if deltak >= MinDlogK or (numpy.max(K_ind) < 2 * kmin and piece == 0):
			kk = numpy.exp(bin_avg(numpy.log(K_ind), count))
			p = bin_avg(P[ind], count)
			d2 = bin_avg(Delta2[ind], count)
			if d2 > 0:
				k_list.append(kk)
				P_list.append(p)
				Delta2_list.append(d2)
				count_list.append(numpy.sum(count))
			ind = [istart]
		else:
			ind.append(istart)
	k_arr = numpy.array(k_list)
	ind = numpy.ones_like(k_arr, dtype=bool)
	if not snapshot_fdm:
		ind = (k_arr >= kmin) & (k_arr <= kmax)
	return k_arr[ind], numpy.array(P_list)[ind], numpy.array(Delta2_list)[ind]

# argument processing
assert 1 <= len(args.plot_vars) <= 2
args.legend_loc = args.legend_loc.replace('_', ' ')
if args.jeans_length:
	args.mark_scale.append('jeans-fuzzy')
	args.mark_scale_value.append(numpy.nan)
if args.jeans_length_eq:
	args.mark_scale.append('jeans-fuzzy-eq')
	args.mark_scale_value.append(args.jeans_length_eq)
if not args.mark_scale_label:
	args.mark_scale_label = [''] * len(args.mark_scale)
assert (
	len(args.mark_scale) == len(args.mark_scale_value) ==
	len(args.mark_scale_label)
)
mark_scale = [
	(k, v, l) for k, v, l in zip(
		args.mark_scale, args.mark_scale_value, args.mark_scale_label
	)
]
# make sure that mirrored ticks at the top are disabled (→ matplotlib_settings)
# to prevent duplicate/wrong ticks with top secondary_xaxis
plt.rc('xtick', top=False)
plt.rc('figure.constrained_layout', hspace=0)
if args.figsize:
	plt.rc('figure', figsize=args.figsize)
# set up figure
figsize = plt.rcParams['figure.figsize']
if args.figaspect:
	figsize=(figsize[0], figsize[0] * args.figaspect)
if args.legend_fontsize:
	try:
		args.legend_fontsize = float(args.legend_fontsize)
	except ValueError: pass
	plt.rc('legend', fontsize=args.legend_fontsize)
if args.with_relative:
	gridspec_kw = {
		'wspace': 0.06,
		'hspace': 0,
		'height_ratios': [1, args.panel_ratio]
	}
	fig, axes = plt.subplots(
		2, 1, sharex='col', sharey='row', figsize=(
			figsize[0], figsize[1] * 1.05 + gridspec_kw['height_ratios'][1]
		), gridspec_kw=gridspec_kw
	)
else:
	fig = plt.figure(figsize=figsize)
	axes = [plt.gca()]
# set up axes
VAR_YLABEL = {
	'P': r'$P(k)$ / $h^{-3}\,\mathrm{Mpc}^3$ (comoving)',
	'Delta2': r'$\Delta(k)^2 = \frac{1}{2\pi^2} k^3 P(k)$',
}
math_style = r'\textstyle' if args.plot_format == 'pgf' else ''
for key, symbol in [
	('P-relative', 'P(k)'), ('Delta2-relative', r'\Delta(k)^2')
]:
	if args.relative_label_override is not None:
		rylabel = args.relative_label_override
	elif args.relative_label_fraction == 'frac':
		rylabel = (
			f'${math_style}' r'\frac{' f'{symbol}' '}{'
			fr'{args.relative_label}' r'}$'
		)
	elif args.relative_label_fraction == '/':
		rylabel = fr'${symbol} / {args.relative_label}$'
	else: assert False
	VAR_YLABEL[key] = rylabel
relative = args.only_relative or args.with_relative
axes_secondvar = []
for ax in axes:
	# disable minor tick labels to save horizontal space
	ax.tick_params(which='minor', labelleft=False, labelright=False)
	# prevent top/bottom x ticks from overlapping into panels above/below
	if ax is not axes[0]:
		ax.tick_params(which='both', top=False)
	if ax is not axes[-1]:
		ax.tick_params(which='both', bottom=False)
	if ax is axes[0]:
		with numpy.errstate(divide='ignore'):
			x2 = ax.secondary_xaxis('top', functions=(
				lambda k: 2 * numpy.pi / k,
				lambda r: 2 * numpy.pi / r,
			))
		if not (
			'top' in args.no_axlabels and
			'top-collapse' in args.no_axlabels_mode
		):
			x2.set_xlabel(r'$r$ / $h^{-1}\,\mathrm{Mpc}$ (comoving)')
		if 'top' in args.no_axlabels:
			x2.xaxis.label.set_color('white')
			x2.xaxis.label.set_alpha(0)
	if ax is axes[-1]:
		if not (
			'bottom' in args.no_axlabels and
			'bottom-collapse' in args.no_axlabels_mode
		):
			ax.set_xlabel(r'$k$ / $h\,\mathrm{Mpc}^{-1}$ (comoving)')
		if 'bottom' in args.no_axlabels:
			ax.xaxis.label.set_color('white')
			ax.xaxis.label.set_alpha(0)
	if not (
		'left' in args.no_axlabels and 'left-collapse' in args.no_axlabels_mode
	):
		if args.only_relative or (len(axes) > 1 and ax is axes[-1]):
			ax.set_ylabel(VAR_YLABEL[f'{args.plot_vars[0]}-relative'])
		else:
			ax.set_ylabel(VAR_YLABEL[args.plot_vars[0]])
		if 'left' in args.no_axlabels:
			ax.yaxis.label.set_color('white')
			ax.yaxis.label.set_alpha(0)
	set_ax_scale(ax, relative and ax is axes[-1])
	if len(args.plot_vars) > 1:
		ax_secondvar = ax.twinx()
		if not (
			'right' in args.no_axlabels and
			'right-collapse' in args.no_axlabels_mode
		):
			if args.only_relative or (len(axes) > 1 and ax is axes[-1]):
				ax_secondvar.set_ylabel(
					VAR_YLABEL[f'{args.plot_vars[1]}-relative']
				)
			else:
				ax_secondvar.set_ylabel(VAR_YLABEL[args.plot_vars[1]])
		if 'right' in args.no_axlabels:
			ax_secondvar.yaxis.label.set_color('white')
			ax_secondvar.yaxis.label.set_alpha(0)
		set_ax_scale(ax_secondvar, relative and ax is axes[-1])
		axes_secondvar.append(ax_secondvar)
	# disable tick numbering as specified by --no-axlabels
	if 'bottom' in args.no_axlabels:
		for t in ax.xaxis.get_ticklabels():
			t.set_color('white')
			t.set_alpha(0)
	if 'top' in args.no_axlabels:
		for t in x2.xaxis.get_ticklabels():
			t.set_color('white')
			t.set_alpha(0)
	if 'left' in args.no_axlabels:
		for t in ax.yaxis.get_ticklabels():
			t.set_color('white')
			t.set_alpha(0)
	if 'right' in args.no_axlabels and len(args.plot_vars) > 1:
		for t in ax_secondvar.yaxis.get_ticklabels():
			t.set_color('white')
			t.set_alpha(0)
assert len(args.plot_vars) == 1 or len(axes) == len(axes_secondvar)

k_min = p_min = numpy.inf
k_max = p_max = 0
box_size = None
# setup for relative plotting
plot_var_ref = lambda K: 1
if relative:
	args.path.insert(args.relative_pos, relative)
	args.path = [relative] + args.path
# argument checks
assert not args.labels or (
	len(args.path) - bool(relative) == len(args.labels) or (
		args.legend_entries and len(args.labels) == sum(
			not x.startswith('EMPTY') for x in args.legend_entries
		)
	)
)
assert not args.line_styles or len(args.line_styles) in (
	1, len(args.path) - bool(relative)
)
assert not args.z or len(args.z) in (1, len(args.path) - bool(relative))
# process snapshots
print('INPUTSPEC_PATH =', INPUTSPEC_PATH)
print()
z_check = None
axionic_idx = 0
jeans_length_marker = False
jeans_length_iso_marker = False
shot_noise_label = False
plot_color_idx_cdm = 0
plot_color_idx_other = 0
for path_idx, path in enumerate(args.path):
	reference = relative and path_idx == 0
	path_idx -= bool(relative)
	if reference:
		print(f'Reference for relative plot: {path}')
	else:
		print(path)
	linear_theory = path.name.startswith('linear_')
	snap_path = None
	snapshot_fdm = False
	ngenic_inputspec = False
	axionic = False
	# path is an axionCAMB matter power spectrum
	if path.suffix == '.dat':
		axionic = True
		z = args.z[0] if len(args.z) == 1 else args.z[path_idx]
		a = 1 / (z + 1)
		# CAMB output is in Mpc/h
		UnitLength_in_cm = MPC_IN_CM
		# TODO: cosmological parameters
		Omega_m0 = OMEGA_M0_DEFAULT
	# path is in N-GenIC input spectrum format
	elif path.name == 'camb_inputspec.txt':
		ngenic_inputspec = True
		z = args.z[0] if len(args.z) == 1 else args.z[path_idx]
		a = 1 / (z + 1)
		# CAMB output is in Mpc/h
		UnitLength_in_cm = MPC_IN_CM
		# TODO: cosmological parameters
		Omega_m0 = OMEGA_M0_DEFAULT
	# path is powerspec.txt or powerspec_*.txt
	elif path.suffix == '.txt':
		snap_num_match = re.search('_(\d+).txt$', path.name)
		if snap_num_match:
			# find associated snapshot
			snap_num = int(snap_num_match.group(1))
			snap_path_file = path.parent / f'snap_{snap_num:03}.hdf5'
			snap_path_dir = path.parent / f'snapdir_{snap_num:03}'
			snap_path = (
				snap_path_file if snap_path_file.exists() else snap_path_dir
			)
			#TODO
#			if '/r-workspace/fdmb_axionic_p8640-256_b10000_m7e-23_123456/' in f'{snap_path.absolute()}':
#				snap_num_match = False
#				snap_path = None
#			assert snap_path.exists(), snap_path
		if not snap_num_match or not snap_path.exists():
			snap_path = None
			run_type, N, box_size, mass = read_snapshot.parse_run_name(path)
			assert run_type, f'can’t determine run characteristics for {path}'
			snapshot_fdm = run_type == 'fdm'
			z = args.z[0] if len(args.z) == 1 else args.z[path_idx]
			a = 1 / (z + 1)
			numpart = N**3
			numpart_per_dim = N
			pmgrid = N
			# TODO: units
			UnitLength_in_cm = KPC_IN_CM
			# TODO: cosmological parameters
			Omega_m0 = OMEGA_M0_DEFAULT
	# path is linear theory
	elif linear_theory:
		z = float(path.name.split('_')[1])
		a = 1 / (z + 1)
		UnitLength_in_cm = KPC_IN_CM
		# TODO: cosmological parameters
		Omega_m0 = OMEGA_M0_DEFAULT
	# path is a snapshot
	else:
		snap_path = path
	if snap_path:
		# read metadata and determine if this is a CDM or FDM snapshot
		snap = Snapshot(snap_path, snapshot_format=args.snapshot_format)
		pmgrid = snap.pmgrid
		numpart = read_snapshot.numpart_total(snap.header, ptype=1)
		numpart_per_dim = round(numpart**(1/3))
		a = snap.header['Time']
		z = 1 / a - 1
		UnitLength_in_cm = snap.header['UnitLength_in_cm']
		assert box_size is None or box_size == snap.box_size, (
			box_size, snap.box_size
		)
		box_size = snap.box_size
		Omega_m0 = snap.header['Omega0']
		assert bool(snap.param['ComovingIntegrationOn'])
		snap_num = read_snapshot.snapshot_number(path, snap.param)
		snapshot_fdm = args.snapshot_format in ('npy', 'raw') or (
			snap.config is not None and 'FDM_PSEUDOSPECTRAL' in snap.config
		) or (snap.param is not None and 'AxionMassEv' in snap.param)
		if snapshot_fdm:
			mass = snap.param['AxionMassEv']
	#TODO
	p = str(path)
	if not ('_cm_' in p or '_correctmass_' in p or '_axionic_' in p):
		if snapshot_fdm and mass in (1e-22, 5e-23, 2.5e-23) or '_wm_' in p:
			print('WARNING: Assuming incorrect axion mass, correcting by h')
			mass *= 0.7
	# line styles
	if not reference:
		if linear_theory or snapshot_fdm or axionic or ngenic_inputspec:
			plot_color_idx = plot_color_idx_other
			plot_color_idx_other += 1
		else:
			plot_color_idx = plot_color_idx_cdm
			plot_color_idx_cdm += 1
		style = None
		if args.styles:
			style = (
				args.styles[0] if len(args.styles) == 1
				else args.styles[path_idx]
			)
			if not style:
				style = None
		plot_context = contextlib.nullcontext()
		if style:
			plot_context = plt.style.context(style)
		elif linear_theory or snapshot_fdm or axionic or ngenic_inputspec:
			plot_context = contextlib.nullcontext()
		elif not args.line_styles:
			plot_context = plt.style.context('grayscale')
		#TODO
#		else:
#			plot_context = contextlib.nullcontext()
		line_style = None
		if args.line_styles:
			line_style = (
				args.line_styles[0] if len(args.line_styles) == 1
				else args.line_styles[path_idx]
			)
			line_style = line_style.strip()
		rc_context = contextlib.nullcontext()
		if args.redshift_colorbar:
			cbar_range = (
				sorted(args.redshift_colorbar_range)
				if args.redshift_colorbar_range else ()
			)
			if args.redshift_colorbar_switch:
				assert cbar_range
				log_diff = (
					numpy.log10(cbar_range[1]) - numpy.log10(cbar_range[0])
				)
				# Note: colorbar is inverted, so need to swap colors here too
				log_diff1 = (
					numpy.log10(args.redshift_colorbar_switch) -
					numpy.log10(cbar_range[0])
				)
				colors1_frac = log_diff1 / log_diff
				# see https://stackoverflow.com/a/31052741
				num_colors = 256
				colors1 = plt.get_cmap()(numpy.linspace(
					0., 1., round(num_colors * colors1_frac)
				))
				colors2 = plt.cm.plasma(numpy.linspace(
					0., 0.6, num_colors - round(num_colors * colors1_frac)
				))
				colors = numpy.vstack((colors1, colors2))
				colormap = matplotlib.colors.LinearSegmentedColormap.from_list(
					'redshift_colormap', colors
				)
			else:
				colormap = plt.get_cmap()
			cbar_norm = matplotlib.colors.LogNorm(*cbar_range)
			cbar_mappable = plt.cm.ScalarMappable(cmap=colormap, norm=cbar_norm)
		elif args.line_color_maps:
			colormap_name = (
				args.line_color_maps[0] if len(args.line_color_maps) == 1
				else args.line_color_maps[path_idx]
			)
			colormap = (
				plt.get_cmap(colormap_name) if colormap_name
				else plt.get_cmap()
			)
			line_colors = plt.cycler(color=colormap(
				numpy.linspace(0.05, 0.95, args.line_color_maps_length)
			))
			rc_context = plt.rc_context({'axes.prop_cycle': line_colors})
		elif not snapshot_fdm and not args.line_styles:
			line_colors = plt.cycler(color=plt.get_cmap('gray')(
				numpy.linspace(0, 1, 6)
			))
			rc_context = plt.rc_context({'axes.prop_cycle': line_colors})
	# plot labels
	UnitLength_in_Mpc = UnitLength_in_cm / MPC_IN_CM
	if not linear_theory and not axionic and not ngenic_inputspec:
		numpart_per_dim = round(numpart**(1/3))
		box_size_Mpc = box_size * UnitLength_in_Mpc
		if not args.legend_entries:
			if snapshot_fdm:
				label = (
					fr'FDM, ${pmgrid}^3$, $mc^2 = '
					fr'{float_to_scientific_notation(mass)}\,$eV'
				)
				if args.legend_box_size:
					label += fr', $L = {box_size_Mpc}\,h^{{-1}}\,\mathrm{{Mpc}}$'
			else:
				label = fr'CDM, ${numpart_per_dim}^3$'
			if not args.equal_time:
				label += fr' ($z = {format_z(z)}$)'
	if args.legend_entries:
		label = ''
	elif args.labels:
		label = args.labels[path_idx]
	if not reference:
		print(f'\tz = {z}')
	if z_check is None:
		z_check = z
	if args.equal_time:
		assert numpy.isclose(z, z_check, atol=1e-9, rtol=1e-2), (z_check, z)
	def get_plot_var(i, P_var, Delta2_var):
		if args.plot_vars[i] == 'P':
			return P_var
		else:
			assert args.plot_vars[i] == 'Delta2'
			return Delta2_var
	# N-GenIC or axionCAMB power spectrum
	if linear_theory or axionic or ngenic_inputspec:
		# N-GenIC inputspec.txt contains k values from 1000 Mpc/h to 0.001 Mpc/h
		if linear_theory:
			label_base = 'linear theory'
			zstart, growthfactor, ps = io_util.read_ngenic_inputspec_output(
				INPUTSPEC_PATH
			)
			# calculate growth factor (value of efunc is independent of H0)
			cosmo = astropy.cosmology.FlatLambdaCDM(
				100 * snap.header['HubbleParam'], Omega_m0
			)
			# inputspec contains the power spectrum scaled to z = 0
			D_plus = growth_factor(cosmo, 1, a)
			K = ps.k_linear / UnitLength_in_Mpc
			Delta2 = ps.d2_linear * D_plus**2
			# TODO: factor 2π² vs. 4π (4π is used in N-GenIC code)?
			P = Delta2 / K**3 * (2 * numpy.pi**2)
		# power spectrum used as tabulated input *for N-GenIC*
		elif ngenic_inputspec:
			label_base = 'linear theory (FDM)'
			ps = pandas.read_csv(path, sep=r'\s+', names=['logk', 'logD'])
			# calculate growth factor (value of efunc is independent of H0)
			cosmo = astropy.cosmology.FlatLambdaCDM(
				100 * snap.header['HubbleParam'], Omega_m0
			)
			# inputspec contains the power spectrum scaled to z = 0
			D_plus = growth_factor(cosmo, 1, a)
			K = 10**ps['logk']
			Delta2 = 10**ps['logD'] * D_plus**2
			# TODO: factor 2π² vs. 4π (4π is used in N-GenIC code)?
			P = Delta2 / K**3 * (2 * numpy.pi**2)
		# axionCAMB power spectrum
		elif axionic:
			label_base = 'linear theory (FDM)'
			path_cdmic = (
				args.axionic_cdm_matterpower[0]
				if len(args.args.axionic_cdm_matterpower) == 1 else
				args.axionic_cdm_matterpower[axionic_idx]
			)
			axionic_spec = linear_theory_axionic(path, path_cdmic, None)
			K = axionic_spec['k_Mpc']
			# axionCAMB spectrum/inputspec contains the power spectrum scaled
			# to z = 0
			D_plus = growth_factor(cosmo, 1, a)
			P = axionic_spec['P_Mpc3'] * D_plus**2
			Delta2 = axionic_spec['Delta2'] * D_plus**2
			axionic_idx += 1
		else: assert False
		# remember reference data
		if reference:
			K_ref = K
			P_ref = scipy.interpolate.interp1d(K, P)
			Delta2_ref = scipy.interpolate.interp1d(K, Delta2)
			K_plot = K
			plot_var = get_plot_var(0, P, Delta2)
			plot_var_ref = get_plot_var(0, P_ref, Delta2_ref)
		# plot
		else:
			for ax in axes:
				if relative and ax is axes[-1]:
					if path_idx in args.relative_exclude:
						continue
					mask = (K >= K_ref.min()) & (K <= K_ref.max())
					plot_var_ref_ax = plot_var_ref
				else:
					mask = K > 0
					plot_var_ref_ax = lambda K: 1
				K_plot = K[mask]
				plot_var = get_plot_var(0, P[mask], Delta2[mask])
				if not args.legend_entries and not args.labels:
					label = label_base
					if not args.equal_time:
						label += fr' ($z = {format_z(z)}$)'
				plot_opt = {}
				if args.line_width:
					plot_opt['linewidth'] = args.line_width
				ax.plot(
					K_plot, plot_var / plot_var_ref_ax(K_plot),
					line_style if line_style else '-', label=label, **plot_opt
				)
	# read and process power spectrum file
	else:
		try:
			# path to power spectrum file was given as input directly
			if path.suffix == '.txt':
				ps_path = path
				if snapshot_fdm or args.powerspec_txt_format == 'fdm':
					ps = io_util.read_fdm_powerspec_csv(ps_path)
				else:
					ps = io_util.read_arepo_powerspec_file(ps_path)
				#TODO
#				found_time = True
			# path to snapshot was given as input
			else:
				ps_path, ps = powerspec_from_snap_num(
					path.parent, snapshot_fdm, snap_num, a
				)
				found_time = True
			if not reference:
				print(f'\t{ps_path}')
			if 'Time' in ps:
				ps_time = ps.Time.iloc[(ps.Time - a).abs().argsort()[0]]
				found_time = numpy.isclose(a, ps_time, atol=1e-9, rtol=1e-2)
				assert (
					(args.include_missing and not reference) or found_time
				), f'{a} {ps_time}'
				ps = ps[ps.Time == ps_time]
				if not reference:
					print(f'\ta = {a}, ps.Time = {pandas.unique(ps["Time"])}')
		except IOError:
			found_time = False
			if not args.include_missing:
				raise
		# add label to legend and skip for missing data
		if args.include_missing and not found_time and not args.legend_entries:
			with rc_context, plot_context:
				for ax in axes:
					ax.plot(
						# need to specify C<num> because the Axes object uses
						# the property cycle that was in effect when the object
						# was *created* (cf. Axes.set_prop_cycle), whereas
						# C<num> is evaluated at the time Axes.plot is called
						# and uses the *global* property cycle instead of the
						# one specific to the Axes object (cf. below)
						[], line_style if line_style else f'C{plot_color_idx}-',
						label=f'{label} [MISSING]'
					)
			continue
		# process file if valid
		if len(ps) % args.BINS_PS != 0 or len(ps) == 0:
			print(ps)
			warnings.warn(
				'Unexpected power spectrum file contents, ignoring:\n'
				f'\t{ps_path}, rows = {len(ps)}'
			)
			assert not reference
		assert 'Time' not in ps or len(pandas.unique(ps['Time'])) == 1
		ps_path_combined = path.parent / 'powerspec.txt'
		num_fold = 3 if not snapshot_fdm and ps_path != ps_path_combined else 1
		#TODO
		num_fold = len(ps) // args.BINS_PS
		if num_fold not in (1, 3): num_fold = 1
		K_combine = []
		P_combine = []
		Delta2_combine = []
		def ps_fold(ps, fold):
			result = ps[fold * args.BINS_PS:(fold + 1) * args.BINS_PS]
			assert len(result) == args.BINS_PS, len(result)
			return result
		for fold in range(num_fold):
			ps_f = ps_fold(ps, fold)
			# Delta2 = K**3 * P / (2 * numpy.pi**2)
			K_fold, P_fold, Delta2_fold = rebin(
				num_fold == 1, fold, box_size_Mpc, pmgrid,
				ps_f.Kbin.array / UnitLength_in_Mpc,
				ps_f.PowerUncorrected.array * box_size_Mpc**3,
				ps_f.DeltaUncorrected.array,
				ps_f.CountModes.array
			)
			K_combine.append(K_fold)
			P_combine.append(P_fold)
			Delta2_combine.append(Delta2_fold)
		# omit last bin, which is incomplete
		K = numpy.concatenate(K_combine)[:-1]
		P = numpy.concatenate(P_combine)[:-1]
		Delta2 = numpy.concatenate(Delta2_combine)[:-1]
		if not args.plot_beyond_nyquist and (
			snapshot_fdm and num_fold == 1 and z > 14
		):
			# don’t show modes beyond the Nyquist frequency during linear
			# evolution
			nyquist_mask = K < numpy.pi * (pmgrid - 1) / box_size_Mpc
			#TODO
			if len(K[nyquist_mask]) > 0:
				K = K[nyquist_mask]
				P = P[nyquist_mask]
				Delta2 = Delta2[nyquist_mask]
		if not reference:
			print(f'\tk_min = {K.min()}')
		# remember power spectrum specified by --only-relative or
		# --with-relative
		if reference:
			K_ref = K
			K_plot = K
			plot_var = get_plot_var(0, P, Delta2)
			plot_var_ref = scipy.interpolate.interp1d(K_ref, plot_var)
		# plot
		else:
			with rc_context, plot_context:
				for ax, ax_secondvar in itertools.zip_longest(
					axes, axes_secondvar
				):
					if relative and ax is axes[-1]:
						if path_idx in args.relative_exclude:
							continue
						mask = (K >= K_ref.min()) & (K <= K_ref.max())
						plot_var_ref_ax = plot_var_ref
					else:
						mask = K > 0
						plot_var_ref_ax = lambda K: 1
					K_plot = K[mask]
					P_plot = P[mask]
					Delta2_plot = Delta2[mask]
					plot_var = get_plot_var(0, P_plot, Delta2_plot)
					plot_opt = {}
					if args.line_width:
						plot_opt['linewidth'] = args.line_width
					if args.redshift_colorbar:
						plot_opt['color'] = colormap(cbar_norm(z))
					line, = ax.plot(
						K_plot, plot_var / plot_var_ref_ax(K_plot),
						# need to specify C<num> because the Axes object uses
						# the property cycle that was in effect when the object
						# was *created* (cf. Axes.set_prop_cycle), whereas
						# C<num> is evaluated at the time Axes.plot is called
						# and uses the *global* property cycle instead of the
						# one specific to the Axes object (cf. above)
						line_style if line_style else f'C{plot_color_idx}',
						label=label, **plot_opt
					)
					if len(args.plot_vars) > 1:
						plot_var_second = get_plot_var(1, P_plot, Delta2_plot)
						plot_var_second_ref = scipy.interpolate.interp1d(
							K, plot_var
						)
						plot_var_second_ref_ax = (
							plot_var_ref if relative and ax is axes[-1]
							else lambda K: 1
						)
						ax_secondvar.plot(
							K_plot,
							plot_var_second / plot_var_second_ref_ax(K_plot),
							'--', color=line.get_color(), **plot_opt
						)
					# optionally plot Jeans length for FDM
					if 'jeans-fuzzy' in args.mark_scale and snapshot_fdm:
						# equivalent equations for Jeans scale:
						# arXiv:astro-ph/0003365 eq. (4),
						# arXiv:0806.0232 eq. (14)
						#	[with factor (Ω_m)^0.25 to cover non-matter dom.];
						# arXiv:1508.04621 eq. (3)
						# arXiv:1510.07633 eq. (101)
						# arXiv:1610.08297 eq. (40) [missing factor of a?],
						# arXiv:1809.04744 eq. (1)
						# arXiv:2005.03254v1 eq. (114)
#						h = snap.header['HubbleParam']  # 0.7  #TODO
						cosmo = astropy.cosmology.FlatLambdaCDM(
							100 * snap.header['HubbleParam'], Omega_m0
						)
						# this is the comoving Jeans scale
						m_qty = (
							astropy.units.Quantity(mass, 'eV') /
							astropy.constants.c**2
						)
						hbar_qty = astropy.constants.hbar
						k_J_Mpc = (
							(6 * Omega_m0 * a)**(1/4) *
							(cosmo.H0 * m_qty / hbar_qty)**(1/2)
						).to_value(f'{cosmo.h}/Mpc')
						k_J_Mpc_mustafa = (
							6**(1/4) * a *
							(cosmo.H(z) * m_qty / hbar_qty)**(1/2)
						).to_value(f'{cosmo.h}/Mpc')
						r_J_Mpc = 2 * numpy.pi / k_J_Mpc
						if ax is axes[0]:
							print(f'\tk_Jeans_fuzzy = {k_J_Mpc} h/Mpc (comoving)')
							print(f'\tk_Jeans_fuzzy = {k_J_Mpc_mustafa} h/Mpc (comoving) [Mustafa]')
							print(f'\tr_Jeans_fuzzy = {r_J_Mpc} Mpc/h (comoving)')
						plot_var_interp = scipy.interpolate.interp1d(
							K_plot, plot_var
						)
						if args.equal_time and args.equal_mass:
							if not jeans_length_marker:
								ax.axvline(
									k_J_Mpc, linestyle='--', color='gray'
								)
								if ax is axes[-1]:
									jeans_length_marker = '--'
						else:
							try:
								ax.plot(
									[k_J_Mpc], [
										plot_var_interp(k_J_Mpc) /
										plot_var_ref_ax(k_J_Mpc)
									],
									'*', color=line.get_color(),
									markeredgecolor='k'
								)
								jeans_length_marker = '*'
							except ValueError:
								ax.axvline(
									k_J_Mpc, linestyle='--',
									color=line.get_color()
								)
								jeans_length_marker = '--'
					# optionally plot isocurvature Jeans length for FDM
					if 'jeans-iso' in args.mark_scale and snapshot_fdm:
						cosmo = snap.cosmology
						m_qty = (
							astropy.units.Quantity(mass, 'eV') /
							astropy.constants.c**2
						)
						hbar_qty = astropy.constants.hbar
						for key, k_s, label in mark_scale:
							if key == 'jeans-iso':
								break
						k_s_qty = astropy.units.Quantity(k_s, f'{cosmo.h}/Mpc')
						sigma = k_s_qty / (a * m_qty / hbar_qty)
						k_J_iso_Mpc = (
							a * cosmo.H(z) / sigma
						).to_value(f'{cosmo.h}/Mpc')
						r_J_iso_Mpc = 2 * numpy.pi / k_J_iso_Mpc
						if ax is axes[0]:
							print(
								f'\tk_Jeans_iso = {k_J_iso_Mpc} h/Mpc '
								'(comoving)'
							)
							print(
								f'\tr_Jeans_iso = {r_J_iso_Mpc} Mpc/h '
								'(comoving)'
							)
						if k_J_iso_Mpc >= K_plot.min():
							plot_var_interp = scipy.interpolate.interp1d(
								K_plot, plot_var
							)
							if args.equal_time and args.equal_mass:
								#TODO
								assert False
#								if not jeans_length_iso_marker:
#									ax.axvline(
#										k_J_iso_Mpc, linestyle='--', color='gray'
#									)
#									if ax is axes[-1]:
#										jeans_length_iso_marker = '--'
							else:
								try:
									ax.plot(
										[k_J_iso_Mpc], [
											plot_var_interp(k_J_iso_Mpc) /
											plot_var_ref_ax(k_J_iso_Mpc)
										],
										'o', color=line.get_color(),
										markeredgecolor='k'
									)
									jeans_length_iso_marker = 'o'
								except ValueError:
									#TODO
									assert False
#									ax.axvline(
#										k_J_iso_Mpc, linestyle='--',
#										color=line.get_color()
#									)
#									jeans_length_iso_marker = '--'
					# plot shot noise for CDM
					# TODO: differentiate between CDM, baryons, and CDM+baryons
					# TODO: only show on the right part of the plot, after
					#       power spectrum has dropped to the shot noise level?
					snapshot_gas = 'type0' in path.name
					if snapshot_gas:
#						#TODO: why was this necessary?
#						if path.parent.name == 'fdmb_axionic_p8640-256_b10000_m7e-23_123456':
#							numpart = 256**3
#						else:
						numpart = read_snapshot.numpart_total(
							snap.header, ptype=0
						)
						assert numpart > 0
					if not snapshot_fdm or snapshot_gas:
						shot_noise = box_size_Mpc**3 / numpart
						if relative and ax is axes[-1]:
							ax.plot(
								K_plot, shot_noise / plot_var_ref(K_plot),
								**PLOT_OPT_SHOT_NOISE
							)
						else:
							ax.axhline(shot_noise, **PLOT_OPT_SHOT_NOISE)
						shot_noise_label = True
	# determine k_min/k_max, P_min/P_max
	k_min = min(k_min, K.min())
	k_max = max(k_max, K.max())
	p_min = min(p_min, plot_var.min())
	p_max = max(p_max, plot_var.max())
	p_min_ref = min(p_min, (plot_var / plot_var_ref(K_plot)).min())
	p_max_ref = max(p_max, (plot_var / plot_var_ref(K_plot)).max())

# final plot settings
legend = None
for ax in axes:
	# axis limits
	if args.plot_xlim:
		ax.set_xlim(args.plot_xlim)
	else:
		ax.set_xlim(k_min, k_max)
	if ax is axes[-1] and args.plot_ylim_relative:
		ax.set_ylim(args.plot_ylim_relative)
	elif args.plot_ylim:
		ax.set_ylim(args.plot_ylim)
	else:
		if relative and ax is axes[-1]:
			p_min_ax, p_max_ax = p_min_ref, p_max_ref
		else:
			p_min_ax, p_max_ax = p_min, p_max
		y_min = 10**numpy.floor(numpy.log10(p_min_ax))
		y_max = 10**numpy.ceil(numpy.log10(p_max_ax))
		ax.set_ylim(y_min, y_max)
	# minor tick settings
	#TODO: do this for all plotting scripts?
	xloc_maj = ax.xaxis.get_major_locator()
	xloc_min = ax.xaxis.get_minor_locator()
	yloc_maj = ax.yaxis.get_major_locator()
	yloc_min = ax.yaxis.get_minor_locator()
	if hasattr(yloc_maj, '_base'):
		yticks = yloc_maj()
		# use minor ticks to mark major tick positions if axis starts skipping
		# major ticks
		if len(yticks) >= 2 and not numpy.isclose(
			yticks[1] / yticks[0], yloc_maj._base
		):
			yloc_min.set_params(numticks=100, subs=[1.0])
			# avoid automatic minor tick labels, since space is already limited
			# if we’re in this scenario
			ax.tick_params(which='minor', labelleft=False, labelright=False)
		# for relative axis with large vertical range: use fewer minor ticks to
		# avoid becoming too crowded
		elif args.with_relative and ax is axes[-1]:
			rylim0, rylim1 = ax.get_ylim()
			if rylim1 / rylim0 > yloc_maj._base**2:
				yloc_min.set_params(subs=0.2 * numpy.arange(1, 5))
	# legend
	if args.legend_entries:
		i_label = 0
		for style in args.legend_entries:
			label = args.labels[i_label]
			if style.startswith('EMPTY'):
				ax.plot(
					[], [], color='none',
					label=' ' + '\n' * (style.count('\n') + style.count(r'\n'))
				)
			else:
				ax.plot([], [], style, label=label)
				i_label += 1
	aux_legend_entries = []
	# Jeans length (“fuzzy”)
	if jeans_length_marker:
		label = 'FDM Jeans length'
		if 'jeans-fuzzy-eq' in args.mark_scale:
			label += fr' ($z = {format_z(z)}$)'
		color = 'k'
		if args.equal_time and args.equal_mass:
			color = 'gray'
		if args.aux_legend:
			if jeans_length_marker in matplotlib.lines.Line2D.lineStyles:
				key_opt = dict(linestyle=jeans_length_marker)
			else:
				key_opt = dict(linestyle='', marker=jeans_length_marker)
			aux_legend_entries.append(matplotlib.lines.Line2D(
				[], [], color=color, label=label, **key_opt,
			))
		else:
			ax.plot([], jeans_length_marker, color=color, label=label)
	# Jeans length (“isocurvature”)
	if jeans_length_iso_marker:
		label = 'Jeans length'
		color = 'k'
		if args.equal_time and args.equal_mass:
			color = 'gray'
		if args.aux_legend:
			if jeans_length_iso_marker in matplotlib.lines.Line2D.lineStyles:
				key_opt = dict(linestyle=jeans_length_iso_marker)
			else:
				key_opt = dict(linestyle='', marker=jeans_length_iso_marker)
			aux_legend_entries.append(matplotlib.lines.Line2D(
				[], [], color=color, label=label, **key_opt,
			))
		else:
			ax.plot([], jeans_length_iso_marker, color=color, label=label)
	# Jeans length at equality (“fuzzy”)
	if 'jeans-fuzzy-eq' in args.mark_scale:
		#TODO do this properly
		z_eq = 3300
		a_eq = 1 / (z_eq + 1)
		for key, m_ev, _ in mark_scale:
			if key == 'jeans_fuzzy_eq':
				break
		m_qty = astropy.units.Quantity(m_ev, 'eV') / astropy.constants.c**2
		k_J_eq_Mpc = (
			(6 * Om0 * a_eq)**(1/4) *
			(H0_qty * m_qty / hbar_qty)**(1/2)
		).to_value(f'{cosmo.h}/Mpc')
		print(f'\tk_Jeans(z=z_eq) = {k_J_eq_Mpc} h/Mpc (comoving)')
		label = 'FDM Jeans length ($z = z_{\mathrm{eq}}$)'
		if args.aux_legend:
			label_opt = dict(label='')
		else:
			label_opt = dict(label=label)
		line = ax.axvline(k_J_eq_Mpc, linestyle=':', color='gray', **label_opt)
		if args.aux_legend:
			vline_jeans_eq_legend = matplotlib.lines.Line2D([], [])
			vline_jeans_eq_legend.update_from(line)
			vline_jeans_eq_legend.set_label(label)
			aux_legend_entries.append(vline_jeans_eq_legend)
	# manually marked scales
	for key, k, label in mark_scale:
		if key == 'manual':
			ax.axvline(k, linestyle='--', color='gray')
			# can’t use leading whitespace in text with text.usetex, see
			# https://github.com/matplotlib/matplotlib/issues/24166
			# workaround: get shifted coordinates from dummy text
			if plt.rcParams['text.usetex']:
				space_txt = ax.text(k, ax.get_ylim()[0], '_', alpha=0)
				space_redge_disp = space_txt.get_window_extent().get_points()[1]
				space_redge_data = ax.transData.inverted().transform(
					space_redge_disp
				)
				k = space_redge_data[0]
			ax.text(k, ax.get_ylim()[0], label, va='bottom')
	# shot noise
	if shot_noise_label:
		label = 'shot noise limit'
		if args.aux_legend:
			aux_legend_entries.append(matplotlib.lines.Line2D(
				[], [], **PLOT_OPT_SHOT_NOISE, label=label,
			))
		else:
			ax.plot([], **PLOT_OPT_SHOT_NOISE, label=label)
	if ax is axes[0]:
		if args.aux_legend:
			aux_legend = ax.legend(
				handles=aux_legend_entries,
				loc=args.legend_loc if args.no_legend else 'upper left'
			)
			ax.add_artist(aux_legend)
		if not args.no_legend:
			legend = ax.legend(loc=args.legend_loc, ncols=args.legend_ncols)

# colorbar
if args.redshift_colorbar:
	cbar = plt.colorbar(cbar_mappable, ax=axes)
	cbar.ax.invert_yaxis()
	cbar.set_label(r'$z$')

# title
title = fr'$L = {box_size_Mpc}\,h^{{-1}}\,\mathrm{{Mpc}}$'
if args.equal_time:
	if z >= 0:
		title += f', $z = {format_z(z)}$'
	else:
		title += f', $a = {a:.{args.time_digits}f}$'
if not args.no_title:
	fig.suptitle(title)


# redshift text (lower left/upper right corner)
# Needs to be done as late as possible so that the rest of the figure is
# already in place!
if args.equal_time:
	loc = 'lower left'
	# check if legend is already in lower left corner, and put redshift
	# text in upper right corner if this is the case
	if legend:
		if args.legend_loc == 'lower left':
			loc = 'upper right'
		elif args.legend_loc != 'upper right':
			# save figure to a temporary file so that figure layout is done
			# with the correct backend
			with tempfile.TemporaryDirectory() as tmp, numpy.errstate(
				divide='ignore'
			):
				fig.savefig(
					Path(tmp) / f'tmp.{args.plot_format}', transparent=True
				)
			legend_bbox = legend.get_window_extent().transformed(
				axes[0].transAxes.inverted()
			)
			if matplotlib.transforms.Bbox.intersection(
				legend_bbox, matplotlib.transforms.Bbox([[0, 0], [1/4, 1/4]])
			):
				loc = 'upper right'
	axes[0].add_artist(matplotlib.offsetbox.AnchoredText(
		f'$z = {format_z(z)}$', loc=loc
	))

# save plot
plot_file_name = 'powerspec'
if args.equal_time:
	plot_file_name += f'_{a:.{args.time_digits}f}'
if args.only_relative:
	plot_file_name += '_ratio'
if args.plot_file_name:
	plot_file_name = args.plot_file_name
savefig_opts = {}
if args.plot_format != 'pdf':
	savefig_opts['dpi'] = args.dpi
with numpy.errstate(divide='ignore'):
	fig.savefig(
		f'{args.plot_file_prefix}{plot_file_name}.{args.plot_format}',
		transparent=True, **savefig_opts
	)

