import collections
import gc
import sys
import time
import numpy
import snapshot
from mpi_util import mpi_comm, mpi_rank, mpi_size, print0

FDM_DENSITY_DATASET = '/FuzzyDM/Density'
ELEMENT_SIZES = {
	'psi': snapshot.GRID_ELEMENT_MEM_SIZE,
	'energies': 6 * numpy.dtype('float64').itemsize,
	# TODO: better estimates for the following entries
	'dataset': numpy.dtype('float64').itemsize,
	'particles': 3 * numpy.dtype('float64').itemsize,
}
EnergyGrids = collections.namedtuple(
	'EnergyGrids', 'quantum, vx, vy, vz, rho'
)

cache = None
cache_meta = None

#TODO: update from ix_start, ix_end to full slices
def default_update_callback(snap, it_start, ix_chunks, ix_start, ix_end, tdiff):
	if it_start:
		num_tot_str = f'{ix_chunks[-1][-1]:,}'
		len_max = len(num_tot_str)
		print0(
			f'Reading {ix_start: >{len_max},} to {ix_end - 1: >{len_max},} '
			f'(max: {num_tot_str})',
			flush=True
		)
	elif tdiff > 5:
		print0(f'\tTook {tdiff} sec.', flush=True)

def clear_cache():
	cache = None
	cache_meta = None
	gc.collect()

def read_grid(snap, data_slice, grid_type, use_rho, datasets, chunk_idx):
	grid = None
	ds_rho = False
	if grid_type == 'dataset':
		ds_grid = [
			snap.read_snapshot(ds, data_slice=data_slice) for ds in datasets
			if ds != FDM_DENSITY_DATASET
		]
		ds_rho = FDM_DENSITY_DATASET in datasets
		if ds_rho:
			use_rho = True
	if grid_type in ('psi', 'energies') or ds_rho:
		psi_re = snap.read_snapshot('/FuzzyDM/PsiRe', data_slice=data_slice)
		if use_rho:
			grid = psi_re**2
		else:
			grid = numpy.zeros_like(psi_re, dtype='c16')
			grid.real = psi_re
		del psi_re
		im_present = snap.dataset_present('/FuzzyDM/PsiIm')
		psi_im = (
			snap.read_snapshot('/FuzzyDM/PsiIm', data_slice=data_slice)
			if im_present else 0
		)
		if use_rho:
			grid += psi_im**2
		else:
			assert im_present
			grid.imag = psi_im
		del psi_im
		gc.collect()
	if grid_type == 'dataset':
		grid_rho = grid
		grid = ds_grid
		if ds_rho:
			grid.insert(datasets.index(FDM_DENSITY_DATASET), grid_rho)
		grid = tuple(grid)
	if grid_type == 'energies':
		cell_vol = (snap.box_size / snap.pmgrid)**3
		# need density for kinetic energy
		rho = grid
		vx = snap.read_snapshot(
			'/FuzzyDM/MomentumDensity0', data_slice=data_slice
		) / rho
		vy = snap.read_snapshot(
			'/FuzzyDM/MomentumDensity1', data_slice=data_slice
		) / rho
		vz = snap.read_snapshot(
			'/FuzzyDM/MomentumDensity2', data_slice=data_slice
		) / rho
		quantum = snap.read_snapshot(
			'/FuzzyDM/(GradSqrtRho)^2', data_slice=data_slice
		)
		hbar = snap.constant_in_internal_units('hbar')
		c = snap.constant_in_internal_units('c')
		m_ev = snap.param['AxionMassEv']
		m = snap.qty_in_internal_units(m_ev, 'eV') / c**2
		if chunk_idx == 0:
			print0(
				f'iterate_grid: Using constants hbar = {hbar}, m = {m}, '
				f'hbar/m = {hbar / m}'
			)
		quantum *= (hbar / m)**2 * cell_vol / 2
		grid = EnergyGrids(quantum=quantum, vx=vx, vy=vy, vz=vz, rho=rho)
	return grid

def iterate(
	snap, f, data, param=None, update_callback=default_update_callback,
	grid_type='psi', use_rho=True, datasets=None,
	chunk_mem_size=snapshot.CHUNK_MEM_SIZE_DEFAULT, data_slice=...,
	cache_full_grid=True
):
	global cache, cache_meta
	assert grid_type in ('psi', 'energies', 'velocity', 'dataset')
	if not cache_full_grid:
		clear_cache()
	if grid_type == 'energies':
		use_rho = True
	#TODO: make this cleaner (not hard-coding /FuzzyDM/PsiRe)
	data_slice, indices = snap.read_snapshot_indices(
		'/FuzzyDM/PsiRe', data_slice
	)
	for s in data_slice:
		assert s.step in (1, None)
	slab_slice = data_slice[1:]
	result_shape = numpy.broadcast(*indices).shape
	x_indices = numpy.ravel(indices[0])
	ix_min = x_indices[0]
	print0(
		f'iterate_grid.iterate(): Restricting iteration to range {data_slice}'
	)
	print0(
		f'iterate_grid.iterate(): \t (shape of iterated grid: {result_shape})'
	)
	element_size = ELEMENT_SIZES[grid_type]
	if grid_type == 'dataset':
		element_size *= len(datasets)
	ix_chunks, max_num_chunks = snap.determine_chunks(
		mpi_rank, mpi_size, element_size=element_size,
		chunk_mem_size=chunk_mem_size, num_slabs=result_shape[0],
		slab_size=numpy.prod(result_shape[1:], dtype=int),
		return_max_num_chunks=True
	)
	ix_chunks = [
		(
			(ix_min + ix_start) % snap.pmgrid,
			snap.pmgrid if ix_min + ix_end == snap.pmgrid else
				(ix_min + ix_end) % snap.pmgrid
		)
		for ix_start, ix_end in ix_chunks
	]
	print0(
		f'iterate_grid.iterate(): max_num_chunks = {max_num_chunks}, '
		f'ix_chunks = {ix_chunks[:8]}{" … " if len(ix_chunks) > 8 else ""}'
		f'{ix_chunks[-2:] if len(ix_chunks) > 8 else ""}'
	)
	for chunk_idx, [ix_start, ix_end] in enumerate(ix_chunks):
		t0 = time.perf_counter()
		if update_callback is not None:
			update_callback(snap, True, ix_chunks, ix_start, ix_end, 0)
		chunk_slice = numpy.index_exp[ix_start:ix_end] + slab_slice
		cache_invalid = (
			cache is None or
			cache_meta != (snap, grid_type, use_rho, datasets, data_slice)
		)
		if not cache_full_grid or cache_invalid:
			grid = read_grid(
				snap, chunk_slice, grid_type, use_rho, datasets, chunk_idx
			)
		if cache_full_grid and max_num_chunks == 1:
			if cache_invalid:
				print0(
					'iterate_grid.iterate(): Saving grid to cache because the '
					'entire grid fits into memory'
				)
				cache = grid
				cache_meta = (snap, grid_type, use_rho, datasets, data_slice)
			else:
				print0('iterate_grid.iterate(): Using cached grid')
				grid = cache
		ix_info = (ix_min, ix_start, ix_end)
		args = grid, snap.pmgrid, ix_info, data_slice, param, data
		if isinstance(f, collections.abc.Iterable):
			for func in f:
				func(*args)
		else:
			f(*args)
		t1 = time.perf_counter()
		if update_callback is not None:
			update_callback(snap, False, ix_chunks, ix_start, ix_end, t1 - t0)


PARTICLE_DATASETS = 'Coordinates,Velocities,Masses,ParticleIDs'.split(',')
ParticleData = collections.namedtuple(
	'ParticleData', PARTICLE_DATASETS,
	defaults=[
		# Velocities
		numpy.empty((0, 3), dtype='f4'),
		# Masses
		numpy.empty((0,), dtype='f8'),
		# ParticleIDs
		numpy.empty((0,), dtype='u8'),
	]
)

cache_particles = None
cache_meta_particles = None


def dataset_name(dataset):
	return dataset.lstrip('/').split('/', maxsplit=1)[1]

def read_particles(snap, data_slice, datasets, region):
	#TODO: support GADGET snapshots in snapshot.py
	if region is None:
		data = {
			dataset_name(ds):
			snap.read_snapshot(ds, data_slice=data_slice)
			for ds in datasets
		}
	else:
		t0 = time.perf_counter()
		if mpi_rank == 0:
			# TODO: Does not seem to use yt’s parallelism with
			#       yt.enable_parallelism() -> works on 1 CPU only, extremely
			#       slow!
			import yt
			ytds = yt.load(snap.paths[0])
			ytds_region = ytds.box(*region)
			data = {
				dataset_name(ds): numpy.array_split(
					ytds_region[tuple(ds.lstrip('/').split('/', 1))].ndview,
					mpi_size
				) for ds in datasets
			}
		else:
			data = {dataset_name(ds): None for ds in datasets}
		t1 = time.perf_counter()
		print0(f'YT read took {t1 - t0} sec.', flush=True)
		t0 = time.perf_counter()
		for ds_name in data:
			data[ds_name] = mpi_comm.scatter(data[ds_name], root=0)
		t1 = time.perf_counter()
		print0(f'Scatter took {t1 - t0} sec.', flush=True)
	return data


#TODO: multiple ptypes, different datasets per ptype
def iterate_particles(
	snap, f, data, param=None, update_callback=default_update_callback,
	ptype=1, datasets=('Coordinates',), region=None,
	chunk_mem_size=snapshot.CHUNK_MEM_SIZE_DEFAULT,
	data_slice=numpy.index_exp[:, ...], cache_full_data=True
):
	assert all(ds in ParticleData._fields for ds in datasets)
	datasets = tuple(f'/PartType{ptype}/{ds}' for ds in datasets)
	data_slice = numpy.index_exp[data_slice]
	global cache_particles, cache_meta_particles
	if not cache_full_data:
		clear_cache_particles()
	cache_invalid = (
		cache_particles is None or
		cache_meta_particles[:-1] != (snap, datasets, data_slice) or 
		cache_meta_particles[-1] is not None != region is not None or
		(
			cache_meta_particles[-1] is not None and region is not None and
			any(
				numpy.any(rpc != rp)
				for rpc, rp in zip(cache_meta_particles[-1], region)
			)
		)
	)
	part_slice, *rest_slice = data_slice
	rest_slice = tuple(rest_slice)
	numpart_total = snap.numpart_total(ptype)
	print0(
		f'iterate_particles(): Restricting iteration to range {data_slice}'
	)
	if region is None:
		element_size = ELEMENT_SIZES['particles'] * len(datasets)
		part_chunks, max_num_chunks = snap.determine_chunks(
			mpi_rank, mpi_size, element_size=element_size,
			chunk_mem_size=chunk_mem_size, num_slabs=numpart_total, slab_size=1,
			return_max_num_chunks=True
		)
		print0(
			f'iterate_particles(): max_num_chunks = {max_num_chunks}, '
			f'ix_chunks = {part_chunks[:8]}'
			f'{" … " if len(part_chunks) > 8 else ""}'
			f'{part_chunks[-2:] if len(part_chunks) > 8 else ""}'
		)
		part_range = range(*part_slice.indices(numpart_total))
		part_first = part_range.start
		part_chunks = [
			(part_first + part_start, part_first + part_end)
			for part_start, part_end in part_chunks
		]
	else:
		print0(
			f'iterate_particles(): Restricting iteration to region {region}'
		)
		subregions_xpoints = numpy.linspace(
			region[0][0], region[1][0], num=mpi_size + 1
		)
		# this works properly: yt regions check coordinates via
		# left_edge <= x < right_edge
		subregion_x = [
			subregions_xpoints[mpi_rank], subregions_xpoints[mpi_rank + 1]
		]
		subregion = (numpy.copy(region[0]), numpy.copy(region[1]))
		for i in range(2):
			subregion[i][0] = subregion_x[i]
		print0(f'iterate_grid.iterate(): Processing subregion {subregion}')
		#TODO: how to read particles in chunks using yt?
		assert data_slice == numpy.index_exp[:, ...]
		part_chunks = [(-1, -1)]
		max_num_chunks = 1
	for part_start, part_end in part_chunks:
		t0 = time.perf_counter()
		if update_callback is not None:
			update_callback(snap, True, part_chunks, part_start, part_end, 0)
		chunk_slice = numpy.index_exp[part_start:part_end] + rest_slice
		if not cache_full_data or cache_invalid:
			pdata = ParticleData(
				**read_particles(snap, chunk_slice, datasets, region)
			)
		if cache_full_data and max_num_chunks == 1:
			if cache_invalid:
				print0(
					'iterate_particles(): Saving particle data to cache '
					'because everything fits into memory'
				)
				cache_particles = pdata
				cache_meta_particles = (snap, datasets, data_slice, region)
			else:
				print0('iterate_particles(): Using cached particle data')
				pdata = cache_particles
		if region is not None:
			#TODO
			part_chunks = [(0, len(getattr(pdata, dataset_name(datasets[0]))))]
			part_start, part_end = part_chunks[0]
		args = (pdata, numpart_total, part_start, part_end, param, data)
		if isinstance(f, collections.abc.Iterable):
			for func in f:
				func(*args)
		else:
			f(*args)
		t1 = time.perf_counter()
		if update_callback is not None:
			update_callback(
				snap, False, part_chunks, part_start, part_end, t1 - t0
			)

