#!/usr/bin/env python3
import argparse
import collections
import pickle
from pathlib import Path
import h5py
import matplotlib.pyplot as plt
import numpy
import numpy.linalg
import matplotlib_settings
import bin_density
import units_util
from snapshot import Snapshot


parser = argparse.ArgumentParser(description='Plot evolution of spheroidal '
	'collections of N-body test particles over time')
parser.add_argument('--ptype', type=int, default=1,
	help='particle type to use (default: %(default)d)')
#parser.add_argument('--ic', type=Path, required=True,
#	help='initial snapshot (must have corresponding FoF output) containing the '
#		'spheroids')
parser.add_argument('--num-groups', type=int, required=True,
	help='number of spheroids')
parser.add_argument('--num-part-per-group', type=int, required=True,
	help='number of particles per spheroid')
parser.add_argument('--ids-offset', type=int, default=1,
	help='start ID of test particles (IDs are assumed to be consecutive)')
parser.add_argument('--xlim', nargs=2, type=float,
	help='the time range to plot')
parser.add_argument('--ylim', nargs=2, type=float,
	help='the distance range to plot')
parser.add_argument('--plot-times', nargs='+', type=float,
	help='snapshot times (in Gyr) to plot')
parser.add_argument('--plot-individually', action='store_true',
	help='output individual plots for evolution of each group')
parser.add_argument('--cache', type=Path,
	help='path to cache to save/load')
parser.add_argument('--plot-var', choices=('mean', 'median', 'rms'),
	default='rms',
	help='the distance variable to plot (default: %(default)s)')
parser.add_argument('--plot-dir', type=Path, default=Path(),
	help='directory to write plots to (default: ./)')
parser.add_argument('--plot-format', default='png',
	help='the file format of the generated plots (default: %(default)s)')
parser.add_argument('--dpi', type=float, default=200.,
	help='the resolution of the generated plots (default: %(default)s)')
parser.add_argument('path', type=Path, nargs='+',
	help='the paths to the snapshot files; any normal files are interpreted '
		'as single-file snapshots, while all files in any given directory '
		'are interpreted as a single multi-file snapshot')


def main():
	# get particle IDs for initial spheroids
#	ic = Snapshot(args.ic)
#	_, _, group_ids = ic.read_fof_tab(read_ids=True)
#	assert not args.num_groups or len(group_ids) == args.num_groups
#	assert not args.num_part_per_group or all(
#		len(g) == args.num_part_per_group for g in group_ids
#	)
	group_ids = [
		numpy.arange(o, o + args.num_part_per_group) for o in range(
			args.ids_offset,
			args.ids_offset + args.num_groups * args.num_part_per_group,
			args.num_part_per_group
		)
	]
	# load cache or process paths
	if args.cache and args.cache.exists():
		print(f'Loading cache from “{args.cache}”')
		with open(args.cache, 'rb') as f:
			data = pickle.load(f)
	else:
		data = [collections.defaultdict(list) for _ in range(len(group_ids))]
		for snap_path in args.path:
			process_path(snap_path, group_ids, data)
		if args.cache:
			with open(args.cache, 'wb') as f:
				print(f'Writing cache to “{args.cache}”')
				pickle.dump(data, f)
	# plot results separately
	if args.plot_individually:
		for idx, g in enumerate(data):
			plt.figure()
			plt.plot(g['time_gyr'], g[f'{args.plot_var}_kpc'])
			plot_setup_time()
			plt.savefig(
				f'{idx:03}.{args.plot_format}', dpi=args.dpi, bbox_inches='tight',
				transparent=True
			)
			plt.close()
	# plot all results in one plot (time continuous)
	if args.plot_times is None:
		plt.figure()
		plot_setup_time()
		for g in data:
			plt.plot(g['time_gyr'], g[f'{args.plot_var}_kpc'])
		plt.savefig(
			f'all.{args.plot_format}', dpi=args.dpi, bbox_inches='tight',
			transparent=True
		)
	# plot all results in one plot (initial radius continuous)
	else:
		plt.figure()
		plot_setup_radius()
		times = numpy.array(data[0]['time_gyr'])
		r0 = [g[f'{args.plot_var}_kpc'][0] for g in data]
		num_digits = len(str(max(args.plot_times)))
		for t_target in args.plot_times:
			idx = numpy.argmin(numpy.abs(times - t_target))
			r = [g[f'{args.plot_var}_kpc'][idx] for g in data]
			t = times[idx]
			plt.plot(r0, r, label=f'{t:{num_digits}.1f} Gyr')
		plt.legend()
		plt.savefig(
			f'all_r.{args.plot_format}', dpi=args.dpi, bbox_inches='tight',
			transparent=True
		)


def process_path(snap_path, group_ids, data):
	print(snap_path)
	snap = Snapshot(snap_path)
	snaptime = snap.header['Time']
	units = units_util.get_units(snap.header)
	pos = snap.read_snapshot(f'/PartType{args.ptype}/Coordinates')
	ids = snap.read_snapshot(f'/PartType{args.ptype}/ParticleIDs')
	assert numpy.all(
		numpy.concatenate(group_ids) == numpy.sort(ids[ids >= args.ids_offset])
	)
	for g, g_data in zip(group_ids, data):
		g_mask = numpy.isin(ids, g)
		g_pos = pos[g_mask]
		g_pos_first = numpy.array([snap.box_size / 2] * 3)  #g_pos[0]  #TODO
		g_com = bin_density.coord_periodic_wrap_vec(
			snap.box_size, numpy.mean(bin_density.dist_periodic_wrap_vec(
				snap.box_size, g_pos - g_pos_first
			), axis=0) + g_pos_first
		)
		g_dist_vec = bin_density.dist_periodic_wrap_vec(
			snap.box_size, g_pos - g_com
		)
		g_dist = numpy.linalg.norm(g_dist_vec, axis=1)
		g_mean = numpy.mean(g_dist)
		g_median = numpy.median(g_dist)
		g_rms = numpy.sqrt(numpy.mean(g_dist**2))
		g_data['time_gyr'].append(snaptime * 1e-3 * units.time_in_myr)
		g_data['mean_kpc'].append(g_mean * units.length_in_kpc)
		g_data['median_kpc'].append(g_median * units.length_in_kpc)
		g_data['rms_kpc'].append(g_rms * units.length_in_kpc)


def plot_setup_time():
	plt.xlabel('$t$ / Gyr')
	plt.ylabel(r'$r_{\mathrm{' f'{args.plot_var}' r'}}$ / kpc')
	plt.xlim(left=0)
	if args.xlim:
		plt.xlim(args.xlim)
	plt.ylim(bottom=0)
	if args.ylim:
		plt.ylim(args.ylim)
	plt.grid(linestyle=':', linewidth=0.5)


def plot_setup_radius():
	plt.xlabel(r'$r_{\mathrm{' f'{args.plot_var}' r'}}(0)$ / kpc')
	plt.ylabel(r'$r_{\mathrm{' f'{args.plot_var}' r'}}(t)$ / kpc')
	if args.xlim:
		plt.xlim(args.xlim)
	if args.ylim:
		plt.ylim(args.ylim)
	plt.xscale('log')
	plt.yscale('log')
	plt.grid(linestyle=':', linewidth=0.5)


if __name__ == '__main__':
	args = parser.parse_args()
	main()

