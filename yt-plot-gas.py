#!/usr/bin/env python3
import argparse
import itertools
from pathlib import Path
import matplotlib.pyplot as plt
import numpy
import yt
import io_util
import units_util
import bin_density
import snapshot


parser = argparse.ArgumentParser(description='Create a projection plot of a '
	'the gas density in an AREPO snapshot')
parser.add_argument('--axis', choices=('0', '1', '2', 'x', 'y', 'z'),
	default='2',
	help='the projection axis')
parser.add_argument('--right-handed', type=bool,
	help='the “right_handed” argument to yt.ProjectionPlot')
parser.add_argument('--width', type=float, nargs=2,
	help='the “width” argument to yt.ProjectionPlot')
parser.add_argument('--depth', type=float,
	help='the depth of the projection (analogous to the “depth” argument to '
		'yt.OffAxisProjectionPlot)')
parser.add_argument('--depth-unit', default='Mpc',
	help='length unit for --depth (default: %(default)s)')
parser.add_argument('--buff-size', type=int, nargs=2,
	help='the “buff_size” argument to yt.ProjectionPlot')
parser.add_argument('--clim', nargs=2, type=float, default=(1e0, 1e3),
	help='the plot’s range in units of the average density (default: '
		'%(default)s)')
parser.add_argument('--colorbar-extend',
	help='setting to force for the colorbar “extend” option')
parser.add_argument('--no-colorbar', action='store_true',
	help='disable colorbar')
parser.add_argument('--figsize', type=float, nargs=2,
	help='the figure size in inches')
parser.add_argument('--dpi', type=float, default=200.,
	help='the resolution of the generated plot(s) (default: %(default)s)')
parser.add_argument('--output-path', type=Path, nargs='+',
	help='paths to write the outputs to')
parser.add_argument('path', type=Path, nargs='+',
	help='the paths to the snapshot files; any normal files are interpreted '
		'as single-file snapshots, while all files in any given directory '
		'are interpreted as a single multi-file snapshot')


# the color gets brighter when amount > 1 and darker when amount < 1
def adjust_lightness(color, amount):
	import matplotlib.colors
	import colorsys
	c = colorsys.rgb_to_hls(*matplotlib.colors.to_rgb(color))
	return colorsys.hls_to_rgb(c[0], max(0, min(1, amount * c[1])), c[2])


def main():
	if args.axis.isdigit:
		args.axis = int(args.axis)
	if args.figsize:
		plt.rc('figure', figsize=args.figsize)
	if not args.output_path:
		args.output_path = [None] * len(args.path)
	assert len(args.path) == len(args.output_path)
	# process paths
	plots = []
	for path_args in zip(args.path, args.output_path):
		plots.append(process_path(*path_args))
	# combined plot
	fig, grid = plt.subplots(1, len(plots), layout='constrained', squeeze=False)
	figsize = fig.get_size_inches()
	for ax, (proj, field) in zip(grid.flat, plots, strict=True):
		plot = proj.plots[field]
		plot.figure = fig
		plot.axes = ax
		proj.render()
	# yt manually overrides figure and axes positions (set_position) and sizes
	# in proj.render(), so reset size and re-activate constrained layout to get
	# the requested layout
	fig.set_size_inches(figsize)
	for ax in grid.flat:
		ax.set_in_layout(True)
	#TODO
	cbar_opt = {'pad': 0.02}
	if args.colorbar_extend:
		cbar_opt['extend'] = args.colorbar_extend
	proj, field = plots[-1]
	cbar = fig.colorbar(proj.plots[field].image, ax=grid, **cbar_opt)
	cbar.set_label(
		r'$\rho_{\mathrm{b}}/\langle \rho_{\mathrm{b}} \rangle$',
		labelpad=-2
	)
	# save plot
	fig.savefig(args.output_path[0], dpi=args.dpi)


def process_path(path, output_path):
	print(path)
	snap = snapshot.Snapshot(path)
	units = units_util.get_units(snap.header)
	rho_avg_baryons = (
		snap.header['OmegaBaryon'] *
		snap.constant_in_internal_units('critical_density0')
	)
	# load particle data
	if path.is_dir():
		yt_path = snap.paths[0]
	else:
		yt_path = path
	print(yt_path)
	ds = yt.load(yt_path)
	field = ('gas', 'density')
	# create projection plot
	proj_args = {}
	for key in ('right_handed', 'width', 'buff_size'):
		args_val = getattr(args, key)
		if args_val is not None:
			proj_args[key] = args_val
	if args.depth:
		depth = ds.quan(args.depth, args.depth_unit)
		left_edge = ds.domain_left_edge.copy()
		right_edge = ds.domain_right_edge.copy()
		left_edge[args.axis] = ds.domain_center[args.axis] - 0.5 * depth
		right_edge[args.axis] = ds.domain_center[args.axis] + 0.5 * depth
		proj_args['data_source'] = ds.box(left_edge, right_edge)
	proj = yt.ProjectionPlot(
		ds, args.axis, field,  fontsize=plt.rcParams['font.size'], **proj_args
	)
	zdist = snap.box_size
	proj.set_unit(
		field,
		f'{rho_avg_baryons} * code_mass/code_length**3 * {zdist} * code_length'
	)
	proj.set_zlim(field, *args.clim)
	proj.set_colorbar_label(
		field, r'$\rho_{\mathrm{b}}/\langle \rho_{\mathrm{b}} \rangle$'
	)
	proj.set_background_color('all', 'black')
	proj.annotate_scale(
		# self.corner == "lower_right": self.pos = (0.89, 0.052)
		pos=(0.8, 0.052), coeff=2.5,
		#TODO: don’t hard-code code_length as Mpc
		#TODO: yt turns fractional coeff into integer?
		unit='1e3 * code_length',
#		scale_text_format='{scale}$\,h^{{-1}}\,$Mpc',
		scale_text_format='2.5$\,h^{{-1}}\,$Mpc',
	)
	proj.hide_axes(draw_frame=True)
	if args.no_colorbar:
		proj.hide_colorbar()
	proj.plots[field].cax.tick_params(
		which='both', direction=plt.rcParams['ytick.direction']
	)
	#TODO
#	proj.save(output_path, mpl_kwargs=dict(dpi=args.dpi))
	return proj, field


if __name__ == '__main__':
	args = parser.parse_args()
	main()

