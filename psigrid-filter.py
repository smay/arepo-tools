#!/usr/bin/env python3
import argparse
import collections
import subprocess
import time
from pathlib import Path
import h5py
import numba
import numexpr
import numpy
import iterate_grid
import snapshot
from snapshot import Snapshot
from mpi_util import MPI, mpi_comm, mpi_rank, mpi_size, print0


FDM_GROUP_NAME = '/FuzzyDM'
DATASET_NAME_RE = f'{FDM_GROUP_NAME}/PsiRe'
DATASET_NAME_IM = f'{FDM_GROUP_NAME}/PsiIm'
DATASET_NAME_RHO_PROJECTED = f'{FDM_GROUP_NAME}/RhoProjected'
FDM_DENSITY_DATASET = f'{FDM_GROUP_NAME}/Density'
MAX_MEM_SIZE = 80 * 1024**3


parser = argparse.ArgumentParser(description='Post-process large snapshots of '
	'fuzzy dark matter simulations for plotting')
parser.add_argument('--type',
	choices=('downscale', 'project'), default='project',
	help='the type of filter to apply to the wave function grid')
parser.add_argument('--scale-factor', type=int,
	help='the factor by which to downscale the grid dimensions')
parser.add_argument('--datasets', nargs='+',
	default=[DATASET_NAME_RE, DATASET_NAME_IM],
	help='process the given datasets (instead of the wave function)')
parser.add_argument('--snap-chunk-mem-size', type=float,
	default=snapshot.CHUNK_MEM_SIZE_DEFAULT / 2 / 1024**2,
	help='maximum amount of memory to use for snapshot data per core, in MiB '
		'(default: %(default)g)')
parser.add_argument('--output-path', type=Path, nargs='+',
	help='paths to write the outputs to')
parser.add_argument('path', type=Path, nargs='+',
	help='the paths to the snapshot files; any normal files are interpreted '
		'as single-file snapshots, while all files in any given directory '
		'are interpreted as a single multi-file snapshot')


Param = collections.namedtuple(
	'Param', 'scale_factor, sub_data, sub_data_slice, mpi_rank',
	defaults=[False, numpy.index_exp[:, :, :], None]
)
assert Param._field_defaults.keys() == {
	'sub_data', 'sub_data_slice', 'mpi_rank'
}

# currently only implemented for the benefit of extract-snapshot-region.py;
# it might make sense to merge the two?
@numba.njit
def downscale_grid_bulk(grid, pmgrid, ix_info, data_slice, param, outputs):
	assert len(param) == len(outputs)
	with numba.objmode(t0='f8'):
		t0 = time.perf_counter()
	for idx in range(len(param)):
		mpi_rank_ = param[idx].mpi_rank
		with numba.objmode(t0='f8'):
			t1 = time.perf_counter()
			if mpi_rank_ == 0 and (idx == 0 or t1 - t0 > 30):
				print(
					(
						'\tCurrently processing sub-grid {: 4} (of {: 4}) on '
						'rank {}…'
					).format(idx, len(param), mpi_rank_),
					flush=True
				)
				t0 = t1
		downscale_grid(
			grid, pmgrid, ix_info, data_slice, param[idx], outputs[idx]
		)

@numba.njit
def downscale_grid(grid, pmgrid, ix_info, data_slice, param, out):
	ix_min, ix_start, ix_end = ix_info
	scale_factor = param.scale_factor
	assert len(grid) == len(out)
	for g in grid:
		assert g.shape == grid[0].shape
	N = grid[0].shape[-1]
	assert grid[0].shape[1:] == (N, N)
	# N <= pmgrid: size of grid being iterated
	# N_in <= N:   size of grid to be downscaled
	# 3 different cases:
	# 1) param.sub_data_slice = None, N = pmgrid, N_in = N:
	#      full box downscaling
	# 2) param.sub_data_slice = None, N < pmgrid, N_in = N:
	#      single halo downscaling
	# 3) param.sub_data_slice != None, N = pmgrid, N_in < N:
	#      bulk halo downscaling
	# This file currently only uses case 1); the others are thus far only needed
	# in extract-snapshot-region.py.
	if param.sub_data:
		target_slice = param.sub_data_slice
		ix_offset = target_slice[0].start
		ix_stop = target_slice[0].stop
		iy_offset = target_slice[1].start
		iy_stop = target_slice[1].stop
		iz_offset = target_slice[2].start
		iz_stop = target_slice[2].stop
		assert ix_stop - ix_offset == iy_stop - iy_offset == iz_stop - iz_offset
		N_in = ix_stop - ix_offset
	else:
		ix_offset = 0
		ix_stop = N
		iy_offset = 0
		iy_stop = N
		iz_offset = 0
		iz_stop = N
		N_in = N
	assert N_in <= N <= pmgrid
	assert N_in % scale_factor == 0
	N_out = N_in // scale_factor
	for o in out:
		assert o.shape == out[0].shape
		assert o.shape == (N_out, N_out, N_out)
	for grid_idx in range(len(grid)):
		g = grid[grid_idx]
		o = out[grid_idx]
		for ix in range(ix_offset, ix_stop):
			gix = ix % N
			if not (ix_start <= gix < ix_end):
				continue
			ox = new_idx(N_out, scale_factor, ix - ix_offset)
			for iy in range(iy_offset, iy_stop):
				oy = new_idx(N_out, scale_factor, iy - iy_offset)
				for iz in range(iz_offset, iz_stop):
					oz = new_idx(N_out, scale_factor, iz - iz_offset)
					lix = gix - ix_start
					giy = iy % N
					giz = iz % N
					o[ox, oy, oz] += g[lix, giy, giz]

@numba.njit
def new_idx(pmgrid_out, scale_factor, old_idx):
	offset = 0
	# alternative: to “center” the old indices around the new one
	#offset = (scale_factor - 1) // 2
	i_new = ((old_idx + offset) // scale_factor) % pmgrid_out
	return i_new

def downscale_allocate_output(
	snap, datasets, scale_factor, data_slice=..., print_messages=False
):
	assert scale_factor > 0
	data_slice_, indices = snap.read_snapshot_indices(
		f'{FDM_GROUP_NAME}/PsiRe' if datasets[0] == FDM_DENSITY_DATASET
		else datasets[0],
		data_slice
	)
	shape = numpy.broadcast(*indices).shape
	assert all(s == shape[0] for s in shape)
	pmgrid = shape[0]
	assert pmgrid % scale_factor == 0
	pmgrid_out = pmgrid // scale_factor
	if print_messages:
		print0(f'\tScaling down from {pmgrid}³ to {pmgrid_out}³')
	snap.determine_dataset_info(*[
		ds for ds in datasets if ds != FDM_DENSITY_DATASET
	])
	dtypes = [
		# Need to convert to str because h5py somehow gives dtype('<f8')
		# even though numpy.dtype('f8') results in dtype('float64')
		# (all of these compare equally as well).
		# According to numpy documentation, native byte order should always be
		# given as '=', but the dtype from h5py has dt.byteorder == '<'?
		# https://numpy.org/doc/stable/reference/generated/numpy.dtype.byteorder.html
		'f8' if ds == FDM_DENSITY_DATASET else
			str(snap.dataset_dtype[ds])
		for ds in datasets
	]
	out = tuple(
		numpy.zeros((pmgrid_out,) * 3, dtype=dt) for dt in dtypes
	)
	return out

def downscale(
	snap, datasets, scale_factor, data_slice=..., print_messages=False
):
	out = downscale_allocate_output(
		snap, datasets, scale_factor, data_slice=data_slice,
		print_messages=print_messages
	)
	param = Param(
		scale_factor=scale_factor, sub_data=False,
		sub_data_slice=numpy.index_exp[:, :, :], mpi_rank=mpi_rank
	)
	iterate_grid.iterate(
		snap, downscale_grid, out, param=param, grid_type='dataset',
		datasets=datasets, data_slice=data_slice,
		chunk_mem_size=round(args.snap_chunk_mem_size * 1024**2)
	)
	for ds, data in zip(datasets, out):
		mpi_comm.Reduce(
			MPI.IN_PLACE if mpi_rank == 0 else data, data, op=MPI.SUM,
			root=0
		)
		if mpi_rank == 0:
			data /= scale_factor**3
			if ds == FDM_DENSITY_DATASET and pmgrid == snap.pmgrid:
				rho_avg_dm = (
					(snap.header['Omega0'] - snap.header['OmegaBaryon']) *
					snap.constant_in_internal_units('critical_density0')
				)
				assert numpy.isclose(
					numpy.mean(data), rho_avg_dm, rtol=5e-3, atol=0
				), f'{numpy.mean(data)=} {rho_avg_dm=}'
	return out


if __name__ == '__main__':
	args = parser.parse_args()
	assert args.scale_factor >= 1
	output_paths_given = False
	if args.output_path:
		output_paths_given = True
		assert len(args.output_path) == len(args.path)
	else:
		args.output_path = [
			path.with_name(f'{path.name}_{args.type}') for path in args.path
		]
	for path, path_out in zip(args.path, args.output_path):
		print0(path)
		if f'_{args.type}' in path.name:
			print0('\tSnapshot already processed, skipping')
			continue
		trailing_slash = ''
		if path.is_dir():
			snap_files = sorted(path.iterdir())
			trailing_slash = '/'
			assert snap_files
		else:
			snap_files = [path]
		# read metadata
		snap = Snapshot(snap_files)
		pmgrid = snap.pmgrid
		num_files = len(snap_files)
		assert pmgrid % num_files == 0
		# process files
		if args.type == 'downscale':
			out = downscale(
				snap, args.datasets, args.scale_factor, print_messages=True
			)
			assert all(s == out[0].shape[0] for data in out for s in data.shape)
			pmgrid_out = out[0].shape[0]
			# write to output file
			if not output_paths_given:
				path_out = f'{path_out}{pmgrid_out}.hdf5'
			print0(f'\tWriting data to {path_out}')
			if mpi_rank == 0:
				with h5py.File(path_out, 'a') as fout:
					for ds, data in zip(args.datasets, out):
						fout[ds] = data
		elif args.type == 'project':
			# copy snapshot
			subprocess.run(
				['rsync', '-ahvP', f'{path}{trailing_slash}', path_out],
				check=True
			)
			# update timestamp for copied directory
			subprocess.run(['touch', path_out], check=True)
			slabs_per_file = pmgrid // num_files
			assert slabs_per_file > 1
			for snap_file in snap_files:
				print(f'\tWriting data to {path_out / snap_file.name}')
				with \
					h5py.File(snap_file, 'r') as snap_in, \
					h5py.File(path_out / snap_file.name, 'a') as snap_out \
				:
					psi_re = snap_in[DATASET_NAME_RE]
					psi_im = snap_in[DATASET_NAME_IM]
					rho = (
						numexpr.evaluate('psi_re**2 + psi_im**2')
					).reshape(slabs_per_file, pmgrid, pmgrid)
					z_proj_min = 0
					z_proj_max = pmgrid
					rho_proj = numpy.sum(
						rho[:, :, z_proj_min:z_proj_max], axis=-1
					) / (z_proj_max - z_proj_min)
					# create dataset for projected density
					snap_out[FDM_GROUP_NAME].create_dataset_like(
						DATASET_NAME_RHO_PROJECTED, snap_out[DATASET_NAME_RE],
						shape=(slabs_per_file, pmgrid), dtype=rho.dtype
					)
					del snap_out[DATASET_NAME_RE]
					del snap_out[DATASET_NAME_IM]
					snap_out[DATASET_NAME_RHO_PROJECTED][:] = rho_proj
		else:
			assert False

