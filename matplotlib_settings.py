import matplotlib.pyplot as plt

plt.rc('xtick', top=True)
plt.rc('ytick', right=True)
plt.rc('xtick.minor', visible=True)
plt.rc('ytick.minor', visible=True)
plt.rc('savefig', bbox='tight', pad_inches=0.05, transparent=True)
plt.rc('figure', dpi=600)
plt.rc('figure.constrained_layout', use=True, h_pad=0.01, w_pad=0.01)

plt.rc('text', usetex=True)
plt.rc('pgf', texsystem='lualatex', rcfonts=False)

plt.rc('font', family='serif', size=10.95)
plt.rc('legend', fontsize='small')
# default:
#plt.rc('lines', linewidth=1.5)
plt.rc('pgf', preamble=r'''
	% workaround for https://github.com/matplotlib/matplotlib/issues/26892
	\usepackage[fontsize=12pt]{scrextend}
	\usepackage{unicode-math}
	\setmainfont{TeX Gyre Pagella}
	\setsansfont{TeX Gyre Pagella}
	\setmathfont{TeX Gyre Pagella Math}
''')
# default:
# figure.figsize:     6.4, 4.8  # figure size in inches
plt.rc('figure', figsize=(6.2, 3.7))
