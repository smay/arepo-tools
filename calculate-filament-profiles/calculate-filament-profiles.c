// NOTE: only for FDM!
#include <assert.h>
#include <complex.h>
#include <inttypes.h>
#include <limits.h>
#include <math.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdnoreturn.h>
#include <string.h>

//#include <getopt.h>
#include <hdf5.h>
// use FFTW3 for grid data distribution
#include <fftw3-mpi.h>
#include <mpi.h>

// TODO: HDF5 ngrid attribute
// TODO: de-duplicate with phase-to-velocity.c

#ifdef __STDC_NO_COMPLEX__
#error "__STDC_NO_COMPLEX__ defined, complex math not supported"
#endif

#if !defined(__has_attribute) && !defined(__GNUC__)
// remove GCC-style attributes if the compiler does not support them
#define __attribute__(x)
#endif


#define ARRAY_LEN(x) ((sizeof (x)) / (sizeof (x)[0]))

typedef unsigned char uchar;
typedef unsigned short ushort;
typedef unsigned int uint;
typedef unsigned long ulong;
typedef unsigned long long ulonglong;
typedef size_t LocalIdx;
struct hdf5_dset_info {
	const char *path;
	uint num_dims;
	hid_t dset[2];
	hid_t dtype[2];
	hid_t dspace[2];
};
typedef void (*process_buffer_func)(
	void *, const void *, struct hdf5_dset_info, LocalIdx, LocalIdx
);

#ifndef M_PI
const double M_PI = 3.14159265358979323846;
#endif
const double HBAR   = 1.0545718176461565e-34;  // J * s
const double EV     = 1.602176634e-19;  // J
const double CLIGHT = 299792458.0;  // m/s
const double KPC_IN_CM       = 3.085677581491367e21;
const double SOLAR_MASS_IN_G = 1.988409870698051e33;

enum {
	MAXLEN_PATH = 512,
	NUMDIMS = 3,
	R_BINS = 1000
};
const char *const HDF5_GROUP_NAME = "/FuzzyDM";
const char *const HDF5_DATASET_NAME[] = {"PsiRe", "PsiIm"};
const size_t BUFFER_SIZE = 32 * ((size_t) 1024 * 1024);

struct filament_data {
	uint num_filaments;
	uint *nsamp;
	double (**filaments)[NUMDIMS];
	double (**shifts)[NUMDIMS];
};
struct profile_data {
	double r0_kpc;
	double r1_kpc;
	double (*rho_ms_kpc3)[R_BINS];
	uint (*count)[R_BINS];
	size_t num_profiles;
};
enum bin_spacing {
	SPACING_LINEAR, SPACING_LOG
};

int WORLD_RANK;
int WORLD_SIZE;
uint FILES_OPEN_IN_PARALLEL = 80;
struct {
	size_t local_num_el;
	uint local_nx;
	uint local_x_start;
	uint nx;
	uint ny;
	uint nz;
	uint n[NUMDIMS];
	uint64_t num_cells_tot;
	uint *slabs_x_per_rank;
} GRID;
struct {
	uint num_files;
	char snap_file_base[MAXLEN_PATH];
	double hbar;
	double mass;
	double mass_ev;
	double box_size;
	double a;
	double HubbleParam;
	double UnitLength_in_cm;
	double UnitMass_in_g;
	double UnitTime_in_s;
} PARAM;
struct {
	double length_in_kpc;
	double mass_in_solar_mass;
} UNITS;


static inline LocalIdx IDX(const uint x, const uint y, const uint z) {
	return (LocalIdx) (x * GRID.ny + y) * GRID.nz + z;
}

static inline ulong umin(const ulong a, const ulong b) {
	return a < b ? a : b;
}

static inline bool isclose(
	const double a, const double b, const double rtol, const double atol
) {
	return fabs(a - b) <= fmax(rtol * fmax(fabs(a), fabs(b)), atol);
}

static inline double time(void) {
	return MPI_Wtime();
}

void __attribute__((format(printf, 1, 2))) printf0(
	const char *const format, ...
) {
	if (WORLD_RANK == 0) {
		va_list args;
		va_start(args, format);
		vprintf(format, args);
		va_end(args);
		fflush(stdout);
	}
}

#define terminate(format, ...) \
	terminate_fullinfo(__FILE__, __LINE__, __func__, (format), __VA_ARGS__)
noreturn void terminate_fullinfo(
	const char *const file, const int line, const char *const func, 
	const char *const format, ...
) {
	va_list args;
	va_start(args, format);
	fprintf(
		stderr,
		"TERMINATE: Rank=%d in %s:%d, %s():\n\t", WORLD_RANK, file, line, func
	);
	vfprintf(stderr, format, args);
	fprintf(stderr, "\n");
	va_end(args);
	exit(EXIT_FAILURE);
}

void __attribute__((format(printf, 2, 3))) file_path_sprintf(
	char *const buf, const char *const fmt, ...
) {
	va_list l;
	va_start(l, fmt);
	const int status = vsnprintf(buf, MAXLEN_PATH, fmt, l);
	if (status >= MAXLEN_PATH)
		terminate(
			"length of file path exceeds MAXLEN_PATH = %u: %s",
			MAXLEN_PATH, buf
		);
	va_end(l);
}

bool file_exists(const char *const path) {
	FILE *file = NULL;
	if ((file = fopen(path, "rb"))) {
		fclose(file);
		return true;
	}
	return false;
}

struct split_num_exp { double significand; int exponent; }
split_num_exp(const double x) {
	const int exponent = floor(log10(fabs(x)));
	const double significand = x / pow(10, exponent);
	return (struct split_num_exp){significand, exponent};
}

double unit_isclose_power10(const double x) {
	struct split_num_exp s = split_num_exp(x);
	if (isclose(s.significand, 1., 1e-3, 0))
		return pow(10, s.exponent);
	else
		return NAN;
}


// copied/adapted from AREPO get_file_path()
static char *get_file_path(const char *const snap_path, const uint filenr) {
	char *path = malloc(MAXLEN_PATH * sizeof *path);
	assert(path);
	if (PARAM.num_files > 1) {
		// determine directory of given file
		char snap_dir[MAXLEN_PATH];
		strcpy(snap_dir, snap_path);
		strrchr(snap_dir, '/')[0] = '\0';
		// determine snapshot number
		const char *snap_num_substr = strrchr(snap_path, '_');
		assert(snap_num_substr);
		const long snap_num = strtoul(snap_num_substr + 1, NULL, 10);
		// build resulting path for snapshot file with the given filenr
		file_path_sprintf(
			path, "%s/%s_%03lu.%d.hdf5", snap_dir, PARAM.snap_file_base,
			snap_num, filenr
		);
	}
	else {
		strcpy(path, snap_path);
	}
	return path;
}


// copied/adapted from AREPO fdm_read_grid_dims()
static void read_grid_dims(
	const char *const path, const uint filenr, uint *const num_dims,
	hsize_t (*const num_cells)[NUMDIMS], hssize_t *const num_cells_tot
) {
	// open HDF5 resources
	hid_t hdf5_file = H5Fopen(path, H5F_ACC_RDONLY, H5P_DEFAULT);
	hid_t hdf5_grp = H5Gopen(hdf5_file, HDF5_GROUP_NAME, H5P_DEFAULT);
	assert(hdf5_file != H5I_INVALID_HID && hdf5_grp != H5I_INVALID_HID);
	hid_t hdf5_dset[2];
	hid_t hdf5_dspace[2];
	int ndims[2];
	for (uint cpart = 0; cpart < 2; cpart++) {
		hdf5_dset[cpart] =
			H5Dopen(hdf5_grp, HDF5_DATASET_NAME[cpart], H5P_DEFAULT);
		hdf5_dspace[cpart] = H5Dget_space(hdf5_dset[cpart]);
		assert(
			hdf5_dset[cpart] != H5I_INVALID_HID &&
			hdf5_dspace[cpart] != H5I_INVALID_HID
		);
		ndims[cpart] = H5Sget_simple_extent_ndims(hdf5_dspace[cpart]);
		assert(ndims[cpart] > 0);
	}
	// consistency checks
	if (ndims[0] != ndims[1])
		terminate(
			"Number of dimensions different between datasets %s and %s: "
			"%d != %d",
			HDF5_DATASET_NAME[0], HDF5_DATASET_NAME[1], ndims[0], ndims[1]
		);
	num_dims[filenr] = ndims[0];
	const uint nd = num_dims[filenr];
	if (nd != 1 && nd != NUMDIMS)
		terminate(
			"Unexpected number of dimensions for datasets %s and %s: %d",
			HDF5_DATASET_NAME[0], HDF5_DATASET_NAME[1], nd
		);
	// read number of cells for this file
	const int nd_check =
		H5Sget_simple_extent_dims(hdf5_dspace[0], num_cells[filenr], NULL);
	assert(nd_check == (int) nd);
	num_cells_tot[filenr] = H5Sget_simple_extent_npoints(hdf5_dspace[0]);
	assert(num_cells_tot[filenr] > 0);
	// confirm that the grid data is divided into complete slabs
	if (
		(num_cells_tot[filenr] % (GRID.ny * GRID.nz) != 0) ||
		(nd >= 2 && num_cells[filenr][1] != GRID.ny) ||
		(nd >= 3 && num_cells[filenr][2] != GRID.nz)
	)
		terminate(
			"Input file %s contains incomplete slabs!\n"
			"num_dims=%u, num_cells_tot=%" PRId64 ", num_cells[y]=%" PRIu64
			", num_cells[z]=%" PRIu64 ", GRID.ny=%td, GRID.nz=%td",
			path, nd, (int64_t) num_cells_tot[filenr],
			(uint64_t) (nd >= 2 ? num_cells[1] : 0),
			(uint64_t) (nd >= 3 ? num_cells[2] : 0),
			GRID.ny, GRID.nz
		);
	// close HDF5 resources
	for (uint cpart = 0; cpart < 2; cpart++) {
		H5Sclose(hdf5_dspace[cpart]);
		H5Dclose(hdf5_dset[cpart]);
	}
	H5Gclose(hdf5_grp);
	H5Fclose(hdf5_file);
}


// copied/adapted from AREPO fdm_read_ic_grid()
// (split into read_wave_function(), read_file(), and read_datasets())
static void read_datasets(
	const struct hdf5_dset_info hdf5_ds, const LocalIdx read_offset,
	const LocalIdx read_count, const LocalIdx thisrank_read, void *buffer_tmp,
	void *buffer_out, process_buffer_func process_buffer
) {
	// memory dataspace
	const hid_t hdf5_dataspace_in_memory =
		H5Screate_simple(1, (hsize_t []){2 * read_count}, NULL);
	assert(hdf5_dataspace_in_memory != H5I_INVALID_HID);
	// file dataspace selection
	hsize_t start[NUMDIMS] = {0};
	hsize_t count[NUMDIMS];
	if (hdf5_ds.num_dims == 1) {
		start[0] = read_offset;
		count[0] = read_count;
	}
	else {
		// TODO: this doesn’t work yet for num_dims > 1
		assert(false);
	}
	// read data and process for output buffer
	for (uint cpart = 0; cpart < 2; cpart++) {
		H5Sselect_hyperslab(
			hdf5_dataspace_in_memory, H5S_SELECT_SET, (hsize_t []){cpart},
			(hsize_t []){2}, (hsize_t []){read_count}, NULL
		);
		H5Sselect_hyperslab(
			hdf5_ds.dspace[cpart], H5S_SELECT_SET, start, NULL, count, NULL
		);
		const herr_t status = H5Dread(
			hdf5_ds.dset[cpart], hdf5_ds.dtype[cpart], hdf5_dataspace_in_memory,
			hdf5_ds.dspace[cpart], H5P_DEFAULT, buffer_tmp
		);
		assert(status >= 0);
	}
	process_buffer(buffer_out, buffer_tmp, hdf5_ds, thisrank_read, read_count);
	// close HDF5 resources
	H5Sclose(hdf5_dataspace_in_memory);
}


// copied/adapted from AREPO fdm_read_ic_grid()
// (split into read_wave_function(), read_file(), and read_datasets())
static LocalIdx read_file(
	const char *const path, const uint num_dims, const hssize_t num_cells_tot,
	const LocalIdx rank_cells, LocalIdx thisrank_read,
	const uint64_t cells_read_this_file, void *buffer_tmp, void *buffer_out,
	process_buffer_func process_buffer
) {
	// open HDF5 resources
	const hid_t hdf5_file = H5Fopen(path, H5F_ACC_RDONLY, H5P_DEFAULT);
	const hid_t hdf5_grp = H5Gopen(hdf5_file, HDF5_GROUP_NAME, H5P_DEFAULT);
	assert(hdf5_file != H5I_INVALID_HID && hdf5_grp != H5I_INVALID_HID);
	struct hdf5_dset_info hdf5_ds;
	hdf5_ds.path = path;
	hdf5_ds.num_dims = num_dims;
	for (uint cpart = 0; cpart < 2; cpart++) {
		hdf5_ds.dset[cpart] =
			H5Dopen(hdf5_grp, HDF5_DATASET_NAME[cpart], H5P_DEFAULT);
		hdf5_ds.dspace[cpart] = H5Dget_space(hdf5_ds.dset[cpart]);
		assert(
			hdf5_ds.dset[cpart] != H5I_INVALID_HID &&
			hdf5_ds.dspace[cpart] != H5I_INVALID_HID
		);
		hdf5_ds.dtype[cpart] = H5Dget_type(hdf5_ds.dset[cpart]);
	}
	// metadata consistency checks
	if (H5Tequal(hdf5_ds.dtype[0], hdf5_ds.dtype[1]) <= 0)
		terminate(
			"Datatypes differ between datasets %s and %s",
			HDF5_DATASET_NAME[0], HDF5_DATASET_NAME[1]
		);
	if (H5Sextent_equal(hdf5_ds.dspace[0], hdf5_ds.dspace[1]) <= 0)
		terminate(
			"Dataspace extent differs between datasets %s and %s",
			HDF5_DATASET_NAME[0], HDF5_DATASET_NAME[1]
		);
	// read from file in chunks of the buffer size until all cells
	// for this rank have been read or the file ends
	const size_t grid_buffer_size =
		BUFFER_SIZE / (2 * H5Tget_size(hdf5_ds.dtype[0]));
	LocalIdx thisrank_read_this_file = 0;
	uint64_t cells_remaining_in_file = num_cells_tot - cells_read_this_file;
	assert(cells_remaining_in_file > 0);
	while (thisrank_read < rank_cells && cells_remaining_in_file > 0) {
		cells_remaining_in_file = num_cells_tot - cells_read_this_file -
			thisrank_read_this_file;
		LocalIdx read_count = rank_cells - thisrank_read;
		if (cells_remaining_in_file < read_count)
			read_count = cells_remaining_in_file;
		if (grid_buffer_size < read_count)
			read_count = grid_buffer_size;
		read_datasets(
			hdf5_ds, cells_read_this_file + thisrank_read_this_file, read_count,
			thisrank_read, buffer_tmp, buffer_out, process_buffer
		);
		// increment counters
		thisrank_read += read_count;
		thisrank_read_this_file += read_count;
	}
	assert(
		cells_remaining_in_file != 0 ||
		(hssize_t) (cells_read_this_file + thisrank_read_this_file) ==
			num_cells_tot
	);
	// close HDF5 resources
	for (uint cpart = 0; cpart < 2; cpart++) {
		H5Tclose(hdf5_ds.dtype[cpart]);
		H5Sclose(hdf5_ds.dspace[cpart]);
		H5Dclose(hdf5_ds.dset[cpart]);
	}
	H5Gclose(hdf5_grp);
	H5Fclose(hdf5_file);
	return thisrank_read;
}


// copied/adapted from AREPO fdm_read_ic_grid()
// (split into read_wave_function(), read_file(), and read_datasets())
static void read_wave_function(
	const char *const snap_path, void *buffer_out,
	process_buffer_func process_buffer
) {
	assert(PARAM.num_files > 0);
	// in order to determine which rank needs to read which files, first read
	// grid sizes sequentially on rank 0 (for simplicity)
	uint num_dims;
	uint num_dims_files[PARAM.num_files];
	hsize_t num_cells[PARAM.num_files][NUMDIMS];
	hssize_t num_cells_tot[PARAM.num_files];
	if (WORLD_RANK == 0) {
		for (uint filenr = 0; filenr < PARAM.num_files; filenr++) {
			char *path = get_file_path(snap_path, filenr);
			read_grid_dims(
				path, filenr, num_dims_files, num_cells, num_cells_tot
			);
			free(path);
			// check num_dims consistency
			if (num_dims_files[filenr] != num_dims_files[0])
				terminate(
					"Number of dimensions for %s and %s differs between input "
					"files 0 and %d: %d != %d",
					HDF5_DATASET_NAME[0], HDF5_DATASET_NAME[1], filenr,
					num_dims_files[0], num_dims_files[filenr]
				);
		}
		num_dims = num_dims_files[0];
	}
	MPI_Bcast(&num_dims, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
	MPI_Bcast(num_cells, sizeof num_cells, MPI_BYTE, 0, MPI_COMM_WORLD);
	MPI_Bcast(
		num_cells_tot, sizeof num_cells_tot, MPI_BYTE, 0, MPI_COMM_WORLD
	);
	// verify grid size consistency
	uint64_t cells_total = 0;
	for (uint filenr = 0; filenr < PARAM.num_files; filenr++)
		cells_total += num_cells_tot[filenr];
	if (cells_total != GRID.num_cells_tot)
		terminate(
			"%" PRIu64 " = number of wave function cells != "
			"GRID.nx * GRID.ny * GRID.nz = %td * %td * %td = %" PRIu64,
			cells_total, GRID.nx, GRID.ny, GRID.nz, GRID.num_cells_tot
		);
	// read grid data from snapshot files in parallel
	void *buffer_tmp = malloc(BUFFER_SIZE);
	uint filenr = 0;
	int rank = 0;
	while (GRID.slabs_x_per_rank[rank] == 0 && rank < WORLD_SIZE)
		rank++;
	uint64_t cells_read = 0;
	uint64_t cells_read_this_file = 0;
	LocalIdx thisrank_read = 0;
	while (cells_read < cells_total) {
		assert(rank < WORLD_SIZE);
		assert(filenr < PARAM.num_files);
		uint open_files = 0;
		LocalIdx rank_cells_remaining = 0;
		while (
			open_files < FILES_OPEN_IN_PARALLEL && filenr < PARAM.num_files
		) {
			LocalIdx rank_cells =
				(LocalIdx) GRID.slabs_x_per_rank[rank] * GRID.ny * GRID.nz;
			if (rank_cells_remaining == 0)
				rank_cells_remaining = rank_cells;
			// if the current rank is active, read the current file until
			// either all cells for this rank have been read or the file ends
			if (rank == WORLD_RANK) {
				assert(thisrank_read < rank_cells);
				assert(rank_cells_remaining <= rank_cells);
				char *path = get_file_path(snap_path, filenr);
				printf0("Reading file %u on rank %d…\n", filenr, WORLD_RANK);
				thisrank_read = read_file(
					path, num_dims, num_cells_tot[filenr], rank_cells,
					thisrank_read, cells_read_this_file, buffer_tmp, buffer_out,
					process_buffer
				);
				free(path);
			}
			// if the current file has ended and the current rank still has
			// cells to read: open the next file on the same rank
			if (
				(hssize_t) (cells_read_this_file + rank_cells_remaining) >
				num_cells_tot[filenr]
			) {
				LocalIdx rank_cells_read =
					num_cells_tot[filenr] - cells_read_this_file;
				cells_read += rank_cells_read;
				rank_cells_remaining -= rank_cells_read;
				filenr++;
				cells_read_this_file = 0;
			}
			// the current rank has read all of its cells: open the same file
			// (or the next, if the current file has ended at the same time)
			// on the next rank
			else {
				cells_read += rank_cells_remaining;
				cells_read_this_file += rank_cells_remaining;
				rank_cells_remaining = 0;
				rank++;
				open_files++;
				if ((hssize_t) cells_read_this_file == num_cells_tot[filenr]) {
					filenr++;
					cells_read_this_file = 0;
				}
			}
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
	// consistency checks
	LocalIdx thisrank_cells = (LocalIdx) GRID.local_nx * GRID.ny * GRID.nz;
	assert(thisrank_read == thisrank_cells);
	assert(cells_read == cells_total);
	// finalize
	free(buffer_tmp);
}


static void process_buffer_rho(
	void *buffer_out, const void *const psi_buf,
	const struct hdf5_dset_info hdf5_ds, const LocalIdx start,
	const LocalIdx length
) {
	double *rho = buffer_out;
	const hid_t hdf5_float = H5Tcopy(H5T_NATIVE_FLOAT);
	const hid_t hdf5_double = H5Tcopy(H5T_NATIVE_DOUBLE);
	const hid_t hdf5_dt = hdf5_ds.dtype[0];
	const bool is_float = H5Tequal(hdf5_dt, hdf5_float) > 0;
	const bool is_double = H5Tequal(hdf5_dt, hdf5_double) > 0;
	for (LocalIdx i = 0; i < length; i++) {
		double complex psi_i;
		if (is_float)
			psi_i = ((float complex *) psi_buf)[i];
		else if (is_double)
			psi_i = ((double complex *) psi_buf)[i];
		else
			terminate(
				"Unexpected HDF5 data type in input file %s", hdf5_ds.path
			);
		rho[start + i] = pow(creal(psi_i), 2) + pow(cimag(psi_i), 2);
	}
	H5Tclose(hdf5_float);
	H5Tclose(hdf5_double);
}


static void set_units(const hid_t hdf5_header) {
	hid_t hdf5_attr;
	herr_t status;
	hdf5_attr = H5Aopen_name(hdf5_header, "HubbleParam");
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &PARAM.HubbleParam);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	hdf5_attr = H5Aopen_name(hdf5_header, "UnitLength_in_cm");
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &PARAM.UnitLength_in_cm);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	hdf5_attr = H5Aopen_name(hdf5_header, "UnitMass_in_g");
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &PARAM.UnitMass_in_g);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	double UnitVelocity_in_cm_per_s;
	hdf5_attr = H5Aopen_name(hdf5_header, "UnitVelocity_in_cm_per_s");
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &UnitVelocity_in_cm_per_s);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	PARAM.UnitTime_in_s = PARAM.UnitLength_in_cm / UnitVelocity_in_cm_per_s;
	// apply units
	PARAM.hbar = HBAR * 1e7;
	PARAM.hbar *= pow(PARAM.HubbleParam, 2) / pow(PARAM.UnitLength_in_cm, 2) *
		1 / PARAM.UnitMass_in_g * PARAM.UnitTime_in_s;
	PARAM.mass = PARAM.mass_ev * 1e3 * EV / pow(CLIGHT, 2);
	PARAM.mass *= PARAM.HubbleParam / PARAM.UnitMass_in_g;
	// get factors for “astrophysical” system of units
	UNITS.length_in_kpc = PARAM.UnitLength_in_cm / KPC_IN_CM;
	const double isclose_length = unit_isclose_power10(UNITS.length_in_kpc);
	if (isfinite(isclose_length))
		UNITS.length_in_kpc = isclose_length;
	UNITS.mass_in_solar_mass = PARAM.UnitMass_in_g / SOLAR_MASS_IN_G;
	const double isclose_mass = unit_isclose_power10(UNITS.mass_in_solar_mass);
	if (isfinite(isclose_mass))
		UNITS.mass_in_solar_mass = isclose_mass;
}


void read_metadata(const char *snap_path) {
	// read snapshot metadata
	const hid_t hdf5_file = H5Fopen(snap_path, H5F_ACC_RDONLY, H5P_DEFAULT);
	assert(hdf5_file != H5I_INVALID_HID);
	const hid_t hdf5_config = H5Gopen(hdf5_file, "/Config", H5P_DEFAULT);
	assert(hdf5_config != H5I_INVALID_HID);
	const hid_t hdf5_param = H5Gopen(hdf5_file, "/Parameters", H5P_DEFAULT);
	assert(hdf5_param != H5I_INVALID_HID);
	const hid_t hdf5_header = H5Gopen(hdf5_file, "/Header", H5P_DEFAULT);
	assert(hdf5_header != H5I_INVALID_HID);
	hid_t hdf5_attr;
	herr_t status;
	double pmgrid_attr;
	hdf5_attr = H5Aopen_name(hdf5_config, "PMGRID");
	const hid_t pmgrid_type = H5Aget_type(hdf5_attr);
	assert(H5Tequal(pmgrid_type, H5T_NATIVE_DOUBLE) > 0);
	H5Tclose(pmgrid_type);
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &pmgrid_attr);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	const uint pmgrid = pmgrid_attr;
	assert(pmgrid == pmgrid_attr);
	hdf5_attr = H5Aopen_name(hdf5_param, "NumFilesPerSnapshot");
	status = H5Aread(hdf5_attr, H5T_NATIVE_UINT, &PARAM.num_files);
	assert(status >= 0);
	assert(PARAM.num_files > 0);
	H5Aclose(hdf5_attr);
	for (size_t i = 0; i < sizeof PARAM.snap_file_base; i++)
		PARAM.snap_file_base[i] = 0;
	hdf5_attr = H5Aopen_name(hdf5_param, "SnapshotFileBase");
	const hid_t atype = H5Aget_type(hdf5_attr);
	assert(H5Tget_class(atype) == H5T_STRING);
	assert(H5Tis_variable_str(atype) == 0);
	assert(H5Tget_size(atype) < sizeof PARAM.snap_file_base);
	status = H5Aread(hdf5_attr, atype, PARAM.snap_file_base);
	assert(status >= 0);
	H5Tclose(atype);
	H5Aclose(hdf5_attr);
	hdf5_attr = H5Aopen_name(hdf5_header, "BoxSize");
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &PARAM.box_size);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	hdf5_attr = H5Aopen_name(hdf5_param, "AxionMassEv");
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &PARAM.mass_ev);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	hdf5_attr = H5Aopen_name(hdf5_header, "Time");
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &PARAM.a);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	// units
	set_units(hdf5_header);
	// close HDF5 resources
	H5Gclose(hdf5_config);
	H5Gclose(hdf5_param);
	H5Gclose(hdf5_header);
	H5Fclose(hdf5_file);
	// initialize GRID
	GRID.nx = GRID.n[0] = pmgrid;
	GRID.ny = GRID.n[1] = pmgrid;
	GRID.nz = GRID.n[2] = pmgrid;
	GRID.num_cells_tot = 1;
	for (uint i = 0; i < ARRAY_LEN(GRID.n); i++)
		GRID.num_cells_tot *= GRID.n[i];
	ptrdiff_t local_nx, local_x_start;
	GRID.local_num_el = fftw_mpi_local_size_3d(
		GRID.nx, GRID.ny, GRID.nz, MPI_COMM_WORLD, &local_nx, &local_x_start
	);
	ptrdiff_t local_nx_many, local_x_start_many;
	const size_t local_num_el_many = fftw_mpi_local_size_many(
		2, (ptrdiff_t []){GRID.nx, GRID.ny}, GRID.nz,
		FFTW_MPI_DEFAULT_BLOCK, MPI_COMM_WORLD,
		&local_nx_many, &local_x_start_many
	);
	GRID.local_nx = local_nx;
	GRID.local_x_start = local_x_start;
	assert(local_nx == local_nx_many);
	assert(local_x_start == local_x_start_many);
	assert(GRID.local_num_el >= (size_t) GRID.local_nx * GRID.ny * GRID.nz);
	assert(GRID.local_num_el >= local_num_el_many);
	GRID.slabs_x_per_rank = malloc(WORLD_SIZE * sizeof *GRID.slabs_x_per_rank);
	MPI_Allgather(
		&GRID.local_nx, 1, MPI_UNSIGNED,
		GRID.slabs_x_per_rank, 1, MPI_UNSIGNED, MPI_COMM_WORLD
	);
}


static void read_line(char *line, const size_t count, FILE *f) {
	const char *success = fgets(line, count, f);
	const size_t len = strlen(line);
	assert(len > 0);
	assert(success && line[len - 1] == '\n');
}


static char *advance_line(char *line) {
	while (
		line[0] == ' ' ||
		line[0] == '[' ||
		line[0] == ']' ||
		line[0] == ','
	)
		line++;
	return line;
}

static struct filament_data read_NDskl_ascii(const char *const path) {
	enum { MAX_LINE_LEN = 1024 };
	char line[MAX_LINE_LEN];
	char *l;
	char *l_next;
	struct filament_data result = { 0 };
	FILE *f = fopen(path, "rb");
	if (!f)
		terminate("Could not open “%s”", path);
	read_line(line, ARRAY_LEN(line), f);
	assert(strncmp(line, "ANDSKEL", strlen("ANDSKEL")) == 0);
	read_line(line, ARRAY_LEN(line), f);
	const ulong ndims = strtoul(line, NULL, 10);
	assert(ndims == NUMDIMS);
	read_line(line, ARRAY_LEN(line), f);
	assert(strncmp(line, "#", strlen("#")) == 0);
	read_line(line, ARRAY_LEN(line), f);
	assert(strncmp(line, "BBOX", strlen("BBOX")) == 0);
	l = line;
	l += strlen("BBOX");
	l = advance_line(l);
	long x0[NUMDIMS];
	for (uint i = 0; i < ARRAY_LEN(x0); i++) {
		x0[i] = strtol(l, &l_next, 10);
		l = l_next;
		l = advance_line(l);
		assert(x0[i] == 0);
	}
	l = advance_line(l);
	ulong dx[NUMDIMS];
	for (uint i = 0; i < ARRAY_LEN(dx); i++) {
		dx[i] = strtoul(l, &l_next, 10);
		l = l_next;
		l = advance_line(l);
		assert(dx[i] == dx[0]);
		assert(dx[i] > 0);
		assert(GRID.n[i] % dx[i] == 0);
	}
	const uint ndskl_ngrid = dx[0];
	const double box_size_kpc = PARAM.box_size * UNITS.length_in_kpc;
	const double coord_fac = box_size_kpc / ndskl_ngrid;
	read_line(line, ARRAY_LEN(line), f);
	assert(
		strncmp(line, "[CRITICAL POINTS]", strlen("[CRITICAL POINTS]")) == 0
	);
	while (strncmp(line, "[FILAMENTS]", strlen("[FILAMENTS]"))) {
		read_line(line, ARRAY_LEN(line), f);
		assert(!feof(f));
	}
	read_line(line, ARRAY_LEN(line), f);
	result.num_filaments = strtoul(line, NULL, 10);
	assert(result.num_filaments > 0);
	result.nsamp = malloc(result.num_filaments * sizeof *result.nsamp);
	result.filaments = malloc(result.num_filaments * sizeof *result.filaments);
	for (uint fil_idx = 0; fil_idx < result.num_filaments; fil_idx++) {
		read_line(line, ARRAY_LEN(line), f);
		l = line;
//		const ulong cp1 =
		strtoul(l, &l_next, 10);
		l = l_next;
//		const ulong cp2 =
		strtoul(l, &l_next, 10);
		l = l_next;
		const ulong nsamp = strtoul(l, &l_next, 10);
		l = l_next;
		assert(nsamp > 1);
		result.nsamp[fil_idx] = nsamp;
		double (*filament)[NUMDIMS] = malloc(nsamp * sizeof *filament);
		result.filaments[fil_idx] = filament;
		for (ulong point_idx = 0; point_idx < nsamp; point_idx++) {
			read_line(line, ARRAY_LEN(line), f);
			l = line;
			filament[point_idx][0] = strtod(l, &l_next) * coord_fac;
			l = l_next;
			filament[point_idx][1] = strtod(l, &l_next) * coord_fac;
			l = l_next;
			filament[point_idx][2] = strtod(l, &l_next) * coord_fac;
			l = l_next;
		}
		result.filaments[fil_idx] = filament;
	}
	read_line(line, ARRAY_LEN(line), f);
	assert(
		strncmp(
			line, "[CRITICAL POINTS DATA]", strlen("[CRITICAL POINTS DATA]")
		) == 0
	);
	fclose(f);
	return result;
}


static struct filament_data alloc_filament_shifts(
	struct filament_data fil_info
) {
	fil_info.shifts = malloc(fil_info.num_filaments * sizeof *fil_info.shifts);
	for (uint fil_idx = 0; fil_idx < fil_info.num_filaments; fil_idx++) {
		const uint fil_len = fil_info.nsamp[fil_idx];
		assert(fil_len > 1);
		fil_info.shifts[fil_idx] =
			malloc((fil_len - 1) * sizeof *fil_info.shifts[fil_idx]);
	}
	return fil_info;
}


static struct filament_data read_filament_shifts(
	const char *const path, struct filament_data fil_info
) {
	// open HDF5 file
	const hid_t hdf5_file = H5Fopen(path, H5F_ACC_RDONLY, H5P_DEFAULT);
	assert(hdf5_file != H5I_INVALID_HID);
	// read data for each filament
	for (uint fil_idx = 0; fil_idx < fil_info.num_filaments; fil_idx++) {
		const uint fil_len = fil_info.nsamp[fil_idx];
		assert(fil_len > 1);
		// read HDF5 dataset
		char dset_name[16] = { 0 };
		snprintf(dset_name, sizeof dset_name, "%u", fil_idx);
		const hid_t hdf5_dset = H5Dopen(hdf5_file, dset_name, H5P_DEFAULT);
		assert(hdf5_dset != H5I_INVALID_HID);
		const hid_t hdf5_dspace = H5Dget_space(hdf5_dset);
		assert(hdf5_dspace != H5I_INVALID_HID);
		const int ndims = H5Sget_simple_extent_ndims(hdf5_dspace);
		assert(ndims == 2);
		hsize_t dims[2];
		const int ndims_check =
			H5Sget_simple_extent_dims(hdf5_dspace, dims, NULL);
		assert(ndims_check == 2);
		assert(dims[0] == fil_len - 1);
		assert(dims[1] == NUMDIMS);
		const herr_t status = H5Dread(
			hdf5_dset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT,
			fil_info.shifts[fil_idx]
		);
		assert(status >= 0);
		// close HDF5 dataset
		H5Sclose(hdf5_dspace);
		H5Dclose(hdf5_dset);
	}
	// close HDF5 file
	H5Fclose(hdf5_file);
	// return result
	return fil_info;
}


static void get_bin_edges(
	double *const result, size_t num_bins, const double r0, const double r1,
	const bool central_bin, const enum bin_spacing spacing
) {
	const uint offset = central_bin;
	num_bins -= offset;
	const size_t result_len = num_bins + 1;
	result[0] = 0;
	switch (spacing) {
		case SPACING_LOG: {
			const double binfac = num_bins / log(r1 / r0);
			for (size_t i = 0; i < result_len; i++)
				result[offset + i] = r0 * exp(i / binfac);
			break;
		}
		case SPACING_LINEAR: {
			const double binfac = num_bins / (r1 - r0);
			for (size_t i = 0; i < result_len; i++)
				result[offset + i] = r0 + i / binfac;
			break;
		}
		default: assert(false);
	}
	assert(offset + result_len - 1 == R_BINS);
	assert(isclose(result[R_BINS], r1, 1e-6, 0));
}

//TODO: split into two functions to create and write to file
static void save_data(
	const char *const path, const struct filament_data filament_info,
	const size_t filament_start, const struct profile_data profiles
) {
	herr_t status;
	const bool existing_file = file_exists(path);
	const hid_t hdf5_file = existing_file ?
		H5Fopen(path, H5F_ACC_RDWR, H5P_DEFAULT) :
		H5Fcreate(path, H5F_ACC_EXCL, H5P_DEFAULT, H5P_DEFAULT);
	assert(hdf5_file != H5I_INVALID_HID);
	const hid_t hdf5_dataspace_file = H5Screate_simple(
		2, (hsize_t []){filament_info.num_filaments, R_BINS}, NULL
	);
	assert(hdf5_dataspace_file != H5I_INVALID_HID);
	hid_t hdf5_dataset_rho = H5I_INVALID_HID;
	hid_t hdf5_dataset_count = H5I_INVALID_HID;
	if (existing_file) {
		//TODO checks
		// open datasets
		hdf5_dataset_rho = H5Dopen(hdf5_file, "rho_ms_kpc3", H5P_DEFAULT);
		assert(hdf5_dataset_rho != H5I_INVALID_HID);
		hdf5_dataset_count = H5Dopen(hdf5_file, "count", H5P_DEFAULT);
		assert(hdf5_dataset_count != H5I_INVALID_HID);
	}
	else {
		// create datasets
		hid_t hdf5_dataspace_file_bins = H5I_INVALID_HID;
		hid_t hdf5_dataset = H5I_INVALID_HID;
		hid_t dcpl = H5I_INVALID_HID;
		const double fill_double = NAN;
		const uint fill_uint = 0;
		// dataset “r_bins_kpc”
		hdf5_dataspace_file_bins = H5Screate_simple(
			1, (hsize_t []){R_BINS + 1}, NULL
		);
		assert(hdf5_dataspace_file_bins != H5I_INVALID_HID);
		dcpl = H5Pcreate(H5P_DATASET_CREATE);
		assert(dcpl != H5I_INVALID_HID);
		H5Pset_fill_value(dcpl, H5T_NATIVE_DOUBLE, &fill_double);
		hdf5_dataset = H5Dcreate(
			hdf5_file, "r_bins_kpc", H5T_NATIVE_DOUBLE,
			hdf5_dataspace_file_bins, H5P_DEFAULT, dcpl, H5P_DEFAULT
		);
		assert(hdf5_dataset != H5I_INVALID_HID);
		// write radial bins
		double bin_edges[R_BINS + 1];
		get_bin_edges(
			bin_edges, R_BINS, profiles.r0_kpc, profiles.r1_kpc, true,
			SPACING_LOG
		);
		status = H5Dwrite(
			hdf5_dataset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT,
			bin_edges
		);
		assert(status >= 0);
		// close handles
		H5Pclose(dcpl);
		H5Sclose(hdf5_dataspace_file_bins);
		H5Dclose(hdf5_dataset);
		// dataset “rho_ms_kpc3”
		dcpl = H5Pcreate(H5P_DATASET_CREATE);
		assert(dcpl != H5I_INVALID_HID);
		H5Pset_fill_value(dcpl, H5T_NATIVE_DOUBLE, &fill_double);
		hdf5_dataset_rho = H5Dcreate(
			hdf5_file, "rho_ms_kpc3", H5T_NATIVE_DOUBLE,
			hdf5_dataspace_file, H5P_DEFAULT, dcpl, H5P_DEFAULT
		);
		assert(hdf5_dataset_rho != H5I_INVALID_HID);
		H5Pclose(dcpl);
		// dataset “count”
		dcpl = H5Pcreate(H5P_DATASET_CREATE);
		assert(dcpl != H5I_INVALID_HID);
		H5Pset_fill_value(dcpl, H5T_NATIVE_UINT, &fill_uint);
		hdf5_dataset_count = H5Dcreate(
			hdf5_file, "count", H5T_NATIVE_UINT, hdf5_dataspace_file,
			H5P_DEFAULT, dcpl, H5P_DEFAULT
		);
		assert(hdf5_dataset_count != H5I_INVALID_HID);
		H5Pclose(dcpl);
	}
	if (profiles.num_profiles > 0) {
		// set hyperslab to write
		status = H5Sselect_hyperslab(
			hdf5_dataspace_file, H5S_SELECT_SET,
			(hsize_t []){filament_start, 0}, NULL,
			(hsize_t []){profiles.num_profiles, R_BINS}, NULL
		);
		assert(status >= 0);
		// write data
		const hid_t hdf5_dataspace_memory = H5Screate_simple(
			2, (hsize_t []){profiles.num_profiles, R_BINS}, NULL
		);
		status = H5Dwrite(
			hdf5_dataset_rho, H5T_NATIVE_DOUBLE, hdf5_dataspace_memory,
			hdf5_dataspace_file, H5P_DEFAULT, profiles.rho_ms_kpc3
		);
		assert(status >= 0);
		status = H5Dwrite(
			hdf5_dataset_count, H5T_NATIVE_UINT, hdf5_dataspace_memory,
			hdf5_dataspace_file, H5P_DEFAULT, profiles.count
		);
		assert(status >= 0);
		// finalize
		H5Sclose(hdf5_dataspace_memory);
	}
	// finalize
	H5Sclose(hdf5_dataspace_file);
	H5Dclose(hdf5_dataset_rho);
	H5Dclose(hdf5_dataset_count);
	H5Fclose(hdf5_file);
}


static double dist_periodic_wrap(double x, const double box_size) {
	while (x < -box_size / 2)
		x += box_size;
	while (x >= box_size / 2)
		x -= box_size;
	return x;
}

// see e.g.
// https://haggis.readthedocs.io/en/latest/api.html#haggis.math.segment_distance
static double segment_distance(
	const double *const p, const double *const p1, const double *const p2,
	const double box_size
) {
	// parameterize line: l = p1 + t (p2 - p1)
	double dp[NUMDIMS];
	for (uint i = 0; i < NUMDIMS; i++)
		dp[i] = dist_periodic_wrap(p2[i] - p1[i], box_size);
	double dp2 = 0;
	double t = 0;
	for (uint i = 0; i < NUMDIMS; i++) {
		dp2 += pow(dp[i], 2);
		t += dist_periodic_wrap(p[i] - p1[i], box_size) * dp[i];
	}
	t /= dp2;
	if (t < 0 || 1 < t) {
		// allow for slight tolerance in t to account for very short line
		// segments
		const double cell_len = box_size / GRID.nx;
		const double dp = sqrt(dp2);
		const double t_tol = 0.5 * cell_len / dp;
		if (t < -t_tol || 1 + t_tol < t)
			return NAN;
	}
	// calculate distance
	double dist2 = 0;
	for (uint i = 0; i < NUMDIMS; i++) {
		const double intersect_i = p1[i] + t * dp[i];
		const double dist_i = dist_periodic_wrap(p[i] - intersect_i, box_size);
		dist2 += pow(dist_i, 2);
	}
	return sqrt(dist2);
}

static void rho_bin_cells_filament(
	const double *const rho, const struct filament_data fil_info,
	const size_t fil_id, struct profile_data profiles
) {
	//TODO: variations – central_bin, binlog
	const double box_size_kpc = PARAM.box_size * UNITS.length_in_kpc;
	const double cell_len_kpc = box_size_kpc / GRID.nx;
	double *bin_center_rho = &profiles.rho_ms_kpc3[0][0];
	uint *bin_center_count = &profiles.count[0][0];
	double *bin_data_rho = &profiles.rho_ms_kpc3[0][1];
	uint *bin_data_count = &profiles.count[0][1];
	const uint num_bins = R_BINS - 1;
	const double binfac = num_bins / log(profiles.r1_kpc / profiles.r0_kpc);
	assert(binfac > 0);
	double p[NUMDIMS];
	double (*fil_points)[NUMDIMS] = fil_info.filaments[fil_id];
	double (*offsets)[NUMDIMS] = fil_info.shifts[fil_id];
	const uint fil_len = fil_info.nsamp[fil_id];
	for (uint ix_offset = 0; ix_offset < GRID.local_nx; ix_offset++) {
		const uint ix = GRID.local_x_start + ix_offset;
		p[0] = ix * cell_len_kpc;
		for (uint iy = 0; iy < GRID.ny; iy++) {
			p[1] = iy * cell_len_kpc;
			for (uint iz = 0; iz < GRID.nz; iz++) {
				p[2] = iz * cell_len_kpc;
				for (uint fil_pt = 0; fil_pt < fil_len - 1; fil_pt++) {
					double p1[NUMDIMS];
					double p2[NUMDIMS];
					for (uint i = 0; i < ARRAY_LEN(p1); i++) {
						const double oi = offsets[fil_pt][i];
						p1[i] = fil_points[fil_pt][i] + oi;
						p2[i] = fil_points[fil_pt + 1][i] + oi;
					}
					const double r = segment_distance(p, p1, p2, box_size_kpc);
					// TODO: double-counting yes vs. no?
					if (r < INFINITY) {
						const double rho_val = rho[IDX(ix_offset, iy, iz)];
						if (r < profiles.r0_kpc) {
							*bin_center_rho += rho_val;
							*bin_center_count += 1;
						}
						else if (profiles.r0_kpc <= r && r < profiles.r1_kpc) {
							uint r_bin = log(r / profiles.r0_kpc) * binfac;
							// apparently, this can happen due to rounding with
							// log?
							if (r_bin == num_bins)
								r_bin--;
							assert(r_bin < num_bins);
							bin_data_rho[r_bin] += rho_val;
							bin_data_count[r_bin]++;
						}
					}
				}
			}
		}
	}
}

static bool filament_done(const uint *const count, const size_t fil_len) {
	for (size_t i = 0; i < fil_len; i++)
		if (count[i] > 0) {
			return true;
		}
	return false;
}


static void calculate_filament_profiles(
	const char *const snap_path, const char *const ndskl_path,
	const char *const shift_path
) {
	for (uint i = 0; i < NUMDIMS; i++)
		assert(GRID.n[i] == GRID.n[0]);
	struct profile_data profiles;
	profiles.num_profiles = 0;
	// read NDskl ASCII file on rank 0 and broadcast
	struct filament_data fil_info;
	if (WORLD_RANK == 0) {
		fil_info = read_NDskl_ascii(ndskl_path);
		fil_info = alloc_filament_shifts(fil_info);
		if (shift_path)
			fil_info = read_filament_shifts(shift_path, fil_info);
	}
	MPI_Bcast(&fil_info, sizeof fil_info, MPI_BYTE, 0, MPI_COMM_WORLD);
	if (WORLD_RANK > 0) {
		fil_info.filaments =
			malloc(fil_info.num_filaments * sizeof *fil_info.filaments);
		fil_info.nsamp =
			malloc(fil_info.num_filaments * sizeof *fil_info.nsamp);
	}
	MPI_Bcast(
		fil_info.nsamp, fil_info.num_filaments, MPI_UNSIGNED, 0, MPI_COMM_WORLD
	);
	if (WORLD_RANK > 0)
		fil_info = alloc_filament_shifts(fil_info);
	for (uint i = 0; i < fil_info.num_filaments; i++) {
		if (WORLD_RANK > 0)
			fil_info.filaments[i] =
				malloc(fil_info.nsamp[i] * sizeof *fil_info.filaments[i]);
		MPI_Bcast(
			fil_info.filaments[i],
			fil_info.nsamp[i] * ARRAY_LEN(*fil_info.filaments[i]), MPI_DOUBLE,
			0, MPI_COMM_WORLD
		);
		MPI_Bcast(
			fil_info.shifts[i],
			(fil_info.nsamp[i] - 1) * ARRAY_LEN(*fil_info.shifts[i]),
			MPI_DOUBLE, 0, MPI_COMM_WORLD
		);
	}
	printf0("Processing NDskl with %u filaments\n", fil_info.num_filaments);
	const double box_size_kpc = PARAM.box_size * UNITS.length_in_kpc;
	const double cell_len_kpc = box_size_kpc / GRID.nx;
	profiles.r0_kpc = cell_len_kpc;
	profiles.r1_kpc = box_size_kpc / 2;
	char output_path[MAXLEN_PATH];
	size_t fil_start;
	if (WORLD_RANK == 0) {
		// initialize HDF5 file
		snprintf(
			output_path, ARRAY_LEN(output_path), "%s_profiles.hdf5",
			ndskl_path
		);
		const bool existing_file = file_exists(output_path);
		if (existing_file)
			printf0(
				"Not recomputing existing data in file “%s”\n", output_path
			);
		else
			printf0(
				"Could not find “%s” as existing file; starting from scratch\n",
				output_path
			);
		save_data(output_path, fil_info, 0, profiles);
		// find starting filament to process when resuming existing output file
		// TODO: make this check more memory-efficient
		const hid_t hdf5_file = H5Fopen(
			output_path, H5F_ACC_RDONLY, H5P_DEFAULT
		);
		assert(hdf5_file != H5I_INVALID_HID);
		const hid_t hdf5_dataset = H5Dopen(hdf5_file, "count", H5P_DEFAULT);
		assert(hdf5_dataset != H5I_INVALID_HID);
		uint (*count)[R_BINS] = malloc(fil_info.num_filaments * sizeof *count);
		const herr_t status = H5Dread(
			hdf5_dataset, H5T_NATIVE_UINT, H5S_ALL, H5S_ALL, H5P_DEFAULT, count
		);
		assert(status >= 0);
		H5Dclose(hdf5_dataset);
		H5Fclose(hdf5_file);
		for (fil_start = 0; fil_start < fil_info.num_filaments; fil_start++) {
			if (!filament_done(count[fil_start], ARRAY_LEN(count[fil_start])))
				break;
		}
		printf0(
			"%zu filaments left to process for file “%s”\n",
			fil_info.num_filaments - fil_start, ndskl_path
		);
	}
	MPI_Bcast(&fil_start, sizeof fil_start, MPI_BYTE, 0, MPI_COMM_WORLD);
	// read snapshot
	printf0("Reading wave function…\n");
	const double t0 = time();
	double *rho = fftw_alloc_real(GRID.local_num_el);
	read_wave_function(snap_path, rho, process_buffer_rho);
	const double t1 = time();
	printf0("\tDone (took %g sec.).\n", t1 - t0);
	// process filaments
	// TODO: batched/chunked processing of filaments?
	profiles.num_profiles = 1;
	profiles.rho_ms_kpc3 = malloc(sizeof *profiles.rho_ms_kpc3);
	profiles.count = malloc(sizeof *profiles.count);
	for (size_t fil_id = fil_start; fil_id < fil_info.num_filaments; fil_id++) {
		printf0("Processing filament #%zu\n", fil_id);
		const double t0 = time();
		for (uint i = 0; i < ARRAY_LEN(profiles.rho_ms_kpc3[0]); i++) {
			profiles.rho_ms_kpc3[0][i] = 0;
			profiles.count[0][i] = 0;
		}
		rho_bin_cells_filament(rho, fil_info, fil_id, profiles);
		// get global profiles on rank 0
		MPI_Reduce(
			WORLD_RANK == 0 ? MPI_IN_PLACE : profiles.rho_ms_kpc3[0],
			profiles.rho_ms_kpc3[0], ARRAY_LEN(profiles.rho_ms_kpc3[0]),
			MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD
		);
		MPI_Reduce(
			WORLD_RANK == 0 ? MPI_IN_PLACE : profiles.count[0],
			profiles.count[0], ARRAY_LEN(profiles.count[0]), MPI_UNSIGNED,
			MPI_SUM, 0, MPI_COMM_WORLD
		);
		if (WORLD_RANK == 0) {
			// convert units and compute mean density
			for (uint i = 0; i < ARRAY_LEN(profiles.rho_ms_kpc3[0]); i++) {
				const uint count = profiles.count[0][i];
				if (count > 0) {
					profiles.rho_ms_kpc3[0][i] *=
						pow(UNITS.length_in_kpc, 3) * UNITS.mass_in_solar_mass;
					profiles.rho_ms_kpc3[0][i] /= count;
				}
				else
					profiles.rho_ms_kpc3[0][i] = NAN;
			}
			// mark filament as “done” (according filament_done()) even if no
			// cells are included in the profile (can happen for very short
			// filaments)
			for (size_t i = 0; i < fil_info.num_filaments; i++)
				if (!filament_done(
					profiles.count[0], ARRAY_LEN(profiles.count[0])
				))
					profiles.count[0][0] = 1;
			printf0("Writing to file “%s”\n", output_path);
			save_data(output_path, fil_info, fil_id, profiles);
		}
		const double t1 = time();
		printf0("\tDone (took %g sec.).\n", t1 - t0);
	}
}


int main(int argc, char **argv) {
	// initialize
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &WORLD_RANK);
	MPI_Comm_size(MPI_COMM_WORLD, &WORLD_SIZE);
	fftw_mpi_init();
	// command line arguments
	assert(argc == 3 || argc == 4);
	const char *snap_path = argv[1];
	const char *ndskl_path = argv[2];
	const char *shift_path = argc >= 4 ? argv[3] : NULL;
	if (FILES_OPEN_IN_PARALLEL > (uint) WORLD_SIZE)
		FILES_OPEN_IN_PARALLEL = WORLD_SIZE;
	// read PARAM and GRID
	read_metadata(snap_path);
	// print info
	printf0("%s\n", snap_path);
	printf0("%s\n", ndskl_path);
	if (shift_path)
		printf0("%s\n", shift_path);
	printf0("SnapshotFileBase: “%s”\n", PARAM.snap_file_base);
	printf0("PMGRID                        = %u\n", GRID.nx);
	printf0("Box size                      = %g\n", PARAM.box_size);
	printf0("hbar (internal units)         = %g\n", PARAM.hbar);
	printf0("mass (eV/c²)                  = %g\n", PARAM.mass_ev);
	printf0("mass (internal units)         = %g\n", PARAM.mass);
	printf0("hbar / mass (internal units)  = %g\n", PARAM.hbar / PARAM.mass);
	printf0("expected v_max (internal units) = %g\n",
		M_PI * (PARAM.hbar / PARAM.mass) / (PARAM.box_size / GRID.nx));
	printf0("Redshift: z = %g\n", 1 / PARAM.a - 1);
	printf0("\n");
	// do computations
	calculate_filament_profiles(snap_path, ndskl_path, shift_path);
	// finalize
	free(GRID.slabs_x_per_rank);
	fftw_mpi_cleanup();
	MPI_Finalize();
	return EXIT_SUCCESS;
}
