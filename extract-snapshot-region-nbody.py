#!/usr/bin/env python3
import argparse
from pathlib import Path
import h5py
import numba
import numpy


NTYPES = 6

parser = argparse.ArgumentParser(description='Extract a cubic sub-region of an '
	'N-body snapshot')
parser.add_argument('--region-x', type=float, nargs=2, default=(0., numpy.inf),
	help='region to extract in the x direction (default: %(default)s)')
parser.add_argument('--region-y', type=float, nargs=2, default=(0., numpy.inf),
	help='region to extract in the y direction (default: %(default)s)')
parser.add_argument('--region-z', type=float, nargs=2, default=(0., numpy.inf),
	help='region to extract in the z direction (default: %(default)s)')
parser.add_argument('--output-path', type=Path, nargs='+',
	help='paths to write the outputs to')
parser.add_argument('path', type=Path, nargs='+',
	help='the paths to the snapshot files; any normal files are interpreted '
		'as single-file snapshots, while all files in any given directory '
		'are interpreted as a single multi-file snapshot')


def main():
	if not args.output_path:
		args.output_path = [None] * len(args.path)
	assert len(args.path) == len(args.output_path)
#	print(f'Extracting region {data_slice}')
	# process paths
	for path_args in zip(args.path, args.output_path):
		process_path(*path_args)


def process_path(path, output_path):
	print(path)
	if path.is_dir():
		if not output_path:
			output_path = path.with_name(f'{path.name}_region')
			output_path = output_path.mkdir()
		assert output_path.is_dir()
		ip_list = sorted(
			p for p in path.iterdir()
			if not p.is_dir() and not p.suffix == '.ewah'
		)
		op_list = sorted(
			p for p in output_path.iterdir()
			if not p.is_dir() and not p.suffix == '.ewah'
		)
	else:
		if not output_path:
			output_path = path.with_name(f'{path.name}_region.hdf5')
		assert not output_path.is_dir()
		ip_list = [path]
		op_list = [output_path]
	for ip, op in zip(ip_list, op_list):
		with h5py.File(ip, 'r') as input_file, \
			h5py.File(op, 'a') as output_file \
		:
			for ptype in range(NTYPES):
				if f'/PartType{ptype}' in output_file:
					del output_file[f'/PartType{ptype}']
				if f'/PartType{ptype}' in input_file:
					g_in = input_file[f'/PartType{ptype}']
					coord = g_in['Coordinates'][...]
					mask = particle_mask(
						coord, *args.region_x, *args.region_y, *args.region_z
					)
					output_file.create_group(f'/PartType{ptype}')
					g_out = output_file[f'/PartType{ptype}']
					g_out['Coordinates'] = coord[mask]
					for ds in g_in:
						if ds != 'Coordinates':
							g_out[ds] = g_in[ds][...][mask]

@numba.njit
def particle_mask(coord, xmin, xmax, ymin, ymax, zmin, zmax):
	mask = numpy.ones(len(coord), dtype='bool')
	for i in range(len(coord)):
		if (
			coord[i, 0] < xmin or coord[i, 0] > xmax or
			coord[i, 1] < ymin or coord[i, 1] > ymax or
			coord[i, 2] < zmin or coord[i, 2] > zmax
		):
			mask[i] = False
	return mask


if __name__ == '__main__':
	args = parser.parse_args()
	main()

