import uuid
import subprocess


class _MPIShim:
	def __getattr__(self, x):
		return None
	def Is_finalized(self):
		return True
class _MPIFuncShim:
	def __call__(self, *args, **kwargs):
		# TODO: this doesn’t work in all cases; can be made more
		#       sophisticated when the need arises
		assert len(args) == 1
		return args[0]
class _MPICommShim:
	def __getattr__(self, x):
		return _MPIFuncShim()
	def Reduce(self, send, recv, op=None, root=0):
		assert root == 0
		return send if send is not None else recv


def get_shim_variables():
	HAS_MPI = False
	MPI = _MPIShim()
	mpi_comm = _MPICommShim()
	mpi_rank = 0
	mpi_size = 1
	default_print_args = {}
	print0 = print
	mpi_print = print
	mpi_print_each = print
	return locals()


try:
	# workaround for https://github.com/numba/numba/issues/7881
	uuid.getnode()
	# see https://docs.scinet.utoronto.ca/index.php/Python#Error_messages
	if subprocess.run(
		'hostname', capture_output=True, encoding='utf-8', check=True
	).stdout.startswith('nia'):
		import mpi4py.rc
		mpi4py.rc.threads = False
	from mpi4py import MPI

	HAS_MPI = True
	mpi_comm = MPI.COMM_WORLD
	mpi_rank = mpi_comm.Get_rank()
	mpi_size = mpi_comm.Get_size()

	default_print_args = {}

	def print0(*args, **kwargs):
		mpi_print(0, *args, **kwargs)

	def mpi_print(rank, *args, **kwargs):
		k = dict(default_print_args)
		k.update(kwargs)
		if mpi_rank == rank:
			print(*args, **k)

	def mpi_print_each(*args, **kwargs):
		k = dict(default_print_args)
		k.update(kwargs)
		if mpi_rank == 0:
			print(*args, **k)
			for i in range(1, mpi_size):
				other_args = mpi_comm.recv(source=i, tag=77)
				print(*other_args, **k)
		else:
			mpi_comm.send([str(x) for x in args], dest=0, tag=77)

except ModuleNotFoundError:
	globals().update(**get_shim_variables())

