import astropy.units
import collections
import numpy

KPC_IN_CM = astropy.units.Unit('kpc').to('cm')
SOLAR_MASS_IN_G = astropy.units.Unit('M_sun').to('g')
MYR_IN_S = astropy.units.Unit('Myr').to('s')

Units = collections.namedtuple(
	'Units', 'length_in_kpc, mass_in_solar_mass, time_in_myr'
)

def split_num_exp(x):
	exponent = int(numpy.floor(numpy.log10(numpy.abs(x))))
	significand = x / 10**exponent
	return significand, exponent

def unit_isclose_power10(x):
	significand, exponent = split_num_exp(x)
	if numpy.isclose(significand, 1., rtol=1e-3):
		return 10.**exponent
	else:
		return False

def get_units(header):
	length_in_kpc = header['UnitLength_in_cm'] / KPC_IN_CM
	isclose_length = unit_isclose_power10(length_in_kpc)
	if isclose_length:
		length_in_kpc = isclose_length
	mass_in_solar_mass = header['UnitMass_in_g'] / SOLAR_MASS_IN_G
	isclose_mass = unit_isclose_power10(mass_in_solar_mass)
	if isclose_mass:
		mass_in_solar_mass = isclose_mass
	time_in_myr = (
		(header['UnitLength_in_cm'] / header['UnitVelocity_in_cm_per_s']) /
		MYR_IN_S
	)
	isclose_time = unit_isclose_power10(time_in_myr)
	if isclose_time:
		time_in_myr = isclose_time
	units = Units(
		length_in_kpc=length_in_kpc,
		mass_in_solar_mass=mass_in_solar_mass,
		time_in_myr=time_in_myr,
	)
	return units

