#!/usr/bin/env python3
# standard modules
import argparse
import gc
import time
from pathlib import Path
# non-standard modules
import astropy.constants
import astropy.units as units
import mpi4py
import mpi4py_fft
import numba
import numpy
import pandas
# own modules
import read_snapshot
import snapshot
from mpi_util import *

#TODO: get missing values with cos equation for --equation sin

parser = argparse.ArgumentParser(description='Convert wave function phase '
	'in snapshots of fuzzy dark matter to velocity data')
parser.add_argument('--method',
	choices=('fft', 'fft-numpy', 'finite-difference'), default='fft',
	help='method to use to solve the velocity equation')
parser.add_argument('--equation', choices=('phase', 'sin', 'grad-sqrt-rho'),
	default='sin',
	help='form of the velocity equation to solve')
parser.add_argument('--plot-path',
	help='plot histogram to the give file name')
parser.add_argument('--bins', type=int, default=120,
	help='histogram bins')
parser.add_argument('--xlim', nargs=2, type=float, default=(0, 2e4),
	help='plot x range')
parser.add_argument('path', nargs='+', type=Path,
	help='the paths to the snapshot files; any normal files are interpreted '
		'as single-file snapshots, while all files in any given directory '
		'are interpreted as a single multi-file snapshot')
args = parser.parse_args()


GI = numpy.s_

@numba.njit
def vec3_abs(vx, vy, vz, out):
	s = vx.shape
	assert len(s) == 3
	assert vx.shape == vy.shape == vz.shape == out.shape
	for i in range(s[0]):
		for j in range(s[1]):
			for k in range(s[2]):
				out[i, j, k] = numpy.sqrt(
					vx[i, j, k]**2 +
					vy[i, j, k]**2 +
					vz[i, j, k]**2
				)

@numba.njit
def grad_fd(arr, axis, cell_len, out):
	assert arr.shape == out.shape
	s = arr.shape
	assert len(s) == 3
	N = len(arr[axis])
	for i in range(s[0]):
		for j in range(s[1]):
			for k in range(s[2]):
				if axis == 0:
					ip = i + 1
					if ip >= N:
						ip -= N
					im = i - 1
					if im < 0:
						im += N
					out[i, j, k] = (arr[ip, j, k] - arr[im, j, k]) / (2 * cell_len)
				elif axis == 1:
					jp = j + 1
					if jp >= N:
						jp -= N
					jm = j - 1
					if jm < 0:
						jm += N
					out[i, j, k] = (arr[i, jp, k] - arr[i, jm, k]) / (2 * cell_len)
				elif axis == 2:
					kp = k + 1
					if kp >= N:
						kp -= N
					km = k - 1
					if km < 0:
						km += N
					out[i, j, k] = (arr[i, j, kp] - arr[i, j, km]) / (2 * cell_len)
				else: assert False

# TODO: handle points where cos(α) = 0
def velocity_fft_numpy(phase, use_sin=True):
	shape = phase.shape
	sin_a_fft = numpy.fft.rfftn(numpy.sin(phase) if use_sin else phase)
	assert not numpy.any(numpy.isnan(sin_a_fft))
	zero_el = numpy.sum(sin_a_fft == 0)
	if zero_el > 0:
		print0(
			f'WARNING: sin_a_fft has {zero_el} zero elements, make sure that '
			'this is correct/expected!'
		)
		input('Press ENTER to continue… ')
	kxy = 2 * numpy.pi * numpy.fft.fftfreq(PMGRID, cell_len)
	kz = 2 * numpy.pi * numpy.fft.rfftfreq(PMGRID, cell_len)
	k_vec = numpy.meshgrid(kxy, kxy, kz, indexing='ij')
	for k_i in k_vec:
		assert k_i.shape == sin_a_fft.shape
	cos_a = numpy.cos(phase, out=phase) if use_sin else 1
	del phase
	v_vec = [
		numpy.where(
			cos_a == 0, 0, numpy.fft.irfftn(
				1j * (hbar / mass) * k_i * sin_a_fft, shape
			) / cos_a
		)
		for k_i in k_vec
	]
	del sin_a_fft
	del cos_a
	for i in range(len(v_vec)):
		assert not numpy.any(numpy.isnan(v_vec[i]))
	return v_vec

# TODO: handle points where cos(α) = 0
def velocity_fft(pfft, phase, use_sin=True):
	sin_a_fft = mpi4py_fft.newDistArray(pfft, forward_output=True)
	pfft.forward(numpy.sin(phase) if use_sin else phase, sin_a_fft)
	assert not numpy.any(numpy.isnan(sin_a_fft))
	zero_el = numpy.sum(sin_a_fft == 0)
	if zero_el > 0:
		print0(
			f'WARNING: sin_a_fft has {zero_el} zero elements, make sure that '
			'this is correct/expected!'
		)
		input('Press ENTER to continue… ')
	slice_x, slice_y, slice_z = pfft.local_slice()
	assert (
		slice_x.start == slice_z.start == 0 and
		slice_x.stop // 2 + 1 == slice_z.stop == PMGRID // 2 + 1
	), (mpi_rank, slice_y, slice_z)
	kxy = 2 * numpy.pi * numpy.fft.fftfreq(PMGRID, cell_len)
	kx = kxy
	ky = kxy[slice_y]
	kz = 2 * numpy.pi * numpy.fft.rfftfreq(PMGRID, cell_len)
	k_vec = numpy.meshgrid(kx, ky, kz, indexing='ij')
	for k_i in k_vec:
		assert k_i.shape == sin_a_fft.shape
	cos_a = numpy.cos(phase, out=phase) if use_sin else 1
	del phase
	v_vec = [
		numpy.where(
			cos_a == 0, 0,
			pfft.backward(
				1j * (hbar / mass) * k_i * sin_a_fft,
				output_array=mpi4py_fft.newDistArray(pfft, forward_output=False)
			) / cos_a
		)
		for k_i in k_vec
	]
	del sin_a_fft
	del cos_a
	for v_i in v_vec:
		assert not numpy.any(numpy.isnan(v_i))
	return v_vec

# TODO: parallelize finite difference, check phase difference in non-sin case
def velocity_finite_difference(phase, use_sin=True):
	sin_a_shape = numpy.array(phase.shape)
	sin_a_shape += 2
	sin_a = numpy.zeros(tuple(sin_a_shape), dtype='f8')
	sin_a[1:-1, 1:-1, 1:-1] = numpy.sin(phase) if use_sin else phase
	for axis in range(len(sin_a.shape)):
		sin_a[tuple(numpy.roll(GI[0,  :,  :], axis))] = (
			sin_a[tuple(numpy.roll(GI[-2,  :,  :], axis))]
		)
		sin_a[tuple(numpy.roll(GI[-1,  :,  :], axis))] = (
			sin_a[tuple(numpy.roll(GI[1,  :,  :], axis))]
		)
	cos_a = numpy.cos(phase, out=phase) if use_sin else 1
	del phase
	grad_sin_a = [
		grad_sin_a_i[1:-1, 1:-1, 1:-1]
		for grad_sin_a_i in numpy.gradient(sin_a, cell_len)
	]
	del sin_a
	v_vec = [
		numpy.where(cos_a == 0, 0, (hbar / mass) * grad_sin_a_i / cos_a)
		for grad_sin_a_i in grad_sin_a
	]
	del grad_sin_a
	return v_vec

# TODO: parallelize finite difference
def grad_sqrt_rho_sq(sqrt_rho):
	import sys
	sqrt_rho_shape = sqrt_rho.shape
	sr_shape = numpy.array(sqrt_rho_shape)
	sr_shape += 2
	sr = numpy.zeros(tuple(sr_shape), dtype='f8')
	sr[1:-1, 1:-1, 1:-1] = sqrt_rho
	for axis in range(len(sr.shape)):
		sr[tuple(numpy.roll(GI[0,  :,  :], axis))] = (
			sr[tuple(numpy.roll(GI[-2,  :,  :], axis))]
		)
		sr[tuple(numpy.roll(GI[-1,  :,  :], axis))] = (
			sr[tuple(numpy.roll(GI[1,  :,  :], axis))]
		)
	print('refcount:', sys.getrefcount(sqrt_rho))
	del sqrt_rho
	print0('Computing grad')
	result = numpy.zeros(sqrt_rho_shape, dtype='f8')
	for axis in range(len(sr.shape)):
		print0(f'axis = {axis}')
		grad_i = numpy.gradient(sr, cell_len, axis=axis)
		grad_i **= 2
		result += grad_i[1:-1, 1:-1, 1:-1]
		print('refcount:', sys.getrefcount(grad_i))
		del grad_i
		gc.collect()
		time.sleep(3)
	del sr
	return result

# slab decomposition along axis 0
def grad_sqrt_rho_sq_parallel(sqrt_rho):
	print0('Computing grad')
	N = sqrt_rho.shape[-1]
	result = numpy.zeros_like(sqrt_rho)
	for axis in range(1, len(sqrt_rho.shape)):
		print0(f'axis = {axis}')
#		grad_i_sq = numpy.gradient(sr, cell_len, axis=axis)**2
		grad_i_sq = numpy.zeros_like(sqrt_rho)
		grad_fd(sqrt_rho, axis, cell_len, grad_i_sq)
		grad_i_sq **= 2
		result += grad_i_sq
		del grad_i_sq
	print0('Transpose and compute grad_x')
	sqrt_rho_t = sqrt_rho.redistribute(0)
	assert sqrt_rho_t.shape[0] == N
#	grad_x_sq_t = numpy.gradient(sr_t, cell_len, axis=0)**2
	grad_x_sq_t = numpy.zeros_like(sqrt_rho_t)
	grad_fd(sqrt_rho_t, 0, cell_len, grad_x_sq_t)
	grad_x_sq_t **= 2
	del sqrt_rho_t
	grad_x_sq = grad_x_sq_t.redistribute(2)
	print0(result.shape, grad_x_sq.shape, grad_x_sq_t.shape)
	del grad_x_sq_t
	result += grad_x_sq
	del grad_x_sq
	return result

for path_idx, path in enumerate(args.path):
	print0(path)
	snap_files = read_snapshot.snapshot_files(path)
	config, param, header = read_snapshot.read_metadata(snap_files, 'hdf5')
	BoxSize = param['BoxSize']
	mass_ev = param['AxionMassEv']
	# units and constants
	UnitLength_in_cm = header['UnitLength_in_cm']
	UnitMass_in_g = header['UnitMass_in_g']
	UnitVelocity_in_cm_per_s = header['UnitVelocity_in_cm_per_s']
	UnitTime_in_s = UnitLength_in_cm / UnitVelocity_in_cm_per_s
	h = header['HubbleParam']
	hbar = astropy.constants.hbar.to(
		UnitLength_in_cm**2 * units.cm**2 * UnitMass_in_g * units.g /
		(UnitTime_in_s * units.s) / h**2
	).value
	mass = (
		units.Quantity(mass_ev, 'eV') / astropy.constants.c**2
	).to(UnitMass_in_g * units.g / h).value
	# read wave function (distributed across MPI ranks)
	snap = snapshot.Snapshot(snap_files)
	PMGRID = snap.pmgrid
	cell_len = BoxSize / PMGRID
	# determine data to read on each rank (first_slab_x and nslab_x)
	pfft = mpi4py_fft.PFFT(
		MPI.COMM_WORLD, [PMGRID] * 3, dtype='c16', grid=(-1,),
		backend='numpy' #TODO
	)
	psi = mpi4py_fft.newDistArray(pfft, forward_output=False)
	slices = psi.local_slice()
	slice_x, slice_y, slice_z = slices
	assert slice_y == slice_z
	# read grid
	psi_re = snap.read_snapshot('/FuzzyDM/PsiRe', data_slice=slice_x)
	psi.real = psi_re
	del psi_re
	psi_im = snap.read_snapshot('/FuzzyDM/PsiIm', data_slice=slice_x)
	psi.imag = psi_im
	del psi_im
	if args.equation == 'grad-sqrt-rho':
		sqrt_rho = numpy.abs(psi)
		del psi
		gc.collect()
		grad_sqrt_rho_sq_data = grad_sqrt_rho_sq_parallel(sqrt_rho)
		gc.collect()
		numpy.save(f'grad_sqrt_rho_sq.{mpi_rank}.npy', grad_sqrt_rho_sq_data)
	else:
		phase = numpy.angle(psi)
		del psi
		gc.collect()
		# get velocity via u⃗ = ℏ/m ∇α using spectral method (λ = 2π/|∇α|)
		# => use equivalent equation
		# cos(α) u⃗ = ℏ/m ∇ sin(α)
		# to avoid problems with discontinuities in the modulo variable α
		if args.method == 'fft':
			pfft = mpi4py_fft.PFFT(
				MPI.COMM_WORLD, phase.global_shape, dtype=phase.dtype,
				grid=(-1,),
				backend='numpy' #TODO
			)
			phase_dist = mpi4py_fft.newDistArray(pfft, forward_output=False)
			assert slices == phase_dist.local_slice()
			phase_dist[...] = phase
			phase = phase_dist
			del phase_dist
			v_vec = velocity_fft(pfft, phase, args.equation == 'sin')
		elif args.method == 'fft-numpy':
			v_vec = velocity_fft_numpy(phase, args.equation == 'sin')
		elif args.method == 'finite-difference':
			v_vec = velocity_finite_difference(phase, args.equation == 'sin')
		else:
			assert False
		del phase
		gc.collect()
		v_abs = numpy.full_like(v_vec[0], numpy.nan)
		vec3_abs(*v_vec, v_abs)
		assert not numpy.any(numpy.isnan(v_abs))
		del v_vec
		numpy.save(f'v_abs.{mpi_rank}.npy', v_abs)
		print0(
			'Velocity stats:', pandas.Series(v_abs.reshape(-1)).describe(
				percentiles=[0.25, 0.5, 0.75, 0.95]
			), sep='\n'
		)
	print0(f'ℏ/m = {hbar / mass}')
	print0('Velocity criterion: v_max =', (hbar / mass) * numpy.pi / cell_len)

	if args.plot_path:
		hist, bins = numpy.histogram(v_abs, bins=args.bins, range=args.xlim)
		del v_abs
		if mpi_rank != 0:
			del bins
		mpi_comm.Reduce(
			MPI.IN_PLACE if mpi_rank == 0 else hist, hist, op=MPI.SUM, root=0
		)
		if mpi_rank == 0:
			import matplotlib.pyplot as plt
			import matplotlib_settings
			numpy.save('hist.npy', hist)
			# make sure that mirrored ticks at the top are disabled
			# (→ matplotlib_settings)
			# to prevent duplicate/wrong ticks with top secondary_xaxis
			plt.rc('xtick', top=False)
			plt.title(f'{args.equation} {args.method}')
			plt.xlabel('$v$ / km/s')
			plt.ylabel('$N$ (number of cells)')
			with numpy.errstate(divide='ignore'):
				x2 = plt.gca().secondary_xaxis('top', functions=(
					lambda v: 2 * numpy.pi * (hbar / mass) / v,
					lambda l: 2 * numpy.pi * (hbar / mass) / l,
				))
			x2.set_xlabel(r'$\lambda$ / $h^{-1}\,$kpc')
			x2.set_ticks([60, 30, 20, 10, 7.5, 5]) #TODO
			plt.yscale('log')
#			plt.hist(v_abs.reshape(-1), bins=args.bins, range=args.xlim)
			plt.hist(bins[:-1], bins, weights=hist)
			plt.xlim(args.xlim)
			# set next integer multiple of a power of 10 as upper limit for the
			# vertical axis
			ylim_top = 10**numpy.floor(numpy.log10(PMGRID**3))
			ylim_top *= numpy.floor(PMGRID**3 / ylim_top)
			plt.ylim(1e2, ylim_top)
			with numpy.errstate(divide='ignore'):
				plt.savefig(args.plot_path, dpi=200, bbox_inches='tight')

