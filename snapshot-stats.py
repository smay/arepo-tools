#!/usr/bin/env python3
import argparse
from pathlib import Path
import numba
import numpy
import iterate_grid
import read_snapshot
from mpi_util import *
from snapshot import Snapshot

parser = argparse.ArgumentParser(description='Display some information and '
	'statistics about an AREPO snapshot')
parser.add_argument('path', type=Path, nargs='+',
	help='the paths to the snapshot files; any normal files are interpreted '
		'as single-file snapshots, while all files in any given directory '
		'are interpreted as a single multi-file snapshot')
args = parser.parse_args()


@numba.njit
def avg_density(rho, pmgrid, ix_info, data_slice, param, data):
	assert numpy.all(rho >= 0)
	data[0]['rho_sum'] += numpy.sum(rho)
	data[0]['num_cells'] += rho.size

for path in args.path:
	print0(path)
	snap = Snapshot(path)
	data = numpy.zeros(1, dtype=[('rho_sum', 'f8'), ('num_cells', 'i8')])
	iterate_grid.iterate(snap, avg_density, data)
	rho_sum_g = mpi_comm.reduce(data[0]['rho_sum'], op=MPI.SUM, root=0)
	num_cells_g = mpi_comm.reduce(data[0]['num_cells'], op=MPI.SUM, root=0)
	if mpi_rank == 0:
		assert num_cells_g == snap.pmgrid**3, \
			f'{num_cells_g} != {snap.pmgrid**3}'
		print0(f'<ρ> = {rho_sum_g / num_cells_g}')

