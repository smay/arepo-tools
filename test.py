#!/usr/bin/env python3
import importlib
import itertools
import shutil
import subprocess
import sys
import tempfile
import unittest
from pathlib import Path
import h5py
import numpy
import numpy.testing
import snapshot
particles_cic = importlib.import_module('particles-cic')

HDF5_TEMPLATE_PATH = Path('template.hdf5')
PHASE_TO_VELOCITY_PATH = Path('phase-to-velocity')
# constants from phase-to-velocity
HBAR   = 1.0545718176461565e-34  # J * s
EV     = 1.602176634e-19  # J
CLIGHT = 299792458.0  # m/s

# cf. https://stackoverflow.com/a/35608701
def ndindex_array(dimensions, dtype=int):
	return numpy.stack(numpy.indices(dimensions, dtype=dtype), -1).reshape(
		-1, len(dimensions)
	)

def cic_fractions(idx_offset, p_s):
    o = numpy.array(idx_offset)
    fractions = (1 - o) + numpy.sign(o - 0.5) * p_s
    return fractions

def cic_assignment_numpy(pos, cell_len, var):
	PMGRID = 4
	grid = numpy.zeros((PMGRID,) * 3)
	p_f = pos / cell_len - 0.5
	p = numpy.floor(p_f).astype(int)
	p_c = p_f - p
	for o in numpy.ndindex((2,) * 3):
		o = numpy.array(o)
		fractions = o - numpy.sign(o - 0.5) * p_c
		assert numpy.all((fractions >= 0) & (fractions <= 1))
		ipos = (p + o) % PMGRID
		unique_ipos, unique_idx = numpy.unique(ipos, axis=0, return_index=True)
		grid_idx = tuple(unique_ipos.T)
		grid[grid_idx] += \
			numpy.prod(fractions[unique_idx], axis=1) * var[unique_idx]
		dup_ipos = numpy.setdiff1d(numpy.arange(len(var)), unique_idx)
		for idx in dup_ipos:
			grid_idx = tuple(ipos[idx])
			grid[grid_idx] += numpy.prod(fractions[idx]) * var[idx]
	assert numpy.isclose(numpy.sum(grid), numpy.sum(var))
	return grid

def read_snapshot_orig_indices(slic, pmgrid):
	sl = tuple(
		numpy.s_[s.start:pmgrid:s.step]
			if isinstance(s, slice) and s.stop is None else s
		for s in numpy.index_exp[slic]
	)
	indices_check = [
		numpy.ogrid[s] % pmgrid for s in sl if isinstance(s, slice)
	]
	if any(
		s.start >= s.stop for s in snapshot.slice_defaults(sl)
		if isinstance(s, slice)
	) or any(
		idx.size > 0 and idx.flat[0] > idx.flat[-1] for idx in indices_check
	):
		if len(sl) < 3:
			data_slice += numpy.index_exp[...]
		# “fill in” slices without stop values (since numpy.ogrid doesn’t
		# know about the dataset’s shape)
		if ... in sl:
			assert sl.count(...) == 1
			eidx = sl.index(...)
			sl = (
				sl[:eidx] + numpy.index_exp[:] * (3 - len(sl)) +
				sl[eidx + 1:]
			)
		# handle wrap-around (start >= stop)
		sl = tuple(
			numpy.s_[s.start:s.stop + pmgrid:s.step] if s.start >= s.stop else s
			for s in snapshot.slice_defaults(sl)
		)
		return tuple(idx % pmgrid for idx in numpy.ogrid[sl])
	# if stop > start, normal slice semantics can be used
	# in this case, return original slice unmodified
	return slic

def prepare_snap_files(files, data, pmgrid, reshape_1d=True):
	for path, psi_re in zip(files, data):
		with h5py.File(path, 'r+') as f:
			f['/Config'].attrs['PMGRID'] = pmgrid
			f['/Header'].attrs['NumFilesPerSnapshot'] = len(files)
			f['/Parameters'].attrs['NumFilesPerSnapshot'] = len(files)
			for i in range(len(f['/Header'].attrs['NumPart_Total'])):
				f['/Header'].attrs['NumPart_Total'][i] *= len(files)
			del f['/FuzzyDM/PsiRe']
			del f['/FuzzyDM/PsiIm']
			if reshape_1d:
				psi_re = psi_re.reshape(-1)
			f['/FuzzyDM/PsiRe'] = psi_re
			f.create_dataset_like(
				'/FuzzyDM/PsiIm', f['/FuzzyDM/PsiRe'], fillvalue=0.
			)


class Test(unittest.TestCase):
	def test_cic_fractions(self):
		p_s = numpy.array([1/4, 1/16, 7/8])
		self.assertEqual(
			tuple(cic_fractions((0, 0, 0), p_s)),
			(1 - p_s[0], 1 - p_s[1], 1 - p_s[2])
		)
		self.assertEqual(
			tuple(cic_fractions((0, 0, 1), p_s)),
			(1 - p_s[0], 1 - p_s[1], p_s[2])
		)
		self.assertEqual(
			tuple(cic_fractions((0, 1, 0), p_s)),
			(1 - p_s[0], p_s[1], 1 - p_s[2])
		)
		self.assertEqual(
			tuple(cic_fractions((0, 1, 1), p_s)),
			(1 - p_s[0], p_s[1], p_s[2])
		)
		self.assertEqual(
			tuple(cic_fractions((1, 0, 0), p_s)),
			(p_s[0], 1 - p_s[1], 1 - p_s[2])
		)


	def test_cic_assignment(self):
		cell_len = 0.5
		pos = numpy.array([
			[0.8, 0.9, 0.3],
			[0.81, 0.91, 0.31],
			[0.82, 0.92, 0.32],
			[1.3, 1.4, 1.5],
		])
		var = numpy.array([
			99.,
			77.,
			33.,
			8.,
		])
		grid = cic_assignment_numpy(pos, cell_len, var)
		self.assertAlmostEqual(numpy.sum(grid), numpy.sum(var))
		pmgrid = 4
		grid = particles_cic.cic_assignment(pos, pmgrid, cell_len * pmgrid, var)
		self.assertAlmostEqual(numpy.sum(grid), numpy.sum(var))

	def test_read_snapshot(self):
		# test slices
		slices = [
			numpy.s_[...],
			numpy.s_[3:26, ...],
			numpy.s_[3:26, :, :],
			numpy.s_[-17:33, :, :],
			numpy.s_[-17:33:3, :, :],
			numpy.s_[-16:33:3, :, :],
			numpy.s_[-15:33:3, :, :],
			numpy.s_[3:26, 80:95, :],
			numpy.s_[3:26, :, 33:70],
			numpy.s_[:, 80:95, 33:70],
			numpy.s_[3:26, 80:95, 33:70],
			numpy.s_[-40:-25, 80:95, 33:70],
			numpy.s_[3:26, 80:95, -12:8],
			numpy.s_[3:26, -9:17, 33:70],
			numpy.s_[-5:7, 80:95, 33:70],
			numpy.s_[-5:7, -9:17, -12:8],
			numpy.s_[-5:17:2, -9:37:3, -12:28:4],
			numpy.s_[-5:17:3, -9:37:4, -12:28:5],
			numpy.s_[-4:17:3, -8:37:4, -11:28:5],
			numpy.s_[-3:17:3, -7:37:4, -10:28:5],
			# TODO: the following slices all read no data due to how numpy.ogrid
			#       works
			numpy.s_[16:15, 8:7, 29:28],
			numpy.s_[16:5, 8:3, 29:11],
			numpy.s_[16:15:3, 8:7:4, 29:28:5],
			numpy.s_[-15:-16, -7:-8, -28:-29],
			numpy.s_[-15:-26, -7:-12, -11:-36],
			numpy.s_[-15:-16:4, -7:-8:3, -28:-29:5],
			# workaround: use upper bound > PMGRID
			numpy.s_[16:115, 8:107, 29:128],
			numpy.s_[16:105, 8:103, 29:111],
			numpy.s_[16:115:3, 8:107:4, 29:128:5],
			# TODO: check error in this case!
#			numpy.s_[-15:-5, -7:-3, -11:-29],
			# TODO: the following cases don’t work because they result in (e.g.)
			#       numpy.s_[84:84, 92:92, 71:71] (for PMGRID = 100)
#			numpy.s_[-16:84, -8:92, -29:71],
#			numpy.s_[-16:69, -8:49, -29:53],
#			numpy.s_[-16:84:3, -8:92:4, -29:71:5],
			# similar cases, but only reading at most 99 elements per dimension
			numpy.s_[-16:83, -8:91, -29:70],
			numpy.s_[-16:69, -8:49, -29:53],
			numpy.s_[-16:83:3, -8:91:4, -29:70:5],
		]
		for reshape_1d in (True, False):
			with tempfile.TemporaryDirectory() as temp_dir:
				tdir = Path(temp_dir)
				# single file
				pmgrid = 100
				orig = numpy.arange(pmgrid**3, dtype='f8').reshape((pmgrid,) * 3)
				d = tdir / 'single'
				d.mkdir()
				s = shutil.copy(HDF5_TEMPLATE_PATH, d)
				prepare_snap_files([s], [orig], pmgrid, reshape_1d=reshape_1d)
				snap = snapshot.Snapshot([s])
				for sl in slices:
					oidx = read_snapshot_orig_indices(sl, pmgrid)
					data = snap.read_snapshot('/FuzzyDM/PsiRe', data_slice=sl)
					numpy.testing.assert_array_equal(data, orig[oidx])
				# multiple files, multiple slabs per file
				pmgrid = 100
				orig = numpy.arange(pmgrid**3, dtype='f8').reshape((pmgrid,) * 3)
				for num_files in (17, 19, 20):
					d = tdir / f'mult-mult-{num_files}'
					d.mkdir()
					files = [
						shutil.copy(HDF5_TEMPLATE_PATH, d / f'snap_000.{i}.hdf5')
						for i in range(num_files)
					]
					dx = pmgrid // len(files)
					psi_re = [
						orig[
							i * dx:(i + 1) * dx if i + 1 < len(files)
								else None,
							...
						] for i in range(len(files))
					]
					prepare_snap_files(
						files, psi_re, pmgrid, reshape_1d=reshape_1d
					)
					snap = snapshot.Snapshot(files)
					for sl in slices:
						oidx = read_snapshot_orig_indices(sl, pmgrid)
						data = snap.read_snapshot('/FuzzyDM/PsiRe', data_slice=sl)
						numpy.testing.assert_array_equal(data, orig[oidx])
				# multiple files, multiple slabs per file, unevenly distributed
				pmgrid = 117
				orig = numpy.arange(pmgrid**3, dtype='f8').reshape((pmgrid,) * 3)
				d = tdir / 'mult-mult-uneven'
				d.mkdir()
				slabs_x_per_file = [1, 12, 2, 1, 1, 1, 5, 1, 6, 64, 9, 13, 1]
				first_slab_x_of_file = numpy.cumsum([0] + slabs_x_per_file[:-1])
				assert sum(slabs_x_per_file) == pmgrid
				files = [
					shutil.copy(HDF5_TEMPLATE_PATH, d / f'snap_000.{i}.hdf5')
					for i in range(len(slabs_x_per_file))
				]
				psi_re = [
					orig[slabstart_x:slabstart_x + nslab_x]
					for slabstart_x, nslab_x in zip(
						first_slab_x_of_file, slabs_x_per_file
					)
				]
				prepare_snap_files(files, psi_re, pmgrid, reshape_1d=reshape_1d)
				snap = snapshot.Snapshot(files)
				for sl in slices:
					oidx = read_snapshot_orig_indices(sl, pmgrid)
					data = snap.read_snapshot('/FuzzyDM/PsiRe', data_slice=sl)
					numpy.testing.assert_array_equal(data, orig[oidx])
				# multiple files, single slab per file
				pmgrid = 101
				orig = numpy.arange(pmgrid**3, dtype='f8').reshape((pmgrid,) * 3)
				d = tdir / 'mult-single'
				d.mkdir()
				files = [
					shutil.copy(HDF5_TEMPLATE_PATH, d / f'snap_000.{i}.hdf5')
					for i in range(pmgrid)
				]
				psi_re = [orig[i:i + 1] for i in range(len(files))]
				prepare_snap_files(files, psi_re, pmgrid, reshape_1d=reshape_1d)
				snap = snapshot.Snapshot(files)
				for sl in slices:
					oidx = read_snapshot_orig_indices(sl, pmgrid)
					data = snap.read_snapshot('/FuzzyDM/PsiRe', data_slice=sl)
					numpy.testing.assert_array_equal(data, orig[oidx])


	def test_phase_to_velocity(self):
		# running mpiexec using subprocess does not work if MPI has been
		# initialized in this process, so make sure this hasn’t accidentally
		# happened anywhere
		assert 'mpi4py' not in sys.modules
		# set parameters
		m_ev = 3.7e-23
		pmgrid = 169
		box_size = 277.8
		phase_step = 2 * numpy.pi / 13
		zero_shift = 17
		#TODO: make this work without “+ 1e-10”
		self.assertAlmostEqual(
			(pmgrid * phase_step + 1e-10) % (2 * numpy.pi), 0
		)
		# compute phase from input parameters
		# start with 1D phase array (this will result in phase corresponding
		# to velocity in z direction)
		cell_len = box_size / pmgrid
		grad_phase = phase_step / cell_len
		phase = (
			grad_phase * (numpy.arange(pmgrid) - zero_shift) * cell_len +
			numpy.pi / 2
		)
		psi = numpy.zeros((pmgrid,) * 3, dtype='c16')
		psi[...] = numpy.exp(1j * phase)
		phase_check = phase % (2 * numpy.pi)
		phase_check[phase_check >= numpy.pi] -= 2 * numpy.pi
		self.assertAlmostEqual(
			phase_check[0] - phase_check[-1],
			phase_check[1] - phase_check[0]
		)
		numpy.testing.assert_allclose(
			numpy.angle(psi[3, 7, :]), phase_check
		)
		# get 3D phase arrays for velocity in x, y, z directions
		phase_z = numpy.angle(psi)
		phase_x = numpy.moveaxis(phase_z, 2, 0)
		phase_y = numpy.moveaxis(phase_z, 2, 1)
		for fx, fy, fz in itertools.product((-1, 0, 1), repeat=3):
			with tempfile.TemporaryDirectory() as temp_dir:
				tdir = Path(temp_dir)
				vel_str = []
				for f, s in zip((fx, fy, fz), ('vx', 'vy', 'vz')):
					if f != 0:
						vel_str.append(f'{f:+}'[0] + s)
				vel_str = '_'.join(vel_str)
				in_path = shutil.copy(
					HDF5_TEMPLATE_PATH, tdir / f'test_{vel_str}.hdf5'
				)
				phase = fx * phase_x + fy * phase_y + fz * phase_z
				psi = numpy.exp(1j * phase)
				with h5py.File(in_path, 'r+') as f:
					# set redshift to 0
					f['Header'].attrs['Time'] = 1.
					# get units
					HubbleParam = f['Header'].attrs['HubbleParam']
					UnitLength_in_cm = f['Header'].attrs['UnitLength_in_cm']
					UnitMass_in_g = f['Header'].attrs['UnitMass_in_g']
					UnitVelocity_in_cm_per_s = (
						f['Header'].attrs['UnitVelocity_in_cm_per_s']
					)
					UnitTime_in_s = UnitLength_in_cm / UnitVelocity_in_cm_per_s
					hbar = HBAR * 1e7
					hbar *= (
						HubbleParam**2 / UnitLength_in_cm**2 / UnitMass_in_g *
						UnitTime_in_s
					)
					mass = m_ev * 1e3 * EV / CLIGHT**2
					mass *= HubbleParam / UnitMass_in_g
					# calculate expected velocity in internal units
					vel = (hbar / mass) * grad_phase
					# write HDF5 metadata
					f['Config'].attrs['PMGRID'] = float(pmgrid)
					f['Parameters'].attrs['BoxSize'] = box_size
					f['Header'].attrs['BoxSize'] = box_size
					f['Parameters'].attrs['AxionMassEv'] = m_ev
					# write HDF5 data
					del f['/FuzzyDM/PsiRe']
					del f['/FuzzyDM/PsiIm']
					f['/FuzzyDM/PsiRe'] = numpy.ravel(psi.real)
					f['/FuzzyDM/PsiIm'] = numpy.ravel(psi.imag)
				# compile and run phase-to-velocity
				subprocess.run(
					['make', 'clean'], cwd=PHASE_TO_VELOCITY_PATH, check=True,
					stdout=subprocess.DEVNULL
				)
				subprocess.run(
					['make', 'ubuntu'], cwd=PHASE_TO_VELOCITY_PATH, check=True,
					stdout=subprocess.DEVNULL
				)
				subprocess.run(
					[
						'mpiexec', '-n', '4',
						PHASE_TO_VELOCITY_PATH / 'phase-to-velocity',
						in_path
					], check=True, stdout=subprocess.DEVNULL
				)
				# verify output
				out_path = in_path.with_name(f'{in_path.name}_vel.hdf5')
				velocities = []
				with h5py.File(out_path, 'r') as f:
					px_spec = f['/FuzzyDM/MomentumDensity0'][...]
					py_spec = f['/FuzzyDM/MomentumDensity1'][...]
					pz_spec = f['/FuzzyDM/MomentumDensity2'][...]
					px_fd = f['/FuzzyDM/MomentumDensity0_fd'][...]
					py_fd = f['/FuzzyDM/MomentumDensity1_fd'][...]
					pz_fd = f['/FuzzyDM/MomentumDensity2_fd'][...]
#					vx_spec = f['/FuzzyDM/Velocities0'][...]
#					vy_spec = f['/FuzzyDM/Velocities1'][...]
#					vz_spec = f['/FuzzyDM/Velocities2'][...]
#					vx_fd = f['/FuzzyDM/Velocities0_fd'][...]
#					vy_fd = f['/FuzzyDM/Velocities1_fd'][...]
#					vz_fd = f['/FuzzyDM/Velocities2_fd'][...]
				# normally v = p / ρ, but
				# |psi|² = 1 (see above), ρ = |psi|²
				velocities.append([px_spec, py_spec, pz_spec])
#				velocities.append([vx_spec, vy_spec, vz_spec])
#				velocities.append([vx_fd, vy_fd, vz_fd])
				for vels in velocities:
					for v, f in zip(vels, (fx, fy, fz)):
						numpy.testing.assert_allclose(v, f * vel, atol=1e-10)
				# finite difference momentum density has larger error for
				# constant velocity
				numpy.testing.assert_allclose(v, f * vel, rtol=5e-2, atol=1e-10)


if __name__ == '__main__':
	unittest.main()

