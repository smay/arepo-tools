module Snapshots

using Cosmology
using EllipsisNotation
using EllipsisNotation: (..) as …
using FilePaths
using FilePathsBase: / as ⊘
using HDF5
using KeywordNew
using OrderedCollections
using OffsetArrays
import PhysicalConstants.CODATA2018 as PhysicalConstants
using PhysicalConstants.CODATA2018: G
using PyFormattedStrings
using StaticArrays
using StructArrays
using Util
using WrappingIndices

export Snapshot, SnapshotError,
	snapshot_files, arepo_filename_key, output_path,
	snapshot_number, snapshot_number_fof, numpart_total,
	get_run_name, parse_run_name,
	add_velocities!, add_potential!, fof_path, fof_files, get_type,
	dataset_present, ix_to_file, file_to_ix,
	determine_dataset_info, dataset_indices, determine_chunks,
	read_snap_data, read_fof_tab, read_metadata

# TODO: option to skip looking at headers for every snapshot file
# TODO: only supports HDF5
# TODO: also fully handle auxiliary files here (group catalogs etc.)

@enum OutputType snapshot fof_tab
@enum DatasetType grid particles
@enum SnapshotFormat gadget=1 gadget_variant=2 hdf5=3 npy raw

const GRID_ELEMENT_MEM_SIZE = 2sizeof(Float64)
# 2 GiB chunk size (80 GiB for 40 cores)
const CHUNK_MEM_SIZE_DEFAULT = 2 * 1024^3
const VELOCITY_DATASETS = Set((
	"/FuzzyDM/Velocities0", "/FuzzyDM/Velocities1", "/FuzzyDM/Velocities2",
	"/FuzzyDM/(GradSqrtRho)^2",
	"/FuzzyDM/Velocities0_fd", "/FuzzyDM/Velocities1_fd", "/FuzzyDM/Velocities2_fd",
	"/FuzzyDM/(GradSqrtRho)^2_fd",
	"/FuzzyDM/MomentumDensity0", "/FuzzyDM/MomentumDensity1",
	"/FuzzyDM/MomentumDensity2",
	"/FuzzyDM/MomentumDensity0_fd", "/FuzzyDM/MomentumDensity1_fd",
	"/FuzzyDM/MomentumDensity2_fd",
))
const POTENTIAL_DATASETS = Set(("/FuzzyDM/Potential",))
const CUMSUM_DATASETS = Set(("/DensityGrid/cumsum",))


struct SnapshotError <: Exception
	msg::AbstractString
end

@kwnew struct Snapshot
	path::AbstractPath
	paths::OffsetVector{AbstractPath, Vector{AbstractPath}}
	config::Dict
	param::Dict
	header::Dict
	pmgrid::Union{Int, Nothing}
	box_size::Float64
	dataset_shape::Dict{String, Tuple{Vararg{Int}}}
	dataset_shape_reshaped::Dict{String, Tuple{Vararg{Int}}}
	dataset_dtype::Dict{String, DataType}
	slabs_x_per_file::Dict{String, OffsetVector{Int, Vector{Int}}}
	first_slab_x_of_file::Dict{String, OffsetVector{Int, Vector{Int}}}
	velocity_paths::Vector{AbstractPath}
	potential_paths::Vector{AbstractPath}
	cumsum_paths::Vector{AbstractPath}
	slice::Bool
	types::Set{DatasetType}

	function Snapshot(
		paths; snapshot_format::SnapshotFormat=hdf5, sort_paths=true
	)
		paths = to_paths(paths)
		if sort_paths
			sort!(paths; by=arepo_filename_key)
		end
		@assert !isempty(paths)
		path = length(paths) == 1 ? paths[begin] : parent(paths[begin])
		snapshot_format = snapshot_format
		config, param, header = read_metadata(paths, snapshot_format)
		pmgrid = haskey(config, "PMGRID") ? Int(config["PMGRID"]) : nothing
		box_size = param["BoxSize"]
		dataset_shape = Dict()
		dataset_shape_reshaped = Dict()
		dataset_dtype = Dict()
		slabs_x_per_file = Dict()
		first_slab_x_of_file = Dict()
		velocity_paths = Vector()
		potential_paths = Vector()
		cumsum_paths = Vector()
		# determine snapshot type and do sanity checks
		slice = false
		types = Set()
		first_part = true
		im_present = false
		local d_shape_grid
		d_len_grid = 0
		d_len_part = 0
		for path in paths
			h5open(path, "r") do snap_part
				# check for grid data
				if haskey(snap_part, "/FuzzyDM")
					if haskey(snap_part, "/FuzzyDM/PsiRe")
						if first_part
							push!(types, grid)
							im_present = haskey(snap_part, "/FuzzyDM/PsiIm")
						end
						@assert grid ∈ types
						@assert haskey(snap_part, "/FuzzyDM/PsiIm") ==
							im_present
						d_re = snap_part["/FuzzyDM/PsiRe"]
						d_im = snap_part["/FuzzyDM/PsiIm"]
					else
						if first_part
							slice = true
						end
						@assert slice
						d_re = snap_part["/FuzzyDM/PsiReSlice"]
						d_im = snap_part["/FuzzyDM/PsiImSlice"]
					end
					@assert !im_present || size(d_re) == size(d_im)
					@assert ndims(d_re) > 1 || size(d_re)[end] % pmgrid^2 == 0
					@assert ndims(d_re) == 1 ||
						size(d_re)[begin:end - 1] == ntuple(Returns(pmgrid), 2)
					if first_part
						d_shape_grid = size(d_re)[begin:end - 1]
					end
					@assert size(d_re)[begin:end - 1] == d_shape_grid
					d_len_grid += size(d_re)[end]
				end
				@assert grid ∉ types || haskey(snap_part, "/FuzzyDM")
				# check for particle data
				if haskey(snap_part, "/PartType1/Coordinates")
					p1_pos = snap_part["/PartType1/Coordinates"]
					p1_mass = header["MassTable"][2]
					# TODO: check for variable particle masses
					if !isempty(p1_pos) && p1_mass > 0
						if first_part
							push!(types, particles)
						end
						@assert particles ∈ types
					end
					header = Dict(attrs(snap_part["Header"]))
					@assert size(p1_pos)[end] == header["NumPart_ThisFile"][2]
					d_len_part += size(p1_pos)[end]
				end
				@assert particles ∉ types ||
					haskey(snap_part, "/PartType1/Coordinates")
			end
			first_part = false
		end
		grid_3d = grid ∈ types && !slice
		@assert !grid_3d || !isempty(d_shape_grid) || d_len_grid == pmgrid^3
		@assert !grid_3d || isempty(d_shape_grid) || d_len_grid == pmgrid
		@assert particles ∉ types || d_len_part == numpart_total(header, 1)
		return new(@__LOCALVARS__)
	end
end

const NTYPES_DEFAULT = 6
Base.@kwdef struct Group{T}
	len::SVector{NTYPES_DEFAULT, UInt32} = zeros(NTYPES_DEFAULT)
	mass::SVector{NTYPES_DEFAULT, T} = zeros(NTYPES_DEFAULT)
	cm::SVector{3, T} = fill(3, NaN)
	pos::SVector{3, T} = fill(3, NaN)
	vel::SVector{3, T} = fill(3, NaN)
	tot_len::UInt32 = 0
	tot_mass::T = NaN
	m200::T = NaN
	r200::T = NaN
	potegy::T = NaN
	num_file::UInt32 = 0
end


function snapshot_files(path::AbstractPath; sort_paths=true)
	exists(path) || throw(SnapshotError("File/directory $path does not exist"))
	if isdir(path)
		dir_paths =
			[p for p in iterdir(path) if !isdir(p) && extension(p) ≠ "ewah"]
		snap_files = sort_paths ?
			sort(dir_paths; by=arepo_filename_key) : dir_paths
		if isempty(snap_files)
			throw(SnapshotError("Directory $path is empty"))
		end
	else
		snap_files = [path]
	end
	return OffsetArrays.Origin(0)(snap_files)
end
snapshot_files(path; kwargs...) = snapshot_files(Path(path); kwargs...)

arepo_filename_key(s::AbstractString; num_re=r"\d+") = [
	(i = tryparse(Int, text); isnothing(i) ? lowercase(text) : i)
	for text in keepsplit(s, num_re)
]
arepo_filename_key(s::AbstractPath; kwargs...) =
	arepo_filename_key(basename(s); kwargs...)

function output_path(path, config, param; kind::OutputType=snapshot)
	snapshot_base = _snapshot_name_base(path, param)
	fof_base = _fof_name_base(path, config)
	snap_format = SnapshotFormat(param["SnapFormat"])
	suffix = snap_format == hdf5 && !isdir(path) ? ".hdf5" : ""
	name = basename(path)
	if occursin(snapshot_base, name)
		base = snapshot_base
		snap_num = snapshot_number(path, param)
	elseif occursin(fof_base, name)
		base = fof_base
		snap_num = snapshot_number_fof(path, config)
	else @assert false
	end
	snap_extra = match(base * r"(.*_)?(\d+)" * suffix, name)[1]
	if isnothing(snap_num)
		throw(SnapshotError("Could not determine snap_num for path “$(path)”"))
	end
	dir = dirname(path)
	result = dirname ⊘ f"{base}{snap_num:03d}{suffix}"
	return exists(result) || !exists(result_extra) ? result : result_extra
end

function snapshot_number(path::AbstractPath, param)
	base = _snapshot_name_base(path, param)
	return _snapshot_number_impl(path, base)
end

function snapshot_number_fof(path::AbstractPath, config)
	base = _fof_name_base(path, config)
	return _snapshot_number_impl(path, base)
end

function _snapshot_name_base(path::AbstractPath, param)
	base = param["SnapshotFileBase"]
	if isdir(path)
		base *= "dir"
	end
	base *= "_"
	return base
end

_fof_name_base(path::AbstractPath, config) =
	isdir(path) ? "groups_" :
		(haskey(config, "SUBFIND") ? "fof_subhalo_tab_" : "fof_tab_")

function _snapshot_number_impl(path::AbstractPath, base)
	name = basename(path)
	if !occursin(base, name)
		return nothing
	end
	try
		return parse(Int, match(r"^.*_(\d+)(\.[^.]+)?$", name)[1])
	catch ArgumentError
		return nothing
	end
end

function numpart_total(header, ptype=0:length(header["NumPart_Total"]) - 1)
	num_bits = sizeof(eltype(header["NumPart_Total"]))
	return sum(
		Int(header["NumPart_Total_HighWord"][1 + p]) << num_bits +
		Int(header["NumPart_Total"][1 + p])
		for p in ptype
	)
end

function get_run_name(path::AbstractPath)
	p = parent(canonicalize(path))
	if basename(p) == "output"
		p = parent(p)
	end
	return basename(p)
end

function parse_run_name(path::AbstractPath)
	run_name = get_run_name(path)
	re_int = raw"\d+"
	re_float = raw"\d+(\.\d+)?(e-?\d+)?"
	run_type = match(r"(^|_)(l?cdm|fdm)b?($|_)", run_name)[2]
	N = parse(
		Int, match(Regex("(^|_)p($re_int)(-$re_int)?(\$|_)"), run_name)[2]
	)
	box_size = parse(
		Float64, match(Regex("(^|_)b($re_float)(\$|_)"), run_name)[2]
	)
	mass_result = match(Regex("(^|_)m($re_float)(\$|_)", run_name))
	mass = isnothing(mass_result) ? nothing : parse(Float64, mass_result[2])
	return run_type, N, box_size, mass
end

to_paths(path::AbstractPath) = snapshot_files(path)
to_paths(path::AbstractString) = to_paths(Path(path))
to_paths(paths) = OffsetArrays.Origin(0)(Path.(paths))


function Base.getproperty(snap::Snapshot, x::Symbol)
	if x ≡ :cosmology
		return cosmology(;
			h=snap.header["HubbleParam"], OmegaM=snap.header["Omega0"], OmegaR=0
		)
	else
		return getfield(snap, x)
	end
end

function add_velocities!(snap::Snapshot, paths; sort_paths=true)
	@assert grid ∈ snap.types
	@assert isempty(snap.velocity_paths)
	append!(snap.velocity_paths, to_paths(paths))
	if sort_paths
		sort!(snap.velocity_paths; by=arepo_filename_key)
	end
	@assert !isempty(snap.velocity_paths)
	d_len = 0
	for path in snap.velocity_paths
		h5open(path, "r") do snap_part
			for i in 0:2
				@assert any(haskey.(
					Ref(snap_part),
					("/FuzzyDM/Velocities$i", "/FuzzyDM/MomentumDensity$i")
				))
			end
			d = haskey(snap_part, "/FuzzyDM/Velocities0") ?
				snap_part["/FuzzyDM/Velocities0"] :
				snap_part["/FuzzyDM/MomentumDensity0"]
			@assert ndims(d) == 1
			@assert length(d) % snap.pmgrid^2 == 0
			d_len += length(d)
		end
	end
	@assert d_len == snap.pmgrid^3
	return
end

function add_potential!(snap::Snapshot, paths; sort_paths=true)
	@assert grid ∈ snap.types
	append!(snap.potential_paths, to_paths(paths))
	if sort_paths
		sort!(snap.potential_paths; by=arepo_filename_key)
	end
	@assert !isempty(snap.potential_paths)
	d_len = 0
	for path in snap.potential_paths
		h5open(path, "r") do snap_part
			@assert haskey(snap_part, only(POTENTIAL_DATASETS))
			d = snap_part[only(POTENTIAL_DATASETS)]
			@assert ndims(d) == 1
			@assert length(d) % snap.pmgrid^2 == 0
			d_len += length(d)
		end
	end
	@assert d_len == snap.pmgrid^3
	return
end

#TODO: unify this extra path handling
function add_cumsum!(snap::Snapshot, paths; sort_paths=true)
	@assert grid ∈ snap.types
	append!(snap.cumsum_paths, to_paths(paths))
	if sort_paths
		sort!(snap.cumsum_paths; by=arepo_filename_key)
	end
	@assert !isempty(snap.cumsum_paths)
	d_len = 0
	for path in snap.cumsum_paths
		h5open(path, "r") do snap_part
			@assert haskey(snap_part, only(CUMSUM_DATASETS))
			d = snap_part[only(CUMSUM_DATASETS)]
			@assert ndims(d) == 1
			@assert length(d) % snap.pmgrid^2 == 0
			d_len += length(d)
		end
	end
	@assert d_len == snap.pmgrid^3
	return
end

fof_path(snap::Snapshot) =
	output_path(snap.path, snap.config, snap.param, kind=fof)

fof_files(snap::Snapshot) = snapshot_files(fof_path(snap))

read_fof_tab(snap::Snapshot; kwargs...) =
	read_fof_tab(fof_files(snap); kwargs...)

function get_type(snap::Snapshot, preference::DatasetType)
	if preference ∈ snap.types
		return preference
	elseif particles ∈ snap.types
		return particles
	elseif grid ∈ snap.types
		return grid
	else @assert false
	end
end

numpart_total(snap::Snapshot, args...) = numpart_total(snap.header, args...)

function dataset_present(snap::Snapshot, dataset)
	result = false
	first_part = true
	for path in snap.paths
		h5open(path, "r") do snap_part
			if first_part
				result = haskey(snap_part, dataset)
			end
			@assert haskey(snap_part, dataset) == result
		end
		first_part = false
	end
	return result
end

function ix_to_file(snap::Snapshot, dataset, ix)
	determine_dataset_info(snap, dataset)
	ix_len = snap.first_slab_x_of_file[dataset][end] +
		snap.slabs_x_per_file[dataset][end]
	@assert all(≥(0), ix)
	@assert all(<(ix_len), ix)
	result = Int[]
	for i_slice in ix
		for i_file in 0:length(snap.paths) - 1
			if i_slice in file_to_ix(snap, dataset, i_file)
				push!(result, i_file)
				break
			end
		end
	end
	@assert length(result) == length(ix)
	return result
end

function file_to_ix(snap::Snapshot, dataset, file_num)
	determine_dataset_info(snap, dataset)
	return [
		(
			snap.first_slab_x_of_file[dataset][i]:
			snap.first_slab_x_of_file[dataset][i] +
				snap.slabs_x_per_file[dataset][i]
		) for i in file_num
	]
end

function determine_dataset_info(snap::Snapshot, datasets...)
	datasets = filter(
		ds -> !haskey(snap.dataset_shape, ds), normalize_dataset.(datasets)
	)
	if isempty(datasets)
		return
	end
	paths = [
		(
			isempty(snap.velocity_paths) ? snap.paths : snap.velocity_paths,
			filter(∈(VELOCITY_DATASETS), datasets),
		),
		(
			isempty(snap.potential_paths) ? snap.paths : snap.potential_paths,
			filter(∈(POTENTIAL_DATASETS), datasets),
		),
		(
			isempty(snap.cumsum_paths) ? snap.paths : snap.cumsum_paths,
			filter(∈(CUMSUM_DATASETS), datasets),
		),
		(
			snap.paths, filter(
				∉(VELOCITY_DATASETS ∪ POTENTIAL_DATASETS ∪ CUMSUM_DATASETS),
				datasets
			),
		)
	]
	# determine total dataset shapes, slabs_x_per_file,
	# first_slab_x_of_file
	first_part = Dict(dataset => true for dataset in datasets)
	dataset_len = Dict(dataset => 0 for dataset in datasets)
	dataset_shape = Dict{eltype(datasets), Union{Tuple{Vararg{Int}}, Nothing}}(
		dataset => nothing for dataset in datasets
	)
	slabstart_x = Dict(dataset => 1 for dataset in datasets)
	for (file_paths, file_datasets) in paths
		@assert !isempty(file_paths) || isempty(file_datasets)
		for file_path in file_paths
			h5open(file_path, "r") do snap_part
				for dataset in file_datasets
					@assert haskey(snap_part, dataset) "$dataset"
					d = snap_part[dataset]
					shape = size(d)
					# column-major
					dataset_len[dataset] += shape[end]
					dtype = eltype(d)
					if first_part[dataset]
						dataset_shape[dataset] = shape
						snap.dataset_dtype[dataset] = dtype
						snap.slabs_x_per_file[dataset] =
							OffsetArrays.Origin(0)([])
						snap.first_slab_x_of_file[dataset] =
							OffsetArrays.Origin(0)([])
					end
					@assert shape[begin:end - 1] ==
						dataset_shape[dataset][begin:end - 1]
					@assert dtype == snap.dataset_dtype[dataset]
					# keep track of nslab_x and slabstart_x
					divisor = startswith(dataset, "/FuzzyDM/") && ndims(d) == 1 ?
						snap.pmgrid^2 : 1
					# column-major
					nslab_x = shape[end] ÷ divisor
					push!(snap.slabs_x_per_file[dataset], nslab_x)
					push!(
						snap.first_slab_x_of_file[dataset], slabstart_x[dataset]
					)
					# update slabstart_x
					slabstart_x[dataset] += nslab_x
					first_part[dataset] = false
				end
			end
		end
	end
	# store determined shape for each dataset
	for dataset in datasets
		@assert length(snap.slabs_x_per_file[dataset]) ==
			length(snap.first_slab_x_of_file[dataset]) ==
			length(snap.paths)
		snap.dataset_shape[dataset] = (
			dataset_shape[dataset][begin:end - 1]..., dataset_len[dataset]
		)
		snap.dataset_shape_reshaped[dataset] = snap.dataset_shape[dataset]
		grid_data = startswith(dataset, "/FuzzyDM/") ||
			startswith(dataset, "/DensityGrid/")
		if grid_data
			num_elements = prod(snap.dataset_shape[dataset])
			is_slice = endswith(dataset, "Slice")
			@assert !is_slice || num_elements % snap.pmgrid^2 == 0
			@assert is_slice ||
				num_elements == snap.pmgrid^3 (num_elements, snap.pmgrid)
			if length(snap.dataset_shape[dataset]) == 1
				# reshape 1D dataset into 3D
				if is_slice
					@assert dataset_len[dataset] % snap.pmgrid^2 == 0
					snap.dataset_shape_reshaped[dataset] = (
						ntuple(Returns(snap.pmgrid), 2)...,
						num_elements ÷ snap.pmgrid^2
					)
				else
					@assert dataset_len[dataset] == snap.pmgrid^3
					snap.dataset_shape_reshaped[dataset] =
						ntuple(Returns(snap.pmgrid), 3)
				end
			else
				@assert !is_slice ||
					snap.dataset_shape[dataset][begin:end - 1] ==
					ntuple(Returns(snap.pmgrid), 2)
				@assert is_slice ||
					snap.dataset_shape[dataset] ==
					ntuple(Returns(snap.pmgrid), 3)
			end
		end
		ptype_match = match(r"/PartType(\d)/", dataset)
		@assert isnothing(ptype_match) ||
			dataset_len[dataset] == numpart_total(snap, Int(ptype_match[1]))
	end
	@assert keys(snap.dataset_shape) == keys(snap.dataset_shape_reshaped) ==
		keys(snap.dataset_dtype)
	return
end

function dataset_indices(snap::Snapshot, dataset, data_slice::Tuple)
	determine_dataset_info(snap, dataset)
	dataset_shape = snap.dataset_shape_reshaped[dataset]
	@assert length(data_slice) == length(dataset_shape) || (…) ∈ data_slice
	# “fill in” …
	#TODO: is this necessary?
	if (…) in data_slice
		eidx_vec = findall(==(…), data_slice)
		@assert length(eidx_vec) == 1
		eidx = only(eidx_vec)
		data_slice = (
			data_slice[begin:eidx - 1]...,
			ntuple(Returns(:), length(dataset_shape) - length(data_slice) + 1)...,
			data_slice[eidx + 1:end]...,
		)
	end
	@assert all(step(s) > 0 for s in data_slice if s isa AbstractRange)
	@assert length(data_slice) == length(dataset_shape)
	# TODO: hard-codes HDF5.jl indexing; could store dataset_axes in addition to
	#       dataset_shape to fix this
	return ((
		only(to_indices(1:dim, (slice,)))
		for (slice, dim) in zip(data_slice, dataset_shape)
	)...,)
end
dataset_indices(snap::Snapshot, dataset, data_slice) =
	dataset_indices(snap, dataset, Tuple(data_slice))

# doesn’t support negative steps (start:step:stop); not supported by HDF5.jl
# anyway (see HDF5.BlockRange)
# doesn’t support multiple wrap-around for x
#TODO: investigate memory mapping
#      -> https://juliaio.github.io/HDF5.jl/stable/#Memory-mapping
function read_snap_data(snap::Snapshot, dataset; data_slice::Tuple=(:, …))
	paths = snap.paths
	ds = normalize_dataset(dataset)
	if ds ∈ VELOCITY_DATASETS && !isempty(snap.velocity_paths)
		paths = snap.velocity_path
	end
	if ds ∈ POTENTIAL_DATASETS && !isempty(snap.potential_paths)
		paths = snap.potential_paths
	end
	# get dataset shape
	determine_dataset_info(snap, dataset)
	dataset_shape_orig = snap.dataset_shape[dataset]
	dataset_shape = snap.dataset_shape_reshaped[dataset]
	dataset_dtype = snap.dataset_dtype[dataset]
	dataset_len = dataset_shape[end]
	num_elements = prod(dataset_shape)
	@assert num_elements % dataset_len == 0
	# special cases: particle data, (fuzzy DM) grid data
	ptype_match = match(r"/PartType(\d)/", ds)
	particle_data = !isnothing(ptype_match)
	particle_velocities = false
	if particle_data
		ptype = parse(Int, ptype_match[1])
		particle_velocities = match(r"/PartType(\d)/Velocities", ds)
	end
	grid_data = any(startswith.(Ref(ds), ("/FuzzyDM/", "/DensityGrid/")))
	# calculate number of elements per dimension
	num_elements_per_dim = num_elements_per_dim_colmajor(dataset_shape)
	@assert num_elements_per_dim[end] == num_elements ÷ dataset_len
	# determine indices and result array size
	# TODO: how much could this be generalized using HDF5.jl?
	#       cf. https://stackoverflow.com/a/38775157 (h5py)
	#       HDF5.jl seems more restricted (only accepts ranges for indexing)
	data_slice = dataset_indices(snap, dataset, data_slice)
	x_slice = data_slice[end]  # column-major
	# doesn’t support multiple wrap-around for x
	@assert length(split_ranges(x_slice)) ≤ 2
	result_shape = length.(data_slice)
	num_elements_per_dim_result = num_elements_per_dim_colmajor(result_shape)
	num_in_slab = Int64(prod(result_shape[begin:end - 1]))
	reshape_1d = grid_data && length(dataset_shape_orig) == 1
	result = fill(
		dataset_dtype <: AbstractFloat ? dataset_dtype(NaN) : dataset_dtype(-1),
		result_shape
	)
	if reshape_1d
		# flattened view of array
		result1d = reshape(result, :)
	end
	result_offsets = ((firstindex(result, i) for i in 1:ndims(result))...,)
	result_xend = lastindex(result, ndims(result))
	# determine files to load
	file_nums = indices_to_files(
		dataset, x_slice, snap.first_slab_x_of_file[dataset],
		snap.slabs_x_per_file[dataset]
	)
	# read data from corresponding files
	x_read = 0
	x_read_extra = 0
	for file_num in file_nums
		slabstart_x = snap.first_slab_x_of_file[dataset][file_num]
		nslab_x = snap.slabs_x_per_file[dataset][file_num]
		# adjust for local indexing within the current file
		# construct source and destination ranges
		# TODO: hard-codes HDF5.jl indexing (see above)
		x_thisfile_slices = filter(!isempty, [
			(r ∩ (slabstart_x .+ (0:nslab_x - 1))) .+ (1 - slabstart_x)
			for r in split_ranges(x_slice)
		])
		# TODO: hard-codes HDF5.jl indexing (see above)
		# TODO: inefficient
		@assert !isempty(x_thisfile_slices) 
		@assert all(all(i ∈ 1:nslab_x for i in r) for r in x_thisfile_slices)
		slices_src = (
			(split_ranges(r) for r in data_slice[begin:end - 1])...,
			x_thisfile_slices
		)
		slices_src_lengths = ((
			cumsum((0, (length(r) for r in ranges)...))
			for ranges in slices_src
		)...,)
		x_slices_dest = [
			(result_offsets[end] + x_read) .+
			(0:length(x_thisfile_slices[begin]) - 1)
		]
		if length(x_thisfile_slices) > 1
			@assert length(x_thisfile_slices) == 2
			@assert file_num == first(file_nums)
			x_slices_dest = [
				only(x_slices_dest),
				result_xend - length(x_thisfile_slices[end]) + 1:result_xend
			]
		end
		slices_dest = (
			(
				[(
					result_offsets[i] .+ (L[i]:L[i + 1] - 1)
					for i in eachindex(L)[begin:end - 1]
				)...]
				for L in slices_src_lengths[begin:end - 1]
			)...,
			x_slices_dest
		)
		@assert length.(slices_dest) == length.(slices_src)
		# read HDF5 data
		x_read_thisfile = 0
		file_path = paths[file_num]
		h5open(file_path, "r") do snap_part
			d = snap_part[dataset]
			@assert size(d)[begin:end - 1] == dataset_shape_orig[begin:end - 1]
			@assert length(d) % num_elements_per_dim[end] == 0
			if particle_data
				header = attrs(snap_part["Header"])
				@assert header["NumPart_ThisFile"][ptype] == nslab_x
			end
			# when reshaping: (generally) can’t use single range due to
			# non-constant step size in HDF5 dataset
			if reshape_1d
				@assert length(dataset_shape) > 1
				@assert ndims(d) == 1
				@assert length(d) == nslab_x * num_elements_per_dim[end]
				# when reading full, contiguous slabs (most common case):
				# optimize read by using single range
				# TODO: hard-codes HDF5.jl indexing (see above)
				partial_slices = data_slice .!= (:).(1, dataset_shape)
				num_partial = length(partial_slices)
				for ps in partial_slices
					if ps
						break
					else
						num_partial -= 1
					end
				end
				last_partial = max(length(dataset_shape) - num_partial + 1, 2)
				if step(data_slice[end]) == 1 &&
						!any(partial_slices[begin:end - 1])
					@assert num_in_slab == num_elements_per_dim[end]
					for (xsl_dest, xsl_src) in zip(
						slices_dest[end], slices_src[end]
					)
						@assert isone(step(xsl_dest))
						@assert isone(step(xsl_src))
						iodest = firstindex(result, ndims(result))
						# TODO: hard-codes HDF5.jl indexing (see above)
						iosrc = 1
						oxsl_dest = xsl_dest .- iodest
						oxsl_src = xsl_src .- iosrc
						sl_dest_slab = iodest .+ (
							first(oxsl_dest) * num_in_slab:
							(last(oxsl_dest) + 1) * num_in_slab - 1
						)
						sl_src_slab = iosrc .+ (
							first(oxsl_src) * num_in_slab:
							(last(oxsl_src) + 1) * num_in_slab - 1
						)
						result1d[sl_dest_slab] = d[sl_src_slab]
						x_read_thisfile += length(xsl_src)
					end
				# otherwise: read slices as large as possible in a loop
				# TODO: check how many read calls would be done and read
				#       larger slabs (discarding surplus data) if number is
				#       too high (i.e. impose a maximum number of read
				#       calls)?
				else
					for (sl_dest, sl_src) in zip(
						Iterators.product(slices_dest...),
						Iterators.product(slices_src...)
					)
						@assert num_partial > 0
						if partial_slices[begin]
							@assert length(sl_src[begin]) ==
								length(sl_dest[begin])
							slab_size = length(sl_src[begin])
							sl_src_slab = sl_src[begin] .- first(sl_src[begin])
						else
							@assert isone(step(sl_dest[begin]))
							slab_size = num_elements_per_dim[last_partial]
							@assert slab_size ==
								num_elements_per_dim_result[last_partial]
							sl_src_slab = 0:slab_size - 1
						end
						for (idx_offset_dest, partial_idx_src) in zip(
							Iterators.product((
								0:length(i) - 1
								for i in sl_src[last_partial:end]
							)...),
							Iterators.product(sl_src[last_partial:end]...)
						)
							# TODO: hard-codes HDF5.jl indexing (see above)
							offset_src = first(sl_src[last_partial - 1]) + sum(
								(i - 1) * dim_num for (i, dim_num) in zip(
									partial_idx_src,
									num_elements_per_dim[last_partial:end]
								)
							)
							offset_dest = first(sl_dest[last_partial - 1]) + sum(
								(o + first(s) - f) * dim_num
								for (o, s, f, dim_num) in zip(
									idx_offset_dest, sl_dest[last_partial:end],
									result_offsets[last_partial:end],
									num_elements_per_dim_result[last_partial:end]
								)
							)
							result1d[offset_dest .+ (0:slab_size - 1)] =
								d[offset_src .+ sl_src_slab]
						end
					end
					x_read_thisfile += sum(length.(slices_src[end]))
				end
			# when not reshaping: straightforward
			else
				for (sl_dest, sl_src) in zip(
					Iterators.product(slices_dest...),
					Iterators.product(slices_src...)
				)
					result[sl_dest...] = d[sl_src...]
				end
				x_read_thisfile += sum(length.(slices_src[end]))
			end
		end
		@assert x_read_thisfile == sum(length.(x_thisfile_slices))
		x_read += x_read_thisfile
		if length(x_thisfile_slices) > 1
			extra = length(x_thisfile_slices[end])
			x_read -= extra
			x_read_extra += extra
		end
	end
	x_read_total = x_read + x_read_extra
	@assert x_read_total == length(x_slice)
	@assert x_read_total * num_in_slab == length(result)
	@assert all(isfinite(x) for x in result)
	if particle_velocities && snap.param["ComovingIntegrationOn"]
		# convert from GADGET velocity
		result /= sqrt(snap.header["Time"])
	end
	return result
end

function indices_to_files(dataset, idx, first_slab_x_of_file, slabs_x_per_file)
	@assert length(first_slab_x_of_file) == length(slabs_x_per_file)
	num_files = length(first_slab_x_of_file)
	idx_len = first_slab_x_of_file[end] + slabs_x_per_file[end]
	@assert all(idx .≥ 1)
	@assert all(idx .≤ idx_len)
	result = OrderedSet{Int}()
	file_num = 0
	for ip in idx
		while (
			ip < first_slab_x_of_file[file_num] ||
			ip ≥ first_slab_x_of_file[file_num] + slabs_x_per_file[file_num]
		)
			file_num = (file_num + 1) % num_files
		end
		push!(result, file_num)
	end
	return result
end

function determine_chunks(
	snap, mpi_rank, mpi_size;
	num_slabs=snap.pmgrid, slab_size=snap.pmgrid^2,
	element_size=GRID_ELEMENT_MEM_SIZE, chunk_mem_size=CHUNK_MEM_SIZE_DEFAULT,
	return_max_num_chunks=false
)
	# make sure we’re using large enough integers
	slab_size = Int64(slab_size)
	# determine which region to read
	nslab_x = num_slabs ÷ mpi_size
	nslab_x_max = Int(ceil(num_slabs / mpi_size))
	slabstart_x = mpi_rank * nslab_x
	nslab_x_rest = num_slabs % mpi_size
	if mpi_rank < nslab_x_rest
		nslab_x += 1
		slabstart_x += mpi_rank
	else
		slabstart_x += nslab_x_rest
	end
	# determine x chunk size
	x_chunk_size_target = isinf(chunk_mem_size) ?
		Inf : max(chunk_mem_size ÷ (slab_size * element_size), 1)
	x_chunk_size = min(x_chunk_size_target, nslab_x)
	# determine chunks
	ix_len = slabstart_x + nslab_x
	res = Tuple{Int, Int}[]
	ix_start = slabstart_x
	while ix_start < ix_len
		ix_end = min(ix_start + x_chunk_size, ix_len)
		push!(res, (ix_start, ix_end))
		ix_start = ix_end
	end
	if return_max_num_chunks
		return res, Int(ceil(nslab_x_max / x_chunk_size_target))
	else
		return res
	end
end


#TODO
#	def convert_qty_from_internal_units(
#		self, value, target_unit, return_qty=False
#	):
#		target_unit_in_internal_units = self.qty_in_internal_units(
#			1, target_unit
#		)
#		if numpy.isclose(target_unit_in_internal_units, 1):
#			target_unit_in_internal_units = 1.
#		qty = astropy.units.Quantity(
#			value / target_unit_in_internal_units, target_unit
#		)
#		return qty if return_qty else qty.value


#function qty_in_internal_units(snap, qty; return_qty=false)
#	h = snap.header["HubbleParam"]
#	UnitLength_in_cm = snap.header["UnitLength_in_cm"] / h
#	UnitVelocity_in_cm_per_s = snap.header["UnitVelocity_in_cm_per_s"]
#	UnitMass_in_g = snap.header["UnitMass_in_g"] / h
#	UnitTime_in_s = UnitLength_in_cm / UnitVelocity_in_cm_per_s
#	au = astropy.units
#	qty = au.Quantity(value, unit).decompose([
#		au.Unit(UnitLength_in_cm * au.cm),
#		au.Unit(UnitTime_in_s * au.s),
#		au.Unit(UnitMass_in_g * au.g)
#	])
#	return return_qty ? qty : ustrip(qty)
#end


#function constant_in_internal_units(self, name, return_qty=false):
#	if name ≡ :H0
#		result = qty_in_internal_units(
#			snap, 100snap.header["HubbleParam"] * u"km/(s * Mpc)";
#			return_qty=true
#		)
#	elseif name ≡ :ρ_c0
#		G = constant_in_internal_units(snap, :G; return_qty=true)
#		H0 = constant_in_internal_units(snap, :H0; return_qty=true)
#		result = 3H0^2 / (8π * G)
#	else
#		qty = getproperty(PhysicalConstants, name)
#		result = qty_in_internal_units(snap, result; return_qty=true)
#	end
#	return return_qty ? result : ustrip(result)
#end


read_metadata(snapshot_files, snapshot_format::SnapshotFormat) =
	read_metadata(snapshot_files, Val(snapshot_format))
# HDF5 snapshot
function read_metadata(snapshot_files, ::Val{hdf5})
	@assert !isempty(snapshot_files)
	config = first_config = nothing
	param = first_param = nothing
	first_part = true
	local first_header
	local first_header_check
	for file_name in snapshot_files
		h5open(file_name, "r") do snap_part
			if haskey(snap_part, "/Config")
				config = Dict(attrs(snap_part["/Config"]))
			end
			if haskey(snap_part, "/Parameters")
				param = Dict(attrs(snap_part["/Parameters"]))
			end
			header = Dict(attrs(snap_part["/Header"]))
			if first_part
				first_part = false
				if !isnothing(config)
					first_config = copy(config)
				end
				if !isnothing(param)
					first_param = copy(param)
				end
				first_header = copy(header)
				pop!(first_header, "NumPart_ThisFile")
				first_header_check = copy(first_header)
			end
			# consistency checks
			header_check = copy(header)
			pop!(header_check, "NumPart_ThisFile")
			@assert (
				header["NumFilesPerSnapshot"] == length(snapshot_files)
			) "Mismatch of header with number of files: \
				$(header["NumFilesPerSnapshot"]) ≠ \
				$(length(snapshot_files))"
			@assert config == first_config
			@assert param == first_param
			@assert header_check == first_header_check
		end
	end
	return first_config, first_param, first_header
end
# GADGET snapshot
function read_metadata(snapshot_files, ::Val{gadget})
	#TODO
#	header = snap_gadget.read_header(snapshot_files)
	@assert false "TODO"
	return nothing, nothing, header
end
# numpy or raw binary files
read_metadata(snapshot_files, ::Union{Val{npy}, Val{raw}}) =
	(nothing, nothing, nothing)

function read_fof_tab(
	fof_tab_files; read_data=true, read_ids=true,
	# arguments limiting data size only apply to IDs
	start_group=0, max_data_size=0
)
	@assert !isempty(fof_tab_files)
	@assert !read_ids || (read_data && read_ids)
	@assert start_group ≥ 0
	@assert max_data_size ≥ 0
	curr_group = 1
	curr_group_for_ids = 1
	curr_id_in_group = 1
	tot_ids = 0
	ids_dataset_present = falses(length(fof_tab_files))
	if read_ids
		ids = Vector{Vector{Int64}}[]
	end
	first_part = true
	for (num_file, file_name) in senumerate(
		sort(fof_tab_files; by=arepo_filename_key); start=0
	)
		h5open(file_name, "r") do fof_tab_part
			config = fof_tab_part["/Config"]
			param = fof_tab_part["/Parameters"]
			header = fof_tab_part["/Header"]
			h5grp = fof_tab_part["/Group"]
			if first_part
				first_part = false
				first_config = Dict(attrs(config))
				first_param = Dict(attrs(param))
				first_header = Dict(attrs(header))
				first_header_check = Dict(attrs(header))
				pop!(first_header_check, "Ngroups_ThisFile")
				pop!(first_header_check, "Nsubgroups_ThisFile")
				pop!(first_header_check, "Nids_ThisFile")
				groups = StructVector(fill(
					Group{Float32}(), first_header["Ngroups_Total"]
				))
			end
			# consistency checks
			header_check = Dict(attrs(header))
			pop!(header_check, "Ngroups_ThisFile")
			pop!(header_check, "Nsubgroups_ThisFile")
			pop!(header_check, "Nids_ThisFile")
			@assert attrs(header)["NumFiles"] == length(fof_tab_files)
			@assert Dict(attrs(config)) == first_config
			@assert Dict(attrs(param)) == first_param
			@assert Dict(attrs(header_check)) == first_header_check
			# read data
			ngroup = attrs(header)["Ngroups_ThisFile"]
			if read_data && length(h5grp) > 0
				idx = curr_group:curr_group + ngroup
				groups.num_file[idx] = num_file
				groups.len[idx] = h5grp["GroupLenType"]
				groups.mass[idx] = h5grp["GroupMassType"]
				groups.cm[idx] = h5grp["GroupCM"]
				groups.pos[idx] = h5grp["GroupPos"]
				groups.vel[idx] = h5grp["GroupVel"]
				groups.tot_len[idx] = h5grp["GroupLen"]
				groups.tot_mass[idx] = h5grp["GroupMass"]
				if haskey(h5grp, "Group_M_Mean200")
					groups.m200[idx] = h5grp["Group_M_Mean200"]
				end
				if haskey(h5grp, "Group_R_Mean200")
					groups.r200[idx] = h5grp["Group_R_Mean200"]
				end
				if haskey(h5grp, "Group_Epot_Mean200")
					groups.potegy[idx] = h5grp["Group_Epot_Mean200"]
				end
				@assert all(
					sum(h5grp["GroupLenType"]; dims=1) .== h5grp["GroupLen"]
				)
				@assert all(
					sum(h5grp["GroupMassType"]; dims=1) .≈ h5grp["GroupMass"]
				)
			end
			# read IDs
			ids_dataset_present[num_file] = haskey(h5ids, "ID")
			if ids_dataset_present[num_file]
				if read_ids
					curr_group_for_ids, curr_id_in_group = read_fof_ids!(
						ids, fof_tab_part, groups, curr_group_for_ids,
						curr_id_in_group; start_group, max_data_size
					)
				end
				tot_ids += length(ids_dataset)
			end
		end
		# update current group offset
		curr_group += ngroup
	end
	if !any(ids_dataset_present)
		# skip check if the ID dataset was not present in any input file
		tot_ids = sum(groups.tot_len)
	end
	@assert curr_group == first_header["Ngroups_Total"] (
		"$curr_group ≠ $(first_header["Ngroups_Total"])"
	)
	if read_data
		@assert sum(groups.tot_len) == tot_ids (
			"$(sum(groups.tot_len)) != $tot_ids"
		)
	end
	@assert !read_ids || !isempty(ids)
	@assert !read_ids || (curr_group == curr_group_for_ids == length(groups)) (
		"$curr_group $curr_group_for_ids $(length(groups))"
	)
	@assert !read_ids || (
		start_group > 0 || max_data_size > 0 || length(groups) == length(ids)
	) "$(length(groups)) $(length(ids))"
	if read_ids
		range_num_ids = sum(length(gids) for gids in ids)
		range_tot_len = sum(
			groups.tot_len[start_group:start_group + length(ids)]
		)
		@assert range_num_ids == range_tot_len "$range_num_ids ≠ $range_tot_len"
		@assert !(start_group > 0 || max_data_size > 0) ||
			range_num_ids == tot_ids "$range_num_ids ≠ $tot_ids"
		check_fof_ids(ids, first_config)
	end
	return (first_config, first_param, first_header), groups,
		read_ids ? ids : nothing
end

function read_fof_ids!(
	ids, h5file, groups, curr_group, curr_id_in_group;
	start_group, max_data_size
)
	h5ids = h5file["/IDs/ID"]
	# determine size of ids in bytes
	nbytes_ids = sum(sizeof(gids) for gids in ids)
	# start ID read loop
	curr_id_in_file = 1
	id_len_file = length(h5ids)
	while curr_id_in_file < id_len_file
		lidx = curr_group - start_group
		group_len = groups.tot_len[curr_group]
		ids_left_in_group = group_len - curr_id_in_group
		ids_left_in_file = id_len_file - curr_id_in_file
		num_ids_to_read = min(ids_left_in_group, ids_left_in_file)
		# only read actual data after start group and while maximum data size
		# has not been reached, but always finish reading a started group
		read_data = curr_group >= start_group && (
			max_data_size == 0 || nbytes_ids < max_data_size ||
			lidx < length(ids)
		)
		if read_data
			if lastindex(ids) + 1 == lidx
				# start reading IDs for a new group
				push!(ids, fill(-1, group_len))
				nbytes_ids += group_len * sizeof(eltype(ids[lidx]))
			end
			@assert firstindex(ids) ≤ lidx ≤ lastindex(ids)
			ids[lidx][curr_id_in_group:curr_id_in_group + num_ids_to_read] =
				h5ids[curr_id_in_file:curr_id_in_file + num_ids_to_read]
		end
		if curr_id_in_group + num_ids_to_read == group_len
			# finished reading IDs for the current group
			@assert !read_data || length(ids[lidx]) == group_len
			curr_group += 1
			curr_id_in_group = 0
		else
			# file ended, but IDs for the current group are still incomplete
			curr_id_in_group += num_ids_to_read
		end
		curr_id_in_file += num_ids_to_read
	end
	@assert curr_id_in_file == id_len_file
	return curr_group, curr_id_in_group
end

function check_fof_ids(ids, config)
	@assert all(all(≥(0), gids) for gids in ids)
	@assert all(allunique(gids) for gids in ids)
	pmgrid = Int(config["PMGRID"])
	if haskey(config, "FDM_PSEUDOSPECTRAL")
		@assert all(all(gids < pmgrid^3) for gids in ids) [
			(
				group_idx, length(gids), length(gids[gids ≥ pmgrid^3]),
				gids, gids[gids < pmgrid^3], gids[gids ≥ pmgrid^3]
			) for (group_idx, gids) in senumerate(ids; start=0)
		]
	end
end

normalize_dataset(dataset) = "/" * lstrip(==('/'), dataset)

num_elements_per_dim_rowmajor(shape) =
	[collect(reverse(cumprod(reverse(shape)))[begin + 1:end]); 1]

num_elements_per_dim_colmajor(shape) =
	[1; collect(cumprod(shape)[begin:end - 1])]


end
