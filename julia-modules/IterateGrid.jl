module IterateGrid

using EllipsisNotation
using EllipsisNotation: (..) as …
using Formatting

push!(LOAD_PATH, @__DIR__)
using MPIUtil
using Snapshots

export iterate_grid


FDM_DENSITY_DATASET = "/FuzzyDM/DENSITY"
ELEMENT_SIZES = Dict(
	:psi => Snapshots.GRID_ELEMENT_MEM_SIZE,
	:energies => 6sizeof(Float64),
	# TODO: better estimates for the following entries
	:dataset => sizeof(Float64),
	:particles => 3sizeof(Float64),
)

cache = nothing
cache_meta = nothing

#TODO: update from ix_start, ix_end to full slices
function default_update_callback(
	snap, it_start, ix_chunks, ix_start, ix_end, tdiff
)
	if it_start
		num_tot_str = format(ix_chunks[end][end], commas=true)
		width = length(num_tot_str)
		println0(
			"Reading $(format(ix_start; width, commas=true)) to \
			$(format(ix_end; width, commas=true)) (max: $num_tot_str)"
		)
		flush(stdout)
	elseif tdiff > 5
		println0("\tTook $tdiff sec.")
		flush(stdout)
	end
end

function clear_cache()
	cache = nothing
	cache_meta = nothing
	GC.gc()
end

function read_grid(snap, data_slice, grid_type, use_rho, datasets, chunk_idx)
	grid = nothing
	ds_rho = false
	if grid_type ≡ :dataset
		ds_grid = [
			read_snap_data(snap, ds; data_slice) for ds in datasets
			if ds != FDM_DENSITY_DATASET
		]
		ds_rho = FDM_DENSITY_DATASET in datasets
		if ds_rho
			use_rho = true
		end
	end
	if grid_type in (:psi, :energies, :cumsum) || ds_rho
		psi_re = read_snap_data(snap, "/FuzzyDM/PsiRe"; data_slice)
		if use_rho
			grid = psi_re.^2
		else
			grid = zeros_like(ComplexF64, size(psi_re))
			grid += psi_re
		end
		psi_re = nothing
		im_present = dataset_present(snap, "/FuzzyDM/PsiIm")
		psi_im = im_present ?
			read_snap_data(snap, "/FuzzyDM/PsiIm"; data_slice) : 0
		if use_rho
			grid += psi_im.^2
		else
			@assert im_present
			grid .+= psi_im * im
		end
		psi_im = nothing
		GC.gc()
	end
	if grid_type ≡ :dataset
		grid_rho = grid
		grid = ds_grid
		if ds_rho
			insert!(
				grid, findfirst(==(FDM_DENSITY_DATASET), datasets), grid_rho
			)
		end
		grid = (grid...,)
	end
	if grid_type ≡ :energies
		cell_vol = (snap.box_size / snap.pmgrid)^3
		# need density for kinetic energy
		rho = grid
		vx = read_snap_data(snap, "/FuzzyDM/MomentumDensity0"; data_slice) / rho
		vy = read_snap_data(snap, "/FuzzyDM/MomentumDensity1"; data_slice) / rho
		vz = read_snap_data(snap, "/FuzzyDM/MomentumDensity2"; data_slice) / rho
		quantum = read_snap_data(snap, "/FuzzyDM/(GradSqrtRho)^2"; data_slice)
		#TODO
#		hbar = snap.constant_in_internal_units('hbar')
#		c = snap.constant_in_internal_units('c')
#		m_ev = snap.param['AxionMassEv']
#		m = snap.qty_in_internal_units(m_ev, 'eV') / c**2
#		if chunk_idx == 1
#			println0(
#				"iterate_grid: Using constants hbar = $hbar, m = $m, \
#				hbar/m = $(hbar / m)"
#			)
#		end
#		quantum *= (hbar / m)**2 * cell_vol / 2
		grid = (; quantum, vx, vy, vz, rho)
	end
	if grid_type ≡ :cumsum
		rho = grid
		cumsum = read_snap_data(snap, "/DensityGrid/cumsum"; data_slice)
	end
	return grid
end

function iterate_grid(
	f, snap;
	update_callback=default_update_callback,
	grid_type=:psi, use_rho=true, datasets=nothing,
	chunk_mem_size=Snapshots.CHUNK_MEM_SIZE_DEFAULT, data_slice=(:, …),
	cache_full_grid=true, mpi_rank=mpi_rank(), mpi_size=mpi_size()
)
	global cache, cache_meta
	@assert grid_type in (:psi, :energies, :velocity, :dataset)
	if !cache_full_grid
		clear_cache()
	end
	if grid_type in (:energies, :cumsum)
		use_rho = true
	end
	#TODO: make this cleaner (not hard-coding /FuzzyDM/PsiRe)
	data_slice = dataset_indices(snap, "/FuzzyDM/PsiRe", data_slice)
	for s in data_slice
		@assert step(s) == 1
	end
	slab_slice = data_slice[begin:end - 1]
	result_shape = length.(data_slice)
	ix_min = first(data_slice[end])
	println0(
		"iterate_grid(): Restricting iteration to range $data_slice"
	)
	println0(
		"iterate_grid(): \t (shape of iterated grid: $result_shape)"
	)
	element_size = ELEMENT_SIZES[grid_type]
	if grid_type ≡ :dataset
		element_size *= length(datasets)
	end
	ix_chunks, max_num_chunks = determine_chunks(
		snap, mpi_rank, mpi_size; element_size=element_size,
		chunk_mem_size=chunk_mem_size, num_slabs=result_shape[end],
		slab_size=prod(result_shape[begin:end - 1]),
		return_max_num_chunks=true
	)
	ix_chunks = [
		(
			mod1(ix_min + ix_start, snap.pmgrid),
			mod1(ix_min + ix_end - 1, snap.pmgrid)
		) for (ix_start, ix_end) in ix_chunks
	]
	println0(
		"iterate_grid(): max_num_chunks = $max_num_chunks, \
		ix_chunks = $(ix_chunks[begin:min(end, 8)])\
		$(length(ix_chunks) > 8 ? " … " : "")\
		$(length(ix_chunks) > 8 ? ix_chunks[end - 2:end] : "")"
	)
	for (chunk_idx, (ix_start, ix_end)) in pairs(ix_chunks)
		t0 = time()
		if !isnothing(update_callback)
			update_callback(snap, true, ix_chunks, ix_start, ix_end, 0)
		end
		chunk_slice = (slab_slice..., ix_start:ix_end)
		cache_invalid = isnothing(cache) ||
			cache_meta != (snap, grid_type, use_rho, datasets, data_slice)
		if !cache_full_grid || cache_invalid
			grid = read_grid(
				snap, chunk_slice, grid_type, use_rho, datasets, chunk_idx
			)
		end
		if cache_full_grid && max_num_chunks == 1
			if cache_invalid
				println0(
					"iterate_grid(): Saving grid to cache because the entire \
					entire grid fits into memory"
				)
				cache = grid
				cache_meta = (snap, grid_type, use_rho, datasets, data_slice)
			else
				println0("iterate_grid(): Using cached grid")
				grid = cache
			end
		end
		ix_info = (pmgrid=snap.pmgrid, min=ix_min, start=ix_start, stop=ix_end)
		args = (grid, ix_info, data_slice)
		if applicable(iterate, f)
			for func in f
				func(args...)
			end
		else
			f(args...)
		end
		t1 = time()
		if !isnothing(update_callback)
			update_callback(snap, false, ix_chunks, ix_start, ix_end, t1 - t0)
		end
	end
end


#PARTICLE_DATASETS = 'Coordinates,Velocities,Masses,ParticleIDs'.split(',')
#ParticleData = collections.namedtuple(
#	'ParticleData', PARTICLE_DATASETS,
#	defaults=[
#		# Velocities
#		numpy.empty((0, 3), dtype='f4'),
#		# Masses
#		numpy.empty((0,), dtype='f8'),
#		# ParticleIDs
#		numpy.empty((0,), dtype='u8'),
#	]
#)

#cache_particles = None
#cache_meta_particles = None


#def dataset_name(dataset):
#	return dataset.lstrip('/').split('/', maxsplit=1)[1]

#def read_particles(snap, data_slice, datasets, region):
#	#TODO: support GADGET snapshots in snapshot.py
#	if region is None:
#		data = {
#			dataset_name(ds):
#			snap.read_snapshot(ds, data_slice=data_slice)
#			for ds in datasets
#		}
#	else:
#		t0 = time.perf_counter()
#		if mpi_rank == 0:
#			# TODO: Does not seem to use yt’s parallelism with
#			#       yt.enable_parallelism() -> works on 1 CPU only, extremely
#			#       slow!
#			import yt
#			ytds = yt.load(snap.paths[0])
#			ytds_region = ytds.box(*region)
#			data = {
#				dataset_name(ds): numpy.array_split(
#					ytds_region[tuple(ds.lstrip('/').split('/', 1))].ndview,
#					mpi_size
#				) for ds in datasets
#			}
#		else:
#			data = {dataset_name(ds): None for ds in datasets}
#		t1 = time.perf_counter()
#		println0(f'YT read took {t1 - t0} sec.', flush=True)
#		t0 = time.perf_counter()
#		for ds_name in data:
#			data[ds_name] = mpi_comm.scatter(data[ds_name], root=0)
#		t1 = time.perf_counter()
#		println0(f'Scatter took {t1 - t0} sec.', flush=True)
#	return data


##TODO: multiple ptypes, different datasets per ptype
#def iterate_particles(
#	snap, f, data, param=None, update_callback=default_update_callback,
#	ptype=1, datasets=('Coordinates',), region=None,
#	chunk_mem_size=snapshot.CHUNK_MEM_SIZE_DEFAULT,
#	data_slice=numpy.index_exp[:, ...], cache_full_data=True
#):
#	assert all(ds in ParticleData._fields for ds in datasets)
#	datasets = tuple(f'/PartType{ptype}/{ds}' for ds in datasets)
#	data_slice = numpy.index_exp[data_slice]
#	global cache_particles, cache_meta_particles
#	if not cache_full_data:
#		clear_cache_particles()
#	cache_invalid = (
#		cache_particles is None or
#		cache_meta_particles[:-1] != (snap, datasets, data_slice) or 
#		cache_meta_particles[-1] is not None != region is not None or
#		(
#			cache_meta_particles[-1] is not None and region is not None and
#			any(
#				numpy.any(rpc != rp)
#				for rpc, rp in zip(cache_meta_particles[-1], region)
#			)
#		)
#	)
#	part_slice, *rest_slice = data_slice
#	rest_slice = tuple(rest_slice)
#	numpart_total = snap.numpart_total(ptype)
#	println0(
#		f'iterate_particles(): Restricting iteration to range {data_slice}'
#	)
#	if region is None:
#		element_size = ELEMENT_SIZES['particles'] * len(datasets)
#		part_chunks, max_num_chunks = snap.determine_chunks(
#			mpi_rank, mpi_size, element_size=element_size,
#			chunk_mem_size=chunk_mem_size, num_slabs=numpart_total, slab_size=1,
#			return_max_num_chunks=True
#		)
#		println0(
#			f'iterate_particles(): max_num_chunks = {max_num_chunks}, '
#			f'ix_chunks = {part_chunks[:8]}'
#			f'{" … " if len(part_chunks) > 8 else ""}'
#			f'{part_chunks[-2:] if len(part_chunks) > 8 else ""}'
#		)
#		part_range = range(*part_slice.indices(numpart_total))
#		part_first = part_range.start
#		part_chunks = [
#			(part_first + part_start, part_first + part_end)
#			for part_start, part_end in part_chunks
#		]
#	else:
#		println0(
#			f'iterate_particles(): Restricting iteration to region {region}'
#		)
#		subregions_xpoints = numpy.linspace(
#			region[0][0], region[1][0], num=mpi_size + 1
#		)
#		# this works properly: yt regions check coordinates via
#		# left_edge <= x < right_edge
#		subregion_x = [
#			subregions_xpoints[mpi_rank], subregions_xpoints[mpi_rank + 1]
#		]
#		subregion = (numpy.copy(region[0]), numpy.copy(region[1]))
#		for i in range(2):
#			subregion[i][0] = subregion_x[i]
#		println0(f'iterate_grid.iterate(): Processing subregion {subregion}')
#		#TODO: how to read particles in chunks using yt?
#		assert data_slice == numpy.index_exp[:, ...]
#		part_chunks = [(-1, -1)]
#		max_num_chunks = 1
#	for part_start, part_end in part_chunks:
#		t0 = time.perf_counter()
#		if update_callback is not None:
#			update_callback(snap, True, part_chunks, part_start, part_end, 0)
#		chunk_slice = numpy.index_exp[part_start:part_end] + rest_slice
#		if not cache_full_data or cache_invalid:
#			pdata = ParticleData(
#				**read_particles(snap, chunk_slice, datasets, region)
#			)
#		if cache_full_data and max_num_chunks == 1:
#			if cache_invalid:
#				println0(
#					'iterate_particles(): Saving particle data to cache '
#					'because everything fits into memory'
#				)
#				cache_particles = pdata
#				cache_meta_particles = (snap, datasets, data_slice, region)
#			else:
#				println0('iterate_particles(): Using cached particle data')
#				pdata = cache_particles
#		if region is not None:
#			#TODO
#			part_chunks = [(0, len(getattr(pdata, dataset_name(datasets[0]))))]
#			part_start, part_end = part_chunks[0]
#		args = (pdata, numpart_total, part_start, part_end, param, data)
#		if isinstance(f, collections.abc.Iterable):
#			for func in f:
#				func(*args)
#		else:
#			f(*args)
#		t1 = time.perf_counter()
#		if update_callback is not None:
#			update_callback(
#				snap, False, part_chunks, part_start, part_end, t1 - t0
#			)


end
