module MPIUtil

#TODO !!!
__precompile__(false)

using MPI

export @define_mpi_globals, mpi_comm, mpi_rank, mpi_size,
	println0, mpi_println, mpi_println_each, dump0, mpi_dump

const _mpi_comm = Ref{MPI.Comm}()
const _mpi_rank = Ref{Int}()
const _mpi_size = Ref{Int}()

function __init__()
	MPI.Init()
	_mpi_comm[] = MPI.COMM_WORLD
	_mpi_rank[] = MPI.Comm_rank(_mpi_comm[])
	_mpi_size[] = MPI.Comm_size(_mpi_comm[])
end


mpi_comm() = _mpi_comm[]
mpi_rank() = _mpi_rank[]
mpi_size() = _mpi_size[]

macro define_mpi_globals()
	mod = @__MODULE__
	return esc(quote
		const mpi_comm = $mod.mpi_comm()
		const mpi_rank = $mod.mpi_rank()
		const mpi_size = $mod.mpi_size()
	end)
end

println0(args...; kwargs...) = mpi_println(0, args...; kwargs...)

function mpi_println(rank, args...; kwargs...)
	if mpi_rank() == rank
		println(args...; kwargs...)
	end
end

dump0(x; kwargs...) = mpi_dump(0, x; kwargs...)

function mpi_dump(rank, x; kwargs...)
	if mpi_rank() == rank
		dump(x; kwargs...)
	end
end

function mpi_println_each(args...; kwargs...)
	if mpi_rank() == 0
		println(args...; kwargs...)
		for i in 1:mpi_size() - 1
			other_args = MPI.recv(mpi_comm(); source=i, tag=77)
			println(other_args...; kwargs...)
		end
	else
		MPI.send(string.(args), mpi_comm(); dest=0, tag=77)
	end
end


end
