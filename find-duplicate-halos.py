#!/usr/bin/env python3
import argparse
import collections
from pathlib import Path
import numba
import numpy
import pandas
import bin_density
import m200_util
import read_snapshot
import read_snapshot.hdf5 as read_hdf5

parser = argparse.ArgumentParser(description='Find halos which are within '
	'R200 of another halo in a FoF group catalog')
parser.add_argument('path', type=Path, nargs='+',
	help='the path to the fof_tab catalog; any normal files are interpreted '
		'as single files, while all files in any given directory are '
		'interpreted as a single multi-file output')
args = parser.parse_args()


ResultEntry = collections.namedtuple('ResultEntry', 'id1, id2, dist_kpc')

def main():
	for path in args.path:
		# read metadata
		fof_tab_files = read_snapshot.snapshot_files(path)
		config, param, header = read_hdf5.read_metadata(
			fof_tab_files, otype='fof_tab'
		)
		box_size = header['BoxSize']
		# read group catalog
		halo_data = m200_util.read_m200_data(path)
		# process halos
#		result = process_halos(box_size, halo_data)
		keep = numpy.ones(len(halo_data), dtype='bool')
		result = process_halos(
			box_size, halo_data.id.to_numpy(),
			halo_data[['cx_kpc', 'cy_kpc', 'cz_kpc']].to_numpy(),
			halo_data.r200_kpc.to_numpy(),
			keep
		)
		# write results
		print(f'Total: {len(halo_data) - numpy.sum(keep)} overlapping halos')
		result = pandas.DataFrame(result)
		result.to_csv(
			path.with_name(f'{path.name}_duplicates.csv'), index=False
		)
		numpy.save(path.with_name(f'{path.name}_keep.npy'), keep)

@numba.njit
def process_halos(box_size, ids, center_kpc, r200_kpc, keep):
	assert len(ids) == len(center_kpc) == len(r200_kpc)
	assert numpy.all(ids == numpy.arange(len(ids)))
	result = []
	for halo1 in range(len(ids)):
		if halo1 % 200 == 0:
			print('Processing halo', ids[halo1], '/', len(ids))
		center1 = center_kpc[halo1]
		r200_1 = r200_kpc[halo1]
		count = 0
		for halo2 in range(halo1 + 1, len(ids)):
			center2 = center_kpc[halo2]
			r200_2 = r200_kpc[halo2]
			dist_vec = bin_density.dist_periodic_wrap_vec(
				box_size, center1 - center2
			)
			#TODO: how much overlap to consider?
			#TODO: how to handle “chains” of halos?
			dist2 = numpy.dot(dist_vec, dist_vec)
			if dist2 < (r200_1 + r200_2)**2:
				count += 1
				result.append(ResultEntry(
					id1=ids[halo1], id2=ids[halo2], dist_kpc=numpy.sqrt(dist2)
				))
				if r200_2 <= r200_1:
					keep[halo2] = False
				else:
					keep[halo1] = False
		if count > 1:
			print('Halo', ids[halo1], ':  Overlap with', count, 'other halos')
	return result

# def process_halos(data):
# 	result = []
# 	for halo1 in data.itertuples(index=False):
# 		if halo1.id % 200 == 0: print('Processing halo', halo1.id)
# 		center1 = numpy.array([data.cx_kpc, data.cy_kpc, data.cz_kpc])
# 		r200 = data.r200_kpc
# 		for halo2 in data.itertuples(index=False):
# 			if halo1 == halo2: continue
# 			center2 = numpy.array([data.cx_kpc, data.cy_kpc, data.cz_kpc])
# 			dist_vec = bin_density.dist_periodic_wrap_vec(
# 				box_size, center1 - center2
# 			)
# 			#TODO: how much overlap to consider?
# 			#TODO: how to handle “chains” of halos?
# 			dist2 = numpy.dot(dist_vec, dist_vec)
# 			if dist2 < r200**2:
# 				result.append(ResultEntry(
# 					id1=halo1.id, id2=halo2.id, dist=numpy.sqrt(dist2)
# 				))


if __name__ == '__main__':
	main()
