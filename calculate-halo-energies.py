#!/usr/bin/env python3
import argparse
import collections
import time
from pathlib import Path
import astropy.units
import h5py
import numba
import numpy
import iterate_grid
import mpi_util
import read_snapshot
import read_snapshot.hdf5 as read_hdf5
import snapshot
from bin_density import coord_periodic_wrap, coord_periodic_wrap_vec, dist_periodic_wrap
from mpi_util import MPI, mpi_comm, mpi_rank, mpi_size, print0
from snapshot import Snapshot


Energies = collections.namedtuple('Energies', 'kinetic, quantum')
IterateParam = collections.namedtuple(
	'IterateParam',
	'center_g3, ir200, com, vel_com, cell_len, cell_vol, scale_factor, hubble, '
	'mpi_rank'
)

parser = argparse.ArgumentParser(description='Calculate kinetic and quantum '
	'halo energy components for fuzzy dark matter FoF catalogs')
parser.add_argument('--halo-id-offset', type=int, default=0,
	help='do not process halos with IDs smaller than the given ID '
		'(default: %(default)d)')
parser.add_argument('--fof-group-min-len', type=int, default=8,
	help='minimum FoF length to be processed (default: %(default)d)')
parser.add_argument('--max-halos-per-iteration', type=int,
	help='maximum number of halos to process per iteration')
parser.add_argument('--snap-chunk-mem-size', type=float,
	default=snapshot.CHUNK_MEM_SIZE_DEFAULT / 1024**2,
	help='maximum amount of memory to use for snapshot data per core, in MiB '
		'(default: %(default)g)')
parser.add_argument('--load', type=Path,
	help='path to a previously-computed set of halo energies (default: '
		'<path>_halo_energy.hdf5')
parser.add_argument('--output-file-prefix', default='',
	help='prefix for the output file names')
parser.add_argument('--velocity-path', type=Path, nargs='+',
	help='separate snapshot files containing the velocity and density gradient '
		'(see argument “path”)')
parser.add_argument('path', type=Path, nargs='+',
	help='the path to the fof_tab catalog; any normal files are interpreted '
		'as single files, while all files in any given directory are '
		'interpreted as a single multi-file output')


def main():
	mpi_util.default_print_args['flush'] = True
	if args.velocity_path:
		assert len(args.velocity_path) == len(args.path)
	else:
		args.velocity_path = [None] * len(args.path)
	assert len(args.path) == len(args.velocity_path)
	if args.load:
		assert len(args.path) == 1, (
			'--load doesn’t really make sense with more than 1 path'
		)
	# process paths
	for path, vel_path in zip(args.path, args.velocity_path):
		process_path(path, vel_path)
		print0()

def process_path(path, vel_path):
	print0(path)
	if vel_path:
		print0(f'\t{vel_path}')
	fof_tab_files = read_snapshot.snapshot_files(path)
	# read fof_tab data
	fof_tab_data = None
	if mpi_rank == 0:
		fof_tab_data = read_hdf5.read_fof_tab(fof_tab_files, read_ids=False)
	# broadcast fof_tab data to other ranks
	if mpi_size > 1:
		fof_tab_data = mpi_comm.bcast(fof_tab_data, root=0)
	[fof_config, fof_param, fof_header], groups, _ = fof_tab_data
	print0(f'Processing fof_tab with {len(groups)} groups')
	if len(groups) == 0:
		print0('fof_tab contains no groups')
	# read snapshot metadata
	print0('Reading snapshot metadata…')
	snap = Snapshot(read_snapshot.output_path(path, fof_config, fof_param))
	if vel_path:
		snap.add_velocities(vel_path)
	snaptime = snap.header['Time']
	z = 1 / snaptime - 1
	print0(f'\tRedshift: z = {z}')
	# check existing energy data
	halo_ids_todo = None
	num_chunks = None
	output_path = args.load if args.load else path.with_name(
		f'{args.output_file_prefix}{path.name}_halo_energy.hdf5'
	)
	if mpi_rank == 0:
		if output_path.exists():
			print0(f'Not recomputing existing data in file “{output_path}”')
			with h5py.File(output_path, 'r') as of:
				assert of['KineticEnergy'].shape == of['QuantumEnergy'].shape
				assert of['KineticEnergy'].shape == (len(groups),), (
					f'“{output_path}” contains data with shape '
					f'{of["KineticEnergy"].shape} (expected {(len(groups),)})'
				)
		else:
			print0(
				f'“{output_path}” not found as existing file, starting from '
				'scratch'
			)
			assert not args.load
			# initialize HDF5 datasets
			with h5py.File(output_path, 'w') as of:
				of.create_dataset(
					'Velocity', shape=(len(groups), 3), dtype='f8',
					fillvalue=numpy.nan
				)
				of.create_dataset_like('CenterOfMass', of['Velocity'])
				of.create_dataset(
					'KineticEnergy', shape=(len(groups),), dtype='f8',
					fillvalue=numpy.nan
				)
				of.create_dataset_like('QuantumEnergy', of['KineticEnergy'])
				of.create_dataset_like('GridMass', of['KineticEnergy'])
		# determine which halo energies are both requested and missing
		with h5py.File(output_path, 'r') as of:
			halos_not_done = numpy.isnan(of['KineticEnergy'][...])
			halo_ids_todo = []
			for i in range(len(groups)):
				if (
					i >= args.halo_id_offset and
					groups[i].tot_len >= args.fof_group_min_len and
					halos_not_done[i]
				):
					halo_ids_todo.append(i)
			del halos_not_done
		print0(f'{len(halo_ids_todo)} halos left to process for file “{path}”')
		if len(halo_ids_todo) > 0:
			print0(f'(starting with halo {halo_ids_todo[0]})')
			max_num_halos = len(halo_ids_todo)
			if args.max_halos_per_iteration:
				max_num_halos = args.max_halos_per_iteration
			chunk_num_halos = min(max_num_halos, len(halo_ids_todo))
			num_chunks = int(numpy.ceil(len(halo_ids_todo) / chunk_num_halos))
			print0(
				f'Can process at most {max_num_halos} halos at once; will '
				f'have to perform {num_chunks} grid iterations'
			)
			print0()
	num_chunks = mpi_comm.bcast(num_chunks, root=0)
	halo_ids_todo = mpi_comm.bcast(halo_ids_todo, root=0)
	if len(halo_ids_todo) == 0:
		return
	print0('Starting calculations')
	for num_iteration, halo_ids_chunk in enumerate(
		numpy.array_split(halo_ids_todo, num_chunks), start=1
	):
		print0(
			f'Starting grid iteration {num_iteration}/{num_chunks}'
		)
		print0('Now calculating halo center-of-mass velocities')
		process_chunk_velocities(snap, groups, halo_ids_chunk, output_path)
		print0()
		print0('Now calculating halo energies')
		process_chunk_energies(snap, groups, halo_ids_chunk, output_path)
		print0()

def process_chunk_velocities(snap, groups, halo_ids_chunk, output_path):
	com, velocities, grid_mass = calculate_velocities(
		snap, groups, halo_ids_chunk
	)
	# save velocities to file
	if mpi_rank == 0:
		print0(f'Writing to file “{output_path}”')
		with h5py.File(output_path, 'a') as of:
			of['CenterOfMass'][halo_ids_chunk] = com
			of['Velocity'][halo_ids_chunk] = velocities
			of['GridMass'][halo_ids_chunk] = grid_mass

def process_chunk_energies(snap, groups, halo_ids_chunk, output_path):
	# need center of mass and COM velocity for kinetic energy
	com = None
	vel_com = None
	if mpi_rank == 0:
		with h5py.File(output_path, 'r') as of:
			com = of['CenterOfMass'][...]
			vel_com = of['Velocity'][...]
	com = mpi_comm.bcast(com, root=0)
	vel_com = mpi_comm.bcast(vel_com, root=0)
	energies = calculate_energies(snap, groups, halo_ids_chunk, com, vel_com)
	# save energies to file
	if mpi_rank == 0:
		print0(f'Writing to file “{output_path}”')
		with h5py.File(output_path, 'a') as of:
			of['KineticEnergy'][halo_ids_chunk] = energies.kinetic
			of['QuantumEnergy'][halo_ids_chunk] = energies.quantum

def construct_param_list(snap, groups, halo_ids, pos_com, vel_com):
	print0(f'Constructing param list for {len(halo_ids)} halos…')
	t0 = time.perf_counter()
	cell_len = snap.box_size / snap.pmgrid
	hubble = snap.hubble_function()
	param = numba.typed.List()
	for halo_id in halo_ids:
		halo = groups[halo_id]
		halo_center = halo.pos
		halo_center_g3 = numpy.floor(halo_center / cell_len + 0.1).astype('i8')
		assert not numpy.isnan(halo.r200)
		ir200 = halo.r200 / cell_len
		com = None if pos_com is None else pos_com[halo_id]
		vel = None if vel_com is None else vel_com[halo_id]
		param.append(IterateParam(
			center_g3=halo_center_g3, ir200=ir200, com=com, vel_com=vel,
			cell_len=cell_len, cell_vol=cell_len**3,
			scale_factor=snap.header['Time'], hubble=hubble, mpi_rank=mpi_rank
		))
	t1 = time.perf_counter()
	print0(f'\tTook {t1 - t0} sec.')
	return param

def calculate_velocities(snap, groups, halo_ids):
	a = snap.header['Time']
	cell_len = snap.box_size / snap.pmgrid
	com = numpy.zeros((len(halo_ids), 3), dtype='f8')
	vel = numpy.zeros((len(halo_ids), 3), dtype='f8')
	mass = numpy.zeros(len(halo_ids), dtype='f8')
	param = construct_param_list(snap, groups, halo_ids, None, None)
	t0 = time.perf_counter()
	iterate_grid.iterate(
		snap, halo_energies_bulk_factory(halo_velocity), (com, vel, mass),
		grid_type='energies', param=param,
		chunk_mem_size=round(args.snap_chunk_mem_size * 1024**2)
	)
	t1 = time.perf_counter()
	mpi_comm.Barrier()
	print0(f'Grid iteration took {t1 - t0} sec. in total')
	com_g = mpi_comm.reduce(com, op=MPI.SUM, root=0)
	vel_g = mpi_comm.reduce(vel, op=MPI.SUM, root=0)
	mass_g = mpi_comm.reduce(mass, op=MPI.SUM, root=0)
	if mpi_rank == 0:
		assert numpy.all(mass_g >= 0)
		numpy.divide(com_g.T, mass_g, out=com_g.T)
		for halo_idx in range(len(com)):
			com_g[halo_idx] = coord_periodic_wrap_vec(
				snap.box_size,
				com_g[halo_idx] + cell_len * param[halo_idx].center_g3
			)
		numpy.divide(vel_g.T, mass_g, out=vel_g.T)
	return com_g, vel_g, mass_g

def calculate_energies(snap, groups, halo_ids, com, vel_com):
	# calculate energy values for the given halos
	_ = numpy.zeros(len(halo_ids), dtype='f8')
	energies = Energies(
		kinetic=numpy.zeros_like(_),
		quantum=numpy.zeros_like(_),
	)
	del _
	# process grid
	param = construct_param_list(snap, groups, halo_ids, com, vel_com)
	t0 = time.perf_counter()
	iterate_grid.iterate(
		snap, halo_energies_bulk_factory(halo_energy), energies,
		grid_type='energies', param=param,
		chunk_mem_size=round(args.snap_chunk_mem_size * 1024**2)
	)
	t1 = time.perf_counter()
	mpi_comm.Barrier()
	print0(f'Grid iteration took {t1 - t0} sec. in total')
	# get global energy values
	kinetic_g = mpi_comm.reduce(energies.kinetic, op=MPI.SUM, root=0)
	quantum_g = mpi_comm.reduce(energies.quantum, op=MPI.SUM, root=0)
	return Energies(kinetic=kinetic_g, quantum=quantum_g)

def halo_energies_bulk_factory(halo_func):
	@numba.njit
	def halo_energies_bulk(
		energy_grids, pmgrid, ix_info, data_slice, param, data
	):
		ix_min, ix_start, ix_end = ix_info
		assert len(param) == len(data[0]) == len(data[1])
		if len(data) > 2:
			assert len(data) == 3
			assert len(param) == len(data[2])
		assert (
			energy_grids.quantum.shape == energy_grids.vx.shape ==
			energy_grids.vy.shape == energy_grids.vz.shape ==
			energy_grids.rho.shape
		)
		assert energy_grids.rho.shape[1:] == (pmgrid, pmgrid)
		with numba.objmode(t0='f8'):
			t0 = time.perf_counter()
		for halo_idx in range(len(param)):
			if halo_idx % 10 == 0:
				with numba.objmode(t0='f8'):
					t1 = time.perf_counter()
					if param[halo_idx].mpi_rank == 0 and (
						halo_idx == 0 or t1 - t0 > 30
					):
						print(
							(
								'\tCurrently processing halo {: 4} (of {: 4}) '
								'on rank {}…'
							).format(
								halo_idx, len(param), param[halo_idx].mpi_rank
							), flush=True
						)
						t0 = t1
			halo_func(
				energy_grids, pmgrid, ix_info, data_slice, param[halo_idx],
				(halo_idx, data)
			)
	return halo_energies_bulk

@numba.njit
def halo_energy(energy_grids, pmgrid, ix_info, data_slice, param, data):
	ix_min, ix_start, ix_end = ix_info
	v_grid = [energy_grids.vx, energy_grids.vy, energy_grids.vz]
	ix = [0, 0, 0]
	rho_grid = energy_grids.rho
	qnt_grid = energy_grids.quantum
	halo_idx, [kin, qnt] = data
	offset_ix, offset_iy, offset_iz = param.center_g3
	ir200 = param.ir200
	a = param.scale_factor
	H = param.hubble
	com = param.com
	vel_com = param.vel_com
	cell_len = param.cell_len
	cell_vol = param.cell_vol
	box_size = cell_len * pmgrid
	for dix in range(-ir200, ir200 + 1):
		ix[0] = coord_periodic_wrap(pmgrid, offset_ix + dix)
		if not (ix_start <= ix[0] < ix_end):
			continue
		for diy in range(-ir200, ir200 + 1):
			ix[1] = coord_periodic_wrap(pmgrid, offset_iy + diy)
			for diz in range(-ir200, ir200 + 1):
				ix[2] = coord_periodic_wrap(pmgrid, offset_iz + diz)
				ir2 = dix**2 + diy**2 + diz**2
				if ir2 <= ir200**2:
					m = rho_grid[ix[0] - ix_start, ix[1], ix[2]] * cell_vol
					dv2 = 0.
					for i in range(len(v_grid)):
						dv2 += (
							v_grid[i][ix[0] - ix_start, ix[1], ix[2]] -
							vel_com[i] +
							H * a * dist_periodic_wrap(
								box_size, ix[i] * cell_len - com[i]
							)
						)**2
					kin_val = 1/2 * m * dv2
					qnt_val = qnt_grid[ix[0] - ix_start, ix[1], ix[2]]
					# add to total
					kin[halo_idx] += kin_val
					qnt[halo_idx] += qnt_val

@numba.njit
def halo_velocity(energy_grids, pmgrid, ix_info, data_slice, param, data):
	ix_min, ix_start, ix_end = ix_info
	vx_grid = energy_grids.vx
	vy_grid = energy_grids.vy
	vz_grid = energy_grids.vz
	rho_grid = energy_grids.rho
	halo_idx, [com_out, vel_out, mass_out] = data
	offset_ix, offset_iy, offset_iz = param.center_g3
	ir200 = param.ir200
	cell_len = param.cell_len
	cell_vol = param.cell_vol
	for dix in range(-ir200, ir200 + 1):
		ix = coord_periodic_wrap(pmgrid, offset_ix + dix)
		if not (ix_start <= ix < ix_end):
			continue
		for diy in range(-ir200, ir200 + 1):
			iy = coord_periodic_wrap(pmgrid, offset_iy + diy)
			for diz in range(-ir200, ir200 + 1):
				iz = coord_periodic_wrap(pmgrid, offset_iz + diz)
				ir2 = dix**2 + diy**2 + diz**2
				if ir2 <= ir200**2:
					m = rho_grid[ix - ix_start, iy, iz] * cell_vol
					com_out[halo_idx, 0] += m * cell_len * dix
					com_out[halo_idx, 1] += m * cell_len * diy
					com_out[halo_idx, 2] += m * cell_len * diz
					vel_out[halo_idx, 0] += m * vx_grid[ix - ix_start, iy, iz]
					vel_out[halo_idx, 1] += m * vy_grid[ix - ix_start, iy, iz]
					vel_out[halo_idx, 2] += m * vz_grid[ix - ix_start, iy, iz]
					mass_out[halo_idx] += m


if __name__ == '__main__':
	args = parser.parse_args()
	main()

