#include <assert.h>
#include <complex.h>
#include <inttypes.h>
#include <limits.h>
#include <math.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdnoreturn.h>
#include <string.h>

//#include <getopt.h>
#include <hdf5.h>
#include <fftw3-mpi.h>
#include <mpi.h>

#ifdef __STDC_NO_COMPLEX__
#error "__STDC_NO_COMPLEX__ defined, complex math not supported"
#endif

#if !defined(__has_attribute) && !defined(__GNUC__)
/* remove GCC-style attributes if the compiler does not support them */
#define __attribute__(x)
#endif


#define ARRAY_LEN(x) ((sizeof (x)) / (sizeof (x)[0]))

typedef unsigned char uchar;
typedef unsigned short ushort;
typedef unsigned int uint;
typedef unsigned long ulong;
typedef unsigned long long ulonglong;
typedef double fft_real;
typedef fftw_complex fft_complex;
typedef size_t   LocalIdx;
typedef uint64_t GlobalIdx;
const MPI_Datatype GLOBAL_IDX_MPI = MPI_UINT64_T;
#define GLOBAL_IDX_PRI PRIu64
struct hdf5_dset_info {
	const char *path;
	uint num_dims;
	hid_t dset[2];
	hid_t dtype[2];
	hid_t dspace[2];
};
typedef void (*process_buffer_func)(
	void *, const void *, struct hdf5_dset_info, LocalIdx, LocalIdx
);

#ifndef M_PI
const double M_PI = 3.14159265358979323846;
#endif
const double HBAR   = 1.0545718176461565e-34;  // J * s
const double EV     = 1.602176634e-19;  // J
const double CLIGHT = 299792458.0;  // m/s
const double COS_TOL = 1e-3;

const char *const HDF5_GROUP_NAME = "/FuzzyDM";
const char *const HDF5_DATASET_NAME[] = {"PsiRe", "PsiIm"};
enum { MAXLEN_PATH = 512 };
uint FILES_OPEN_IN_PARALLEL = 80;
enum { NUMDIMS = 3 };
enum { TAG_VEL_DATA = 666 };
const size_t BUFFER_SIZE = 32 * ((size_t) 1024 * 1024);

int WORLD_RANK;
int WORLD_SIZE;
struct {
	size_t local_num_el_c2c;
	uint local_nx_c2c;
	uint local_x_start_c2c;
	uint local_ny_c2c;
	uint local_y_start_c2c;
	size_t local_real_num_el;
	size_t local_complex_num_el;
	uint local_nx;
	uint local_x_start;
	uint local_ny;
	uint local_y_start;
	uint nx;
	uint ny;
	uint nz;
	uint nz_complex;
	uint n[NUMDIMS];
	uint stride_z_real;
	uint stride_z_complex;
	GlobalIdx num_cells_tot;
	uint *slabs_x_per_rank;
	uint *slabs_x_per_rank_c2c;
} GRID;
struct {
	uint num_files;
	char snap_file_base[MAXLEN_PATH];
	double hbar;
	double mass;
	double mass_ev;
	double box_size;
	double a;
	double HubbleParam;
	double UnitLength_in_cm;
	double UnitMass_in_g;
	double UnitTime_in_s;
} PARAM;


static inline LocalIdx LIDX_TO_PADDED(const LocalIdx i) {
	const size_t stride_diff_z = GRID.stride_z_real - GRID.nz;
	const size_t num_strides_z = i / GRID.nz;
	return i + num_strides_z * stride_diff_z;
}

static inline LocalIdx FFT_RI(const uint x, const uint y, const uint z) {
	return (LocalIdx) (x * GRID.ny + y) * GRID.stride_z_real + z;
}

static inline LocalIdx FFT_CI_T(const uint x, const uint y, const uint z) {
	return (LocalIdx) (y * GRID.nx + x) * GRID.stride_z_complex + z;
}

static inline LocalIdx IDX(const uint x, const uint y, const uint z) {
	return (LocalIdx) (x * GRID.ny + y) * GRID.nz + z;
}

static inline LocalIdx IDX_T(const uint x, const uint y, const uint z) {
	return (LocalIdx) (y * GRID.nx + x) * GRID.nz + z;
}

static inline ulong umin(const ulong a, const ulong b) {
	return a < b ? a : b;
}

static inline uint periodic_wrap(int x, const uint N) {
	while (x < 0)
		x += N;
	while ((uint) x >= N)
		x -= N;
	return x;
}

static inline double phase_diff_wrap(double phase_diff) {
	while (phase_diff < -M_PI)
		phase_diff += 2 * M_PI;
	while (phase_diff >= M_PI)
		phase_diff -= 2 * M_PI;
	return phase_diff;
}

static inline bool isclose(
	const double a, const double b, const double rtol, const double atol
) {
	return fabs(a - b) <= fmax(rtol * fmax(fabs(a), fabs(b)), atol);
}

static inline double time(void) {
	return MPI_Wtime();
}

static inline double fftfreq(const size_t n, const double d, const size_t i) {
	assert(i < n);
	return (i < n / 2 ? i : (double) i - n) / (d * n);
}

static inline double rfftfreq(const size_t n, const double d, const size_t i) {
	assert(i < n / 2 + 1);
	return i / (d * n);
}

static inline double k_vec(
	const uint i, const uint x, const uint y, const uint z
) {
	const double n_i = GRID.n[i];
	const double d_i = PARAM.box_size / n_i;
	double freq;
	switch (i) {
		case 0:
			freq = fftfreq(n_i, d_i, x);
			break;
		case 1:
			freq = fftfreq(n_i, d_i, y);
			break;
		case 2:
			freq = rfftfreq(n_i, d_i, z);
			break;
		default: assert(false);
	}
	return 2 * M_PI * freq;
}

static inline double k_vec_c2c(
	const uint i, const uint x, const uint y, const uint z
) {
	const double n_i = GRID.n[i];
	const double d_i = PARAM.box_size / n_i;
	double freq;
	switch (i) {
		case 0:
			freq = fftfreq(n_i, d_i, x);
			break;
		case 1:
			freq = fftfreq(n_i, d_i, y);
			break;
		case 2:
			freq = fftfreq(n_i, d_i, z);
			break;
		default: assert(false);
	}
	return 2 * M_PI * freq;
}

void __attribute__((format(printf, 1, 2))) printf0(
	const char *const format, ...
) {
	if (WORLD_RANK == 0) {
		va_list args;
		va_start(args, format);
		vprintf(format, args);
		va_end(args);
		fflush(stdout);
	}
}

#define terminate(format, ...) \
	terminate_fullinfo(__FILE__, __LINE__, __func__, (format), __VA_ARGS__)
noreturn void terminate_fullinfo(
	const char *const file, const int line, const char *const func, 
	const char *const format, ...
) {
	va_list args;
	va_start(args, format);
	fprintf(
		stderr,
		"TERMINATE: Rank=%d in %s:%d, %s():\n\t", WORLD_RANK, file, line, func
	);
	vfprintf(stderr, format, args);
	fprintf(stderr, "\n");
	va_end(args);
	exit(EXIT_FAILURE);
}

void __attribute__((format(printf, 2, 3))) file_path_sprintf(
	char *const buf, const char *const fmt, ...
) {
	va_list l;
	va_start(l, fmt);
	const int status = vsnprintf(buf, MAXLEN_PATH, fmt, l);
	if (status >= MAXLEN_PATH)
		terminate(
			"length of file path exceeds MAXLEN_PATH = %u: %s",
			MAXLEN_PATH, buf
		);
	va_end(l);
}

bool file_exists(const char *const path) {
	FILE *file = NULL;
	if ((file = fopen(path, "rb"))) {
		fclose(file);
		return true;
	}
	return false;
}


// copied/adapted from AREPO get_file_path()
static char *get_file_path(const char *const snap_path, const uint filenr) {
	char *path = malloc(MAXLEN_PATH * sizeof *path);
	assert(path);
	if (PARAM.num_files > 1) {
		// determine directory of given file
		char snap_dir[MAXLEN_PATH];
		strcpy(snap_dir, snap_path);
		strrchr(snap_dir, '/')[0] = '\0';
		// determine snapshot number
		const char *snap_num_substr = strrchr(snap_path, '_');
		assert(snap_num_substr);
		const long snap_num = strtoul(snap_num_substr + 1, NULL, 10);
		// build resulting path for snapshot file with the given filenr
		file_path_sprintf(
			path, "%s/%s_%03lu.%d.hdf5", snap_dir, PARAM.snap_file_base,
			snap_num, filenr
		);
	}
	else {
		strcpy(path, snap_path);
	}
	return path;
}


// copied/adapted from AREPO fdm_read_grid_dims()
static void read_grid_dims(
	const char *const path, const uint filenr, uint *const num_dims,
	hsize_t (*const num_cells)[NUMDIMS], hssize_t *const num_cells_tot
) {
	// open HDF5 resources
	hid_t hdf5_file = H5Fopen(path, H5F_ACC_RDONLY, H5P_DEFAULT);
	hid_t hdf5_grp = H5Gopen(hdf5_file, HDF5_GROUP_NAME, H5P_DEFAULT);
	assert(hdf5_file != H5I_INVALID_HID && hdf5_grp != H5I_INVALID_HID);
	hid_t hdf5_dset[2];
	hid_t hdf5_dspace[2];
	int ndims[2];
	for (uint cpart = 0; cpart < 2; cpart++) {
		hdf5_dset[cpart] =
			H5Dopen(hdf5_grp, HDF5_DATASET_NAME[cpart], H5P_DEFAULT);
		hdf5_dspace[cpart] = H5Dget_space(hdf5_dset[cpart]);
		assert(
			hdf5_dset[cpart] != H5I_INVALID_HID &&
			hdf5_dspace[cpart] != H5I_INVALID_HID
		);
		ndims[cpart] = H5Sget_simple_extent_ndims(hdf5_dspace[cpart]);
		assert(ndims[cpart] > 0);
	}
	// consistency checks
	if (ndims[0] != ndims[1])
		terminate(
			"Number of dimensions different between datasets %s and %s: "
			"%d != %d",
			HDF5_DATASET_NAME[0], HDF5_DATASET_NAME[1], ndims[0], ndims[1]
		);
	num_dims[filenr] = ndims[0];
	const uint nd = num_dims[filenr];
	if (nd != 1 && nd != NUMDIMS)
		terminate(
			"Unexpected number of dimensions for datasets %s and %s: %d",
			HDF5_DATASET_NAME[0], HDF5_DATASET_NAME[1], nd
		);
	// read number of cells for this file
	const int nd_check =
		H5Sget_simple_extent_dims(hdf5_dspace[0], num_cells[filenr], NULL);
	assert(nd_check == (int) nd);
	num_cells_tot[filenr] = H5Sget_simple_extent_npoints(hdf5_dspace[0]);
	assert(num_cells_tot[filenr] > 0);
	// confirm that the grid data is divided into complete slabs
	if (
		(num_cells_tot[filenr] % (GRID.ny * GRID.nz) != 0) ||
		(nd >= 2 && num_cells[filenr][1] != GRID.ny) ||
		(nd >= 3 && num_cells[filenr][2] != GRID.nz)
	)
		terminate(
			"Input file %s contains incomplete slabs!\n"
			"num_dims=%u, num_cells_tot=%" PRId64 ", num_cells[y]=%" PRIu64
			", num_cells[z]=%" PRIu64 ", GRID.ny=%td, GRID.nz=%td",
			path, nd, (int64_t) num_cells_tot[filenr],
			(uint64_t) (nd >= 2 ? num_cells[1] : 0),
			(uint64_t) (nd >= 3 ? num_cells[2] : 0),
			GRID.ny, GRID.nz
		);
	/* close HDF5 resources */
	for (uint cpart = 0; cpart < 2; cpart++) {
		H5Sclose(hdf5_dspace[cpart]);
		H5Dclose(hdf5_dset[cpart]);
	}
	H5Gclose(hdf5_grp);
	H5Fclose(hdf5_file);
}


// copied/adapted from AREPO fdm_read_ic_grid()
// (split into read_wave_function(), read_file(), and read_datasets())
static void read_datasets(
	const struct hdf5_dset_info hdf5_ds, const LocalIdx read_offset,
	const LocalIdx read_count, const LocalIdx thisrank_read, void *buffer_tmp,
	void *buffer_out, process_buffer_func process_buffer
) {
	// memory dataspace
	const hid_t hdf5_dataspace_in_memory =
		H5Screate_simple(1, (hsize_t []){2 * read_count}, NULL);
	assert(hdf5_dataspace_in_memory != H5I_INVALID_HID);
	// file dataspace selection
	hsize_t start[NUMDIMS] = {0};
	hsize_t count[NUMDIMS];
	if (hdf5_ds.num_dims == 1) {
		start[0] = read_offset;
		count[0] = read_count;
	}
	else {
		// TODO: this doesn’t work yet for num_dims > 1
		assert(false);
	}
	// read data and process for output buffer
	for (uint cpart = 0; cpart < 2; cpart++) {
		H5Sselect_hyperslab(
			hdf5_dataspace_in_memory, H5S_SELECT_SET, (hsize_t []){cpart},
			(hsize_t []){2}, (hsize_t []){read_count}, NULL
		);
		H5Sselect_hyperslab(
			hdf5_ds.dspace[cpart], H5S_SELECT_SET, start, NULL, count, NULL
		);
		const herr_t status = H5Dread(
			hdf5_ds.dset[cpart], hdf5_ds.dtype[cpart], hdf5_dataspace_in_memory,
			hdf5_ds.dspace[cpart], H5P_DEFAULT, buffer_tmp
		);
		assert(status >= 0);
	}
	process_buffer(buffer_out, buffer_tmp, hdf5_ds, thisrank_read, read_count);
	// close HDF5 resources
	H5Sclose(hdf5_dataspace_in_memory);
}


// copied/adapted from AREPO fdm_read_ic_grid()
// (split into read_wave_function(), read_file(), and read_datasets())
static LocalIdx read_file(
	const char *const path, const uint num_dims, const hssize_t num_cells_tot,
	const LocalIdx rank_cells, LocalIdx thisrank_read,
	const uint64_t cells_read_this_file, void *buffer_tmp, void *buffer_out,
	process_buffer_func process_buffer
) {
	// open HDF5 resources
	const hid_t hdf5_file = H5Fopen(path, H5F_ACC_RDONLY, H5P_DEFAULT);
	const hid_t hdf5_grp = H5Gopen(hdf5_file, HDF5_GROUP_NAME, H5P_DEFAULT);
	assert(hdf5_file != H5I_INVALID_HID && hdf5_grp != H5I_INVALID_HID);
	struct hdf5_dset_info hdf5_ds;
	hdf5_ds.path = path;
	hdf5_ds.num_dims = num_dims;
	for (uint cpart = 0; cpart < 2; cpart++) {
		hdf5_ds.dset[cpart] =
			H5Dopen(hdf5_grp, HDF5_DATASET_NAME[cpart], H5P_DEFAULT);
		hdf5_ds.dspace[cpart] = H5Dget_space(hdf5_ds.dset[cpart]);
		assert(
			hdf5_ds.dset[cpart] != H5I_INVALID_HID &&
			hdf5_ds.dspace[cpart] != H5I_INVALID_HID
		);
		hdf5_ds.dtype[cpart] = H5Dget_type(hdf5_ds.dset[cpart]);
	}
	// metadata consistency checks
	if (H5Tequal(hdf5_ds.dtype[0], hdf5_ds.dtype[1]) <= 0)
		terminate(
			"Datatypes differ between datasets %s and %s",
			HDF5_DATASET_NAME[0], HDF5_DATASET_NAME[1]
		);
	if (H5Sextent_equal(hdf5_ds.dspace[0], hdf5_ds.dspace[1]) <= 0)
		terminate(
			"Dataspace extent differs between datasets %s and %s",
			HDF5_DATASET_NAME[0], HDF5_DATASET_NAME[1]
		);
	// read from file in chunks of the buffer size until all cells
	// for this rank have been read or the file ends
	const size_t grid_buffer_size =
		BUFFER_SIZE / (2 * H5Tget_size(hdf5_ds.dtype[0]));
	LocalIdx thisrank_read_this_file = 0;
	uint64_t cells_remaining_in_file = num_cells_tot - cells_read_this_file;
	assert(cells_remaining_in_file > 0);
	while (thisrank_read < rank_cells && cells_remaining_in_file > 0) {
		cells_remaining_in_file = num_cells_tot - cells_read_this_file -
			thisrank_read_this_file;
		LocalIdx read_count = rank_cells - thisrank_read;
		if (cells_remaining_in_file < read_count)
			read_count = cells_remaining_in_file;
		if (grid_buffer_size < read_count)
			read_count = grid_buffer_size;
		read_datasets(
			hdf5_ds, cells_read_this_file + thisrank_read_this_file, read_count,
			thisrank_read, buffer_tmp, buffer_out, process_buffer
		);
		// increment counters
		thisrank_read += read_count;
		thisrank_read_this_file += read_count;
	}
	assert(
		cells_remaining_in_file != 0 ||
		(hssize_t) (cells_read_this_file + thisrank_read_this_file) ==
			num_cells_tot
	);
	// close HDF5 resources
	for (uint cpart = 0; cpart < 2; cpart++) {
		H5Tclose(hdf5_ds.dtype[cpart]);
		H5Sclose(hdf5_ds.dspace[cpart]);
		H5Dclose(hdf5_ds.dset[cpart]);
	}
	H5Gclose(hdf5_grp);
	H5Fclose(hdf5_file);
	return thisrank_read;
}


// copied/adapted from AREPO fdm_read_ic_grid()
// (split into read_wave_function(), read_file(), and read_datasets())
static void read_wave_function(
	const char *const snap_path, void *buffer_out,
	process_buffer_func process_buffer
) {
	assert(PARAM.num_files > 0);
	// in order to determine which rank needs to read which files, first read
	// grid sizes sequentially on rank 0 (for simplicity)
	uint num_dims;
	uint num_dims_files[PARAM.num_files];
	hsize_t num_cells[PARAM.num_files][NUMDIMS];
	hssize_t num_cells_tot[PARAM.num_files];
	if (WORLD_RANK == 0) {
		for (uint filenr = 0; filenr < PARAM.num_files; filenr++) {
			char *path = get_file_path(snap_path, filenr);
			read_grid_dims(
				path, filenr, num_dims_files, num_cells, num_cells_tot
			);
			free(path);
			// check num_dims consistency
			if (num_dims_files[filenr] != num_dims_files[0])
				terminate(
					"Number of dimensions for %s and %s differs between input "
					"files 0 and %d: %d != %d",
					HDF5_DATASET_NAME[0], HDF5_DATASET_NAME[1], filenr,
					num_dims_files[0], num_dims_files[filenr]
				);
		}
		num_dims = num_dims_files[0];
	}
	MPI_Bcast(&num_dims, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
	MPI_Bcast(num_cells, sizeof num_cells, MPI_BYTE, 0, MPI_COMM_WORLD);
	MPI_Bcast(
		num_cells_tot, sizeof num_cells_tot, MPI_BYTE, 0, MPI_COMM_WORLD
	);
	// verify grid size consistency
	uint64_t cells_total = 0;
	for (uint filenr = 0; filenr < PARAM.num_files; filenr++)
		cells_total += num_cells_tot[filenr];
	if (cells_total != GRID.num_cells_tot)
		terminate(
			"%" PRIu64 " = number of wave function cells != "
			"GRID.nx * GRID.ny * GRID.nz = %td * %td * %td = %" PRIu64,
			cells_total, GRID.nx, GRID.ny, GRID.nz, GRID.num_cells_tot
		);
	// read grid data from snapshot files in parallel
	void *buffer_tmp = malloc(BUFFER_SIZE);
	uint filenr = 0;
	int rank = 0;
	while (GRID.slabs_x_per_rank[rank] == 0 && rank < WORLD_SIZE)
		rank++;
	uint64_t cells_read = 0;
	uint64_t cells_read_this_file = 0;
	LocalIdx thisrank_read = 0;
	while (cells_read < cells_total) {
		assert(rank < WORLD_SIZE);
		assert(filenr < PARAM.num_files);
		uint open_files = 0;
		LocalIdx rank_cells_remaining = 0;
		while (
			open_files < FILES_OPEN_IN_PARALLEL && filenr < PARAM.num_files
		) {
			LocalIdx rank_cells =
				(LocalIdx) GRID.slabs_x_per_rank[rank] * GRID.ny * GRID.nz;
			if (rank_cells_remaining == 0)
				rank_cells_remaining = rank_cells;
			// if the current rank is active, read the current file until
			// either all cells for this rank have been read or the file ends
			if (rank == WORLD_RANK) {
				assert(thisrank_read < rank_cells);
				assert(rank_cells_remaining <= rank_cells);
				char *path = get_file_path(snap_path, filenr);
				printf0("Reading file %u on rank %d…\n", filenr, WORLD_RANK);
				thisrank_read = read_file(
					path, num_dims, num_cells_tot[filenr], rank_cells,
					thisrank_read, cells_read_this_file, buffer_tmp, buffer_out,
					process_buffer
				);
				free(path);
			}
			// if the current file has ended and the current rank still has
			// cells to read: open the next file on the same rank
			if (
				(hssize_t) (cells_read_this_file + rank_cells_remaining) >
				num_cells_tot[filenr]
			) {
				LocalIdx rank_cells_read =
					num_cells_tot[filenr] - cells_read_this_file;
				cells_read += rank_cells_read;
				rank_cells_remaining -= rank_cells_read;
				filenr++;
				cells_read_this_file = 0;
			}
			// the current rank has read all of its cells: open the same file
			// (or the next, if the current file has ended at the same time)
			// on the next rank
			else {
				cells_read += rank_cells_remaining;
				cells_read_this_file += rank_cells_remaining;
				rank_cells_remaining = 0;
				rank++;
				open_files++;
				if ((hssize_t) cells_read_this_file == num_cells_tot[filenr]) {
					filenr++;
					cells_read_this_file = 0;
				}
			}
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
	// consistency checks
	LocalIdx thisrank_cells = (LocalIdx) GRID.local_nx * GRID.ny * GRID.nz;
	assert(thisrank_read == thisrank_cells);
	assert(cells_read == cells_total);
	// finalize
	free(buffer_tmp);
}


static void process_buffer_psi(
	void *buffer_out, const void *const psi_buf,
	const struct hdf5_dset_info hdf5_ds, const LocalIdx start,
	const LocalIdx length
) {
	fft_complex *psi = buffer_out;
	const hid_t hdf5_float = H5Tcopy(H5T_NATIVE_FLOAT);
	const hid_t hdf5_double = H5Tcopy(H5T_NATIVE_DOUBLE);
	const hid_t hdf5_dt = hdf5_ds.dtype[0];
	const bool is_float = H5Tequal(hdf5_dt, hdf5_float) > 0;
	const bool is_double = H5Tequal(hdf5_dt, hdf5_double) > 0;
	for (LocalIdx i = 0; i < length; i++) {
		double complex psi_i;
		if (is_float)
			psi_i = ((float complex *) psi_buf)[i];
		else if (is_double)
			psi_i = ((double complex *) psi_buf)[i];
		else
			terminate(
				"Unexpected HDF5 data type in input file %s", hdf5_ds.path
			);
		psi[start + i] = psi_i;
	}
	H5Tclose(hdf5_float);
	H5Tclose(hdf5_double);
}


static void process_buffer_phase_padded(
	void *buffer_out, const void *const psi_buf,
	const struct hdf5_dset_info hdf5_ds, const LocalIdx start,
	const LocalIdx length
) {
	fft_real *phase = buffer_out;
	const hid_t hdf5_float = H5Tcopy(H5T_NATIVE_FLOAT);
	const hid_t hdf5_double = H5Tcopy(H5T_NATIVE_DOUBLE);
	const hid_t hdf5_dt = hdf5_ds.dtype[0];
	const bool is_float = H5Tequal(hdf5_dt, hdf5_float) > 0;
	const bool is_double = H5Tequal(hdf5_dt, hdf5_double) > 0;
	for (LocalIdx i = 0; i < length; i++) {
		double complex psi_i;
		if (is_float)
			psi_i = ((float complex *) psi_buf)[i];
		else if (is_double)
			psi_i = ((double complex *) psi_buf)[i];
		else
			terminate(
				"Unexpected HDF5 data type in input file %s", hdf5_ds.path
			);
		phase[LIDX_TO_PADDED(start + i)] = carg(psi_i);
	}
	H5Tclose(hdf5_float);
	H5Tclose(hdf5_double);
}


static void process_buffer_sqrt_rho_padded(
	void *buffer_out, const void *const psi_buf,
	const struct hdf5_dset_info hdf5_ds, const LocalIdx start,
	const LocalIdx length
) {
	fft_real *sqrt_rho = buffer_out;
	const hid_t hdf5_float = H5Tcopy(H5T_NATIVE_FLOAT);
	const hid_t hdf5_double = H5Tcopy(H5T_NATIVE_DOUBLE);
	const hid_t hdf5_dt = hdf5_ds.dtype[0];
	const bool is_float = H5Tequal(hdf5_dt, hdf5_float) > 0;
	const bool is_double = H5Tequal(hdf5_dt, hdf5_double) > 0;
	for (LocalIdx i = 0; i < length; i++) {
		double complex psi_i;
		if (is_float)
			psi_i = ((float complex *) psi_buf)[i];
		else if (is_double)
			psi_i = ((double complex *) psi_buf)[i];
		else
			terminate(
				"Unexpected HDF5 data type in input file %s", hdf5_ds.path
			);
		sqrt_rho[LIDX_TO_PADDED(start + i)] = cabs(psi_i);
	}
	H5Tclose(hdf5_float);
	H5Tclose(hdf5_double);
}


static void process_buffer_phase_contiguous(
	void *buffer_out, const void *const psi_buf,
	const struct hdf5_dset_info hdf5_ds, const LocalIdx start,
	const LocalIdx length
) {
	fft_real *phase = buffer_out;
	const hid_t hdf5_float = H5Tcopy(H5T_NATIVE_FLOAT);
	const hid_t hdf5_double = H5Tcopy(H5T_NATIVE_DOUBLE);
	const hid_t hdf5_dt = hdf5_ds.dtype[0];
	const bool is_float = H5Tequal(hdf5_dt, hdf5_float) > 0;
	const bool is_double = H5Tequal(hdf5_dt, hdf5_double) > 0;
	for (LocalIdx i = 0; i < length; i++) {
		double complex psi_i;
		if (is_float)
			psi_i = ((float complex *) psi_buf)[i];
		else if (is_double)
			psi_i = ((double complex *) psi_buf)[i];
		else
			terminate(
				"Unexpected HDF5 data type in input file %s", hdf5_ds.path
			);
		phase[start + i] = carg(psi_i);
	}
	H5Tclose(hdf5_float);
	H5Tclose(hdf5_double);
}


static void process_buffer_sqrt_rho_contiguous(
	void *buffer_out, const void *const psi_buf,
	const struct hdf5_dset_info hdf5_ds, const LocalIdx start,
	const LocalIdx length
) {
	fft_real *sqrt_rho = buffer_out;
	const hid_t hdf5_float = H5Tcopy(H5T_NATIVE_FLOAT);
	const hid_t hdf5_double = H5Tcopy(H5T_NATIVE_DOUBLE);
	const hid_t hdf5_dt = hdf5_ds.dtype[0];
	const bool is_float = H5Tequal(hdf5_dt, hdf5_float) > 0;
	const bool is_double = H5Tequal(hdf5_dt, hdf5_double) > 0;
	for (LocalIdx i = 0; i < length; i++) {
		double complex psi_i;
		if (is_float)
			psi_i = ((float complex *) psi_buf)[i];
		else if (is_double)
			psi_i = ((double complex *) psi_buf)[i];
		else
			terminate(
				"Unexpected HDF5 data type in input file %s", hdf5_ds.path
			);
		sqrt_rho[start + i] = cabs(psi_i);
	}
	H5Tclose(hdf5_float);
	H5Tclose(hdf5_double);
}


static void set_units(const hid_t hdf5_header) {
	hid_t hdf5_attr;
	herr_t status;
	hdf5_attr = H5Aopen_name(hdf5_header, "HubbleParam");
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &PARAM.HubbleParam);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	hdf5_attr = H5Aopen_name(hdf5_header, "UnitLength_in_cm");
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &PARAM.UnitLength_in_cm);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	hdf5_attr = H5Aopen_name(hdf5_header, "UnitMass_in_g");
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &PARAM.UnitMass_in_g);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	double UnitVelocity_in_cm_per_s;
	hdf5_attr = H5Aopen_name(hdf5_header, "UnitVelocity_in_cm_per_s");
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &UnitVelocity_in_cm_per_s);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	PARAM.UnitTime_in_s = PARAM.UnitLength_in_cm / UnitVelocity_in_cm_per_s;
	// apply units
	PARAM.hbar = HBAR * 1e7;
	PARAM.hbar *= pow(PARAM.HubbleParam, 2) / pow(PARAM.UnitLength_in_cm, 2) *
		1 / PARAM.UnitMass_in_g * PARAM.UnitTime_in_s;
	PARAM.mass = PARAM.mass_ev * 1e3 * EV / pow(CLIGHT, 2);
	PARAM.mass *= PARAM.HubbleParam / PARAM.UnitMass_in_g;
}


// copied from AREPO distribute_file()
static void distribute_file(
	const uint nfiles, uint *const filenr, int *const master, int *const last
){
	const uint ranks_left = WORLD_SIZE % nfiles;
	if (ranks_left == 0) {
		const uint ranks_per_file = WORLD_SIZE / nfiles;
		const uint group = WORLD_RANK / ranks_per_file;
		*master = group * ranks_per_file;
		*last = (group + 1) * ranks_per_file - 1;
		*filenr = group;
	}
	else {
		const double ranks_per_file = ((double) WORLD_SIZE) / nfiles;
		assert(ranks_per_file >= 1);
		*last = -1;
		for (uint i = 0; i < nfiles; i++) {
			*master = *last + 1;
			*last   = (i + 1) * ranks_per_file;
			if (*last >= WORLD_SIZE)
				*last = *last - 1;
			if (*last < *master)
				terminate("%s", "last < master");
			*filenr = i;
			if (i == nfiles - 1)
				*last = WORLD_SIZE - 1;
			if (*master <= WORLD_RANK && WORLD_RANK <= *last)
				return;
		}
	}
}


// adapted from AREPO fdm_pseudospectral_io_write() (mostly consisting of
// grid_io_write())
void write_file(
	const char *const path, const int write_rank, const int last_rank,
	const char *const dataset_name, const fft_real *const data,
	const size_t *const strides, const size_t num_strides
) {
	// open HDF5 file
	hid_t hdf5_file = H5I_INVALID_HID;
	if (WORLD_RANK == write_rank) {
		hdf5_file = H5Fopen(path, H5F_ACC_RDWR, H5P_DEFAULT);
		assert(hdf5_file != H5I_INVALID_HID);
	}
	assert(write_rank <= last_rank);
	if (strides)
		for(size_t i = 1; i < num_strides; i++)
			assert(strides[i] % strides[i - 1] == 0);
	if (WORLD_RANK < write_rank || WORLD_RANK > last_rank)
		return;
	// determine amount of data to write
	uint nslab_x_total = 0;
	for(int rank = write_rank; rank <= last_rank; rank++)
		nslab_x_total += GRID.slabs_x_per_rank[rank];
	// create HDF5 structures
	// order of Ngrid: x, y, z
	// order of strides, Ngrid_stride: z, y, x (fastest-varying index first)
	const size_t Ngrid[] = { nslab_x_total, GRID.ny, GRID.nz };
	GlobalIdx Ngrid_stride[] = { 1, 1, 1 };
	hsize_t dims[] = { 1 };
	for (uint i = 0; i < NUMDIMS; i++) {
		for (uint j = i + 1; j < NUMDIMS; j++)
			Ngrid_stride[NUMDIMS - 1 - i] *= Ngrid[j];
		dims[0] *= Ngrid[i];
	}
	hid_t hdf5_grp = H5I_INVALID_HID;
	hid_t hdf5_dataspace_in_file = H5I_INVALID_HID;
	hid_t hdf5_dataset = H5I_INVALID_HID;
	if (WORLD_RANK == write_rank) {
		bool grp_exists =
			H5Lexists(hdf5_file, HDF5_GROUP_NAME, H5P_DEFAULT) > 0;
		if (grp_exists)
			hdf5_grp = H5Gopen(hdf5_file, HDF5_GROUP_NAME, H5P_DEFAULT);
		else
			hdf5_grp = H5Gcreate(
				hdf5_file, HDF5_GROUP_NAME, H5P_DEFAULT, H5P_DEFAULT,
				H5P_DEFAULT
			);
		assert(hdf5_grp != H5I_INVALID_HID);
		hdf5_dataspace_in_file = H5Screate_simple(1, dims, NULL);
		assert(hdf5_dataspace_in_file != H5I_INVALID_HID);
		hdf5_dataset = H5Dcreate(
			hdf5_grp, dataset_name, H5T_NATIVE_DOUBLE, hdf5_dataspace_in_file,
			H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT
		);
		assert(hdf5_dataset != H5I_INVALID_HID);
	}
	// communicate and write data
	// Note: The data must be collected and written in the order of the
	//       rank numbers to preserve the correct grid locations.
	size_t bufferstart = 0;
	size_t pcsum = 0;
	fft_real *buffer = malloc(BUFFER_SIZE);
	size_t remaining_space = BUFFER_SIZE / sizeof *buffer;
	for (int rank = write_rank; rank <= last_rank; rank++) {
		LocalIdx pcsum_this_rank = 0;
		LocalIdx n_for_this_rank =
			(LocalIdx) GRID.slabs_x_per_rank[rank] * GRID.ny * GRID.nz;
		LocalIdx stride_offset = 0;
		while (n_for_this_rank > 0) {
			LocalIdx pc = n_for_this_rank;
			if (pc > remaining_space)
				pc = remaining_space;
			// fill write buffer
			if (rank == WORLD_RANK)
				for (LocalIdx idx = 0; idx < pc; idx++) {
					const LocalIdx idx_this_rank = pcsum_this_rank + idx;
					const LocalIdx idx_data = idx_this_rank + stride_offset;
					buffer[idx] = data[idx_data];
					if (isnan(buffer[idx]))
						terminate("NaN in output at LocalIdx %zu", idx_data);
					// check if stride_offset needs to be updated
					if (strides) {
						stride_offset += strides[0] - 1;
						for (size_t dim = 1; dim < num_strides; dim++)
							if ((idx_this_rank + 1) % Ngrid_stride[dim] == 0) {
								const size_t dim_stride =
									strides[dim] / strides[dim - 1];
								const size_t dim_stride_contiguous =
									Ngrid[NUMDIMS - dim];
								assert(dim_stride >= dim_stride_contiguous);
								stride_offset +=
									dim_stride - dim_stride_contiguous;
							}
					}
				}
			// communicate to write_rank
			if (WORLD_RANK == write_rank && rank != WORLD_RANK)
				MPI_Recv(
					buffer, sizeof *buffer * pc, MPI_BYTE, rank,
					TAG_VEL_DATA, MPI_COMM_WORLD, MPI_STATUS_IGNORE
				);
			if (WORLD_RANK != write_rank && rank == WORLD_RANK)
				MPI_Ssend(
					buffer, sizeof *buffer * pc, MPI_BYTE, write_rank,
					TAG_VEL_DATA, MPI_COMM_WORLD
				);
			// update counters
			bufferstart += pc;
			pcsum_this_rank += pc;
			remaining_space -= pc;
			n_for_this_rank -= pc;
			// write to file if buffer full or all data collected
			if (remaining_space == 0 || n_for_this_rank == 0) {
				// write stuff (number of elements equal to bufferstart)
				if (WORLD_RANK == write_rank) {
					// set hyperslab to write
					// TODO: write 3D array to HDF5 file instead of 1D
					H5Sselect_hyperslab(
						hdf5_dataspace_in_file, H5S_SELECT_SET,
						(hsize_t []){pcsum}, NULL,
						(hsize_t []){bufferstart}, NULL
					);
					// write data
					const hid_t hdf5_dataspace_memory = H5Screate_simple(
						1, (hsize_t []){bufferstart}, NULL
					);
					const herr_t status = H5Dwrite(
						hdf5_dataset, H5T_NATIVE_DOUBLE, hdf5_dataspace_memory,
						hdf5_dataspace_in_file, H5P_DEFAULT, buffer
					);
					assert(status >= 0);
					H5Sclose(hdf5_dataspace_memory);
				}
				// update position in file (pcsum) and reset buffer
				// position (bufferstart, remaining_space)
				pcsum += bufferstart;
				bufferstart = 0;
				remaining_space = BUFFER_SIZE / sizeof *buffer;
			}
		}
	}
	// finalize
	free(buffer);
	if (WORLD_RANK == write_rank) {
		H5Sclose(hdf5_dataspace_in_file);
		H5Dclose(hdf5_dataset);
		H5Gclose(hdf5_grp);
		H5Fclose(hdf5_file);
	}
}


// adapted from AREPO savepositions()
static void save_data(
	const char *const snap_path, const char *const dataset_name,
	const fft_real *const data, const bool output_file_exists,
	const size_t *const strides, const size_t num_strides
) {
	uint ngroups = PARAM.num_files / FILES_OPEN_IN_PARALLEL;
	if (PARAM.num_files % FILES_OPEN_IN_PARALLEL)
		ngroups++;
	// assign ranks to output files
	uint filenr = -1;
	int master_rank = -1, last_rank = -1;
	distribute_file(PARAM.num_files, &filenr, &master_rank, &last_rank);
	char output_path[MAXLEN_PATH];
	if (PARAM.num_files > 1)
		file_path_sprintf(output_path, "%s_vel.%u.hdf5", snap_path, filenr);
	else
		file_path_sprintf(output_path, "%s_vel.hdf5", snap_path);
	if (WORLD_RANK == master_rank) {
		if (!output_file_exists && file_exists(output_path))
			terminate("Output file %s exists, but shouldn’t!", output_path);
		if (output_file_exists && !file_exists(output_path))
			terminate("Output file %s doesn’t exist, but should!", output_path);
	}
	// write files in groups
	for (uint gr = 0; gr < ngroups; gr++) {
		if (filenr / FILES_OPEN_IN_PARALLEL == gr) {
			// OK, it’s this rank’s turn
			if (
				WORLD_RANK == master_rank &&
				filenr % FILES_OPEN_IN_PARALLEL == 0
			) {
				printf(
					"\tWriting files group %u out of %u, files %u–%lu "
					"(total of %d files): “%s”\n", gr + 1, ngroups,
					filenr,
					umin(filenr + FILES_OPEN_IN_PARALLEL, PARAM.num_files) - 1,
					PARAM.num_files, output_path
				);
				fflush(stdout);
			}
			// initialize output file in first iteration
			if (WORLD_RANK == master_rank && !file_exists(output_path)) {
				const hid_t hdf5_file = H5Fcreate(
					output_path, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT
				);
				assert(hdf5_file != H5I_INVALID_HID);
				const hid_t hdf5_grp = H5Gcreate(
					hdf5_file, HDF5_GROUP_NAME, H5P_DEFAULT, H5P_DEFAULT,
					H5P_DEFAULT
				);
				assert(hdf5_grp != H5I_INVALID_HID);
				H5Gclose(hdf5_grp);
				H5Fclose(hdf5_file);
			}
			// write file
			write_file(
				output_path, master_rank, last_rank, dataset_name, data,
				strides, num_strides
			);
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
}


void read_metadata(const char *snap_path) {
	// read snapshot metadata
	const hid_t hdf5_file = H5Fopen(snap_path, H5F_ACC_RDONLY, H5P_DEFAULT);
	assert(hdf5_file != H5I_INVALID_HID);
	const hid_t hdf5_config = H5Gopen(hdf5_file, "/Config", H5P_DEFAULT);
	assert(hdf5_config != H5I_INVALID_HID);
	const hid_t hdf5_param = H5Gopen(hdf5_file, "/Parameters", H5P_DEFAULT);
	assert(hdf5_param != H5I_INVALID_HID);
	const hid_t hdf5_header = H5Gopen(hdf5_file, "/Header", H5P_DEFAULT);
	assert(hdf5_header != H5I_INVALID_HID);
	hid_t hdf5_attr;
	herr_t status;
	double pmgrid_attr;
	hdf5_attr = H5Aopen_name(hdf5_config, "PMGRID");
	const hid_t pmgrid_type = H5Aget_type(hdf5_attr);
	assert(H5Tequal(pmgrid_type, H5T_NATIVE_DOUBLE) > 0);
	H5Tclose(pmgrid_type);
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &pmgrid_attr);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	const uint pmgrid = pmgrid_attr;
	assert(pmgrid == pmgrid_attr);
	hdf5_attr = H5Aopen_name(hdf5_param, "NumFilesPerSnapshot");
	status = H5Aread(hdf5_attr, H5T_NATIVE_UINT, &PARAM.num_files);
	assert(status >= 0);
	assert(PARAM.num_files > 0);
	if ((uint) WORLD_SIZE < PARAM.num_files)
		terminate(
			"Number of ranks (%d) must be at least as large as number of files "
			"to write (%u)!", WORLD_SIZE, PARAM.num_files
		);
	H5Aclose(hdf5_attr);
	for (size_t i = 0; i < sizeof PARAM.snap_file_base; i++)
		PARAM.snap_file_base[i] = 0;
	hdf5_attr = H5Aopen_name(hdf5_param, "SnapshotFileBase");
	const hid_t atype = H5Aget_type(hdf5_attr);
	assert(H5Tget_class(atype) == H5T_STRING);
	assert(H5Tis_variable_str(atype) == 0);
	assert(H5Tget_size(atype) < sizeof PARAM.snap_file_base);
	status = H5Aread(hdf5_attr, atype, PARAM.snap_file_base);
	assert(status >= 0);
	H5Tclose(atype);
	H5Aclose(hdf5_attr);
	hdf5_attr = H5Aopen_name(hdf5_header, "BoxSize");
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &PARAM.box_size);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	hdf5_attr = H5Aopen_name(hdf5_param, "AxionMassEv");
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &PARAM.mass_ev);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	hdf5_attr = H5Aopen_name(hdf5_header, "Time");
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &PARAM.a);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	// units
	set_units(hdf5_header);
	// close HDF5 resources
	H5Gclose(hdf5_config);
	H5Gclose(hdf5_param);
	H5Gclose(hdf5_header);
	H5Fclose(hdf5_file);
	// initialize GRID
	GRID.nx = GRID.n[0] = pmgrid;
	GRID.ny = GRID.n[1] = pmgrid;
	GRID.nz = GRID.n[2] = pmgrid;
	GRID.num_cells_tot = 1;
	for (uint i = 0; i < ARRAY_LEN(GRID.n); i++)
		GRID.num_cells_tot *= GRID.n[i];
	// sizes for C2C transforms
	ptrdiff_t local_nx_c2c, local_x_start_c2c, local_ny_c2c, local_y_start_c2c;
	GRID.local_num_el_c2c = fftw_mpi_local_size_3d_transposed(
		GRID.nx, GRID.ny, GRID.nz, MPI_COMM_WORLD,
		&local_nx_c2c, &local_x_start_c2c, &local_ny_c2c, &local_y_start_c2c
	);
	GRID.local_nx_c2c = local_nx_c2c;
	GRID.local_x_start_c2c = local_x_start_c2c;
	GRID.local_ny_c2c = local_ny_c2c;
	GRID.local_y_start_c2c = local_y_start_c2c;
	assert(
		GRID.local_num_el_c2c >= (size_t) GRID.local_nx_c2c * GRID.ny * GRID.nz
	);
	GRID.slabs_x_per_rank_c2c =
		malloc(WORLD_SIZE * sizeof *GRID.slabs_x_per_rank_c2c);
	MPI_Allgather(
		&GRID.local_nx_c2c, 1, MPI_UNSIGNED,
		GRID.slabs_x_per_rank_c2c, 1, MPI_UNSIGNED, MPI_COMM_WORLD
	);
	// sizes for R2C/C2R transforms
	GRID.nz_complex = GRID.nz / 2 + 1;
	GRID.stride_z_complex = GRID.nz_complex;
	GRID.stride_z_real = 2 * GRID.stride_z_complex;
	ptrdiff_t local_nx, local_x_start, local_ny, local_y_start;
	GRID.local_complex_num_el = fftw_mpi_local_size_3d_transposed(
		GRID.nx, GRID.ny, GRID.stride_z_complex, MPI_COMM_WORLD,
		&local_nx, &local_x_start, &local_ny, &local_y_start
	);
	GRID.local_real_num_el = 2 * GRID.local_complex_num_el;
	GRID.local_nx = local_nx;
	GRID.local_x_start = local_x_start;
	GRID.local_ny = local_ny;
	GRID.local_y_start = local_y_start;
	assert(
		GRID.local_real_num_el >=
		(size_t) GRID.local_nx * GRID.ny * GRID.stride_z_real
	);
	assert(
		GRID.local_complex_num_el >=
		(size_t) GRID.local_nx * GRID.ny * GRID.stride_z_complex
	);
	GRID.slabs_x_per_rank = malloc(WORLD_SIZE * sizeof *GRID.slabs_x_per_rank);
	MPI_Allgather(
		&GRID.local_nx, 1, MPI_UNSIGNED, GRID.slabs_x_per_rank, 1, MPI_UNSIGNED,
		MPI_COMM_WORLD
	);
}


void spectral_differencing_momentum(const char *snap_path) {
	double t0, t1;
	printf0("Starting computation of momentum density (spectral)\n");
	// allocate arrays
	fft_complex *psi = fftw_alloc_complex(GRID.local_num_el_c2c);
	assert(psi);
	fft_complex *psi_conj = fftw_alloc_complex(GRID.local_num_el_c2c);
	assert(psi_conj);
	for (LocalIdx idx = 0; idx < GRID.local_num_el_c2c; idx++) {
		psi[idx] = NAN;
		psi_conj[idx] = NAN;
	}
	fft_real *mom_i = fftw_alloc_real(GRID.local_num_el_c2c);
	assert(mom_i);
	for (LocalIdx idx = 0; idx < GRID.local_num_el_c2c; idx++)
		mom_i[idx] = NAN;
	// create FFTW plans
	printf0("Creating FFTW plans…\n");
	fftw_plan forward_plan = fftw_mpi_plan_dft_3d(
		GRID.nx, GRID.ny, GRID.nz, psi, psi, MPI_COMM_WORLD, FFTW_FORWARD,
		FFTW_ESTIMATE | FFTW_MPI_TRANSPOSED_OUT
	);
	fftw_plan backward_plan = fftw_mpi_plan_dft_3d(
		GRID.nx, GRID.ny, GRID.nz, psi, psi, MPI_COMM_WORLD, FFTW_BACKWARD,
		FFTW_ESTIMATE | FFTW_MPI_TRANSPOSED_IN
	);
	// read wave function and compute conjugate
	printf0("Reading wave function…\n");
	t0 = time();
	read_wave_function(snap_path, psi, process_buffer_psi);
	for (LocalIdx idx = 0; idx < GRID.local_num_el_c2c; idx++)
		psi_conj[idx] = conj(psi[idx]);
	t1 = time();
	printf0("Done (took %g sec.).\n", t1 - t0);
	// compute momentum density according to: ρv⃗ = ℏ Im(ψ* ∇ψ)
	// do all momentum density components
	for (uint i = 0; i < NUMDIMS; i++) {
		printf0("Doing momentum density component %u…\n", i);
		t0 = time();
		// reset psi after each iteration
		if (i > 0) {
			for (LocalIdx idx = 0; idx < GRID.local_num_el_c2c; idx++)
				psi[idx] = conj(psi_conj[idx]);
		}
		// transform wave function forward
		fftw_execute(forward_plan);
		// computations in FFT space
		for (uint y = 0; y < GRID.local_ny_c2c; y++)
			for (uint x = 0; x < GRID.nx; x++)
				for (uint z = 0; z < GRID.nz; z++) {
					const LocalIdx idx = IDX_T(x, y, z);
					const double k_i = k_vec_c2c(
						i, x, GRID.local_y_start_c2c + y, z
					);
					psi[idx] *= I * k_i;
				}
		// transform back
		fftw_execute(backward_plan);
		// computations in real space
		for (uint x = 0; x < GRID.local_nx_c2c; x++)
			for (uint y = 0; y < GRID.ny; y++)
				for (uint z = 0; z < GRID.nz; z++) {
					const LocalIdx idx = IDX(x, y, z);
					// psi and psi_conj already contain a factor of √m, so have
					// to divide by m here even for momentum density
					mom_i[idx] = (PARAM.hbar / PARAM.mass) *
						cimag(psi_conj[idx] * psi[idx]);
					// normalize
					mom_i[idx] /= GRID.num_cells_tot;
					// convert from comoving to physical
					mom_i[idx] /= PARAM.a;
				}
		t1 = time();
		printf0("\tDone (took %g sec.).\n", t1 - t0);
		// output momentum density data
		printf0("\tWriting momentum density data (component %u)…\n", i);
		t0 = time();
		char dataset_name[50];
		sprintf(dataset_name, "MomentumDensity%u", i);
		save_data(snap_path, dataset_name, mom_i, i != 0, NULL, 0);
		MPI_Barrier(MPI_COMM_WORLD);
		t1 = time();
		printf0("\tDone (took %g sec.).\n", t1 - t0);
	}
	// finalize
	fftw_destroy_plan(forward_plan);
	fftw_destroy_plan(backward_plan);
	fftw_free(psi);
	fftw_free(psi_conj);
	fftw_free(mom_i);
}


void spectral_differencing_phase(const char *snap_path) {
	double t0, t1;
	// account for non-contiguous FFT data layout with real FFT
	const size_t r2c_strides[NUMDIMS] =
		{ 1, GRID.stride_z_real, (size_t) GRID.ny * GRID.stride_z_real };
	printf0("Starting computation of velocities (spectral)\n");
	// allocate arrays
	fft_real *phase = fftw_alloc_real(GRID.local_real_num_el);
	assert(phase);
	for (LocalIdx idx = 0; idx < GRID.local_real_num_el; idx++)
		phase[idx] = NAN;
	fft_real *sin_phase = fftw_alloc_real(GRID.local_real_num_el);
	assert(sin_phase);
	fft_complex *sin_phase_fft = (fft_complex *) sin_phase;
	for (LocalIdx idx = 0; idx < GRID.local_real_num_el; idx++)
		sin_phase[idx] = NAN;
	fft_real *cos_phase = fftw_alloc_real(GRID.local_real_num_el);
	assert(cos_phase);
	fft_complex *cos_phase_fft = (fft_complex *) cos_phase;
	for (LocalIdx idx = 0; idx < GRID.local_real_num_el; idx++)
		cos_phase[idx] = NAN;
	// create FFTW plans
	printf0("Creating FFTW plans…\n");
	fftw_plan forward_plan_sin = fftw_mpi_plan_dft_r2c_3d(
		GRID.nx, GRID.ny, GRID.nz, sin_phase, sin_phase_fft, MPI_COMM_WORLD,
		FFTW_ESTIMATE | FFTW_MPI_TRANSPOSED_OUT
	);
	fftw_plan backward_plan_sin = fftw_mpi_plan_dft_c2r_3d(
		GRID.nx, GRID.ny, GRID.nz, sin_phase_fft, sin_phase, MPI_COMM_WORLD,
		FFTW_ESTIMATE | FFTW_MPI_TRANSPOSED_IN
	);
	fftw_plan forward_plan_cos = fftw_mpi_plan_dft_r2c_3d(
		GRID.nx, GRID.ny, GRID.nz, cos_phase, cos_phase_fft, MPI_COMM_WORLD,
		FFTW_ESTIMATE | FFTW_MPI_TRANSPOSED_OUT
	);
	fftw_plan backward_plan_cos = fftw_mpi_plan_dft_c2r_3d(
		GRID.nx, GRID.ny, GRID.nz, cos_phase_fft, cos_phase,
		MPI_COMM_WORLD, FFTW_ESTIMATE | FFTW_MPI_TRANSPOSED_IN
	);
	// read wave function phase and compute sin and cos
	printf0("Reading wave function…\n");
	t0 = time();
	read_wave_function(snap_path, phase, process_buffer_phase_padded);
	t1 = time();
	printf0("Done (took %g sec.).\n", t1 - t0);
	GlobalIdx cos_phase_zeros = 0;
	for (uint x = 0; x < GRID.local_nx; x++)
		for (uint y = 0; y < GRID.ny; y++)
			for (uint z = 0; z < GRID.nz; z++) {
				const LocalIdx idx = FFT_RI(x, y, z);
				sin_phase[idx] = sin(phase[idx]);
				cos_phase[idx] = cos(phase[idx]);
				if (isclose(cos_phase[idx], 0, 0, COS_TOL))
					cos_phase_zeros++;
			}
	GlobalIdx cos_phase_zeros_glob;
	MPI_Allreduce(
		&cos_phase_zeros, &cos_phase_zeros_glob, 1, GLOBAL_IDX_MPI, MPI_SUM,
		MPI_COMM_WORLD
	);
	printf0(
		"In total, %" GLOBAL_IDX_PRI " cells have cos(α) ≈ 0!\n",
		cos_phase_zeros_glob
	);
	// do all velocity components
	for (uint i = 0; i < NUMDIMS; i++) {
		// reset sin/cos after each iteration
		if (i > 0) {
			for (LocalIdx idx = 0; idx < GRID.local_real_num_el; idx++) {
				sin_phase[idx] = sin(phase[idx]);
				cos_phase[idx] = cos(phase[idx]);
			}
		}
		printf0("Doing velocity component %u…\n", i);
		// compute first pass with first equation: cos(α) u⃗ = ℏ/m ∇ sin(α)
		printf0("\tDoing first pass: cos(α) u⃗ = ℏ/m ∇ sin(α)\n");
		t0 = time();
		// transform sin(α) forward
		fftw_execute(forward_plan_sin);
		// computations in FFT space
		for (uint y = 0; y < GRID.local_ny; y++)
			for (uint x = 0; x < GRID.nx; x++)
				for (uint z = 0; z < GRID.nz_complex; z++) {
					const LocalIdx idx = FFT_CI_T(x, y, z);
					const double k_i = k_vec(i, x, GRID.local_y_start + y, z);
					sin_phase_fft[idx] *= I * k_i;
				}
		// transform back
		fftw_execute(backward_plan_sin);
		// computations in real space
		for (uint x = 0; x < GRID.local_nx; x++)
			for (uint y = 0; y < GRID.ny; y++)
				for (uint z = 0; z < GRID.nz; z++) {
					const LocalIdx idx = FFT_RI(x, y, z);
					// equation not valid for cos(α) = 0
					if (isclose(cos_phase[idx], 0, 0, COS_TOL))
						sin_phase[idx] = NAN;
					else
						sin_phase[idx] /= cos_phase[idx];
					// apply constants
					sin_phase[idx] *= PARAM.hbar / PARAM.mass;
					// normalize
					sin_phase[idx] /= GRID.num_cells_tot;
					// convert from comoving to physical
					sin_phase[idx] /= PARAM.a;
				}
		t1 = time();
		printf0("\tFirst pass done (took %g sec.).\n", t1 - t0);
		// consistency check for second pass
		for (uint x = 0; x < GRID.local_nx; x++)
			for (uint y = 0; y < GRID.ny; y++)
				for (uint z = 0; z < GRID.nz; z++) {
					const LocalIdx idx = FFT_RI(x, y, z);
					if (isnan(sin_phase[idx]))
						assert(isclose(cos_phase[idx], 0, 0, COS_TOL));
				}
		// if necessary, compute second pass with second equation:
		// -sin(α) u⃗ = ℏ/m ∇ cos(α)
		if (cos_phase_zeros_glob > 0) {
			printf0("\tDoing second pass: -sin(α) u⃗ = ℏ/m ∇ cos(α)\n");
			t0 = time();
			// transform cos(α) forward
			fftw_execute(forward_plan_cos);
			// computations in FFT space
			for (uint y = 0; y < GRID.local_ny; y++)
				for (uint x = 0; x < GRID.nx; x++)
					for (uint z = 0; z < GRID.nz_complex; z++) {
						const LocalIdx idx = FFT_CI_T(x, y, z);
						const double k_i = k_vec(
							i, x, GRID.local_y_start + y, z
						);
						cos_phase_fft[idx] *= I * k_i;
					}
			// transform back
			fftw_execute(backward_plan_cos);
			// computations in real space
			for (uint x = 0; x < GRID.local_nx; x++)
				for (uint y = 0; y < GRID.ny; y++)
					for (uint z = 0; z < GRID.nz; z++) {
						const LocalIdx idx = FFT_RI(x, y, z);
						// fill in missing values from first pass where
						// cos(α) = 0
						if (isnan(sin_phase[idx])) {
							sin_phase[idx] = -cos_phase[idx] / sin(phase[idx]);
							// apply constants
							sin_phase[idx] *= PARAM.hbar / PARAM.mass;
							// normalize
							sin_phase[idx] /= GRID.num_cells_tot;
							// convert from comoving to physical
							sin_phase[idx] /= PARAM.a;
						}
					}
			t1 = time();
			printf0("\tSecond pass done (took %g sec.).\n", t1 - t0);
		}
		// output velocity data
		printf0("\tWriting velocity data (component %u)…\n", i);
		t0 = time();
		char dataset_name[50];
		sprintf(dataset_name, "Velocities%u", i);
		save_data(
			snap_path, dataset_name, sin_phase, true,
			r2c_strides, ARRAY_LEN(r2c_strides)
		);
		MPI_Barrier(MPI_COMM_WORLD);
		t1 = time();
		printf0("\tDone (took %g sec.).\n", t1 - t0);
	}
	// free velocity computation variables
	fftw_destroy_plan(forward_plan_sin);
	fftw_destroy_plan(backward_plan_sin);
	fftw_destroy_plan(forward_plan_cos);
	fftw_destroy_plan(backward_plan_cos);
	fftw_free(phase);
	fftw_free(cos_phase);
	fftw_free(sin_phase);
}


void spectral_differencing_sqrt_rho(const char *snap_path) {
	double t0, t1;
	// account for non-contiguous FFT data layout with real FFT
	const size_t r2c_strides[NUMDIMS] =
		{ 1, GRID.stride_z_real, (size_t) GRID.ny * GRID.stride_z_real };
	printf0("Starting computation of (∇√ρ)² (spectral)\n");
	// allocate/initialize
	fft_real *sqrt_rho_orig = fftw_alloc_real(GRID.local_real_num_el);
	assert(sqrt_rho_orig);
	for (LocalIdx idx = 0; idx < GRID.local_real_num_el; idx++)
		sqrt_rho_orig[idx] = NAN;
	fft_real *sqrt_rho = fftw_alloc_real(GRID.local_real_num_el);
	assert(sqrt_rho);
	fft_complex *sqrt_rho_fft = (fft_complex *) sqrt_rho;
	for (LocalIdx idx = 0; idx < GRID.local_real_num_el; idx++)
		sqrt_rho[idx] = NAN;
	fft_real *result = fftw_alloc_real(GRID.local_real_num_el);
	assert(result);
	for (LocalIdx idx = 0; idx < GRID.local_real_num_el; idx++)
		result[idx] = NAN;
	for (uint x = 0; x < GRID.local_nx; x++)
		for (uint y = 0; y < GRID.ny; y++)
			for (uint z = 0; z < GRID.nz; z++)
				result[FFT_RI(x, y, z)] = 0;
	// create FFTW plans
	printf0("Creating FFTW plans…\n");
	fftw_plan forward_plan_sqrt_rho = fftw_mpi_plan_dft_r2c_3d(
		GRID.nx, GRID.ny, GRID.nz, sqrt_rho, sqrt_rho_fft, MPI_COMM_WORLD,
		FFTW_ESTIMATE | FFTW_MPI_TRANSPOSED_OUT
	);
	fftw_plan backward_plan_sqrt_rho = fftw_mpi_plan_dft_c2r_3d(
		GRID.nx, GRID.ny, GRID.nz, sqrt_rho_fft, sqrt_rho, MPI_COMM_WORLD,
		FFTW_ESTIMATE | FFTW_MPI_TRANSPOSED_IN
	);
	// read density
	printf0("Reading wave function…\n");
	t0 = time();
	read_wave_function(snap_path, sqrt_rho_orig, process_buffer_sqrt_rho_padded);
	t1 = time();
	printf0("Done (took %g sec.).\n", t1 - t0);
	// do all gradient components
	for (uint i = 0; i < NUMDIMS; i++) {
		t0 = time();
		printf0("Doing component %u of ∇√ρ…\n", i);
		for (LocalIdx idx = 0; idx < GRID.local_real_num_el; idx++)
			sqrt_rho[idx] = sqrt_rho_orig[idx];
		// transform √ρ forward
		fftw_execute(forward_plan_sqrt_rho);
		// computations in FFT space
		for (uint y = 0; y < GRID.local_ny; y++)
			for (uint x = 0; x < GRID.nx; x++)
				for (uint z = 0; z < GRID.nz_complex; z++) {
					const LocalIdx idx = FFT_CI_T(x, y, z);
					const double k_i = k_vec(i, x, GRID.local_y_start + y, z);
					sqrt_rho_fft[idx] *= I * k_i;
				}
		// transform back
		fftw_execute(backward_plan_sqrt_rho);
		// computations in real space
		for (uint x = 0; x < GRID.local_nx; x++)
			for (uint y = 0; y < GRID.ny; y++)
				for (uint z = 0; z < GRID.nz; z++) {
					const LocalIdx idx = FFT_RI(x, y, z);
					// normalize
					sqrt_rho[idx] /= GRID.num_cells_tot;
					// add to result, converting from comoving to physical
					result[idx] += pow(sqrt_rho[idx] / PARAM.a, 2);
				}
		t1 = time();
		printf0("Done (took %g sec.).\n", t1 - t0);
	}
	// output data
	printf0("Writing data…\n");
	t0 = time();
	save_data(
		snap_path, "(GradSqrtRho)^2", result, true,
		r2c_strides, ARRAY_LEN(r2c_strides)
	);
	MPI_Barrier(MPI_COMM_WORLD);
	t1 = time();
	printf0("Done (took %g sec.).\n", t1 - t0);
	// free (∇√ρ)² computation variables
	fftw_destroy_plan(forward_plan_sqrt_rho);
	fftw_destroy_plan(backward_plan_sqrt_rho);
	fftw_free(sqrt_rho_orig);
	fftw_free(sqrt_rho);
	fftw_free(result);
}


void finite_differencing_momentum(const char *snap_path) {
	const double dx = PARAM.box_size / GRID.nx;
	const double dy = PARAM.box_size / GRID.ny;
	const double dz = PARAM.box_size / GRID.nz;
	double t0, t1;
	printf0("Starting computation of momentum density (finite differencing)\n");
	// allocate arrays
	fft_complex *psi = fftw_alloc_complex(GRID.local_num_el_c2c);
	assert(psi);
	for (LocalIdx idx = 0; idx < GRID.local_num_el_c2c; idx++)
		psi[idx] = NAN;
	fft_real *mom_i = fftw_alloc_real(GRID.local_num_el_c2c);
	assert(mom_i);
	for (LocalIdx idx = 0; idx < GRID.local_num_el_c2c; idx++)
		mom_i[idx] = NAN;
	// check grid sizes
	ptrdiff_t local_nx_many, local_x_start_many,
		local_ny_many, local_y_start_many;
	size_t local_num_el_many = fftw_mpi_local_size_many_transposed(
		2, (ptrdiff_t []){GRID.nx, GRID.ny}, GRID.nz,
		FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK,
		MPI_COMM_WORLD,
		&local_nx_many, &local_x_start_many,
		&local_ny_many, &local_y_start_many
	);
	assert(GRID.local_nx_c2c == local_nx_many);
	assert(GRID.local_x_start_c2c == local_x_start_many);
	assert(GRID.local_ny_c2c == local_ny_many);
	assert(GRID.local_y_start_c2c == local_y_start_many);
	assert(GRID.local_num_el_c2c >= local_num_el_many);
	// create FFTW transpose plans
	fftw_plan forward_plan_transpose = fftw_mpi_plan_many_transpose(
		GRID.nx, GRID.ny, 2 * GRID.nz, FFTW_MPI_DEFAULT_BLOCK,
		FFTW_MPI_DEFAULT_BLOCK, (fft_real *) psi, (fft_real *) psi,
		MPI_COMM_WORLD, FFTW_ESTIMATE
	);
	fftw_plan backward_plan_transpose = fftw_mpi_plan_many_transpose(
		GRID.ny, GRID.nx, GRID.nz, FFTW_MPI_DEFAULT_BLOCK,
		FFTW_MPI_DEFAULT_BLOCK, mom_i, mom_i,
		MPI_COMM_WORLD, FFTW_ESTIMATE
	);
	// read wave function
	printf0("Reading wave function…\n");
	t0 = time();
	read_wave_function(snap_path, psi, process_buffer_psi);
	t1 = time();
	printf0("Done (took %g sec.).\n", t1 - t0);
	// do all velocity components
	for (uint ii = 0; ii < NUMDIMS; ii++) {
		const uint i = NUMDIMS - 1 - ii;
		printf0("Doing velocity component %u…\n", i);
		t0 = time();
		// for x component: transpose, do x component, and transpose back
		if (i == 0) {
			fftw_execute(forward_plan_transpose);
			for (uint y = 0; y < GRID.local_ny; y++)
				for (uint x = 0; x < GRID.nx; x++)
					for (uint z = 0; z < GRID.nz; z++) {
						const uint xp = periodic_wrap(x + 1, GRID.nx);
						const uint xm = periodic_wrap((int) x - 1, GRID.nx);
						const LocalIdx idx = IDX_T(x, y, z);
						const LocalIdx idx_xp = IDX_T(xp, y, z);
						const LocalIdx idx_xm = IDX_T(xm, y, z);
						const fft_complex grad_x = (psi[idx_xp] - psi[idx_xm]) /
							(2 * dx);
						mom_i[idx] = (PARAM.hbar / PARAM.mass) *
							cimag(conj(psi[idx]) * grad_x);
						// convert from comoving to physical
						mom_i[idx] /= PARAM.a;
					}
			fftw_execute(backward_plan_transpose);
		}
		else {
			for (uint x = 0; x < GRID.local_nx; x++)
				for (uint y = 0; y < GRID.ny; y++)
					for (uint z = 0; z < GRID.nz; z++) {
						const LocalIdx idx = IDX(x, y, z);
						fft_complex grad_i = 0;
						if (i == 1) {
							const uint yp = periodic_wrap(y + 1, GRID.ny);
							const uint ym = periodic_wrap((int) y - 1, GRID.ny);
							const LocalIdx idx_yp = IDX(x, yp, z);
							const LocalIdx idx_ym = IDX(x, ym, z);
							grad_i = (psi[idx_yp] - psi[idx_ym]) / (2 * dy);
						}
						else if (i == 2) {
							const uint zp = periodic_wrap(z + 1, GRID.nz);
							const uint zm = periodic_wrap((int) z - 1, GRID.nz);
							const LocalIdx idx_zp = IDX(x, y, zp);
							const LocalIdx idx_zm = IDX(x, y, zm);
							grad_i = (psi[idx_zp] - psi[idx_zm]) / (2 * dz);
						}
						else
							assert(false);
						mom_i[idx] = (PARAM.hbar / PARAM.mass) *
								cimag(conj(psi[idx]) * grad_i);
						// convert from comoving to physical
						mom_i[idx] /= PARAM.a;
					}
		}
		t1 = time();
		printf0("\tDone (took %g sec.).\n", t1 - t0);
		// output momentum density data
		printf0("\tWriting momentum density data (component %u)…\n", i);
		t0 = time();
		char dataset_name[50];
		sprintf(dataset_name, "MomentumDensity%u_fd", i);
		save_data(snap_path, dataset_name, mom_i, true, NULL, 0);
		MPI_Barrier(MPI_COMM_WORLD);
		t1 = time();
		printf0("\tDone (took %g sec.).\n", t1 - t0);
	}
	// finalize
	fftw_destroy_plan(forward_plan_transpose);
	fftw_destroy_plan(backward_plan_transpose);
	fftw_free(psi);
	fftw_free(mom_i);
}


void finite_differencing_phase(const char *snap_path) {
	const double dx = PARAM.box_size / GRID.nx;
	const double dy = PARAM.box_size / GRID.ny;
	const double dz = PARAM.box_size / GRID.nz;
	double t0, t1;
	printf0("Starting computation of velocities (finite differencing)\n");
	// allocate arrays
	fft_real *phase = fftw_alloc_real(GRID.local_real_num_el);
	assert(phase);
	for (LocalIdx idx = 0; idx < GRID.local_real_num_el; idx++)
		phase[idx] = NAN;
	fft_real *vel_i = fftw_alloc_real(GRID.local_real_num_el);
	assert(vel_i);
	for (LocalIdx idx = 0; idx < GRID.local_real_num_el; idx++)
		vel_i[idx] = NAN;
	// check grid sizes
	ptrdiff_t local_nx_many, local_x_start_many,
		local_ny_many, local_y_start_many;
	size_t local_num_el_many = fftw_mpi_local_size_many_transposed(
		2, (ptrdiff_t []){GRID.nx, GRID.ny}, GRID.nz,
		FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK,
		MPI_COMM_WORLD,
		&local_nx_many, &local_x_start_many,
		&local_ny_many, &local_y_start_many
	);
	assert(GRID.local_nx == local_nx_many);
	assert(GRID.local_x_start == local_x_start_many);
	assert(GRID.local_ny == local_ny_many);
	assert(GRID.local_y_start == local_y_start_many);
	assert(GRID.local_real_num_el >= local_num_el_many);
	// create FFTW transpose plans
	fftw_plan forward_plan_transpose_vel = fftw_mpi_plan_many_transpose(
		GRID.nx, GRID.ny, GRID.nz, FFTW_MPI_DEFAULT_BLOCK,
		FFTW_MPI_DEFAULT_BLOCK, phase, phase,
		MPI_COMM_WORLD, FFTW_ESTIMATE
	);
	fftw_plan backward_plan_transpose_vel = fftw_mpi_plan_many_transpose(
		GRID.ny, GRID.nx, GRID.nz, FFTW_MPI_DEFAULT_BLOCK,
		FFTW_MPI_DEFAULT_BLOCK, vel_i, vel_i,
		MPI_COMM_WORLD, FFTW_ESTIMATE
	);
	// read wave function phase
	printf0("Reading wave function…\n");
	t0 = time();
	read_wave_function(snap_path, phase, process_buffer_phase_contiguous);
	t1 = time();
	printf0("Done (took %g sec.).\n", t1 - t0);
	// do all velocity components
	for (uint ii = 0; ii < NUMDIMS; ii++) {
		const uint i = NUMDIMS - 1 - ii;
		printf0("Doing velocity component %u…\n", i);
		t0 = time();
		// for x component: transpose, do x component, and transpose back
		if (i == 0) {
			fftw_execute(forward_plan_transpose_vel);
			for (uint y = 0; y < GRID.local_ny; y++)
				for (uint x = 0; x < GRID.nx; x++)
					for (uint z = 0; z < GRID.nz; z++) {
						const uint xp = periodic_wrap(x + 1, GRID.nx);
						const uint xm = periodic_wrap((int) x - 1, GRID.nx);
						const LocalIdx idx = IDX_T(x, y, z);
						const LocalIdx idx_xp = IDX_T(xp, y, z);
						const LocalIdx idx_xm = IDX_T(xm, y, z);
						const double grad_x =
							phase_diff_wrap(phase[idx_xp] - phase[idx_xm]) /
							(2 * dx);
						vel_i[idx] = (PARAM.hbar / PARAM.mass) * grad_x;
						// convert from comoving to physical
						vel_i[idx] /= PARAM.a;
					}
			fftw_execute(backward_plan_transpose_vel);
		}
		else {
			for (uint x = 0; x < GRID.local_nx; x++)
				for (uint y = 0; y < GRID.ny; y++)
					for (uint z = 0; z < GRID.nz; z++) {
						const LocalIdx idx = IDX(x, y, z);
						double grad_i = 0;
						if (i == 1) {
							const uint yp = periodic_wrap(y + 1, GRID.ny);
							const uint ym = periodic_wrap((int) y - 1, GRID.ny);
							const LocalIdx idx_yp = IDX(x, yp, z);
							const LocalIdx idx_ym = IDX(x, ym, z);
							grad_i =
								phase_diff_wrap(phase[idx_yp] - phase[idx_ym]) /
								(2 * dy);
						}
						else if (i == 2) {
							const uint zp = periodic_wrap(z + 1, GRID.nz);
							const uint zm = periodic_wrap((int) z - 1, GRID.nz);
							const LocalIdx idx_zp = IDX(x, y, zp);
							const LocalIdx idx_zm = IDX(x, y, zm);
							grad_i =
								phase_diff_wrap(phase[idx_zp] - phase[idx_zm]) /
								(2 * dz);
						}
						else
							assert(false);
						vel_i[idx] = (PARAM.hbar / PARAM.mass) * grad_i;
						// convert from comoving to physical
						vel_i[idx] /= PARAM.a;
					}
		}
		t1 = time();
		printf0("\tDone (took %g sec.).\n", t1 - t0);
		// output velocity data
		printf0("\tWriting velocity data (component %u)…\n", i);
		t0 = time();
		char dataset_name[50];
		sprintf(dataset_name, "Velocities%u_fd", i);
		save_data(snap_path, dataset_name, vel_i, true, NULL, 0);
		MPI_Barrier(MPI_COMM_WORLD);
		t1 = time();
		printf0("\tDone (took %g sec.).\n", t1 - t0);
	}
	// free velocity computation variables
	fftw_destroy_plan(forward_plan_transpose_vel);
	fftw_destroy_plan(backward_plan_transpose_vel);
	fftw_free(phase);
	fftw_free(vel_i);
}


void finite_differencing_sqrt_rho(const char *snap_path) {
	const double dx = PARAM.box_size / GRID.nx;
	const double dy = PARAM.box_size / GRID.ny;
	const double dz = PARAM.box_size / GRID.nz;
	double t0, t1;
	printf0("Starting computation of (∇√ρ)² (finite differencing)\n");
	// allocate/initialize
	fft_real *sqrt_rho = fftw_alloc_real(GRID.local_real_num_el);
	assert(sqrt_rho);
	for (LocalIdx idx = 0; idx < GRID.local_real_num_el; idx++)
		sqrt_rho[idx] = NAN;
	fft_real *result = fftw_alloc_real(GRID.local_real_num_el);
	assert(result);
	for (LocalIdx idx = 0; idx < GRID.local_real_num_el; idx++)
		result[idx] = NAN;
	for (uint x = 0; x < GRID.local_nx; x++)
		for (uint y = 0; y < GRID.ny; y++)
			for (uint z = 0; z < GRID.nz; z++)
				result[IDX(x, y, z)] = 0;
	fft_real *result_x = fftw_alloc_real(GRID.local_real_num_el);
	assert(result_x);
	for (LocalIdx idx = 0; idx < GRID.local_real_num_el; idx++)
		result_x[idx] = NAN;
	// check grid sizes
	ptrdiff_t local_nx_many, local_x_start_many,
		local_ny_many, local_y_start_many;
		size_t local_num_el_many = fftw_mpi_local_size_many_transposed(
		2, (ptrdiff_t []){GRID.nx, GRID.ny}, GRID.nz,
		FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK,
		MPI_COMM_WORLD,
		&local_nx_many, &local_x_start_many,
		&local_ny_many, &local_y_start_many
	);
	assert(GRID.local_nx == local_nx_many);
	assert(GRID.local_x_start == local_x_start_many);
	assert(GRID.local_ny == local_ny_many);
	assert(GRID.local_y_start == local_y_start_many);
	assert(GRID.local_real_num_el >= local_num_el_many);
	// create FFTW transpose plans
	fftw_plan forward_plan_transpose_grad = fftw_mpi_plan_many_transpose(
		GRID.nx, GRID.ny, GRID.nz, FFTW_MPI_DEFAULT_BLOCK,
		FFTW_MPI_DEFAULT_BLOCK, sqrt_rho, sqrt_rho,
		MPI_COMM_WORLD, FFTW_ESTIMATE
	);
	fftw_plan backward_plan_transpose_grad = fftw_mpi_plan_many_transpose(
		GRID.ny, GRID.nx, GRID.nz, FFTW_MPI_DEFAULT_BLOCK,
		FFTW_MPI_DEFAULT_BLOCK, result_x, result_x,
		MPI_COMM_WORLD, FFTW_ESTIMATE
	);
	// read density
	printf0("Reading wave function…\n");
	t0 = time();
	read_wave_function(snap_path, sqrt_rho, process_buffer_sqrt_rho_contiguous);
	t1 = time();
	printf0("Done (took %g sec.).\n", t1 - t0);
	// do y/z gradient components
	t0 = time();
	printf0("Doing components y/z of ∇√ρ…\n");
	for (uint x = 0; x < GRID.local_nx; x++)
		for (uint y = 0; y < GRID.ny; y++)
			for (uint z = 0; z < GRID.nz; z++) {
				const uint yp = periodic_wrap(y + 1, GRID.ny);
				const uint ym = periodic_wrap((int) y - 1, GRID.ny);
				const uint zp = periodic_wrap(z + 1, GRID.nz);
				const uint zm = periodic_wrap((int) z - 1, GRID.nz);
				const LocalIdx idx_yp = IDX(x, yp, z);
				const LocalIdx idx_ym = IDX(x, ym, z);
				const LocalIdx idx_zp = IDX(x, y, zp);
				const LocalIdx idx_zm = IDX(x, y, zm);
				const double grad_y =
					(sqrt_rho[idx_yp] - sqrt_rho[idx_ym]) / (2 * dy);
				const double grad_z =
					(sqrt_rho[idx_zp] - sqrt_rho[idx_zm]) / (2 * dz);
				// add to result
				const LocalIdx idx = IDX(x, y, z);
				result[idx] += pow(grad_y, 2) + pow(grad_z, 2);
			}
	t1 = time();
	printf0("\tDone (took %g sec.).\n", t1 - t0);
	// transpose, do x component, and transpose back
	printf0("Doing component x of ∇√ρ…\n");
	t0 = time();
	fftw_execute(forward_plan_transpose_grad);
	for (uint y = 0; y < GRID.local_ny; y++)
		for (uint x = 0; x < GRID.nx; x++)
			for (uint z = 0; z < GRID.nz; z++) {
				const uint xp = periodic_wrap(x + 1, GRID.nx);
				const uint xm = periodic_wrap((int) x - 1, GRID.nx);
				const LocalIdx idx = IDX_T(x, y, z);
				const LocalIdx idx_xp = IDX_T(xp, y, z);
				const LocalIdx idx_xm = IDX_T(xm, y, z);
				const double grad_x =
					(sqrt_rho[idx_xp] - sqrt_rho[idx_xm]) / (2 * dx);
				// add to result
				result_x[idx] = pow(grad_x, 2);
			}
	fftw_execute(backward_plan_transpose_grad);
	for (uint x = 0; x < GRID.local_nx; x++)
		for (uint y = 0; y < GRID.ny; y++)
			for (uint z = 0; z < GRID.nz; z++) {
				const LocalIdx idx = IDX(x, y, z);
				result[idx] += result_x[idx];
				// convert from comoving to physical
				result[idx] /= pow(PARAM.a, 2);
			}
	t1 = time();
	printf0("\tDone (took %g sec.).\n", t1 - t0);
	// output data
	printf0("Writing data…\n");
	t0 = time();
	save_data(snap_path, "(GradSqrtRho)^2_fd", result, true, NULL, 0);
	MPI_Barrier(MPI_COMM_WORLD);
	t1 = time();
	printf0("\tDone (took %g sec.).\n", t1 - t0);
	// free (∇√ρ)² computation variables
	fftw_destroy_plan(forward_plan_transpose_grad);
	fftw_destroy_plan(backward_plan_transpose_grad);
	fftw_free(sqrt_rho);
	fftw_free(result);
	fftw_free(result_x);
}


int main(int argc, char **argv) {
	// initialize
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &WORLD_RANK);
	MPI_Comm_size(MPI_COMM_WORLD, &WORLD_SIZE);
	fftw_mpi_init();
	// command line arguments
	assert(argc == 2); // TODO: select method(s)
	const char *snap_path = argv[1];
	if (FILES_OPEN_IN_PARALLEL > (uint) WORLD_SIZE)
		FILES_OPEN_IN_PARALLEL = WORLD_SIZE;
	// read PARAM and GRID
	read_metadata(snap_path);
	// print info
	printf0("SnapshotFileBase: “%s”\n", PARAM.snap_file_base);
	printf0("PMGRID                        = %u\n", GRID.nx);
	printf0("Box size                      = %g\n", PARAM.box_size);
	printf0("hbar (internal units)         = %g\n", PARAM.hbar);
	printf0("mass (eV/c²)                  = %g\n", PARAM.mass_ev);
	printf0("mass (internal units)         = %g\n", PARAM.mass);
	printf0("hbar / mass (internal units)  = %g\n", PARAM.hbar / PARAM.mass);
	printf0("expected v_max (internal units) = %g\n",
		M_PI * (PARAM.hbar / PARAM.mass) / (PARAM.box_size / GRID.nx));
	printf0("\n");
	// do computations
	printf0("======== Spectral differencing (momentum) =========\n");
	spectral_differencing_momentum(snap_path);
	printf0("\n");
	printf0("========== Spectral differencing ((∇√ρ)²) =========\n");
	spectral_differencing_sqrt_rho(snap_path);
	printf0("\n");
//	printf0("========= Finite differencing (momentum) ==========\n");
//	finite_differencing_momentum(snap_path);
//	printf0("\n");
//	printf0("=========== Finite differencing (phase) ===========\n");
//	finite_differencing_phase(snap_path);
//	printf0("\n");
//	printf0("=========== Finite differencing ((∇√ρ)²) ==========\n");
//	finite_differencing_sqrt_rho(snap_path);
//	printf0("\n");
//	printf0("========== Spectral differencing (phase) ==========\n");
//	spectral_differencing_phase(snap_path);
//	printf0("\n");
	// finalize
	free(GRID.slabs_x_per_rank);
	free(GRID.slabs_x_per_rank_c2c);
	fftw_mpi_cleanup();
	MPI_Finalize();
	return EXIT_SUCCESS;
}
