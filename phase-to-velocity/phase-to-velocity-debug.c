#include <assert.h>
#include <complex.h>
#include <inttypes.h>
#include <math.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdnoreturn.h>
#include <string.h>

//#include <getopt.h>
#include <hdf5.h>
#include <fftw3-mpi.h>
#include <mpi.h>

#ifdef __STDC_NO_COMPLEX__
#error"__STDC_NO_COMPLEX__ defined, complex math not supported"
#endif

#if !defined(__has_attribute) && !defined(__GNUC__)
/* remove GCC-style attributes if the compiler does not support them */
#define __attribute__(x)
#endif

#define ARRAY_LEN(x) ((sizeof (x)) / (sizeof (x)[0]))

typedef unsigned char uchar;
typedef unsigned short ushort;
typedef unsigned int uint;
typedef unsigned long ulong;
typedef unsigned long long ulonglong;
typedef double fft_real;
typedef fftw_complex fft_complex;
typedef size_t   LocalIdx;
typedef uint64_t GlobalIdx;
struct hdf5_dset_info {
	const char *path;
	uint num_dims;
	hid_t dset[2];
	hid_t dtype[2];
	hid_t dspace[2];
};
typedef void (*process_buffer_func)(
	void *, const void *, struct hdf5_dset_info, LocalIdx, LocalIdx
);

#ifndef M_PI
const double M_PI = 3.14159265358979323846;
#endif
const double HBAR   = 1.0545718176461565e-34;  // J * s
const double EV     = 1.602176634e-19;  // J
const double CLIGHT = 299792458.0;  // m/s

const char *const HDF5_GROUP_NAME = "/FuzzyDM";
const char *const HDF5_DATASET_NAME[] = {"PsiRe", "PsiIm"};
enum { MAXLEN_PATH = 512 };
enum { FILES_OPEN_IN_PARALLEL = 80 };
enum { NUMDIMS = 3 };
const size_t BUFFER_SIZE = 32 * ((size_t) 1024 * 1024);

int WORLD_RANK;
int WORLD_SIZE;
struct {
	uint local_nx;
	uint local_x_start;
	uint local_ny;
	uint local_y_start;
	uint nx;
	uint ny;
	uint nz;
	uint n[NUMDIMS];
	uint stride_z_complex;
	uint stride_z_real;
	GlobalIdx num_cells_tot;
	uint *slabs_x_per_rank;
} GRID;
struct {
	uint num_files;
	char snap_file_base[MAXLEN_PATH];
	double hbar;
	double mass;
	double mass_ev;
	double box_size;
	double HubbleParam;
	double UnitLength_in_cm;
	double UnitMass_in_g;
	double UnitTime_in_s;
} PARAM;


static inline LocalIdx GIDX_TO_LIDX(GlobalIdx i) {
	return i - GRID.local_x_start * GRID.ny * GRID.nz;
}

static inline LocalIdx LIDX_TO_PADDED(LocalIdx i) {
	const size_t stride_diff_z = GRID.stride_z_real - GRID.nz;
	const size_t num_strides_z = i / GRID.nz;
	return i + num_strides_z * stride_diff_z;
}

static inline LocalIdx FFT_RI(const uint x, const uint y, const uint z) {
	return (LocalIdx) (x * GRID.ny + y) * GRID.stride_z_real + z;
}
static inline LocalIdx FFT_CI_T(const uint x, const uint y, const uint z) {
	return (LocalIdx) (y * GRID.nx + x) * GRID.stride_z_complex + z;
}

static inline double fftfreq(const size_t n, const double d, const size_t i) {
	assert(i < n);
	return (i < n / 2 ? i : (double) i - n) / (d * n);
}

static inline double rfftfreq(const size_t n, const double d, const size_t i) {
	assert(i < n / 2 + 1);
	return i / (d * n);
}

static inline double k_vec(
	const uint i, const uint x, const uint y, const uint z
) {
	const double n_i = GRID.n[i];
	const double d_i = PARAM.box_size / n_i;
	double freq;
	switch (i) {
		case 0:
			freq = fftfreq(n_i, d_i, x);
			break;
		case 1:
			freq = fftfreq(n_i, d_i, y);
			break;
		case 2:
			freq = rfftfreq(n_i, d_i, z);
			break;
		default: assert(false);
	}
	return 2 * M_PI * freq;
}


void __attribute__((format(printf, 1, 2)))
	printf0(const char *const format, ...)
{
	if (WORLD_RANK == 0) {
		va_list args;
		va_start(args, format);
		vprintf(format, args);
		va_end(args);
		fflush(stdout);
	}
}

#define terminate(format, ...) \
	terminate_fullinfo(__FILE__, __LINE__, __func__, (format), __VA_ARGS__)
noreturn void terminate_fullinfo(
	const char *const file, const int line, const char *const func, 
	const char *const format, ...
) {
	va_list args;
	va_start(args, format);
	fprintf(
		stderr,
		"TERMINATE: Rank=%d in %s:%d, %s():\n\t", WORLD_RANK, file, line, func
	);
	vfprintf(stderr, format, args);
	fprintf(stderr, "\n");
	va_end(args);
	exit(EXIT_FAILURE);
}

void __attribute__((format(printf, 2, 3)))
	file_path_sprintf(char *const buf, const char *const fmt, ...)
{
	va_list l;
	va_start(l, fmt);
	const int status = vsnprintf(buf, MAXLEN_PATH, fmt, l);
	if (status >= MAXLEN_PATH)
		terminate(
			"length of file path exceeds MAXLEN_PATH = %u: %s",
			MAXLEN_PATH, buf
		);
	va_end(l);
}

// copied/adapted from AREPO get_file_path()
static char *get_file_path(const char *const snap_path, const uint filenr) {
	char *path = malloc(MAXLEN_PATH * sizeof *path);
	assert(path);
	if (PARAM.num_files > 1) {
		// determine directory of given file
		char snap_dir[MAXLEN_PATH];
		strcpy(snap_dir, snap_path);
		strrchr(snap_dir, '/')[0] = '\0';
		// determine snapshot number
		const char *snap_num_substr = strrchr(snap_path, '_');
		assert(snap_num_substr);
		const long snap_num = strtoul(snap_num_substr + 1, NULL, 10);
		// build resulting path for snapshot file with the given filenr
		file_path_sprintf(
			path, "%s/%s_%03lu.%d.hdf5", snap_dir, PARAM.snap_file_base,
			snap_num, filenr
		);
	}
	else {
		strcpy(path, snap_path);
	}
	return path;
}

// copied/adapted from AREPO fdm_read_grid_dims()
static void read_grid_dims(
	const char *const path, const uint filenr, uint *const num_dims,
	hsize_t (*const num_cells)[NUMDIMS], hssize_t *const num_cells_tot
) {
	// open HDF5 resources
	hid_t hdf5_file = H5Fopen(path, H5F_ACC_RDONLY, H5P_DEFAULT);
	hid_t hdf5_grp = H5Gopen(hdf5_file, HDF5_GROUP_NAME, H5P_DEFAULT);
	assert(hdf5_file != H5I_INVALID_HID && hdf5_grp != H5I_INVALID_HID);
	hid_t hdf5_dset[2];
	hid_t hdf5_dspace[2];
	int ndims[2];
	for (uint cpart = 0; cpart < 2; cpart++) {
		hdf5_dset[cpart] =
			H5Dopen(hdf5_grp, HDF5_DATASET_NAME[cpart], H5P_DEFAULT);
		hdf5_dspace[cpart] = H5Dget_space(hdf5_dset[cpart]);
		assert(
			hdf5_dset[cpart] != H5I_INVALID_HID &&
			hdf5_dspace[cpart] != H5I_INVALID_HID
		);
		ndims[cpart] = H5Sget_simple_extent_ndims(hdf5_dspace[cpart]);
		assert(ndims[cpart] > 0);
	}
	// consistency checks
	if (ndims[0] != ndims[1])
		terminate(
			"Number of dimensions different between datasets %s and %s: "
			"%d != %d",
			HDF5_DATASET_NAME[0], HDF5_DATASET_NAME[1], ndims[0], ndims[1]
		);
	num_dims[filenr] = ndims[0];
	const uint nd = num_dims[filenr];
	if (nd != 1 && nd != NUMDIMS)
		terminate(
			"Unexpected number of dimensions for datasets %s and %s: %d",
			HDF5_DATASET_NAME[0], HDF5_DATASET_NAME[1], nd
		);
	// read number of cells for this file
	const int nd_check =
		H5Sget_simple_extent_dims(hdf5_dspace[0], num_cells[filenr], NULL);
	assert(nd_check == (int) nd);
	num_cells_tot[filenr] = H5Sget_simple_extent_npoints(hdf5_dspace[0]);
	assert(num_cells_tot[filenr] > 0);
	// confirm that the grid data is divided into complete slabs
	if (
		(num_cells_tot[filenr] % (GRID.ny * GRID.nz) != 0) ||
		(nd >= 2 && num_cells[filenr][1] != GRID.ny) ||
		(nd >= 3 && num_cells[filenr][2] != GRID.nz)
	)
		terminate(
			"Input file %s contains incomplete slabs!\n"
			"num_dims=%u, num_cells_tot=%" PRId64 ", num_cells[y]=%" PRIu64
			", num_cells[z]=%" PRIu64 ", GRID.ny=%td, GRID.nz=%td",
			path, nd, (int64_t) num_cells_tot[filenr],
			(uint64_t) (nd >= 2 ? num_cells[1] : 0),
			(uint64_t) (nd >= 3 ? num_cells[2] : 0),
			GRID.ny, GRID.nz
		);
	/* close HDF5 resources */
	for (uint cpart = 0; cpart < 2; cpart++) {
		H5Sclose(hdf5_dspace[cpart]);
		H5Dclose(hdf5_dset[cpart]);
	}
	H5Gclose(hdf5_grp);
	H5Fclose(hdf5_file);
}

// copied/adapted from AREPO fdm_read_ic_grid()
// (split into read_wave_function(), read_file(), and read_datasets())
static void read_datasets(
	const struct hdf5_dset_info hdf5_ds, const LocalIdx read_offset,
	const LocalIdx read_count, const LocalIdx thistask_read, void *buffer_tmp,
	void *buffer_out, process_buffer_func process_buffer
) {
	// memory dataspace
	const hid_t hdf5_dataspace_in_memory =
		H5Screate_simple(1, (hsize_t []){2 * read_count}, NULL);
	assert(hdf5_dataspace_in_memory != H5I_INVALID_HID);
	// file dataspace selection
	hsize_t start[NUMDIMS] = {0};
	hsize_t count[NUMDIMS];
	if (hdf5_ds.num_dims == 1) {
		start[0] = read_offset;
		count[0] = read_count;
	}
	else {
		// TODO: this doesn’t work yet for num_dims > 1
		assert(false);
	}
	// read data and process for output buffer
	for (uint cpart = 0; cpart < 2; cpart++) {
		H5Sselect_hyperslab(
			hdf5_dataspace_in_memory, H5S_SELECT_SET, (hsize_t []){cpart},
			(hsize_t []){2}, (hsize_t []){read_count}, NULL
		);
		H5Sselect_hyperslab(
			hdf5_ds.dspace[cpart], H5S_SELECT_SET, start, NULL, count, NULL
		);
		H5Dread(
			hdf5_ds.dset[cpart], hdf5_ds.dtype[cpart], hdf5_dataspace_in_memory,
			hdf5_ds.dspace[cpart], H5P_DEFAULT, buffer_tmp
		);
	}
	process_buffer(buffer_out, buffer_tmp, hdf5_ds, thistask_read, read_count);
	// close HDF5 resources
	H5Sclose(hdf5_dataspace_in_memory);
}

// copied/adapted from AREPO fdm_read_ic_grid()
// (split into read_wave_function(), read_file(), and read_datasets())
static LocalIdx read_file(
	const char *const path, const uint num_dims, const hssize_t num_cells_tot,
	const LocalIdx task_cells, LocalIdx thistask_read,
	const uint64_t cells_read_this_file, void *buffer_tmp, void *buffer_out,
	process_buffer_func process_buffer
) {
	// open HDF5 resources
	hid_t hdf5_file = H5Fopen(path, H5F_ACC_RDONLY, H5P_DEFAULT);
	hid_t hdf5_grp = H5Gopen(hdf5_file, HDF5_GROUP_NAME, H5P_DEFAULT);
	assert(hdf5_file != H5I_INVALID_HID && hdf5_grp != H5I_INVALID_HID);
	struct hdf5_dset_info hdf5_ds;
	hdf5_ds.path = path;
	hdf5_ds.num_dims = num_dims;
	for (uint cpart = 0; cpart < 2; cpart++) {
		hdf5_ds.dset[cpart] =
			H5Dopen(hdf5_grp, HDF5_DATASET_NAME[cpart], H5P_DEFAULT);
		hdf5_ds.dspace[cpart] = H5Dget_space(hdf5_ds.dset[cpart]);
		assert(
			hdf5_ds.dset[cpart] != H5I_INVALID_HID &&
			hdf5_ds.dspace[cpart] != H5I_INVALID_HID
		);
		hdf5_ds.dtype[cpart] = H5Dget_type(hdf5_ds.dset[cpart]);
	}
	// metadata consistency checks
	if (H5Tequal(hdf5_ds.dtype[0], hdf5_ds.dtype[1]) <= 0)
		terminate(
			"Datatypes differ between datasets %s and %s",
			HDF5_DATASET_NAME[0], HDF5_DATASET_NAME[1]
		);
	if (H5Sextent_equal(hdf5_ds.dspace[0], hdf5_ds.dspace[1]) <= 0)
		terminate(
			"Dataspace extent differs between datasets %s and %s",
			HDF5_DATASET_NAME[0], HDF5_DATASET_NAME[1]
		);
	// read from file in chunks of the buffer size until all cells
	// for this task have been read or the file ends
	const size_t grid_buffer_size =
		BUFFER_SIZE / (2 * H5Tget_size(hdf5_ds.dtype[0]));
	LocalIdx thistask_read_this_file = 0;
	uint64_t cells_remaining_in_file = num_cells_tot - cells_read_this_file;
	assert(cells_remaining_in_file > 0);
	while (thistask_read < task_cells && cells_remaining_in_file > 0) {
		cells_remaining_in_file = num_cells_tot - cells_read_this_file -
			thistask_read_this_file;
		LocalIdx read_count = task_cells - thistask_read;
		if (cells_remaining_in_file < read_count)
			read_count = cells_remaining_in_file;
		if (grid_buffer_size < read_count)
			read_count = grid_buffer_size;
		read_datasets(
			hdf5_ds, cells_read_this_file + thistask_read_this_file, read_count,
			thistask_read, buffer_tmp, buffer_out, process_buffer
		);
		// increment counters
		thistask_read += read_count;
		thistask_read_this_file += read_count;
	}
	assert(
		cells_remaining_in_file != 0 ||
		(hssize_t) (cells_read_this_file + thistask_read_this_file) ==
			num_cells_tot
	);
	// close HDF5 resources
	for (uint cpart = 0; cpart < 2; cpart++) {
		H5Tclose(hdf5_ds.dtype[cpart]);
		H5Sclose(hdf5_ds.dspace[cpart]);
		H5Dclose(hdf5_ds.dset[cpart]);
	}
	H5Gclose(hdf5_grp);
	H5Fclose(hdf5_file);
	return thistask_read;
}

// copied/adapted from AREPO fdm_read_ic_grid()
// (split into read_wave_function(), read_file(), and read_datasets())
static void read_wave_function(
	const char *const snap_path, void *buffer_out,
	process_buffer_func process_buffer
) {
	// in order to determine which task needs to read which files, first read
	// grid sizes sequentially on rank 0 (for simplicity)
	uint num_dims;
	uint num_dims_files[PARAM.num_files];
	hsize_t num_cells[PARAM.num_files][NUMDIMS];
	hssize_t num_cells_tot[PARAM.num_files];
	if (WORLD_RANK == 0) {
		for (uint filenr = 0; filenr < PARAM.num_files; filenr++) {
			char *path = get_file_path(snap_path, filenr);
			read_grid_dims(
				path, filenr, num_dims_files, num_cells, num_cells_tot
			);
			free(path);
			// check num_dims consistency
			if (num_dims_files[filenr] != num_dims_files[0])
				terminate(
					"Number of dimensions for %s and %s differs between input "
					"files 0 and %d: %d != %d",
					HDF5_DATASET_NAME[0], HDF5_DATASET_NAME[1], filenr,
					num_dims_files[0], num_dims_files[filenr]
				);
		}
		num_dims = num_dims_files[0];
	}
	MPI_Bcast(&num_dims, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
	MPI_Bcast(num_cells, sizeof num_cells, MPI_BYTE, 0, MPI_COMM_WORLD);
	MPI_Bcast(
		num_cells_tot, sizeof num_cells_tot, MPI_BYTE, 0, MPI_COMM_WORLD
	);
	// verify grid size consistency
	uint64_t cells_total = 0;
	for (uint filenr = 0; filenr < PARAM.num_files; filenr++)
		cells_total += num_cells_tot[filenr];
	if (cells_total != GRID.num_cells_tot)
		terminate(
			"%" PRIu64 " = number of wave function cells != "
			"GRID.nx * GRID.ny * GRID.nz = %td * %td * %td = %" PRIu64,
			cells_total, GRID.nx, GRID.ny, GRID.nz, GRID.num_cells_tot
		);
	// read grid data from snapshot files in parallel
	void *buffer_tmp = malloc(BUFFER_SIZE);
	uint filenr = 0;
	int task = 0;
	while (GRID.slabs_x_per_rank[task] == 0 && task < WORLD_SIZE)
		task++;
	uint64_t cells_read = 0;
	uint64_t cells_read_this_file = 0;
	LocalIdx thistask_read = 0;
	while (cells_read < cells_total) {
		assert(task < WORLD_SIZE);
		assert(filenr < PARAM.num_files);
		uint open_files = 0;
		LocalIdx task_cells_remaining = 0;
		while (
			open_files < FILES_OPEN_IN_PARALLEL && filenr < PARAM.num_files
		) {
			LocalIdx task_cells =
				(LocalIdx) GRID.slabs_x_per_rank[task] * GRID.ny * GRID.nz;
			if (task_cells_remaining == 0)
				task_cells_remaining = task_cells;
			// if the current rank is active, read the current file until
			// either all cells for this rank have been read or the file ends
			if (task == WORLD_RANK) {
				assert(thistask_read < task_cells);
				assert(task_cells_remaining <= task_cells);
				char *path = get_file_path(snap_path, filenr);
				printf0("Reading file %u…\n", filenr);
				thistask_read = read_file(
					path, num_dims, num_cells_tot[filenr], task_cells,
					thistask_read, cells_read_this_file, buffer_tmp, buffer_out,
					process_buffer
				);
				free(path);
			}
			// if the current file has ended and the current rank still has
			// cells to read: open the next file on the same rank
			if (
				(hssize_t) (cells_read_this_file + task_cells_remaining) >
				num_cells_tot[filenr]
			) {
				LocalIdx task_cells_read =
					num_cells_tot[filenr] - cells_read_this_file;
				cells_read += task_cells_read;
				task_cells_remaining -= task_cells_read;
				filenr++;
				cells_read_this_file = 0;
			}
			// the current rank has read all of its cells: open the same file
			// (or the next, if the current file has ended at the same time)
			// on the next rank
			else {
				cells_read += task_cells_remaining;
				cells_read_this_file += task_cells_remaining;
				task_cells_remaining = 0;
				task++;
				open_files++;
				if ((hssize_t) cells_read_this_file == num_cells_tot[filenr]) {
					filenr++;
					cells_read_this_file = 0;
				}
			}
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
	// consistency checks
	LocalIdx thistask_cells = (LocalIdx) GRID.local_nx * GRID.ny * GRID.nz;
	assert(thistask_read == thistask_cells);
	assert(cells_read == cells_total);
	// finalize
	free(buffer_tmp);
}

static void process_buffer_phase(
	void *buffer_out, const void *const psi_buf,
	const struct hdf5_dset_info hdf5_ds, const LocalIdx start,
	const LocalIdx length
) {
	fft_real *phase = buffer_out;
	const hid_t hdf5_float = H5Tcopy(H5T_NATIVE_FLOAT);
	const hid_t hdf5_double = H5Tcopy(H5T_NATIVE_DOUBLE);
	const hid_t hdf5_dt = hdf5_ds.dtype[0];
	const bool is_float = H5Tequal(hdf5_dt, hdf5_float) > 0;
	const bool is_double = H5Tequal(hdf5_dt, hdf5_double) > 0;
	for (LocalIdx i = 0; i < length; i++) {
		double complex psi_i;
		if (is_float)
			psi_i = ((float complex *) psi_buf)[i];
		else if (is_double)
			psi_i = ((double complex *) psi_buf)[i];
		else
			terminate(
				"Unexpected HDF5 data type in input file %s", hdf5_ds.path
			);
		phase[LIDX_TO_PADDED(start + i)] = carg(psi_i);
	}
	H5Tclose(hdf5_float);
	H5Tclose(hdf5_double);
}

static void set_units(const hid_t hdf5_header) {
	hid_t hdf5_attr;
	herr_t status;
	hdf5_attr = H5Aopen_name(hdf5_header, "HubbleParam");
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &PARAM.HubbleParam);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	hdf5_attr = H5Aopen_name(hdf5_header, "UnitLength_in_cm");
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &PARAM.UnitLength_in_cm);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	hdf5_attr = H5Aopen_name(hdf5_header, "UnitMass_in_g");
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &PARAM.UnitMass_in_g);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	double UnitVelocity_in_cm_per_s;
	hdf5_attr = H5Aopen_name(hdf5_header, "UnitVelocity_in_cm_per_s");
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &UnitVelocity_in_cm_per_s);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	PARAM.UnitTime_in_s = PARAM.UnitLength_in_cm / UnitVelocity_in_cm_per_s;
	// apply units
	PARAM.hbar *= pow(PARAM.HubbleParam, 2) / pow(PARAM.UnitLength_in_cm, 2) *
		1 / PARAM.UnitMass_in_g * PARAM.UnitTime_in_s;
	PARAM.mass *= 1e3 * EV / pow(CLIGHT, 2) * PARAM.HubbleParam *
		1 / PARAM.UnitMass_in_g;
}

int main(int argc, char **argv) {
	// initialize
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &WORLD_RANK);
	MPI_Comm_size(MPI_COMM_WORLD, &WORLD_SIZE);
	fftw_mpi_init();
	// command line arguments
	assert(argc == 2); // TODO: select method
	const char *snap_path = argv[1];
	// read snapshot metadata
	const hid_t hdf5_file = H5Fopen(snap_path, H5F_ACC_RDONLY, H5P_DEFAULT);
	assert(hdf5_file != H5I_INVALID_HID);
	const hid_t hdf5_config = H5Gopen(hdf5_file, "/Config", H5P_DEFAULT);
	assert(hdf5_config != H5I_INVALID_HID);
	const hid_t hdf5_param = H5Gopen(hdf5_file, "/Parameters", H5P_DEFAULT);
	assert(hdf5_param != H5I_INVALID_HID);
	const hid_t hdf5_header = H5Gopen(hdf5_file, "/Header", H5P_DEFAULT);
	assert(hdf5_header != H5I_INVALID_HID);
	hid_t hdf5_attr;
	herr_t status;
	double pmgrid;
	hdf5_attr = H5Aopen_name(hdf5_config, "PMGRID");
	const hid_t pmgrid_type = H5Aget_type(hdf5_attr);
	assert(H5Tequal(pmgrid_type, H5T_NATIVE_DOUBLE) > 0);
	H5Tclose(pmgrid_type);
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &pmgrid);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	hdf5_attr = H5Aopen_name(hdf5_param, "NumFilesPerSnapshot");
	status = H5Aread(hdf5_attr, H5T_NATIVE_UINT, &PARAM.num_files);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	for (size_t i = 0; i < sizeof PARAM.snap_file_base; i++)
		PARAM.snap_file_base[i] = 0;
	hdf5_attr = H5Aopen_name(hdf5_param, "SnapshotFileBase");
	const hid_t atype = H5Aget_type(hdf5_attr);
	assert(H5Tget_class(atype) == H5T_STRING);
	assert(H5Tis_variable_str(atype) == 0);
	assert(H5Tget_size(atype) < sizeof PARAM.snap_file_base);
	status = H5Aread(hdf5_attr, atype, PARAM.snap_file_base);
	assert(status >= 0);
	H5Tclose(atype);
	H5Aclose(hdf5_attr);
	hdf5_attr = H5Aopen_name(hdf5_header, "BoxSize");
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &PARAM.box_size);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	hdf5_attr = H5Aopen_name(hdf5_param, "AxionMassEv");
	status = H5Aread(hdf5_attr, H5T_NATIVE_DOUBLE, &PARAM.mass_ev);
	assert(status >= 0);
	H5Aclose(hdf5_attr);
	// units
	PARAM.hbar = HBAR * 1e7;
	PARAM.mass = PARAM.mass_ev;
	set_units(hdf5_header);
	// close HDF5 resources
	H5Gclose(hdf5_config);
	H5Gclose(hdf5_param);
	H5Gclose(hdf5_header);
	H5Fclose(hdf5_file);
	// initialize GRID
	GRID.nx = GRID.n[0] = pmgrid;
	GRID.ny = GRID.n[1] = pmgrid;
	GRID.nz = GRID.n[2] = pmgrid;
	GRID.num_cells_tot = 1;
	for (uint i = 0; i < ARRAY_LEN(GRID.n); i++)
		GRID.num_cells_tot *= GRID.n[i];
	GRID.stride_z_complex = GRID.nz / 2 + 1;
	GRID.stride_z_real = 2 * GRID.stride_z_complex;
	ptrdiff_t local_nx, local_x_start, local_ny, local_y_start;
	const size_t local_complex_num_el = fftw_mpi_local_size_3d_transposed(
		GRID.nx, GRID.ny, GRID.stride_z_complex, MPI_COMM_WORLD,
		&local_nx, &local_x_start, &local_ny, &local_y_start
	);
	const size_t local_real_num_el = 2 * local_complex_num_el;
	GRID.local_nx = local_nx;
	GRID.local_x_start = local_x_start;
	GRID.local_ny = local_ny;
	GRID.local_y_start = local_y_start;
	assert(
		local_real_num_el >=
		(size_t) GRID.local_nx * GRID.ny * GRID.stride_z_real
	);
	assert(
		local_complex_num_el >=
		(size_t) GRID.local_nx * GRID.ny * GRID.stride_z_complex
	);
	GRID.slabs_x_per_rank = malloc(WORLD_SIZE * sizeof *GRID.slabs_x_per_rank);
	MPI_Allgather(
		&GRID.local_nx, 1, MPI_UNSIGNED, GRID.slabs_x_per_rank, 1, MPI_UNSIGNED,
		MPI_COMM_WORLD
	);
	// print info
	printf0("SnapshotFileBase: “%s”\n", PARAM.snap_file_base);
	printf0("PMGRID                        = %u\n", GRID.nx);
	printf0("Box size                      = %g\n", PARAM.box_size);
	printf0("hbar (internal units)         = %g\n", PARAM.hbar);
	printf0("mass (eV/c²)                  = %g\n", PARAM.mass_ev);
	printf0("mass (internal units)         = %g\n", PARAM.mass);
	printf0("hbar / mass (internal units)  = %g\n", PARAM.hbar / PARAM.mass);
	printf0("\n");
	// allocate arrays
	fft_real *phase = fftw_alloc_real(local_real_num_el);
	for (LocalIdx i = 0; i < local_real_num_el; i++)
		phase[i] = 0;
	assert(phase);
	fft_complex *sin_phase_fft = fftw_alloc_complex(local_complex_num_el);
	assert(sin_phase_fft);
	fft_real *sin_phase = (fft_real *) sin_phase_fft;
	fft_real *cos_phase = phase;
	fft_complex *grad_fft[NUMDIMS];
	fft_real *grad[ARRAY_LEN(grad_fft)];
	for (uint i = 0; i < ARRAY_LEN(grad_fft); i++) {
		grad_fft[i] = fftw_alloc_complex(local_complex_num_el);
		assert(grad_fft[i]);
		grad[i] = (fft_real *) grad_fft[i];
	}
	// initialize FFTW plans
	fftw_plan forward_plan = fftw_mpi_plan_dft_r2c_3d(
		GRID.nx, GRID.ny, GRID.nz, sin_phase, sin_phase_fft, MPI_COMM_WORLD,
		FFTW_MEASURE | FFTW_MPI_TRANSPOSED_OUT
	);
	fftw_plan backward_plans[ARRAY_LEN(grad_fft)];
	for (uint i = 0; i < ARRAY_LEN(backward_plans); i++) {
		backward_plans[i] = fftw_mpi_plan_dft_c2r_3d(
			GRID.nx, GRID.ny, GRID.nz, grad_fft[i], grad[i], MPI_COMM_WORLD,
			FFTW_MEASURE | FFTW_MPI_TRANSPOSED_IN
		);
	}
	// read wave function phase and compute sin and cos
	printf0("Reading wave function…\n");
	read_wave_function(snap_path, phase, process_buffer_phase);
	printf0("Done.\n");
/*	printf0("\n"); //TODO debug*/
	for (uint x = 0; x < GRID.local_nx; x++) {
//		for (uint y = 0; y < GRID.ny; y++)
//			for (uint z = 0; z < GRID.nz; z++) {
				//TODO debug
//				const LocalIdx idx = FFT_RI(x, x, x);
//				printf("phase[%u, %u, %u] = %g\n", x, x, x, phase[idx]);
//			}
	}
/*	printf0("\n"); //TODO debug*/
	for (uint x = 0; x < GRID.local_nx; x++) {
//		for (uint y = 0; y < GRID.ny; y++)
//			for (uint z = 0; z < GRID.nz; z++) {
				//TODO debug
//				const LocalIdx idx = FFT_RI(13, 37, x);
//				printf("phase[%u, %u, %u] = %g\n", 13, 37, x, phase[idx]);
//			}
	}
/*	printf0("\n"); //TODO debug*/
	//TODO debug
/*	printf0("phase[3, 3, 3] = %g\n", phase[FFT_RI(3, 3, 3)]);*/
/*	printf0("\t sin=%g, cos=%g\n", sin(phase[FFT_RI(3, 3, 3)]), cos(phase[FFT_RI(3, 3, 3)]));*/
	for (LocalIdx i = 0; i < local_real_num_el; i++) {
		sin_phase[i] = sin(phase[i]);
		cos_phase[i] = cos(phase[i]);
	}
/*	//TODO debug*/
/*	fft_real *phase_copy = fftw_alloc_real(local_real_num_el);*/
/*	for (LocalIdx i = 0; i < local_real_num_el; i++)*/
/*		phase_copy[i] = phase[i];*/
/*	for (uint x = 0; x < GRID.local_nx; x++)*/
/*		for (uint y = 0; y < GRID.ny; y++)*/
/*			for (uint z = 0; z < GRID.nz; z++) {*/
/*				const LocalIdx idx = FFT_RI(x, y, z);*/
/*				const LocalIdx idx_swap = FFT_RI(x, y, z);*/
/*				sin_phase[idx] = sin(phase_copy[idx_swap]);*/
/*				cos_phase[idx] = cos(phase_copy[idx_swap]);*/
/*			}*/
/*	fftw_free(phase_copy);*/
/*	phase_copy = NULL;*/
/*	//TODO debug*/
/*	for (uint x = 0; x < GRID.local_nx; x++)*/
/*		for (uint y = 0; y < GRID.ny; y++)*/
/*			for (uint z = 0; z < GRID.nz; z++) {*/
/*				const LocalIdx idx = FFT_RI(x, y, z);*/
/*				sin_phase[idx] = sin(2 * M_PI * 7 * (double) z / GRID.nz);*/
/*				cos_phase[idx] = 1;*/
/*			}*/
/*	for (uint z = 0; z < GRID.nz; z++)*/
/*		printf("!!! sin_phase[%u, %u, %u] = %g\n", 73, 123, z, sin_phase[FFT_RI(73, 123, z)]);*/
/*	printf0("\n"); //TODO debug*/
/*	for (uint x = 0; x < GRID.local_nx; x++)*/
/*		printf("!!! sin_phase[%u, %u, %u] = %g\n", x, 73, 123, sin_phase[FFT_RI(x, 73, 123)]);*/
/*	printf0("\n"); //TODO debug*/
/*	//TODO debug*/
/*	printf0("sin_phase[3, 3, 3] = %g\n", sin_phase[FFT_RI(3, 3, 3)]);*/
/*	printf0("cos_phase[3, 3, 3] = %g\n", cos_phase[FFT_RI(3, 3, 3)]);*/
	phase = NULL;
	// transform sin(phase) forward
	fftw_execute(forward_plan);
	//TODO debug
	printf0(
		"sin_phase_fft[3, 3, 3] = %g %+gi\n",
		creal(sin_phase_fft[FFT_CI_T(3, 3, 3)]),
		cimag(sin_phase_fft[FFT_CI_T(3, 3, 3)])
	);
	printf0("\n"); //TODO debug
	for (uint y = 0; y < GRID.local_ny; y++)
		for (uint x = 0; x < GRID.nx; x++)
			for (uint z = 0; z < GRID.stride_z_complex; z++) {
/*	for (uint x = 0; x < GRID.local_nx; x++)*/
/*		for (uint y = 0; y < GRID.ny; y++)*/
/*			for (uint z = 0; z < GRID.stride_z_complex; z++) {*/
				const double complex value = sin_phase_fft[FFT_CI_T(x, y, z)];
				if (cabs(value) > 1e-3)
					printf("%d: FFT(sin(a))[%u, %u, %u] = %g %+gi\n", WORLD_RANK, x, y, z, creal(value), cimag(value)); //TODO debug
			}
	printf0("\n"); //TODO debug
	// do all velocity components
	for (uint i = 0; i < ARRAY_LEN(grad_fft); i++) {
		// computations in FFT-space
		for (uint y = 0; y < GRID.local_ny; y++)
			for (uint x = 0; x < GRID.nx; x++)
				for (uint z = 0; z < GRID.stride_z_complex; z++) {
/*		for (uint x = 0; x < GRID.local_nx; x++)*/
/*			for (uint y = 0; y < GRID.ny; y++)*/
/*				for (uint z = 0; z < GRID.stride_z_complex; z++) {*/
					const LocalIdx idx = FFT_CI_T(x, y, z);
					const double k_i = k_vec(i, x, GRID.local_y_start + y, z);
					grad_fft[i][idx] = I * k_i * sin_phase_fft[idx];
//					grad_fft[i][idx] = sin_phase_fft[idx]; // TODO debug
				}
		//TODO debug
/*		printf0("k_vec_%u[3, 3, 3] = %g\n", i, k_vec(i, 3, 3, 3));*/
/*		printf0(*/
/*			"grad_fft_%u[3, 3, 3] = %g %+gi\n", i,*/
/*			creal(grad_fft[i][FFT_CI_T(3, 3, 3)]),*/
/*			cimag(grad_fft[i][FFT_CI_T(3, 3, 3)])*/
/*		);*/
/*		printf0(*/
/*			"grad_fft_%u[13, 25, 38] = %g %+gi\n", i,*/
/*			creal(grad_fft[i][FFT_CI_T(13, 25, 38)]),*/
/*			cimag(grad_fft[i][FFT_CI_T(13, 25, 38)])*/
/*		);*/
/*		printf0("\n");*/
/*	for (uint y = 0; y < GRID.local_ny; y++)*/
/*		for (uint x = 0; x < GRID.nx; x++)*/
/*			for (uint z = 0; z < GRID.stride_z_complex; z++) {*/
/*		for (uint x = 0; x < GRID.local_nx; x++)*/
/*			for (uint y = 0; y < GRID.ny; y++)*/
/*				for (uint z = 0; z < GRID.stride_z_complex; z++) {*/
/*					const double complex value = grad_fft[i][FFT_CI_T(x, y, z)];*/
/*					if (cabs(value) > 1e-3)*/
/*						printf("%d: I * k[%u] * FFT(sin(a))[%u, %u, %u] = %g %+gi\n", WORLD_RANK, i, x, y, z, creal(value), cimag(value)); //TODO debug*/
/*				}*/
		// transform back
		fftw_execute(backward_plans[i]);
		// computations in real space
		double cos_min = 1e300; //TODO debug
		uint cos_min_x=-1; //TODO debug
		uint cos_min_y=-1; //TODO debug
		uint cos_min_z=-1; //TODO debug
		for (uint x = 0; x < GRID.local_nx; x++)
			for (uint y = 0; y < GRID.ny; y++)
				for (uint z = 0; z < GRID.nz; z++) {
					const LocalIdx idx = FFT_RI(x, y, z);
					// TODO: keep track of places where cos = 0 and get missing
					//       data by repeating with cos
					if (fabs(cos_phase[idx]) < fabs(cos_min)) {
						cos_min = cos_phase[idx];
						cos_min_x=x;cos_min_y=y;cos_min_z=z;
					}
					if (cos_phase[idx] == 0) {
						printf("!!! %d: cos[%u, %u, %u] = 0\n", WORLD_RANK, x, y, z);
						grad[i][idx] = 0;
					}
					else
						grad[i][idx] /= cos_phase[idx];
					// normalize
					grad[i][idx] /= GRID.num_cells_tot;
				}
		printf("!!! %d: cos min value at [%u, %u, %u] = %g\n\n", WORLD_RANK, cos_min_x, cos_min_y, cos_min_z, cos_min);
	}

	//TODO output velocity data
/*	printf0(*/
/*		"grad[3, 3, 3] = %g %g %g\n",*/
/*		grad[0][FFT_RI(3, 3, 3)], grad[1][FFT_RI(3, 3, 3)],*/
/*		grad[2][FFT_RI(3, 3, 3)]*/
/*	);*/
/*	printf0("\n"); //TODO debug*/

/*	for (uint z = 0; z < GRID.nz; z++)*/
/*		printf("!!! grad[%u, %u, %u] = %g %g %g\n", 73, 123, z, grad[0][FFT_RI(73, 123, z)], grad[1][FFT_RI(73, 123, z)], grad[2][FFT_RI(73, 123, z)]);*/
/*	printf0("\n"); //TODO debug*/
/*	for (uint x = 0; x < GRID.local_nx; x++)*/
/*		printf("!!! grad[%u, %u, %u] = %g %g %g\n", x, 73, 123, grad[0][FFT_RI(x, 73, 123)], grad[1][FFT_RI(x, 73, 123)], grad[2][FFT_RI(x, 73, 123)]);*/

	
	printf0(
		"v[3, 3, 3] = %g %g %g\n",
		(PARAM.hbar / PARAM.mass) * grad[0][FFT_RI(3, 3, 3)],
		(PARAM.hbar / PARAM.mass) * grad[1][FFT_RI(3, 3, 3)],
		(PARAM.hbar / PARAM.mass) * grad[2][FFT_RI(3, 3, 3)]
	);
	printf0("\n"); //TODO debug

	for (uint x = 0; x < GRID.local_nx; x++)
		for (uint y = 0; y < GRID.ny; y++)
			for (uint z = 0; z < GRID.nz; z++) {
				const double v0 = (PARAM.hbar / PARAM.mass) * grad[0][FFT_RI(x, y, z)];
				const double v1 = (PARAM.hbar / PARAM.mass) * grad[1][FFT_RI(x, y, z)];
				const double v2 = (PARAM.hbar / PARAM.mass) * grad[2][FFT_RI(x, y, z)];
				const double v = sqrt(pow(v0, 2) + pow(v1, 2) + pow(v2, 2));
				if (z != 11 && fabs(v0) > 1e-6 && fabs(v1) > 1e-6 && !(1.049 < v2 && v2 < 1.05))
					terminate("[%u %u %u], v0 = %g, v1 = %g, v2 = %g", x, y, z, v0, v1, v2);
				if (false&&fabs(v) > 1e-3 && fabs(v) < 1e100)
					printf("%d: v[%u, %u, %u] = %g %g %g\n", WORLD_RANK, x, y, z, v0, v1, v2);
			}

/*	MPI_Barrier(MPI_COMM_WORLD);*/
/*	printf0("DIFF...\n");*/
/*	sleep(1);*/
/*	MPI_Barrier(MPI_COMM_WORLD);*/
/*	double cos_val_123 = -666;*/
/*	if(GRID.local_x_start >= 73 && GRID.local_x_start + GRID.local_nx <= 73)*/
/*	for (uint z = 0; z < GRID.nz; z++) {*/
/*		const double cos_val=2 * M_PI * 7 / PARAM.box_size * cos(2 * M_PI * 7 * (double) z / GRID.nz);*/
/*		if (z == 123)*/
/*			cos_val_123 = cos_val;*/
/*		const double diff_z = cos_val - grad[2][FFT_RI(73 - GRID.local_x_start, 123, z)];*/
/*		if (fabs(diff_z) > 1e-6)*/
/*			printf("!!! %d: diff_z[%u, %u, %u] = %g\n", WORLD_RANK, 73, 123, z, diff_z);*/
/*	}*/
/*	printf0("\n!!! cos[123] = %g\n", cos_val_123);*/

	// finalize
	fftw_destroy_plan(forward_plan);
	for (uint i = 0; i < ARRAY_LEN(backward_plans); i++)
		fftw_destroy_plan(backward_plans[i]);
	fftw_free(cos_phase);
	fftw_free(sin_phase);
	for (uint i = 0; i < ARRAY_LEN(grad_fft); i++)
		fftw_free(grad_fft[i]);
	fftw_mpi_cleanup();
	MPI_Finalize();
	return EXIT_SUCCESS;
}
