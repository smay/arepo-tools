#!/usr/bin/env python3
import shutil

PMGRID = 128

def FDM_FI(x, y, z):
	return PMGRID * (PMGRID * x + y) + z

for p in Path().glob('FDM_clean_???.hdf5'):
	with h5py.File(p, 'r') as f:
		coord = f['/PartType1/Coordinates'][...]
		psi_re = f['/PartType1/PsiRe'][...]
		psi_im = f['/PartType1/PsiIm'][...]
	idx3 = numpy.round((coord - 1) / 2).astype(int)
	assert numpy.all(idx3 >= 0) and numpy.all(idx3 < PMGRID)
	idx = FDM_FI(idx3[:, 0], idx3[:, 1], idx3[:, 2])
	assert numpy.all(numpy.sort(idx) == numpy.arange(PMGRID**3))
	psi_re_grid = numpy.full_like(psi_re, numpy.nan)
	psi_im_grid = numpy.full_like(psi_im, numpy.nan)
	psi_re_grid[idx] = psi_re
	psi_im_grid[idx] = psi_im
	assert numpy.all(numpy.isfinite(psi_re_grid))
	assert numpy.all(numpy.isfinite(psi_im_grid))
	p_copy = p.with_stem(p.stem + '_grid')
	shutil.copy(p, p_copy)
	with h5py.File(p_copy, 'r+') as f:
		f['/FuzzyDM/PsiRe'] = psi_re_grid
		f['/FuzzyDM/PsiIm'] = psi_im_grid
		del f['/PartType1']


