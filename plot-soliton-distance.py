#!/usr/bin/env python3
import argparse
import collections
import pickle
from pathlib import Path
import h5py
import matplotlib.pyplot as plt
import numpy
import numpy.linalg
import matplotlib_settings
import bin_density
import units_util
from snapshot import Snapshot


parser = argparse.ArgumentParser(description='Plot distance of soliton to '
	'center of mass over time')
parser.add_argument('--xlim', nargs=2, type=float,
	help='the time range to plot')
parser.add_argument('--ylim', nargs=2, type=float,
	help='the distance range to plot')
parser.add_argument('--cache', type=Path,
	help='path to cache to save/load')
parser.add_argument('--plot-format', default='png',
	help='the file format of the generated plots (default: %(default)s)')
parser.add_argument('--dpi', type=float, default=200.,
	help='the resolution of the generated plots (default: %(default)s)')
parser.add_argument('path', type=Path, nargs='+',
	help='the paths to the snapshot files; any normal files are interpreted '
		'as single-file snapshots, while all files in any given directory '
		'are interpreted as a single multi-file snapshot')


def main():
	# load cache or process paths
	if args.cache and args.cache.exists():
		print(f'Loading cache from “{args.cache}”')
		with open(args.cache, 'rb') as f:
			data = pickle.load(f)
	else:
		data = collections.defaultdict(list)
		for snap_path in args.path:
			process_path(snap_path, data)
		if args.cache:
			with open(args.cache, 'wb') as f:
				print(f'Writing cache to “{args.cache}”')
				pickle.dump(data, f)
	# plot results
	plot_setup()
	plt.plot(data['time_gyr'], data[f'dist_kpc'])
	plt.savefig(
		f'soliton_distance.{args.plot_format}', dpi=args.dpi,
		bbox_inches='tight', transparent=True
	)


def process_path(snap_path, data):
	print(snap_path)
	snap = Snapshot(snap_path)
	snaptime = snap.header['Time']
	units = units_util.get_units(snap.header)
	cell_len = snap.box_size / snap.pmgrid
	rho = (
		snap.read_snapshot(f'/FuzzyDM/PsiRe')**2 +
		snap.read_snapshot(f'/FuzzyDM/PsiIm')**2
	)
	maxpos = numpy.array(
		numpy.unravel_index(numpy.argmax(rho), rho.shape)
	) * cell_len
	# cell volume cancels out
	com = numpy.sum(
		ndindex_array(rho.shape) * cell_len * rho.reshape(-1, 1), axis=0
	) / numpy.sum(rho)
	dist = numpy.linalg.norm(maxpos - com)
	data['time_gyr'].append(snaptime * 1e-3 * units.time_in_myr)
	data['dist_kpc'].append(dist * units.length_in_kpc)


# cf. https://stackoverflow.com/a/35608701 (first comment)
def ndindex_array(dimensions, dtype=int):
	return numpy.stack(numpy.indices(dimensions, dtype=dtype), -1).reshape(
		-1, len(dimensions)
	)

def plot_setup():
	plt.xlabel('$t$ / Gyr')
	plt.ylabel(r'$r_{\mathrm{hs}}$ / kpc')
	plt.xlim(left=0)
	if args.xlim:
		plt.xlim(args.xlim)
	plt.ylim(bottom=0)
	if args.ylim:
		plt.ylim(args.ylim)
	plt.grid(linestyle=':', linewidth=0.5)


if __name__ == '__main__':
	args = parser.parse_args()
	main()

